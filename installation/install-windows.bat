@echo off

SET OPGUI=%USERPROFILE%\opgui
echo %OPGUI%

:: Prerequisites -> node installation , path variables for msys
echo [PROGRESS]  Backend Install INIT

cd %OPGUI%
echo %OPGUI%
cd %OPGUI%\backend
echo %OPGUI%\backend

mkdir build
cd build
cmake -G "MinGW Makefiles" -DWITH_TESTS=ON -DWITH_DOC=ON ..
cmake --build .

cd %OPGUI%\frontend\openAPI\generateApiClientFrontend
echo %OPGUI%\frontend\openAPI\generateApiClientFrontend

rm -rf dist
rm -rf node_modules

call yarn install || "yarn install failed in openapi generated dir"
call  yarn build || "yarn build failed in openapi generated dir"

cd %OPGUI%\frontend

rm -rf node_modules
rm -rf dist


call yarn install || "yarn install failed in frontend dir"
call yarn build || "yarn build failed in frontend dir"
call cp -r dist ..\backend\build || "failed to copy dist in backend build dir"

call cp %OPGUI%\backendConfig.json %OPGUI%\backend\build\ || "yarn install failed to copy config file into build dir"