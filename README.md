# opGUI Installation Guide for Users

## Installing on Windows

1. **Download the App**

   Go to [this page](https://ci.eclipse.org/openpass/job/openpass%20-%20opGUI/view/change-requests/job/MR-71/) to download the latest version of opGUI.
   Click on “opGUI.zip” to download the last successful version.
   ![download app page](images/app-download.png)
   
    Unzip the contents in the folder of your choice.

2. **Install the Build Environment**

   **MSYS2**

   On Windows, the build environment of choice is MSYS2 programming tools. MSYS2 is used to install some third-party software on which openPASS depends. Also, the unix-like shell simplifies c++ compilation on Windows. For more details go to [MSYS2 website](https://www.msys2.org/).

   - **Download MSYS2:**

     The latest 64-bit packages are located [here](https://www.msys2.org/). Follow the installation instructions.

   - **Install MSYS2:**

     Run the downloaded executable and adjust suggested settings to your needs (defaults are fine). In the following, it is assumed that MSYS2 is installed under `C:\msys64`.

   - **Understand the Build Environment:**

     MSYS2 provides three different environments, located in the MSYS2 installation directory. Make sure you open the one below:

     ```
     msys2 mingw64
     ```
     ![msys lookup](images/msys-lookup.png)

3. **Install the required libraries.**

   After opening MSys, you will be prompted with a terminal.
   Type the following command:

   ```
   pacman -S mingw-w64-x86_64-qt5-base
   ```

   (This will install the Qt related libraries needed for the application to run).
   Once the installation is complete, we need to add the path of the libraries to our windows environment so that they can be accessed by the application.

4. **Adding the installed libraries to our windows environment**

   - Open Control Panel.
   - Go to System and Security > System.
   - Click on "Advanced system settings" on the left sidebar.
   - In the System Properties window, click on the "Environment Variables" button.
   - Under "System variables", find the Path variable and select it, then click on "Edit...".
   - Click on "New" and add the path to MSYS2's bin directory (usually something like `C:\msys64\mingw64\bin`).
   - Click "OK" to save the changes.

5. **Fill the backendConfig.json file:**

   Go to the unzipped folder and open the “backendConfig.json” file using the text editor app of your choice.
   Replace each instance of “<USER_HOME_PATH>” with the path from your workspace.
   For example  “<USER_HOME_PATH>” will become “C:/users/JohnDoe”.
   Making the new full path:
   ```
   "MODULES_FOLDER": "C:/users/JohnDoe/opSimulation/bin/core/modules”
   ```
   Note: For the app to run, all properties must be filled with valid paths.
   Save your changes.

6. **Run the App**
   Click the `OPGUICore.exe` file to run the app.
   If everything is correct you will something like this
   ![terminal-image](images/terminal-app-run.png)
   
   Now open your browser and go to the address specified in the terminal (`127.0.0.1:8848` or `localhost:8848`)
   If everything is correct you will see something like this
   
    ![app-preview](images/app-preview.png)

**Troubleshooting**
- If the executable is instantly closing after opening make sure
   - All the paths in `backendConfig.json` are correct.
   - All the Qt libraries you installed exist in the environment you added to windows (you can go to the folder and check for the `.dll` files).
- If you want more details about what's wrong. You can run the `.exe` file in PowerShell. This will provide you with more feedback on what’s missing.
  For example:
  In here we gave the `backendConfig.json` file invalid paths.
  
    ![terminal-help](images/terminal-help.png)



# opGUI Installation Guide for Developers

## Prerequisites

Before installing opGUI, please ensure you have the following prerequisites correctly installed:

1. **Node.js (Version 18.13.2):**

   - For Windows 11: Download from [here](https://nodejs.org/dist/v18.13.0/node-v18.13.0-x86.msi).

   - For Debian: Run the following commands in your terminal:

     ```bash
     curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
     source ~/.profile
     nvm install 18.13.0
     ```

2. **NPM (Version 8.5.0):**

   - Included with Node.js when installing node.

3. **Yarn (Version 1.22.17):**

   - For Windows 11: Download from [here](https://github.com/yarnpkg/yarn/releases/download/v1.22.19/yarn-1.22.19.msi).

   - For Debian: Install using npm with the following command:

     ```bash
     npm install --global yarn
     ```

## Installation Steps

1. Clone this repository into your HOME folder. It is also mandatory to have opSimulation installed in your HOME directory.

## Windows:

1. Open a terminal and navigate to the project directory located in your home folder:

   ```bash
   cd %USERPROFILE%\opgui
   ```

2. Install the required dependencies using yarn:

   ```bash
   yarn install
   ```

3. Execute the Windows installation procedure:

   ```bash
   yarn win-install
   ```

4. To run the application, simply execute the following command:

   ```bash
   yarn win-launch
   ```

## Debian:

1. Open a terminal and navigate to the project directory located in your home folder:

   ```bash
   cd $HOME/opgui
   ```

2. Install the required dependencies using yarn:

   ```bash
   yarn install
   ```

3. Execute the Debian installation procedure:

   ```bash
   yarn deb-install
   ```

4. To run the application, simply execute the following command:

   ```bash
   yarn deb-launch
   ```

Please make sure to carefully follow the above steps to ensure a successful installation and execution of opGUI. If you encounter any issues or have any questions, do not hesitate to reach out to us for support. Happy coding!

## Configuration File

The configuration file is named `backendConfig.json` and is expected to be located in the `/backend/` directory. This JSON file can contain several key-value pairs representing different configuration settings. Here's a brief overview of the keys that can be included in `backendConfig.json` and the corresponding settings they represent:

- **BACKEND_PORT**: The port on which the backend service runs.
- **BACKEND_IP**: The IP address of the backend service.
- **PATH_DATABASE_PCM**: The path to the PCM (Pulse Code Modulation) database.
- **AGENT_COPY_FILE**: The path to the file used for copying agent data.
- **AGENT_FOLLOW_FILE**: The path to the file used for agent following operations.
- **MODULES_FOLDER**: The path to the folder containing the system modules.
- **COMPONENTS_FOLDER**: The path to the folder containing the system components.
- **SYSTEMS_FOLDER**: The path to the folder containing the system configurations.
- **PATH_OPENPASS_CORE**: The path to the Openpass Core.
- **OPSIMULATION_EXE**: The path to the Openpass Simulation executable.
- **OPSIMULATION_MANAGER_XML**: The path to the Openpass Simulation Manager XML file.
- **OPSIMULATION_MANAGER_EXE**: The path to the Openpass Simulation Manager executable.
- **WORKSPACE**: The path to the workspace of the system.
- **PATH_CONVERTED_CASES**: The path to the converted cases.
- **PATH_LOG_FILE**: The path to the log file of the system.

#### Example JSON Configuration

Here is an example of how the `backendConfig.json` file might look:

```json
{
  "BACKEND_PORT": 8080,
  "BACKEND_IP": "192.168.1.1",
  "PATH_DATABASE_PCM": "/path/to/database/pcm",
  "AGENT_COPY_FILE": "/path/to/agent/copy/file",
  "AGENT_FOLLOW_FILE": "/path/to/agent/follow/file",
  "MODULES_FOLDER": "/path/to/modules/folder",
  "COMPONENTS_FOLDER": "/path/to/components/folder",
  "SYSTEMS_FOLDER": "/path/to/systems/folder",
  "PATH_OPENPASS_CORE": "/path/to/openpass/core",
  "OPSIMULATION_EXE": "/path/to/opsimulation/exe",
  "OPSIMULATION_MANAGER_XML": "/path/to/opsimulation/manager/xml",
  "OPSIMULATION_MANAGER_EXE": "/path/to/opsimulation/manager/exe",
  "WORKSPACE": "/path/to/workspace",
  "PATH_CONVERTED_CASES": "/path/to/converted/cases",
  "PATH_LOG_FILE": "/path/to/log/file"
}
```