/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

function modifyProperty<T extends object, V>(
    obj: T,
    propertyName: string,
    newValue: V
): T {
    const newObj = JSON.parse(JSON.stringify(obj)); // Shallow copy of the original object
    const properties = propertyName.split('.'); // Split the property name by '.'

    let current: any = newObj;

    // Loop through the properties, accessing each nested property
    for (let i = 0; i < properties.length - 1; i++) {
        const prop = properties[i];
        if (!(prop in current)) {
            // Create nested object or array if it doesn't exist
            current[prop] = /^\d+$/.test(properties[i + 1]) ? [] : {};
        }
        current = current[prop]; // Access each nested property
        if (typeof current !== 'object' || current === null) {
            // If at any point a property is not an object, or is null, stop and return
            console.error('Invalid property path');
            return null as any;
        }
    }

    // Modify the final property in the copied object
    const finalProp = properties[properties.length - 1];
    if (Array.isArray(current)) {
        const index = parseInt(finalProp, 10);
        if (isNaN(index)) {
            console.error('Invalid array index');
            return null as any;
        }
        current[index] = newValue;
    } else {
        current[finalProp] = newValue;
    }

    return newObj;
}

export default modifyProperty