/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Connection, SystemUI} from "opGUI-client-frontend";
import { System } from "../components/interfaces/SystemEditor/systemEditor.d";
import { Connections } from "../components/interfaces/SystemEditor/systemEditor.d";

interface AccumConnection {
    [property: string]: Connection
}

export const systemToFront = (system: SystemUI) => {
    const systemFront: System = {
        ...system,
        path: '',
        anchorEl: null,
        connections: system.connections?.reduce((accum: Connections[], sysCon: Connection) => {
            const source = sysCon.source
            const newConnections = sysCon.targets.map((target, conIndex) => ({
                id: accum.length + conIndex,
                source,
                target: target,
            }))
            return [...accum, ...newConnections]
        }, []) || []
    }
    return systemFront
}

export const systemToBackend = (system: System) => {
    const systemBack: SystemUI = {
        id: system.id,
        title: system.title,
        priority: system.priority,
        file: system.file,
        comments: system.comments,
        components: system.components,
        connections: Object.values(system.connections?.reduce((accum: AccumConnection, sysCon) => {
            const connectionId = `${sysCon.source.component}-${sysCon.source.output}`
            if (accum[connectionId]){
                accum[connectionId] = {
                    ...accum[connectionId],
                    targets: [
                        ...accum[connectionId].targets,
                        sysCon.target
                    ]
                }
            }
            else {
                accum[connectionId] = {
                    id: Object.keys(accum).length,
                    source: sysCon.source,
                    targets: [
                        sysCon.target
                    ]
                }
            }
            return accum
        }, {}))
    }
    return systemBack
}