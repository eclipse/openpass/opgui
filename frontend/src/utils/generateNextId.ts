/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

type listType = {
    id: number,
    [param: string]: any,
}

const generateNextId = (list: listType[]) => {
    return list.reduce((maxId, con) => Math.max(maxId, con.id),0) + 1
}

export default generateNextId