/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {fireEvent, screen, waitFor} from "@testing-library/react";
import {act} from "react-dom/test-utils";

const selectFileInFileTree = async (buttonId = 'test-id-input-button-results', isFolder = false, depth = 2)=> {
    const loadSystemButton = screen.getByTestId(buttonId);
    await act(() =>
        fireEvent.click(loadSystemButton)
    );
    const modalTestId = `test-id-modal-Select ${isFolder ? 'Folder' : 'File'}`
    await waitFor(() => expect(screen.getByTestId(modalTestId)).toBeInTheDocument())
    const modal = screen.getByTestId(modalTestId)
    expect(modal).toBeInTheDocument()
    for (let i=0; i< depth; i++){
        const arrowOpenFolderContainer = screen.getByTestId('folder-tree-container').querySelectorAll('.caretContainer')
        await waitFor(() => fireEvent.click(arrowOpenFolderContainer[i]?.children[0]))
    }
    const fileNames = screen.getByTestId('folder-tree-container').querySelectorAll('.displayName')
    await act(() =>
        fireEvent.click(fileNames[depth])
    )
    const selectFileButton = screen.getByTestId('file-tree-select')
    await act(() =>
        fireEvent.click(selectFileButton)
    )
    expect(modal).not.toBeInTheDocument();
}

export default selectFileInFileTree