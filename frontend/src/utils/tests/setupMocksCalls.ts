/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import fetchMock from "jest-fetch-mock";
import isUrl from "./isUrl.ts";

export interface CallHandler {
    url: string;
    response: Promise<Awaited<{
        body: string;
    }>>;
}

const setupMocksCalls = (callHandlers: CallHandler[]) => {
    fetchMock.enableMocks();
    const urlHandler = (request: Request) => {
        const callHandler = callHandlers.find(ch => (
            isUrl(request, ch.url)
        ))
        if (callHandler){
            return callHandler.response
        }
        console.error(`ERROR: there's no mock for request: ${request.url}`)
        return Promise.resolve({})
    }
    fetchMock.mockIf((req: Request) => req.url.includes('api/'), urlHandler)
}

export default setupMocksCalls
