/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import { Box, Button } from '@mui/material';

export const StepperButton = styled(Button)(({ theme }) => ({
    borderRadius: '50%',
    width: theme.spacing(7),
    height: theme.spacing(7),
    minWidth: theme.spacing(7)
}));

export const HideButton = styled(Box)(({ theme }) => ({
    zIndex: 10,
    background: theme.palette.primary.main,
    width: theme.spacing(7),
    height: theme.spacing(7),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: theme.spacing(8),
    cursor: 'pointer'
}));

export const CommentButton = styled(HideButton)(({ theme }) => ({
    background: 'transparent',
    '& svg': {
        width: theme.spacing(6),
        height: theme.spacing(6)
    },
    transition: 'all 0.5s ease-in-out'
}));

export const DraggableSideItem = styled(Box)(() => ({
    '&:hover': {
        'h6': {
            textDecoration: 'underline',
            cursor: 'pointer'
        }
    }
}));

export const AddIconBox = styled(Box)(({ theme }) => ({
    zIndex: 1,
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: theme.spacing(4),
    background: theme.palette.primary.main,
    borderRadius: '50%',
    width: theme.spacing(7),
    height: theme.spacing(7),
    minWidth: theme.spacing(7),
    'svg': {
        color: theme.palette.common.white
    },
    '&:hover': {
        background: 'transparent',
        'svg': {
            color: theme.palette.primary.mainContrast
        }
    }
}));
