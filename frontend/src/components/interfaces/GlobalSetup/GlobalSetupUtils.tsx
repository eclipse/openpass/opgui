/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {ConfigElement} from "opGUI-client-frontend";

export interface CategoryVariable {
    title: string;
    priority: number;
    variables: (ConfigElement & {
        title: string;
    })[]
}
export interface Categories {
    [property: string]: CategoryVariable
}
interface IVariable {
    title: string;
    category: CATEGORY_ID;
}
interface  IVariables {
    [id: string]: IVariable
}

export enum CATEGORY_ID {
    PRE = 'PRE',
    RUN = 'RUN',
    SYSTEM = 'SYSTEM',
    GENERAL = 'GENERAL'
}

const CATEGORIES = {
    PRE: {
        id: CATEGORY_ID.PRE,
        title: 'PRE',
        priority: 6,
    },
    RUN: {
        id: CATEGORY_ID.RUN,
        title: ' RUN',
        priority: 4,
    },
    SYSTEM: {
        id: CATEGORY_ID.SYSTEM,
        title: ' SYSTEM',
        priority: 8,
    },
    GENERAL: {
        id: CATEGORY_ID.GENERAL,
        title: 'GENERAL',
        priority: 10,
    }
}

const VARIABLES: IVariables = {
    PATH_CONVERTED_CASES: {
        title: 'Default path to PCM converted cases',
        category: CATEGORIES.PRE.id
    },
    PATH_OPENPASS_CORE: {
        title: 'Path to Open Pass Core',
        category: CATEGORIES.RUN.id
    },
    OPSIMULATION_EXE: {
        title: 'Path to opSimulation Executable',
        category: CATEGORIES.RUN.id
    },
    OPSIMULATION_MANAGER_EXE: {
        title: 'Path to opSimulation Manager Executable',
        category: CATEGORIES.RUN.id
    },
    OPSIMULATION_MANAGER_XML: {
        title: 'Path to opSimulation Manager XML',
        category: CATEGORIES.RUN.id
    },
    MODULES_FOLDER: {
        title: 'Path to Modules Folder (.dll files)',
        category: CATEGORIES.RUN.id
    },
    COMPONENTS_FOLDER: {
        title: 'Path to components Folder (.xml files)',
        category: CATEGORIES.SYSTEM.id
    },
    PATH_LOG_FILE: {
        title: 'Path to opGUI Log File',
        category: CATEGORIES.GENERAL.id
    },
    WORKSPACE: {
        title: 'Workspace folder',
        category: CATEGORIES.GENERAL.id
    }
}

export const getCategoriesFromVariables = (variables: ConfigElement[], genericCategory: CATEGORY_ID): CategoryVariable[] => {
    const globalSetupCategory = variables.reduce((accum: Categories, currentVariable) => {
        const variable = currentVariable.id in VARIABLES ? VARIABLES[currentVariable.id] : null;
        const category =  variable ? variable.category : genericCategory
        const categoryData = accum[category] || {
            ...CATEGORIES[category],
            variables: []
        }
        const variableData = {
            ...currentVariable,
            title: VARIABLES[currentVariable.id]?.title || currentVariable.id,
        }
        return {
            ...accum,
            [category]: {
                ...categoryData,
                variables: [...categoryData.variables, variableData]
            }
        }
    }, {})
    return Object.values<CategoryVariable>(globalSetupCategory).sort((a,b) => b.priority - a.priority)
}