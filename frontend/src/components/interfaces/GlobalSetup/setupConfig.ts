/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

export type Path = {
    title: string,
    path: string,
}

const Paths : Path[] = [
    {
        title: 'Path to openPASS core folder',
        path: '/Users/opgui-user/opSimulation/bin/core',
    },
    {
        title: 'Path to workspace',
        path: '/Users/opgui-user/Documents/workspaces/opGUI-workspace1',
    },
    {
        title: 'Path to modules folder',
        path: '/Users/opgui-user/opSimulation/bin/core/modules',
    },
    {
        title: 'Path to opSimulation executable',
        path: '/Users/opgui-user/opSimulation/bin/core/opSimulation',
    },
    {
        title: 'Path to opSimulationmanager executable',
        path: '/Users/opgui-user/opSimulation/bin/core/opSimulationManager',
    },
    {
        title: 'Path to opSimulationmanager xml',
        path: '/Users/opgui-user/opSimulation/bin/core/opSimulationManager.xml',
    },
    {
        title: 'Path to log file',
        path: '/Users/opgui-user/Documents/workspaces/opGUI-workspace1/opGuiCore.log',
    }
]

export default Paths