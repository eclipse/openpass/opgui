/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Connections, System } from "./systemEditor.d";

export const isSystemNew = (system: System) => {
    return system.path === ''
}
export const generateUnique = () => {
    return Math.random() + Date.now();
};

const compatibleTypes = {
    numbers: ['int', 'double'],
    vectors: ['intVector', 'doubleVector']
}

export const areTypesCompatile = (typeA: string, typeB: string) => {
    if (typeA === typeB) return true;
    const compatibleList = Object.values(compatibleTypes)
    for (let i = 0 ; i < compatibleList.length ; i++){
        const compatibility = compatibleList[i]
        if (compatibility.includes(typeA) && compatibility.includes(typeB))
            return true
    }
    return false
}

export const isTypeDistribution = (type: string) => {
    const distributions = ['normalDistribution', 'gammaDistribution', 'logNormalDistribution', 'exponentialDistribution', 'uniformDistribution']
    return distributions.includes(type)
}

interface IdsConvert {
    [param: number]: number
}

export const updateComponentIds = (system: System): System => {
    const { components, connections } = system
    const idsConverted = components.reduce((accum: IdsConvert, comp, index) => {
        accum[comp.id] = index
        components[index].id = index
        return accum
    }, {})
    const newConnections = connections.map((connection, index) => {
        const newCon: Connections = {
            id: index,
            source: {
                ...connection.source,
                component: idsConverted[connection.source.component],
            },
            target: {
                ...connection.target,
                component: idsConverted[connection.target.component],
            }
        }
        return newCon
    })
    return {
        ...system,
        connections: newConnections
    }
}