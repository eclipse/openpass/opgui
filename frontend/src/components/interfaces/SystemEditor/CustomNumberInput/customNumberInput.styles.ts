/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";

export const StyledInputRoot = styled('div')(({ theme }) => `
      width: 100%;
      font-family: IBM Plex Sans, sans-serif;
      font-weight: 400;
      border-radius: 8px;
      color: ${theme.palette.greyScale.dark};
      background: ${theme.palette.secondary.contrastText};
      border: 1px solid ${theme.palette.greyScale.medium};
      box-shadow: 0px 2px 4px 'rgba(0,0,0, 0.05)';
      display: grid;
      grid-template-columns: 1fr 19px;
      grid-template-rows: 1fr 1fr;
      overflow: hidden;
      column-gap: 8px;
      padding: 4px;
    
      &.focused {
          border-color: ${theme.palette.primary.main};
          box-shadow: 0 0 0 1px ${theme.palette.primary.dark};
    
          & button:hover {
            background: ${theme.palette.primary.main};
          }
          
          &:focus-visible {
            outline: 0;
      }
      &:hover {
            border-color: ${theme.palette.primary.main};
      }
      `
);

export const StyledInputElement = styled('input')(`
      width: 100%;
      font-size: 0.875rem;
      font-family: inherit;
      font-weight: 400;
      line-height: 1.5;
      grid-column: 1/2;
      grid-row: 1/3;
      color: #303740;
      background: inherit;
      border: none;
      border-radius: inherit;
      padding: 8px 12px;
      outline: 0;
    `,
);
