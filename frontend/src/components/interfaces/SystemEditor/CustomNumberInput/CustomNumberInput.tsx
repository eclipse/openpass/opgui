/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { forwardRef, ForwardedRef, InputHTMLAttributes } from 'react';
import {
    unstable_useNumberInput as useNumberInput,
    UseNumberInputParameters,
} from '@mui/base/unstable_useNumberInput';
import { unstable_useForkRef as useForkRef } from '@mui/utils';
import {StyledInputRoot, StyledInputElement} from './customNumberInput.styles.ts';

const CustomNumberInput = forwardRef(function CustomNumberInput(
    props: UseNumberInputParameters & InputHTMLAttributes<HTMLInputElement>,
    ref: ForwardedRef<HTMLInputElement>,
) {
    const {
        getRootProps,
        getInputProps,
        focused,
    } = useNumberInput(props);
    const inputProps = getInputProps();
    inputProps.ref = useForkRef(inputProps.ref, ref);

    return (
        <StyledInputRoot {...getRootProps()} className={focused ? 'focused' : undefined}>
            <StyledInputElement {...inputProps} />
        </StyledInputRoot>
    );
});

export default CustomNumberInput;
