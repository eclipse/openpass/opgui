/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Box, Divider} from "@mui/material";

export const StyledOuterBox = styled(Box)({
    width: '100%',
    position: 'relative'
});

export const StyledInnerBox = styled(Box)({
    display: "flex",
    justifyContent: "space-between",
    alignItems: 'center',
    width: '100%'
});


export const StyledDivider = styled(Divider)(({ theme }) => ({
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: theme.spacing(-1.75),
    borderColor: theme.palette.greyScale.medium
}));