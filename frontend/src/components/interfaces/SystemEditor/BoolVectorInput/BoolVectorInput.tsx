/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {useState, useEffect} from 'react';
import {Switch, Typography} from "@mui/material";
import {StyledOuterBox, StyledInnerBox, StyledDivider} from "./boolVectorInput.styles.ts";

export interface BoolVectorProps {
    text: string,
    value: string,
    onChange: (e: any) => void
}
const BoolVectorInput = ({text, value, onChange}: BoolVectorProps) => {
    const [newValue, setNewValue] = useState(value);

    useEffect(() => {
        if (value.length === 0) {
            onChange({target: {value: 'false'}});
            setNewValue('false');
        } else {
            setNewValue(value);
        }
    }, [value]);

    const handleBoolChange = (e: any) => {
        const eventValue = e.target.value;
        const isTrue = eventValue.toLowerCase() === 'true';
        const toggleValue = !isTrue
        setNewValue(toggleValue.toString());
        onChange({target: {value: toggleValue.toString()}});
    }

    return (
        <StyledOuterBox>
            <StyledInnerBox>
                <Typography variant={"subtitle1"}>{text}</Typography>
                <Switch
                    checked={newValue === 'true' || false}
                    value={newValue === 'true' || false}
                    name='parameters'
                    onChange={(e) => handleBoolChange(e)}
                />
            </StyledInnerBox>
            <StyledDivider />
        </StyledOuterBox>
    )
}

export default BoolVectorInput;