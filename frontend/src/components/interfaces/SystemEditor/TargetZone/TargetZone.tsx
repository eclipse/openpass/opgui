/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Divider, IconButton, Menu, MenuItem, Switch, Typography, useTheme} from '@mui/material'
import { useCardTarget } from '../../../../hooks/useCardTarget.ts';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import { ConnectionComponent, ConnectionList, DragComponent, ExtendedComponent, ISystemEditor } from '../systemEditor.d';
import { useGlobalContext } from '../../../../context/ContextProvider.tsx';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import ControlPointDuplicateOutlinedIcon from '@mui/icons-material/ControlPointDuplicateOutlined';
import {
    ColorBox,
    NumberInput,
    VectorInput,
    VectorButton,
    StyledBox,
    StyledOutlinedInput, StyleArrowContainer,
} from './targetZone.styles.ts';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import React, { useEffect, useState } from 'react';
import { ColorVariant, Variant } from '../../../../index.d';
import { LightTooltip } from '../../../../styles/general.styles.tsx';
import { Input, Output } from 'opGUI-client-frontend';
import { useXarrow } from 'react-xarrows';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import EditVectorContent from "../EditVectorContent/EditVectorContent.tsx";
import SystemComment from "../../../commonComponents/SystemComment/SystemComment.tsx";
import {areTypesCompatile, isTypeDistribution} from "../EditorUtils.ts";
import ConnectionArrow from "./ConnectionArrow.tsx";
import generateNextId from "../../../../utils/generateNextId.ts";
import DistributionParameter from "../DistributionParameter/DistributionParameter.tsx";

enum ComponentType {
    Component,
    Comment
}

interface Props extends ISystemEditor {
    handleChange: (values: Partial<ISystemEditor>) => void;
    availableElements: DragComponent[];
    zoomScale: number;
}

interface useForceUpdateI{
    (): void
}

const useForceUpdate = (rerenderAmount = 1, callback: useForceUpdateI)=> {
    const [value, setValue] = useState(rerenderAmount);

    useEffect(() => {
        if (value < rerenderAmount){
            setValue(value + 1)
            callback && callback()
        }
    }, [callback, value, rerenderAmount]);
    return { rerenderIndex: value, forceUpdate: () => setValue(0) }; // update state to force render
}


const TargetZone: React.FC<Props> = ({ showComments, availableElements,comments = [], handleChange, selectedSystem, connections, hideConnections, zoomScale }) => {
    const {
        dropElement: dropElementComment,
        dragStartItem: dragStartItemComment,
    } = useCardTarget(comments, ({ availableElements }) => handleChange({ comments: availableElements }));

    const {
        allowDrop,
        dropElement,
        dragStartItem,
    } = useCardTarget(availableElements!, handleChange);

    const colors = [
        '#6DD4CE',
        '#EDEF97',
        '#EA5EC2',
        '#542DF1',
        '#1D2D53',
        '#ADB9CE',
        '#F3DBDB',
        '#2E7D32'
    ];
    const { setShowDiscardModal, modal, setModal } = useGlobalContext();
    const updateXarrow = useXarrow();
    const theme = useTheme();

    const [anchorEl, setAnchorEl] = useState<SVGElement | null>(null);
    const [colorMenu, setColorMenu] = useState<HTMLDivElement | null>(null);
    const [connectionStart, setConnectionStart] = useState<ConnectionComponent | null>(null);
    const [selectedComponent, setSelectedComponent] = useState<null | ExtendedComponent | DragComponent>(null);
    const [newVector, setNewVector] = useState<any[]>([]);
    const open = Boolean(anchorEl);
    const shakeComponents = () => {
        availableElements = availableElements?.map(comp => ({
            ...comp,
            position: {
                ...comp.position,
                x: comp.position.x + (rerenderIndex % 2 == 0 ? .1 : -.1),
            }
        }))
        handleChange({
            availableElements
        })
    }
    const { rerenderIndex, forceUpdate } = useForceUpdate(16, shakeComponents);

    useEffect(() => {
        forceUpdate()
    }, [zoomScale, connections, hideConnections]);

    const handleOpenMenu = (event: React.MouseEvent<SVGElement>, index: number) => {
        setAnchorEl(event.currentTarget);
        setSelectedComponent({ ...availableElements[index], index: index });
    };
    const duplicateItem = () => {
        const newComponents = [...availableElements, {
            ...selectedComponent, position: {
                x: ((selectedComponent as DragComponent).position.x + 120)
            },
            id: generateNextId(availableElements)
        }];
        handleChange({ availableElements: newComponents });
        setAnchorEl(null);
        setSelectedComponent(null);
    }
    const deleteItem = () => {
        if (selectedComponent) {
            setModal({
                ...modal,
                active: true,
                title: 'Delete Component',
                descriptionTitle: `The Component ${selectedComponent.title} will be deleted`,
                description: `After this action, the component cannot be recovered.`,
                loadingBar: false,
                icon: WarningAmberIcon,
                iconVariant: ColorVariant.warning,
                buttons: [
                    {
                        title: 'Cancel',
                        variant: Variant.outlined
                    },
                    {
                        title: 'Delete Component',
                        action: () => {
                            const newComponents = [...availableElements];

                            newComponents.splice(selectedComponent.index, 1);
                            const newConnections = connections.filter(con =>
                                !(con.indexComponentInput === selectedComponent.index || con.indexComponentOutput === selectedComponent.index)
                            ).map((con) => ({
                                ...con,
                                indexComponentInput: newComponents.findIndex((component) => component.id === con.target.component),
                                indexComponentOutput: newComponents.findIndex((component) => component.id === con.source.component)
                            }));
                            handleChange({ availableElements: newComponents, connections: newConnections });
                            setConnectionStart(null);
                            setAnchorEl(null);
                            setSelectedComponent(null);
                            setShowDiscardModal(true);
                        },
                        testId: 'test-id-modalbutton-delete-component'
                    },
                ],
            });
        }
    }

    const removeConnection = (id: number) => {
        setModal({
            ...modal,
            active: true,
            title: 'Delete Connection',
            descriptionTitle: `The connection will be deleted`,
            description: `After this action, the connection cannot be recovered.`,
            loadingBar: false,
            icon: WarningAmberIcon,
            iconVariant: ColorVariant.warning,
            buttons: [
                {
                    title: 'Cancel',
                    variant: Variant.outlined
                },
                {
                    title: 'Delete Connection',
                    action: () => {
                        const newConnections = connections.filter(con => con.id !== id);
                        handleChange({ connections: newConnections });
                    },
                    testId: 'test-id-delete-connection-button'
                },
            ],
        });
    }

    const isConnectionValid = (id: number) => {
        const { input, output } = getConnectionInputOutput(id)
        if (!input || !output) return true
        return areTypesCompatile(input?.type, output?.type)
    }

    const getConnectionInputOutput = (id: number): {
        input: Input | undefined,
        output: Output | undefined
    } => {
        const connection = connections.find(con => con.id === id)
        if (!connection) return { input: undefined, output: undefined };
        const inputComponent = availableElements.find(com => com.id === connection.target.component)
        const outputComponent = availableElements.find(com => com.id === connection.source.component)

        const inputParameter = inputComponent?.inputs?.find(param => param.id === connection.target.input)
        const outputParameter = outputComponent?.outputs?.find(param => param.id === connection.source.output)

        return {
            input: inputParameter,
            output: outputParameter
        }
    }

    const handleConnectionEnd = (element: DragComponent, input: Input, index: number) => {
        if (connectionStart === null) {
            setModal({
                ...modal,
                active: true,
                title: 'Need to select an output first to start a connection',
                descriptionTitle: '',
                description: `Please select and output first.`,
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    }
                ],
            });
            return;
        }
        if (connectionStart.index === index) {
            setModal({
                ...modal,
                active: true,
                title: 'Cannot create a connection with the same component.',
                descriptionTitle: '',
                description: `Please select a different component.`,
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    }
                ],
            });
            return;
        }
        const conExist = connections.find((con) => (
            con.target.input === input.id &&
            con.source.output === connectionStart.output.id &&
            con.source.component === connectionStart.component.id &&
            con.target.component === element.id
        ));

        if (conExist) {
            setModal({
                ...modal,
                active: true,
                title: 'Theres already a connection between these 2 components',
                descriptionTitle: '',
                description: `Please select a different component.`,
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    }
                ],
            });
            return;
        }
        const newConnections = [...connections];
        newConnections.push({
            id: generateNextId(newConnections),
            indexComponentOutput: connectionStart.index,
            indexComponentInput: index,
            source: {
                component: connectionStart.component.id,
                output: connectionStart.output.id
            },
            target: {
                component: element.id,
                input: input.id
            }
        });
        handleChange({ connections: newConnections });
        setConnectionStart(null);
    }

    const getIdOutputElement = (connection: ConnectionList): string => {
        const component = availableElements?.find(comp => comp.id === connection.source.component);
        if (!component) return "";
        if (!component.outputs) return "";
        const output = component.outputs.find(out => out.id === connection.source.output);
        if (!output) return "";
        const id = `${connection.source.output}-${output?.title}-${connection.source.component}-${connection.indexComponentOutput}`;
        return id;
    }

    const getIdInputElement = (connection: ConnectionList): string => {
        const component = availableElements.find(comp => comp.id === connection.target.component);
        if (!component) return "";
        if (!component.inputs) return "";
        const input = component.inputs.find(inp => inp.id === connection.target.input);
        const id = `${connection.target.input}-${input?.title}-${connection.target.component}-${connection.indexComponentInput}`;
        return id;
    }

    const handleConnectionStart = (element: DragComponent, output: Output, index: number) => {
        if (connectionStart === null) {
            setConnectionStart({ component: { ...element }, output: { ...output }, index });
            return;
        }
        if (element.id === connectionStart.component.id && output.id === connectionStart.output.id && index === connectionStart.index) {
            setConnectionStart(null);
            return;
        }
        setModal({
            ...modal,
            active: true,
            title: 'Connection Already Started',
            descriptionTitle: '',
            description: `You already started a connection, please complete it or cancel it`,
            loadingBar: false,
            buttons: [
                {
                    title: 'Close',
                }
            ],
        });
        return;
    }

    const showSystemRequiredModal = () => {
        setModal({
            ...modal,
            active: true,
            title: 'Select A System',
            descriptionTitle: '',
            description: `To add components a system must first be selected`,
            loadingBar: false,
            buttons: [
                {
                    title: 'Close',
                }
            ],
        })
    }

    const finalDrop = (event: React.DragEvent<HTMLDivElement>) => {
        if (selectedSystem === null) {
            showSystemRequiredModal()
            return;
        }
        const props = JSON.parse(event.dataTransfer.getData('text'));
        const isComment = props.componentType === ComponentType.Comment
        if (isComment){
            dropElementComment(event, zoomScale)
        }
        else {
            dropElement(event, zoomScale)
        }
        forceUpdate()
    }
    const handleNumericInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        const numericValue = value.replace(/[^0-9]/g, '');
        event.target.value = numericValue;
    }

    const handleBoolChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, indexList: number, indexParam: number) => {
        const { name, value } = event.target;
        const isTrue = value.toLowerCase() === 'true';
        const newValue = !isTrue;
        const newComponents = [...availableElements];
        newComponents[indexList][name][indexParam]["value"] = newValue.toString();
        handleChange({ availableElements: newComponents });
    }

    const handleValueChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, indexList: number, indexParam: number) => {
        const { name, value } = event.target;
        const newComponents = [...availableElements];
        newComponents[indexList][name][indexParam]["value"] = value;
        handleChange({ availableElements: newComponents });
    }

    const handleDistributionChange = (indexList: number, indexParam: number, variableName: string, variableValue: number)=> {
        const newComponents = [...availableElements];
        if (
            newComponents &&
            newComponents[indexList] &&
            newComponents[indexList]['parameters'] &&
            newComponents[indexList]['parameters']![indexParam]
        ){
            const newValues = {
                ...JSON.parse(newComponents[indexList]['parameters']![indexParam]["value"]),
                [variableName]: variableValue
            }
            newComponents[indexList]['parameters']![indexParam]!["value"] = JSON.stringify(newValues)
            handleChange({ availableElements: newComponents });
        }
    }

    const getColor = (connection: ConnectionList): string => {
        const component = availableElements.find(comp => comp.id === connection.source.component);
        const color = (!component) ? theme.palette.primary.main : component.color;
        return color;
    }

    const handleChangeSchedule = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, indexList: number, key: string) => {
        const { name, value } = event.target;
        const newComponents = [...availableElements];
        newComponents[indexList][name][key] = value;
        handleChange({ availableElements: newComponents });
    }

    const handleChangeColor = (color: string) => {
        if (selectedComponent) {
            const newComponents: any[] = [...availableElements];
            newComponents[selectedComponent.index!]['color'] = color;
            handleChange({availableElements: newComponents});
            setColorMenu(null);
            setSelectedComponent(null);
        }
    }

    useEffect(() => {
        updateXarrow();
    }, [availableElements]);

    const handleChangeVector = (value: string, indexList: number, indexParam: number) => {
        const newComponents: any[] = [...availableElements];
        newComponents[indexList]['parameters'][indexParam]["value"] = value;
        handleChange({ availableElements: newComponents });
    }

    const editVector = (vector: string, index: number, indexParam: number, type: string, id: number) => {
        const switchTitle = () => {
            switch (type) {
                case 'doubleVector':
                    return "Edit Double Vector"
                case 'intVector':
                    return "Edit Integer Vector"
                case 'boolVector':
                    return "Edit Boolean Vector"
                case 'stringVector':
                    return "Edit String Vector"
                default:
                    return "Edit Vector"
            }
        }

        const handleNewVector = (value: string, id: number) => {
            const newVectorValue: any[] = [...newVector];
            newVectorValue.find((v: any) => v.id === id).value = value;
            setNewVector(newVectorValue);
        }

        const vectorState = newVector.find(v => v.id === id);

         setModal({
            ...modal,
            active: true,
            title: switchTitle(),
            loadingBar: false,
            size: 'large',
            component: <EditVectorContent vector={vector} setNewVector={(value: string) => handleNewVector(value, id)} type={type} />,
            buttons: [
                {
                    title: 'Cancel',
                    variant: Variant.outlined,
                    action: () => {
                        handleNewVector(vector, id);
                    }
                },
                {
                    title: 'Save',
                    action: () => {
                        handleChangeVector(vectorState.value, index, indexParam);
                    }
                },
            ],
        });
    }

    const getParameterTypeInput = (param: any, index: number, indexParam: number) => {
        switch (param.type) {
            case 'string': {
                return (
                    <NumberInput
                        data-testid={`test-id-parameter-${index}-${param.title}`}
                        size="small"
                        name='parameters'
                        value={param.value}
                        onChange={(e) => handleValueChange(e, index, indexParam)}
                        id={`${param.title}-${index}`}
                    />
                )
            }
            case 'int': {
                return (
                    <NumberInput
                        data-testid={`test-id-parameter-${index}-${param.title}`}
                        size="small"
                        name='parameters'
                        value={param.value}
                        onChange={(e) => handleValueChange(e, index, indexParam)}
                        id={`${param.title}-${index}`}
                        inputProps={{
                            inputMode: 'numeric',
                            pattern: '[0-9]*',
                            onInput: handleNumericInput
                        }}
                    />
                );
            }
            case 'double': {
                return (
                    <StyledOutlinedInput
                        id={`${param.title}-${index}`}
                        data-testid={`test-id-parameter-${index}-${param.title}`}
                        type={'number'}
                        name='parameters'
                        value={param.value}
                        onChange={(e) => handleValueChange(e, index, indexParam)}
                        inputProps={{
                            maxLength: 13,
                            step: "1"
                        }}
                    />
                )
            }
            case 'bool': {
                return (
                    <Switch
                        id={`${param.title}-${index}`}
                        data-testid={`test-id-parameter-${index}-${param.title}`}
                        checked={param.value === 'true' || false}
                        value={param.value === 'true' || false}
                        name='parameters'
                        onChange={(e) => handleBoolChange(e, index, indexParam)}
                    />
                )
            }
            case 'stringVector':
            case 'boolVector':
            case 'intVector':
            case 'doubleVector': {
                if ((newVector.length === 0) || (newVector.length > 0 && !newVector.find((v: any) => v.id === param.id))) {
                    const newVectorCopy = [...newVector];
                    newVectorCopy.push({id: param.id, value: param.value});
                    setNewVector(newVectorCopy);
                }

                const vector = newVector.find((v: any) => v.id === param.id);

                return (
                    vector &&
                    <StyledBox>
                        <VectorInput
                            data-testid={`test-id-parameter-${index}-${param.title}`}
                            size={"medium"}
                            name='parameters'
                            value={vector.value}
                            id={`${param.title}-${param.id}`}
                            disabled
                        />
                        <VectorButton onClick={() => editVector(vector.value, index, indexParam, param.type, param.id)}
                                      color="primary"
                                      variant="contained"
                                      size={'small'}
                        >
                            <EditOutlinedIcon/>
                        </VectorButton>
                    </StyledBox>
                );
            }
            case 'gammaDistribution':
            case 'uniformDistribution':
            case 'exponentialDistribution':
            case 'normalDistribution':
            case 'logNormalDistribution': {
                const distributionVar = JSON.parse(param.value)
                return (
                    <DistributionParameter
                        variables={distributionVar}
                        onChangeVariable={(name: string, newVal: number) => handleDistributionChange(index, indexParam, name, newVal)} />
                )
            }
            default:
                return (
                    <NumberInput
                        data-testid={`test-id-parameter-${index}-${param.title}`}
                        size="small"
                        name='parameters'
                        value={param.value}
                        onChange={(e) => handleValueChange(e, index, indexParam)}
                        id={`${param.title}-${index}`}
                    />
                );
        }
    }
    const handleAddComment = (zoomScale = 1) => (event: any) => {
        event.preventDefault();
        if (selectedSystem === null){
            showSystemRequiredModal()
            return
        }
        const target = event.target as HTMLDivElement;
        if (target.id !== 'card-target') {
            return;
        }
        const boundingRect = target.getBoundingClientRect();
        const clientX = event.clientX || 1080;
        const clientY = event.clientY || 304;
        const xPosition = (clientX - boundingRect.left + target.scrollLeft) / zoomScale;
        let yPosition = (clientY - boundingRect.top + target.scrollTop) / zoomScale;
        if (yPosition < 0) yPosition = 5;

        const newComment = {
            message: '',
            title: 'Comment',
            position: { x: xPosition, y: yPosition },
            dropped: true,
            editing: true,
        }

        const newComments = [...comments, newComment];

        handleChange({ comments: newComments })
    }

    const handleSaveComment = ({ message }:{ message: string }, index: number) => {
        const newComments = [...comments]
        newComments[index] = {
            ...newComments[index],
            message,
        }

        handleChange({
            comments: newComments
        })
    }

    const handleDeleteComment = (index: number) => {
        const newComments = [...comments]
        newComments.splice(index, 1)
        handleChange({
            comments: newComments
        })
    }

    return (
        <Box
            id='card-target'
            className="droppable-zone"
            sx={{
                backgroundColor: 'transparent',
                transition: 'all 0.5s ease-in-out',
                width: `${Math.max(200, 3 * 100 / zoomScale)}%`,
                height: `${Math.max(95*2, 2 * 105 / zoomScale)}%`,
                position: 'relative',
            }}
            data-testid="test-id-box-droppable-zone"
            onDragOver={allowDrop}
            onDrop={finalDrop}
            onClick={showComments ? handleAddComment(zoomScale) : undefined}
        >
            {availableElements?.map((element, index: number) => (
                <Box
                    data-testid={`test-id-draggable-${index}`}
                    key={index}
                    draggable
                    onDragStart={(ev) => { dragStartItem(ev, { ...element, index, componentType: ComponentType.Component }) }}
                    style={{
                        left: element.position.x,
                        top: element.position.y,
                    }}
                    sx={{
                        position: 'absolute',
                        zIndex: 2,
                        background: theme.palette.common.white,
                        width: theme.spacing(80),
                        border: `1px solid ${element.color}`,
                        borderRadius: theme.spacing(1),
                    }}
                    padding={theme.spacing(2)}
                >
                    <Box display='flex' alignItems='center' >
                        <ColorBox
                            data-testid={`test-id-color-button-${index}`}
                            sx={{
                                background: element.color,
                            }}
                            onClick={(ev) => {
                                setColorMenu(ev.currentTarget);
                                setSelectedComponent({ ...availableElements[index], index: index });
                            }}
                        />
                        <Typography flex={1} variant='h6'>
                            {element.title}
                        </Typography>
                        <KeyboardArrowDownOutlinedIcon
                            color="primary"
                            data-testid={`test-id-open-menu-${index}`}
                            onClick={(ev) => handleOpenMenu(ev, index)}
                            sx={{ justifySelf: 'flex-end', cursor: 'pointer', zIndex: 3 }}
                        />
                    </Box>
                    {element.schedule &&
                        <Box sx={{
                            'h6': {
                                paddingTop: theme.spacing(2)
                            },
                            borderTop: `1px solid ${theme.palette.greyScale.medium}`,
                            borderBottom: `1px solid ${theme.palette.greyScale.medium}`,
                            paddingBottom: theme.spacing(4)
                        }}
                            display='flex'
                            alignItems='center'
                            justifyContent='space-between'
                            padding={theme.spacing(2)}
                            marginTop={theme.spacing(2)}>
                            {Object.entries(element.schedule).map(([key, value], i) => (
                                <React.Fragment key={key}>
                                    <NumberInput
                                        data-testid={`schedule-input-${key}-${index}`}
                                        sx={{ marginLeft: 2, marginRight: 1 }}
                                        size="small"
                                        name={'schedule'}
                                        id={`${key}-${index}`}
                                        value={value}
                                        onChange={(e) => handleChangeSchedule(e, index, key)}
                                        inputProps={{
                                            inputMode: 'numeric',
                                            pattern: '[0-9]*',
                                            onInput: handleNumericInput
                                        }}
                                    />
                                    {i !== Object.entries(element.schedule).length - 1 && (
                                        <Typography variant='h6' fontWeight='bold'>
                                            /
                                        </Typography>
                                    )}
                                </React.Fragment>
                            ))}
                        </Box>
                    }
                    {
                        (element.parameters && element.parameters.length > 0) &&
                        <Divider sx={{ background: element.color, marginTop: theme.spacing(4) }} />
                    }
                    {element.parameters &&
                        < Box
                            marginTop={theme.spacing(4)}
                        >
                            {
                                element.parameters.map((param, indexParam) => (
                                    isTypeDistribution(param.type) ? (
                                            <Box
                                                sx={{
                                                    ':first-of-type': {
                                                        borderTop: `1px solid ${theme.palette.greyScale.medium}`
                                                    }
                                                }}
                                                key={param.title + indexParam}
                                                display='flex'
                                                flexDirection={'column'}
                                                alignItems='space-between'
                                                gap={theme.spacing(2)}
                                                borderBottom={`1px solid ${theme.palette.greyScale.medium}`}
                                                padding={theme.spacing(2)}>
                                                <Box display={'flex'} justifyContent={'space-between'}>
                                                    <Typography fontWeight={'bold'}>
                                                        {param.unit !== '' ? `${param.title} [${param.unit}]` : param.title}
                                                    </Typography>
                                                    <Typography>
                                                        {`(${param.type})`}
                                                    </Typography>
                                                </Box>
                                                {getParameterTypeInput(param, index, indexParam)}
                                            </Box>
                                        ) : (
                                            <Box
                                                sx={{
                                                    ':first-of-type': {
                                                        borderTop: `1px solid ${theme.palette.greyScale.medium}`
                                                    }
                                                }}
                                                key={param.title + indexParam}
                                                display='flex'
                                                alignItems='center'
                                                gap={theme.spacing(2)}
                                                borderBottom={`1px solid ${theme.palette.greyScale.medium}`}
                                                padding={theme.spacing(2)}>
                                                <Typography flex={1} fontWeight={'bold'}>
                                                    {param.unit !== '' ? `${param.title} [${param.unit}]` : param.title}
                                                </Typography>
                                                {getParameterTypeInput(param, index, indexParam)}
                                            </Box>
                                    )
                                ))
                            }
                        </Box>
                    }
                    {
                        (element.inputs && element.inputs.length > 0) &&
                        <Divider sx={{ background: element.color, marginTop: theme.spacing(4) }} />
                    }
                    {element.inputs &&
                        <Box
                            marginTop={theme.spacing(4)}
                        >
                            {element.inputs.map((input, indexInput) => (
                                <Box
                                    sx={{
                                        ':first-of-type': {
                                            borderTop: `1px solid ${theme.palette.greyScale.medium}`
                                        }
                                    }}
                                    id={`${input.id}-${input.title}-${element.id}-${index}`}
                                    key={input.title + indexInput}
                                    display='flex'
                                    alignItems='center'
                                    gap={theme.spacing(2)}
                                    borderBottom={`1px solid ${theme.palette.greyScale.medium}`}
                                    padding={theme.spacing(2)}>
                                    <LightTooltip placement="bottom-start" title="Input element">
                                        <IconButton
                                            data-testid={`test-id-input-button-${index}-${input.title}`}
                                            onClick={() => handleConnectionEnd(element, input, index)}
                                            sx={{
                                                padding: 'unset',
                                                background: (connectionStart != null) ?
                                                    connectionStart.component.color :
                                                    'transparent',
                                            }}
                                            aria-label='Remove Icon'>
                                            <LoginOutlinedIcon sx={{ color: (connectionStart != null) ? theme.palette.common.black : theme.palette.primary.accent3 }} />
                                        </IconButton>
                                    </LightTooltip>
                                    <Typography flex={1}>
                                        {input.unit !== '' ? `${input.title} [${input.unit}]` : input.title}
                                    </Typography>
                                </Box>
                            ))
                            }
                        </Box>
                    }

                    {
                        (element.outputs && element.outputs.length > 0) &&
                        <Divider sx={{ background: element.color, marginTop: theme.spacing(4) }} />
                    }
                    {element.outputs &&
                        <Box marginTop={theme.spacing(4)}>
                            {element.outputs.map((output, indexOutput) => (
                                <Box
                                    sx={{
                                        ':first-of-type': {
                                            borderTop: `1px solid ${theme.palette.greyScale.medium}`
                                        }
                                    }}
                                    id={`${output.id}-${output.title}-${element.id}-${index}`}
                                    key={output.title + indexOutput}
                                    display='flex'
                                    alignItems='center'
                                    gap={theme.spacing(2)}
                                    borderBottom={`1px solid ${theme.palette.greyScale.medium}`}
                                    padding={theme.spacing(2)}>
                                    <Box display='flex' gap={theme.spacing(2)} flex={1}></Box>
                                    <Typography >
                                        {output.unit !== '' ? `${output.title} [${output.unit}]` : output.title}
                                    </Typography>
                                    <LightTooltip placement="bottom-start" title="Output element">
                                        <IconButton
                                            data-testid={`test-id-output-button-${index}-${output.title}`}
                                            onClick={() => handleConnectionStart(element, output, index)}
                                            className={`${(connectionStart && connectionStart.index === index && connectionStart.component.id === element.id && connectionStart.output.id === output.id) ? 'selectedConnection' : ''}`}
                                            sx={{
                                                padding: 'unset',
                                                background: (connectionStart != null && connectionStart.component.id === element.id && output.id === connectionStart.output.id && connectionStart.index === index) ?
                                                    connectionStart.component.color :
                                                    'transparent',
                                            }}
                                            aria-label='Output Icon'>
                                            <LogoutOutlinedIcon sx={{ color: theme.palette.primary.accent3 }} />
                                        </IconButton>
                                    </LightTooltip>
                                </Box>
                            ))
                            }
                        </Box>
                    }
                </Box >
            ))
            }
            {!hideConnections && connections.length > 0 &&
                <StyleArrowContainer sizeScale={zoomScale} data-testid={'test-id-arrows-container'}>
                    {connections.map((connection, index) => (
                        <ConnectionArrow
                            key={`${index}`}
                            start={getIdOutputElement(connection)}
                            end={getIdInputElement(connection)}
                            color={getColor(connection)}
                            inputType={getConnectionInputOutput(connection.id)?.input?.type}
                            outputType={getConnectionInputOutput(connection.id)?.output?.type}
                            zoomScale={zoomScale}
                            connection={connection}
                            isValid={isConnectionValid(connection.id)}
                            onClick={() => removeConnection(connection.id)}
                            passProps={{
                                id: `arrow-${connection.id}`,
                            }}
                        />
                    ))}
                </StyleArrowContainer>
            }
            {showComments && comments?.map((comment, index) => {
                return <SystemComment
                    key={index}
                    data-testid={`test-id-comment-${index}`}
                    onDragStart={(ev) => { dragStartItemComment(ev, { ...comment, index, componentType: ComponentType.Comment }) }}
                    onSave={(commentInfo) => handleSaveComment(commentInfo, index)}
                    onDelete={() => handleDeleteComment(index) }
                    draggable
                    position={comment.position}
                    title={comment.title}
                    message={comment.message}
                    editing={comment.editing}
                />
            })}
            <Menu
                id="basic-menu"
                data-testid="test-id-menu-draggale-dropdown"
                anchorEl={anchorEl}
                open={open}
                onClose={() => {
                    setAnchorEl(null);
                    setSelectedComponent(null);
                }}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem data-testid="test-id-menu-item-duplicate" onClick={duplicateItem}>
                    <ControlPointDuplicateOutlinedIcon color="primary" />
                    <Typography sx={{ color: theme.palette.primary.main, paddingLeft: 2 }}>
                        DUPLICATE
                    </Typography>
                </MenuItem>
                <MenuItem data-testid="test-id-menu-item-delete" onClick={deleteItem}>
                    <DeleteOutlineOutlinedIcon color="primary" />
                    <Typography sx={{ color: theme.palette.primary.main, paddingLeft: 2 }}>
                        DELETE
                    </Typography>
                </MenuItem>
            </Menu>
            <Menu
                id="color-menu"
                data-testid="test-id-menu-dropdown-color"
                anchorEl={colorMenu}
                open={Boolean(colorMenu)}
                onClose={() => {
                    setColorMenu(null);
                    setSelectedComponent(null);
                }}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <Box display='flex'>
                    {colors.slice(0, 4).map((color, idx) => (
                        <MenuItem sx={{ paddingX: theme.spacing(2) }} key={idx} onClick={() => handleChangeColor(color)} data-testid={`test-id-menu-item-${idx}`}>
                            <ColorBox sx={{ background: color }} />
                        </MenuItem>
                    ))}
                </Box>
                <Box display='flex'>
                    {colors.slice(4).map((color, idx) => (
                        <MenuItem sx={{ paddingX: theme.spacing(2) }} key={idx + 4} onClick={() => handleChangeColor(color)} data-testid={`test-id-menu-item-${idx + 4}`}>
                            <ColorBox sx={{ background: color }} />
                        </MenuItem>
                    ))}
                </Box>
            </Menu>
        </Box >
    )
}

export default TargetZone