/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Box, Button, OutlinedInput, TextField} from "@mui/material";

export const StyledBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(1)
}));

export const VectorButton = styled(Button)(({ theme }) => ({
    minWidth: 0,
    width: theme.spacing(8),
    height: theme.spacing(8)
}));

export const VectorInput = styled(TextField)(({ theme }) => ({
    background: '#fff',
    marginLeft: 2,
    marginBottom: theme.spacing(2),
    outline: 'none !important',
    marginTop: '5px',
    padding: 0,
    width: '120px',
    height: '30px',
    '&::before': {
        all: 'unset'
    },
    '&.Mui-focused': {
        outline: 'none !important'
    },
    'input': {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: 10,
        paddingRight: 5
    },
    borderRadius: theme.spacing(1)
}));

export const ColorBox = styled(Box)(({ theme }) => ({
    zIndex: 1,
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    marginRight: theme.spacing(1),
    width: theme.spacing(4),
    height: theme.spacing(4),
    minWidth: theme.spacing(4)
}));

export const NumberInput = styled(TextField)(({ theme }) => ({
    background: '#fff',
    marginLeft: 2,
    marginBottom: theme.spacing(2),
    outline: 'none !important',
    marginTop: '5px',
    padding: 0,
    width: '120px',
    height: '30px',
    '&::before': {
        all: 'unset'
    },
    '&.Mui-focused': {
        outline: 'none !important'
    },
    'input': {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: 10,
        paddingRight: 5
    },
    borderRadius: theme.spacing(1)
}));

export const StyledOutlinedInput = styled(OutlinedInput)(({ theme }) => ({
    background: '#fff',
    marginLeft: 2,
    marginBottom: theme.spacing(1),
    outline: 'none !important',
    marginTop: '5px',
    padding: 0,
    width: '120px',
    '&.Mui-focused': {
        outline: 'none !important'
    },
    'input': {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: 10,
        paddingRight: 5
    },
    borderRadius: theme.spacing(1)
}));

export const StyleArrowContainer = styled('div')(({ sizeScale }: { sizeScale: number }) => ({
    '& svg': {
        transform: `scale(${1 / sizeScale }) translate(calc(${50 * (1 - sizeScale)}% - ${20 * (1 - sizeScale)}px), calc(${50 * (1 - sizeScale)}% - ${20 * (1 - sizeScale)}px))`,
        transformOrigin: 'center'
    },
}))

export const ArrowInfo = styled('div')(({ sizeScale = 1, offset }: { sizeScale?: number, offset: { x: number, y: number } }) => ({
    transform: `scale(${1 / Math.pow(sizeScale, 0.5)}) translate(${offset.x}px, ${offset.y}px) !important`
}))