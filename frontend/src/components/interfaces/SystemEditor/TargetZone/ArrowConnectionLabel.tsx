/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {FC, ReactNode, useCallback, useEffect, useState} from "react";
import {ArrowInfo} from "./targetZone.styles.ts";
import AlertIcon from "../../../../assets/icons/alert.svg";
import DeleteIcon from "@mui/icons-material/Delete";
import {IconButton, useTheme} from "@mui/material";
import {LightTooltip} from "../../../../styles/general.styles.tsx";

interface ArrowConnectionLabelProps {
    connectionId: number;
    isValid?: boolean;
    deleteMode?: boolean;
    zoomScale: number;
    className?: string;
    alertText?: string;
    children?: ReactNode;
    onRemove?(): void;
    onMouseOver?(): void;
    onMouseOut?(): void;
}

const ArrowConnectionLabel: FC<ArrowConnectionLabelProps> = (props)=> {
    const { connectionId,
        isValid,
        alertText,
        deleteMode,
        zoomScale,
        children,
        onRemove,
        ...rest
    } = props
    const theme = useTheme()
    const [dimensions, setDimensions] = useState({ x: 100, y: 100 })
    const [zoom, setZoom] = useState(zoomScale)
    const [sizeObserver, setSizeObserver] = useState<ResizeObserver>()

    const handleArrowSizeChange = useCallback(()=> {
        const element = document.getElementById(`arrow-${connectionId}`)
        const dim = element?.getBoundingClientRect()

        setDimensions({
            x: zoomScale !== 1 ? (dim?.width || 100) * (-.1 + 1 / zoomScale * .2) : 0,
            y: zoomScale !== 1 ? (dim?.height || 100) * (-.1 + 1 / zoomScale * .2) : 0,
        })
    }, [zoomScale, connectionId])

    useEffect(() => {
        setZoom(zoomScale)
        const arrowEl = document.getElementById(`arrow-${connectionId}`)
        if (arrowEl){
            sizeObserver?.disconnect()
            const observer = new ResizeObserver(handleArrowSizeChange)
            observer.observe(arrowEl)
            setSizeObserver(observer)
            return () => {
                observer?.disconnect();
            };
        }
    }, [handleArrowSizeChange, zoomScale]);

    useEffect(() => {}, [deleteMode]);

    return (
        <ArrowInfo offset={dimensions} sizeScale={zoom} {...rest} >
            {deleteMode ? (
                <IconButton
                    onClick={()=> onRemove && onRemove()}
                    data-testid={`${connectionId}-delete-button`}
                    sx={{ transform: `scale(${Math.max(1, 1 / zoom)})`}}
                >
                    <DeleteIcon sx={{ color: theme.palette.error.main, transform: 'none !important' }} />
                </IconButton>
            ) : (
                !isValid && (
                    <LightTooltip title={alertText}>
                        <img data-testid={`${connectionId}-invalid-icon`} alt={`${connectionId}`} src={AlertIcon} />
                    </LightTooltip>
                )
            )}
            {children}
        </ArrowInfo>
    )
}

export default ArrowConnectionLabel