/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {ChangeEvent, FC, useEffect, useState} from "react";
import {Box} from "@mui/system";
import {Typography, useTheme} from "@mui/material";
import DecimalInput from "../../../commonComponents/DecimalInput/DecimalInput.tsx";

export type IVariable = {
    [variable: string]: number
}

export type DistributionParameterProps = {
    variables: IVariable,
    onChangeVariable?: (name: string, newVal: number) => void,
    [param: string]: any
}

const DistributionParameter: FC<DistributionParameterProps> = (props) => {
    const theme = useTheme()
    const { variables: variablesProp, onChangeVariable, ...rest } = props
    const [variables, setVariables] = useState<IVariable>(variablesProp)

    useEffect(() => {}, [variables]);

    const handleChange = (name: string, newValue: number) => {
        setVariables({
            ...variables,
            [name]: newValue
        })
        if (onChangeVariable && !isNaN(newValue)) onChangeVariable(name, newValue)
    }

    return (
        <Box {...rest}
             display={'flex'}
             flexDirection={'column'}
        >
            {Object.keys(variables).map((va) => (
                <Box display={'flex'}
                     flexDirection={'row'}
                     alignItems={'center'}
                     marginLeft={'1em'}
                     padding={theme.spacing(1,0)}
                     borderTop={`1px solid ${theme.palette.greyScale.medium}`}>
                    <Typography flex={1}>
                        {`${va.charAt(0).toUpperCase() + va.slice(1)}`}
                    </Typography>
                    <DecimalInput
                        data-testid={`${rest['data-testid']}-distribution-${va}`}
                        name='parameters'
                        initialValue={variables[va]}
                        onChange={(e:ChangeEvent<HTMLInputElement>) => handleChange(va, Number(e.target.value))}
                    />
                </Box>
            ))}
        </Box>
    )
}

export default DistributionParameter