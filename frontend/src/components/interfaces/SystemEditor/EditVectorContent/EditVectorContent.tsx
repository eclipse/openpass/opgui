/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {useState} from "react";
import {DeleteButton, StyledBox, StyledButton, StyledInputLine, StyledOutlinedInput, StyledTextField} from "./editVectorContent.styles.ts";
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import CustomNumberInput from "../CustomNumberInput/CustomNumberInput.tsx";
import BoolVectorInput from "../BoolVectorInput/BoolVectorInput.tsx";

interface Props {
    vector: string,
    setNewVector: (value: string) => void,
    type: string
}

const EditVectorContent = (props: Props) => {
    const {vector, setNewVector, type} = props;
    const vectorArray = vector.slice(1).slice(0, -1).replace(/\s+/g, '').split(',');
    const [arr, setArr] = useState(vectorArray);

    const handleSave = (copyNewVector: any[]) => {
        setArr(copyNewVector);
        setNewVector("[" + copyNewVector.toString() + "]");
    }

    const handleVectorItem = (e: any, index: number) => {
        const newValue = e.target.value;
        const copyNewVector = [...arr];
        copyNewVector[index] = newValue;
        handleSave(copyNewVector);
    }

    const handleAddInputField = () => {
        const copyNewVector = [...arr, ""];
        handleSave(copyNewVector);
    }

    const handleDeleteInputField = (index: number) => {
        const copyNewVector = [...arr];
        copyNewVector.splice(index, 1);
        handleSave(copyNewVector);
    }

    return (
        <>
            <StyledBox>
                {arr.map((item: string, index: number) => {
                    return <StyledInputLine key={index}>
                        {type === 'doubleVector' && <StyledOutlinedInput
                            type={"number"}
                            key={index}
                            value={item}
                            inputProps={{
                                maxLength: 13,
                                step: "1"
                            }}
                            onChange={(e: any) => handleVectorItem(e, index)}
                        />}
                        {type === 'stringVector' && <StyledTextField
                            type={"text"}
                            key={index}
                            value={item}
                            onChange={(e: any) => handleVectorItem(e, index)}
                        />}
                        {type === 'intVector' && <CustomNumberInput
                            key={index}
                            value={Number(item)}
                            onChange={(e: any) => handleVectorItem(e, index)}
                        />}
                        {type === 'boolVector' && <BoolVectorInput
                            key={index}
                            text={`Element number ${index + 1}`}
                            value={item}
                            onChange={(e: any) => handleVectorItem(e, index)}
                        />}
                        {index > 0 ? <DeleteButton onClick={() => handleDeleteInputField(index)}>
                            <DeleteOutlinedIcon/>
                        </DeleteButton> : <DeleteButton sx={{width: "36px"}} disabled></DeleteButton>}
                    </StyledInputLine>
                })}
            </StyledBox>
            <StyledButton variant='contained' onClick={handleAddInputField}>Add</StyledButton>
        </>
    );
}

export default EditVectorContent;