/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Button, Grid, InputAdornment, Menu, MenuItem, Typography, useTheme} from "@mui/material";
import UploadFromFileTree from "../../../../commonComponents/UploadInput/UploadFromFileTree.tsx";
import {INPUT_FILE, PATHS, Variant} from "../../../../../index.d";
import {
    BaseInputStyled,
    FilterCheckbox,
    FooterComponent,
    LightTooltip,
    SeparatorLine,
    StickyDiv
} from "../../../../../styles/general.styles.tsx";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import SearchIcon from "@mui/icons-material/Search";
import SimulationIdComponent from "../../SimulationIdComponent.tsx";
import {
    convertToConfigs,
    exportToSimulation,
    getConfigElements,
    getFileContent,
    pathToConvertedCasesPost,
    sendPCMFile,
    verifyPath
} from "../../../../../utils/api.util.ts";
import {useForm} from "../../../../../hooks/useForm.ts";
import {ISetupConfig, ISimulationId, SIMULATIONS} from "../../setupConfigs.d";
import {useCallback, useEffect, useMemo, useRef} from "react";
import {useGlobalContext} from "../../../../../context/ContextProvider.tsx";
import {useNavigate} from "react-router-dom";
import {ConfigElement, SelectedExperimentsRequest, SystemRef, VerifyPath200Response} from "opGUI-client-frontend";
import AgentComponent from "../../AgentComponent.tsx";


const BasedOnPCM = () => {
    const theme = useTheme()

    const { values, handleChangeByName, handleInputChange, handleChange } = useForm<ISetupConfig>({
        simulations: [],
        agents: [
            { name: "Agent 1", file: "", selected: false },
            { name: "Agent 2", file: "", selected: false },
            { name: "Other", file: "", selected: false },
        ],
        pathToPcm: '',
        configSet: 'pcm',
        pathToConvertedCases: '',
        searchTerm: '',
        fileFromList: '',
        anchorEl: null,
        isExportDisabled: true
    });
    const { simulations,
        pathToPcm,
        pathToConvertedCases,
        agents,
        searchTerm,
        fileFromList,
        anchorEl,
        isExportDisabled } = values;
    const { setModal, modal, setShowDiscardModal, setSelectedTab, setExportedFile } = useGlobalContext()
    const navigation = useNavigate();
    const open = Boolean(anchorEl);
    const agentContainerRef = useRef<HTMLDivElement>(null);

    const filterData = useCallback((filterText: string) => {
        if (filterText.length === 0) return [...simulations]
        const copySimulations = [...simulations];
        const searchTerms = filterText.split(",").map((term) => term.trim());
        return copySimulations.filter((item) => (searchTerms.includes(item.id.toString()) || item.id.toString().includes(filterText)));
    }, [simulations]) ;

    const filteredSimulations = useMemo(()=>filterData(searchTerm), [searchTerm, filterData])

    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        handleChangeByName('anchorEl', event.currentTarget);
    };

    const setUnsetAll = (state: boolean) => {
        const newSimulations = simulations.map((sim) => ({ ...sim, selected: filteredSimulations.includes(sim) ? state : sim.selected }));
        handleChange({
            simulations: newSimulations
        })
    }

    const handleClose = () => {
        handleChangeByName('anchorEl', null);
    };

    const checkUndeterminated = () => {
        const selected = filteredSimulations.filter(sim => sim.selected).length;
        return (filteredSimulations.length > selected && selected > 0)
    }

    const handleFiles = async (name: string, path: string) => {
        if (name === 'pathToPcm') {
            let responseVerify = await verifyPath(path, true);
            if ('error' in responseVerify) {
                setModal({
                    ...modal,
                    active: true,
                    loadingBar: false,
                    title: 'Path verification error',
                    descriptionTitle: '',
                    description: responseVerify.message,
                    buttons: [{
                        title: 'Close',
                    }]
                });
                return;
            }
            responseVerify = responseVerify as VerifyPath200Response
            let experiments: ISimulationId[] = [];
            const response = await sendPCMFile(responseVerify?.realPath || '');
            if ('error' in response && response.error) {
                setModal({
                    ...modal,
                    active: true,
                    title: 'Error',
                    descriptionTitle: 'Failed to load the database',
                    description: response.message,
                    loadingBar: false,
                    buttons: [
                        {
                            title: 'Close',
                        },
                    ],
                });
            } else {
                if (response) {
                    experiments = (response as SelectedExperimentsRequest).selectedExperiments.map((sim: string) => ({id: parseInt(sim), selected: false}))
                }
                handleChange({
                    pathToPcm: path,
                    simulations: [...experiments],
                })
                return;
            }
        }
        const pathToConvertedCasesResponse = await pathToConvertedCasesPost(path);
        if ('response' in pathToConvertedCasesResponse) {
            handleChangeByName(name, path);
        }
    }

    const handleCheckedSimulations = (id: number) => {
        const index = simulations.findIndex((sim) => sim.id === id);
        const newSimulations = [...simulations];
        newSimulations[index].selected = !newSimulations[index].selected;
        handleChangeByName(SIMULATIONS, newSimulations);
    }

    const isDisabled = () => {
        return simulations.length === 0;
    }

    const handleConvert = async () => {
        const selectedExperiments: Array<string> = simulations
            .filter((sim) => sim.selected)
            .map((sim) => sim.id.toString());
        const selectedAgents: SystemRef[] = agents
            .filter((age) => age.selected)
            .map((age) => ({
                file: age.file,
                name: age.name
            }));
        if (selectedAgents.length === 0) {
            setModal({
                ...modal,
                active: true,
                title: 'Incomplete Data',
                description: `At least one agent should be selected`,
                descriptionTitle: '',
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    }
                ],
            });
            return;
        }
        const incompleteAgents = selectedAgents.filter(agent => (agent.file === ""))
        if (incompleteAgents.length > 0) {
            setModal({
                ...modal,
                active: true,
                title: 'Incomplete Data',
                description: `The selected agents must have a file loaded.`,
                descriptionTitle: '',
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    }
                ],
            });
            return;
        }
        const response = await convertToConfigs(selectedExperiments, selectedAgents);
        if ('response' in response) {
            handleChange({ isExportDisabled: false });
            setShowDiscardModal(true);
            setModal({
                ...modal,
                active: true,
                title: 'Convert To Configs',
                description: response.response,
                loadingBar: false,
                descriptionTitle: '',
                buttons: [{
                    title: 'Done'
                }]
            });
        }
        if ('error' in response) {
            setModal({
                ...modal,
                active: true,
                loadingBar: false,
                title: 'Error Converting to Configs',
                descriptionTitle: '',
                description: response.message,
                buttons: [{
                    title: 'Close',
                }]
            });
            return;
        }
    }

    const handleExportToRun = (response: any) => {
        // 1 - close the modal
        setModal({
            ...modal,
            active: false
        });

        // 2 - redirect to Run tab
        setSelectedTab(PATHS.SIMULATION_MANAGER);
        navigation(PATHS.SIMULATION_MANAGER);

        // 3 - fill in the exported file
        setExportedFile({
            selectedExperiments: response.selectedExperiments,
            path: response.path,
            xmlContent: response.xmlContent
        });
    }

    const handleExport = async () => {
        const selectedExperiments: Array<string> = simulations
            .filter((sim) => sim.selected)
            .map((sim) => sim.id.toString());
        const response = await exportToSimulation(selectedExperiments);
        if (response) {
            setShowDiscardModal(false);
            setModal({
                ...modal,
                active: true,
                title: 'Export',
                description: "After exporting, you will find the selected IDs in an XML file. Once this process is complete, you will be redirected to the RUN screen to continue with the configuration.",
                loadingBar: false,
                descriptionTitle: '',
                buttons: [{
                    title: 'Cancel',
                    variant: Variant.outlined
                },{
                    title: 'Export',
                    action: () => handleExportToRun(response)
                }]
            });
        }
        if ('error' in response) {
            setModal({
                ...modal,
                active: true,
                loadingBar: false,
                title: 'Error Exporting',
                descriptionTitle: '',
                description: response.message,
                buttons: [{
                    title: 'Close',
                }]
            });
            return;
        }

    }

    const handleCSVPath = async (filePath: string) => {
        const res  = await getFileContent(filePath, true)
        if ('error' in res && res.error) {
            setModal({
                ...modal,
                active: true,
                title: 'Error: Failed to load file',
                description: res.message,
                loadingBar: false,
                buttons: [
                    {
                        title: 'Close',
                    },
                ],
            });
        } else {
            const fileString: string = atob('response' in res ? res.response : '')
            const delimiter = detectDelimiter(fileString);
            let lines = fileString.trim().split(/\r\n|\n|\r/);

            // Checking if the first line is 'simulations' and removing it if true
            if (lines[0].trim().toLowerCase() === "simulations") {
                lines = lines.slice(1);
            }
            const cleanedCSV = lines.join('\n');

            const searchTermFromCSV = cleanedCSV.replace(new RegExp(`[${delimiter}]`, 'g'), ',');

            handleChange({ searchTerm: searchTermFromCSV });
        }
    }

    const detectDelimiter = (str: string) => {
        const delimiters = [',', ';', '\n', '|'];
        for (const delimiter of delimiters) {
            if (str.includes(delimiter)) {
                return delimiter;
            }
        }
        return ',';
    };

    const loadDefaultPathToConvertedCases =  async ()=> {
        const configs = await getConfigElements()
        if (!('error' in configs)){
            const pathToConvertedCases = configs.find((con: ConfigElement) => con.id === 'PATH_CONVERTED_CASES')
            if (pathToConvertedCases && pathToConvertedCases.value){
                handleChangeByName('pathToConvertedCases', pathToConvertedCases.value)
            }else{
                setModal({
                    ...modal,
                    title: 'Error loading default path to converted cases',
                    description: 'The application could not find the path to converted cases in the configuration file. You can set this value in Global Setup inside the app.',
                    active: true,
                })
            }
        }
    }

    useEffect(() => {
        loadDefaultPathToConvertedCases()
    }, []);

    useEffect(() => {
        handleClose();
    }, []);

    return (
        <>
            <StickyDiv sx={{ position: 'relative', backgroundColor: 'transparent' }}>
                <Box marginLeft={4} sx={{ display: 'inline-flex', alignItems: 'center', gap: 5, padding: 1, paddingRight: 4, width: '100%' }}>
                    <Box>
                        <Typography sx={{ flex: .1 }}> Path to PCM database </Typography>
                    </Box>
                    <UploadFromFileTree
                        type={INPUT_FILE.file}
                        extension={'db'}
                        route={pathToPcm}
                        error={pathToPcm === ''}
                        styles={{ flex: .6 }}
                        name='pathToPcm'
                        handleFiles={handleFiles}
                        manualEdit
                    />
                </Box>
            </StickyDiv>
            <StickyDiv sx={{ backgroundColor: theme.palette.grey[100] }}>
                <Box display='flex' alignItems='center' gap={4} marginLeft={3} paddingLeft={2} height='100%'>
                    <Box display='flex' data-testid="test-id-box-dropdown-selector" alignItems='center' onClick={handleClick} sx={{ cursor: 'pointer' }}>
                        <FilterCheckbox indeterminate={checkUndeterminated()} data-testid='test-id-checkbox-setup-filter' color="primary" name='allSelected' />
                        <ArrowDropDownIcon color='primary' />
                    </Box>
                    <Menu
                        id="basic-menu"
                        data-testid="test-id-menu-dropdown"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem data-testid="test-id-menu-item-select-all" onClick={() => setUnsetAll(true)}>
                            <Typography sx={{ color: theme.palette.primary.main }}>
                                SELECT ALL SIMULATIONS
                            </Typography>
                        </MenuItem>
                        <MenuItem data-testid="test-id-menu-item-unselect-all" onClick={() => setUnsetAll(false)}>
                            <Typography sx={{ color: theme.palette.primary.main }}>
                                UNSELECT ALL SIMULATIONS
                            </Typography>
                        </MenuItem>
                    </Menu>
                    <SeparatorLine />
                    <Typography>
                        {simulations.filter((sim) => sim.selected).length} / {simulations.length} IDs Selected
                    </Typography>
                    <SeparatorLine />
                    <BaseInputStyled value={searchTerm} name="searchTerm" onChange={handleInputChange} placeholder="Search by ID" data-testid='test-id-input-setup-filter'
                        startAdornment={
                            <InputAdornment position="start">
                                <SearchIcon sx={{ color: 'black' }} />
                            </InputAdornment>
                        }
                         endAdornment={
                             <LightTooltip title={<>To add more than one ID add ','<br />between the different IDs.</>}>
                                 <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                                     <i style={{}} className="icon-mask primary info-search"></i>
                                 </InputAdornment>
                             </LightTooltip>

                         }
                    />
                </Box>
                <Box sx={{ display: 'inline-flex', alignItems: 'center', gap: 5, padding: 1, paddingRight: 4 }}>
                    <Box>
                        <Typography sx={{ flex: .1 }}> Filter from list </Typography>
                    </Box>
                    <UploadFromFileTree
                        route={fileFromList}
                        data-testid="test-id-button-filter"
                        extension={'csv'}
                        buttonDisabled={simulations.length === 0}
                        type={INPUT_FILE.file}
                        error={false} styles={{ flex: 1 }}
                        name='fileFromList'
                        handleFiles={(_inputName:string, filePath:string) => handleCSVPath(filePath)}
                        manualEdit
                    />
                </Box>
            </StickyDiv>
            <Box paddingTop={2} height={'35vh'} overflow={'auto'}>
                { simulations && simulations.length === 0 &&
                <Typography data-testid="test-id-label-empty-ids" sx={{ paddingLeft: 5, paddingTop: 5 }}>
                    No IDs loaded.
                </Typography>
                }
                <Box data-testid="test-id-container-simulation-id">
                    {(simulations && simulations.length > 0) && filteredSimulations.map((data, index) => (
                        <SimulationIdComponent key={data.id} {...data} index={index} handleChange={handleCheckedSimulations} />
                    ))
                    }
                </Box>
            </Box>
            <Box ref={agentContainerRef} padding={theme.spacing(2)} sx={{ backgroundColor: theme.palette.grey[100], borderBottom: `1px solid ${theme.palette.greyScale.main}` }}>
                <Typography fontWeight={600} padding={theme.spacing(1,1, 3)}>
                    System Configuration Files
                </Typography>
                <Grid container data-testid='test-id-grid-agent-container'>
                    {agents && agents.map((data, index) => (
                        <AgentComponent
                            key={data.name}
                            {...data}
                            agents={agents}
                            handleChange={handleChange}
                            index={index}
                        />
                    ))
                    }
                </Grid>
            </Box>
            <FooterComponent data-testid="test-id-footer-setup">
                <Box marginLeft={3} flex={1}>
                    <Box sx={{ display: 'inline-flex', alignItems: 'center', gap: 5, padding: 1, paddingRight: 4, width: '90%' }}>
                        <Box>
                            <Typography sx={{ flex: .1 }}> Path to converted cases </Typography>
                        </Box>
                        <UploadFromFileTree route={pathToConvertedCases} error={false} styles={{ flex: 1 }} name='pathToConvertedCases' handleFiles={handleFiles} />
                    </Box>
                </Box>
                <Button color='primary' onClick={handleConvert} data-testid="test-id-button-convert" variant='contained' name='convert' disabled={isDisabled()}>Convert to configs</Button>
                <Button color='primary' onClick={handleExport} data-testid="test-id-button-export" variant='contained' disabled={isExportDisabled} sx={{ marginLeft: 5, marginRight: 7 }} >Export to simulation</Button>
            </FooterComponent>
        </>
    )
}

export default BasedOnPCM