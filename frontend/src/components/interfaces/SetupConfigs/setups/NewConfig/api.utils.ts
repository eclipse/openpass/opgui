// This file´s functions will be used once backend is ready
// This is a tempt interface to avoid typescript errors, it will be filled more in the future
// using backend's interface, ask Arnau.
interface ISystemConfigFrontend {
    simulationConfig: {
        observations: {
            loggingGroups: any[]
        }
    }
}
interface ISystemConfigBackend{
    simulationConfig: {
        observations: {
            loggingGroups: string
        }
    }
}

export const covertConfigToBackend = (frontendConfig: ISystemConfigFrontend)=> {
    const { simulationConfig } = frontendConfig

    return {
        ...frontendConfig,
        simulationConfig: {
            ...simulationConfig,
            observations: {
                loggingGroups: simulationConfig.observations.loggingGroups?.join(',') || ''
            }
        }
    }
}

export const covertConfigToFrontend = (backendConfig: ISystemConfigBackend)=> {
    const { simulationConfig } = backendConfig

    return {
        ...backendConfig,
        simulationConfig: {
            ...backendConfig?.simulationConfig,
            observations: {
                ...backendConfig?.simulationConfig.observations,
                loggingGroups: simulationConfig.observations?.loggingGroups?.split(',')
            }
        }
    }
}