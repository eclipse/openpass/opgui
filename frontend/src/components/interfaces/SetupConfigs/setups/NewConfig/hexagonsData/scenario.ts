const scenarioStructure = {
    type: '',
    inputStructure: {
        environment: {
            type: '',
            label: 'Environment',
            inputStructure: {
                timesOfDays: {
                    label: 'Times of Day',
                    type: '',
                    defaultValue: {
                        value: 5,
                        probability: .1
                    },
                    inputStructure: {}
                },
                visibilityDistances: {
                    label: 'Visibility Distances',
                    type: '',
                    defaultValue: {
                        value: 500,
                        probability: .1
                    },
                    inputStructure: {}
                },
                frictions: {
                    label: 'Frictions',
                    type: '',
                    defaultValue: {
                        value: .5,
                        probability: .1
                    },
                    inputStructure: {}
                },
                weathers: {
                    label: 'Weathers',
                    type: '',
                    defaultValue: {
                        probability: .1
                    },
                    inputStructure: {}
                },
                trafficRules: {}
            }
        }
    }
}

export default scenarioStructure