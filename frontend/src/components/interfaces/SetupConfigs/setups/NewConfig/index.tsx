/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Button, Typography} from "@mui/material";
import Hexagon from "./Hexagon.tsx";
import {Positioner} from "./index.styles.ts";
import ArrowIcon from "../../../../../assets/icons/arrow.svg";
import {dependencies, hexagonsData, IDependency, IHexagonData} from "./hexagonsData";
import SideForm from "./SideForm";
import {useMemo, useState} from "react";
import modifyProperty from "../../../../../utils/modifyProperty.ts";
import {useGlobalContext} from "../../../../../context/ContextProvider.tsx";
import {Variant} from "../../../../../index.d";
import ModalComponent from "../../../../Modal/ModalComponent.tsx";
import {FooterComponent} from "../../../../../styles/general.styles.tsx";
import UploadFromFileTree from "../../../../commonComponents/UploadInput/UploadFromFileTree.tsx";

const getDependencyPosition = (dependency: IDependency) => {
    const hexagon = hexagonsData.find(hex => hex.id === dependency.hexagon)
    const dependencyHexagon = hexagonsData.find(hex => hex.id === dependency.hexagonDependency)
    if (!hexagon || !dependencyHexagon){
        return {
            position: {
                x: -100,
                y: -100
            },
            rotation: 0,
        }
    }
    const arrowPosition = {
        x: Math.abs(hexagon.position.x + dependencyHexagon.position.x) / 2,
        y: Math.abs(hexagon.position.y + dependencyHexagon.position.y) / 2
    }
    const arrowRotationDeg = Math.atan(
        (hexagon.position.y - dependencyHexagon.position.y) / (hexagon.position.x - dependencyHexagon.position.x)
    ) - Math.PI / 2

    return {
        position: arrowPosition,
        rotation: arrowRotationDeg
    }
}

const isDifferent = (objectA: object, objectB: object)=> {
    const setupDataString = JSON.stringify(objectA || {})
    const localDataString = JSON.stringify(objectB || {})

    return setupDataString !== localDataString
}

const NewConfig = ()=> {
    const { modal, setModal } = useGlobalContext();
    const [selected, setSelected] = useState(hexagonsData[3])
    const [setupData, setSetupData] = useState<{ [prop: string]: any }>({})
    const [localData, setLocalData] = useState(selected.formInitialData)
    const [_, setNewConfigRoute] = useState('')

    const unsavedChanges = useMemo(() => isDifferent(localData, setupData[selected.id] || {}), [localData, setupData, selected])

    const handleSubmit = (value: object)=> {
        const newSetupData = modifyProperty(setupData, selected.id, value)
        setSetupData(newSetupData)
        setLocalData(value)
    }

    const handleLocalUpdate = (value: object) => {
        setLocalData(value)
        console.log(value)
    }

    const handleChangeHexagon = (hexagonData: IHexagonData) => {
        if (unsavedChanges){
            setModal({
                ...modal,
                active: true,
                title: 'Local changes not saved!',
                description: "You haven't saved your local changes. If you leave now, they will be lost. Continue?",
                buttons: [
                    {
                        title: 'Lose Local Changes',
                        action: () => handleConfirmChangeHexagon(hexagonData),
                        variant: Variant.outlined,
                        testId: 'test-id-modalbutton-continue'
                    },
                    {
                        title: 'Save Changes',
                        action: ()=> {
                            handleSubmit(localData)
                            handleConfirmChangeHexagon(hexagonData)
                        },
                    },
                ],
            })
        }
        else{
            handleConfirmChangeHexagon(hexagonData)
        }
    }

    const handleConfirmChangeHexagon = (hexagonData: IHexagonData) => {
        setSelected(hexagonData)
        setLocalData({...(setupData[hexagonData.id] || hexagonData.formInitialData) })
    }

    const handleFiles = (_name: string, path: string) => {
        setNewConfigRoute(path)
    }

    return (
        <>
            <ModalComponent />
            <Box width={'100%'} height={'calc(100% - 64px)'} display={'flex'}>
                <Box width={'50%'} position={'relative'}>
                    {hexagonsData.map((h,i)=> (
                        <Positioner position={h.position}>
                            <Hexagon
                                title={`${h.title}${isDifferent(setupData[selected.id], localData) && selected.id === h.id ? '*' : ''}`}
                                disabled={h.disabled}
                                selected={selected.id === h.id}
                                onClick={()=> handleChangeHexagon(h)}
                                data-testid={`hexagon-${i}`}
                            />
                        </Positioner>
                    ))}
                    {dependencies.map(d => {
                        const { position, rotation } = getDependencyPosition(d)
                        return (
                            <Positioner position={position} rotation={rotation}>
                                <img src={ArrowIcon} style={{ width: 30, height: 30 }}/>
                            </Positioner>
                        )
                    })}
                </Box>
                <SideForm
                    title={`${selected.title} ${unsavedChanges ? '*' : ''}`}
                    formData={localData || {}} formStructure={selected.formStructure}
                    onSubmit={handleSubmit}
                    onChange={handleLocalUpdate}
                    key={`${selected.id}`}
                />
            </Box>
            <FooterComponent data-testid="test-id-footer-setup">
                <Box marginLeft={3} flex={1}>
                    <Box sx={{ display: 'inline-flex', alignItems: 'center', gap: 5, padding: 1, paddingRight: 4, width: '90%' }}>
                        <Box>
                            <Typography sx={{ flex: .1 }}> Path to converted cases </Typography>
                        </Box>
                        <UploadFromFileTree route={''} handleFiles={handleFiles} styles={{ flex: 1 }} name='pathToConvertedCases' manualEdit />
                    </Box>
                </Box>
                <Button color='primary' data-testid="test-id-button-convert" variant='contained' name='convert'>Convert to configs</Button>
                <Button color='primary' data-testid="test-id-button-export" variant='contained' sx={{ marginLeft: 5, marginRight: 7 }} >Export to simulation</Button>
            </FooterComponent>
        </>
    )
}

export default NewConfig