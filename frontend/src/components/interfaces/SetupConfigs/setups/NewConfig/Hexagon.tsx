/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {HexagonContent, HexagonStyled} from "./index.styles.ts";
import {FC} from "react";

interface HexagonProps {
    title: string;
    disabled?: boolean;
    selected?: boolean;
    onClick?(event: React.MouseEvent<HTMLButtonElement>): void;
    'data-testid'?: string;
}

const Hexagon: FC<HexagonProps> = (props) => {
    const { title, disabled, selected, ...rest } = props
    const testId = rest['data-testid']
    return (
        <HexagonStyled disabled={disabled} selected={selected} {...rest}>
            <HexagonContent data-testid={`${testId}-content`} disabled={disabled} selected={selected}>
                {title}
            </HexagonContent>
        </HexagonStyled>
    )
}

export default Hexagon