/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import {Box, Typography} from "@mui/material";

export const SideContent = styled(Box)(({ theme }) => ({
    background: '#eaeaea',
    width: '70%',
    minWidth: 600,
    maxHeight: 'calc(100% - 35px)',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'start',
    overflow: 'auto',
    zIndex: 10,
    '& .MuiInputBase-root': {
        background: theme.palette.background.default
    },
    borderLeft: `1px solid ${theme.palette.grey[300]}`,
}));

export const StyledForm = styled(Box)(({ theme }) => ({
    width: '100%',
    height: '100%',
    maxHeight: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'start',
    overflow: 'auto',
    zIndex: 10,
    padding: theme.spacing(2),
    '& .MuiInputBase-root': {
        background: theme.palette.background.default
    }
}));

export const StyledSubmit = styled('div')(({ theme }) => ({
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(2),
    borderTop: `1px solid ${theme.palette.grey[300]}`,
}))

export const StyledTitle = styled(Typography)(({ theme }) => ({
    width: '100%',
    fontSize: 16,
    background: theme.palette.primary.main,
    padding: theme.spacing(3, 7),
    color: theme.palette.primary.contrastText
}))