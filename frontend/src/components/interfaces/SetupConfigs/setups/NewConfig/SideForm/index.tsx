/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {SideContent, StyledForm, StyledSubmit, StyledTitle} from "./index.styles.tsx";
import {FC, useState} from "react";
import {Button} from "@mui/material";
import SimulationConfigForm from "./SimulationConfigForm.tsx";
import modifyProperty from "../../../../../../utils/modifyProperty.ts";
import {
    DynamicSelectorProvider
} from "../../../../../commonComponents/DynamicInputs/DynamicSelector/SelectorContext.tsx";

interface SideFormProps {
    title: string;
    formData: any;
    formStructure: object;
    onSubmit?(formData: object): void;
    onChange?(formData: object): void;
}

const SideForm: FC<SideFormProps> = (props) => {
    const { title , formData, onSubmit, onChange } = props
    const [formState, setFormState] = useState(formData)

    const handleChange = (name: string, value: any)=> {
        const newState = modifyProperty(formState, name, value)
        setFormState(newState)
        if (onChange) onChange(newState)
    }

    const handleSubmit = ()=> {
        if (onSubmit) onSubmit(formState)
    }

    return (
        <DynamicSelectorProvider>
            <SideContent>
                <StyledTitle>{title}</StyledTitle>
                <StyledForm>
                    <SimulationConfigForm
                        key={'SimConfig'}
                        onChange={(val: any)=> handleChange('simulationConfig', val)}
                        values={formState.simulationConfig}
                    />
                </StyledForm>
                <StyledSubmit>
                    <Button size={'large'} color={'primary'} variant={"outlined"} onClick={handleSubmit}>Cancel</Button>
                    <Button size={'large'} color={'primary'} variant={"contained"} onClick={handleSubmit}>Save</Button>
                </StyledSubmit>
            </SideContent>
        </DynamicSelectorProvider>
    )
}

export default SideForm