/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {PATHS} from "../../index.d";

interface tabNamesProps {
    name: string,
    pathTo: PATHS,
    tooltipText?: string,
    disabled?: boolean
}

export const tabNames: tabNamesProps[] = [
    {
        name: "Homepage",
        pathTo: PATHS.HOME
    },
    {
        name: "System Editor",
        pathTo: PATHS.SYSTEM_EDITOR,
    },
    {
        name: "Pre",
        pathTo: PATHS.SETUP_CONFIGS,
        tooltipText: "Prepare the simulations configurations"
    },
    {
        name: "Run",
        pathTo: PATHS.SIMULATION_MANAGER,
        tooltipText: "Run the simulations"
    },
    {
        name: "Monitor",
        pathTo: PATHS.SIMULATION_REPORT,
        tooltipText: "Feature not yet available",
        // tooltipText: "Simulation Report",
        disabled: true
    },
    {
        name: "Post",
        pathTo: PATHS.POST_PROCESSING,
        tooltipText: "Feature not yet available",
        // tooltipText: "Post-Processing",
        disabled: true
    },
    {
        name: "Global-setup",
        pathTo: PATHS.GLOBAL_SETUP
    }
];