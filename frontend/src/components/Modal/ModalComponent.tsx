/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { Box, Button, LinearProgress, Modal, Typography, linearProgressClasses, useTheme } from '@mui/material';
import { useGlobalContext } from '../../context/ContextProvider';
import { IGenericRecord, Variant } from  '../../index.d';
import { ModalStyled } from './modalComponent.styles.ts';
import FolderOpenOutlinedIcon from '@mui/icons-material/FolderOpenOutlined';

const ModalComponent = () => {
    const { modal, setModal, progress, setProgress } = useGlobalContext();
    const theme = useTheme();
    const handleClose = (_: object = {}) => {
        setModal({
            title: "",
            active: false,
            description: "",
            buttons: [],
            loadingBar: false,
            size: 'small'
        });
        setProgress(0);
    }
    const handleAction = (action: (props?: IGenericRecord) => void) => {
        action();
        handleClose();
    }
    return (
        <Modal
            open={modal.active}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            data-testid={`test-id-modal-${modal.title}`}
        >
            <ModalStyled size={modal.size}>
                <Box sx={{ background: theme.palette.primary.main, color: theme.palette.common.white, padding: '10px 0px 10px 20px', position: 'relative', borderTopLeftRadius: theme.spacing(1), borderTopRightRadius: theme.spacing(1) }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {modal.title}
                    </Typography>
                    <Typography onClick={handleClose} sx={{ position: 'absolute', right: '20px', fontSize: '1.6rem', bottom: '4px', cursor: 'pointer' }}>
                        &times;
                    </Typography>
                </Box>

                {modal?.descriptionTitle ?
                    <Box sx={{ display: 'flex', flexDirection: 'row', paddingX: 5, paddingTop: 5, flex: 1 }}>
                        { modal.icon && <modal.icon color={modal?.iconVariant} sx={{ width: theme.spacing(16), height: theme.spacing(16) }} /> }
                        <Box sx={{ display: "flex", flexDirection: 'column', marginX: 5, gap: 2 }}>
                            <Typography id="modal-modal-title-description" data-testid={`test-id-modal-title-description-${modal.title}`} variant='h5'>
                                {modal?.descriptionTitle}
                            </Typography>
                            <Typography id="modal-modal-description" data-testid={`test-id-modal-description-${modal.title}`}>
                                {modal.description}
                            </Typography>
                            {modal.component && modal.component}
                        </Box>
                    </Box>
                    :
                    <Box sx={{ marginTop: 5, marginX: 5, flex: 1 }}>
                        <Typography id="modal-modal-description">
                            {modal.description}
                        </Typography>
                        {modal.component && modal.component}
                    </Box>}

                {modal.loadingBar &&
                    <Box sx={{ marginTop: 5, marginX: 5 }}>
                        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'space-between', paddingBottom: 2 }}>
                            <i style={{ width: theme.spacing(8), height: theme.spacing(8) }} className="icon-mask primary file-cog"></i>
                            <FolderOpenOutlinedIcon sx={{ width: theme.spacing(8), height: theme.spacing(8) }} color='primary' />
                        </Box>
                        <LinearProgress sx={{
                            width: '100%', borderRadius: 5, height: 10, [`& .${linearProgressClasses.bar}`]: {
                                borderRadius: 5,
                            }
                        }} variant="determinate" value={progress} />
                    </Box>}

                <Box display='flex' gap={3} alignSelf='flex-end' padding={5}>
                    {modal.buttons.map((button) => (
                        <Button disabled={button.disabled} data-testid={button?.testId} key={button.title} color='primary' variant={button.variant ? button.variant : Variant.contained} onClick={(button.action ? () => button.action && handleAction(button.action) : handleClose)}>{button.title}</Button>
                    ))}
                </Box>
            </ModalStyled>
        </Modal>
    )
}

export default ModalComponent;
