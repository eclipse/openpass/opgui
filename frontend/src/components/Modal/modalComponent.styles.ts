/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import { Box, BoxProps } from '@mui/material';

interface ModalStyledProps {
    size?: string;
}

export const ModalStyled = styled(Box)<BoxProps & ModalStyledProps>(({ theme, size = 'medium' }) => ({
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: theme.spacing(150),
    height: size === 'small' ? theme.spacing(57) : size === 'medium' ? theme.spacing(102) : theme.spacing(122),
    background: theme.palette.common.white,
    bgcolor: 'background.paper',
    borderRadius: theme.spacing(1)
}));
