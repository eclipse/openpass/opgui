/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {OutlinedInput} from "@mui/material";

export const StyledOutlinedInput = styled(OutlinedInput)(({ theme }) => ({
    background: '#fff',
    marginLeft: 2,
    marginBottom: theme.spacing(1),
    outline: 'none !important',
    marginTop: '5px',
    padding: 0,
    width: '120px',
    '&.Mui-focused': {
        outline: 'none !important'
    },
    'input': {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: 10,
        paddingRight: 5
    },
    borderRadius: theme.spacing(1)
}));