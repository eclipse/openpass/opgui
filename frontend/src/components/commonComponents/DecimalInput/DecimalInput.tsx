/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { ChangeEvent, FC, useState } from "react";
import { StyledOutlinedInput } from "./DecimalInput.styles.ts";
import {InputProps} from "@mui/material";

interface DecimalInputProps extends InputProps{
    initialValue: number;
    onChange(ev: ChangeEvent<HTMLInputElement>): void;
}

const DecimalInput:FC<DecimalInputProps> = (props) => {
    const { initialValue, onChange } = props
    const [value, setValue] = useState<string | number>(initialValue)

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value: newValue } = event.target;
        const regex = /^-?\d*[.,]?\d*$/; // Regex to validate number

        // If the new value is empty or matches the regex, call the callback
        if (newValue === '' || regex.test(newValue)) {
            event.target.value = event.target.value.replaceAll(',','.')
            setValue(newValue);
            if (onChange) onChange(event)
        }
    };

    return(
        <StyledOutlinedInput
            {...props}
            type={'text'}
            value={value}
            onChange={handleChange}
            inputProps={{
                step: '.1',
            }}
            />
    )
}

export default DecimalInput