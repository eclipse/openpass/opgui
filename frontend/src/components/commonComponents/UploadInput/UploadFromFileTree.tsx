/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import LoadingButton from '@mui/lab/LoadingButton';
import {ReactNode, useCallback, useEffect, useMemo, useState} from 'react';
import {IGenericRecord, INPUT_FILE, Variant} from '../../../index.d';
import {DivGrouped, InputFile} from '../../../styles/general.styles';
import {useGlobalContext} from '../../../context/ContextProvider';
import {deleteInformation, getFileTreeApi, verifyPath} from '../../../utils/api.util';
import FileSystemTree from '../fileSystemTree/FileSystemTree.tsx';
import {FileTreeExtended, TreeNode} from "opGUI-client-frontend";
import {Autocomplete, useTheme} from "@mui/material";
import {Box} from "@mui/system";

interface Props {
    route: string;
    extension?: string;
    basePath?: string;
    handleFiles: (name: string, path: string, index?: number) => void;
    index?: number;
    name: string;
    styles?: IGenericRecord;
    error?: boolean;
    type?: INPUT_FILE;
    manualEdit?: boolean;
    buttonDisabled?: boolean;
    hideInputPath?: boolean;
    buttonLabel?: string;
    endAdornment?: ReactNode
}

const UploadFromFileTree = (props: Props) => {
    const {
        route,
        handleFiles,
        index,
        name,
        styles,
        error = false,
        type = INPUT_FILE.dir,
        buttonDisabled = false,
        manualEdit = false,
        extension = 'xml',
        basePath = 'workspace',
        hideInputPath,
        buttonLabel = 'Browse',
        endAdornment
    } = props
    const { modal, setModal } = useGlobalContext();
    const [isLoadingFileTree, setIsLoadingFileTree] = useState<boolean>(false);
    const [selectedPath, setSelectedPath] = useState<string>(route);
    const [fileTreeData, setFileTreeData] = useState<FileTreeExtended | undefined>()
    const theme = useTheme()

    const autoCompletePaths = useMemo(()=> {
        if (fileTreeData){
            const paths:string[] = []
            const addNodePath = (currentPath: string, treeNode: TreeNode, isFirst=false) => {
                const path = isFirst ? currentPath : `${currentPath}/${treeNode?.name}`
                const shouldAdd = INPUT_FILE.dir === type || path.endsWith(`.${extension}`)
                if (shouldAdd)
                    paths.push(path)
                treeNode?.children?.forEach((child: TreeNode) => {
                    addNodePath(path, child)
                })
            }
            addNodePath(fileTreeData.basePath, fileTreeData.fileSystemTree, true)
            return paths
        }
        else {
            return []
        }
    }, [fileTreeData])

    const deleteDataFolder = (path: string) => {
        setModal({
            ...modal,
            active: true,
            descriptionTitle: "",
            title: 'Data Found On Folder',
            description: `Information was found inside ${path} folder, would you like to keep it or delete it?.`,
            buttons: [
                {
                    title: 'Keep It',
                    action: () => handleFiles(name, path, index!),
                    variant: Variant.outlined,
                    testId: 'test-id-modalbutton-keepit'
                },
                {
                    title: 'Delete Information',
                    action: async () => {
                        await deleteInformation(path);
                        handleFiles(name, path, index!);
                    },
                    testId: 'test-id-modalbutton-delete-information'
                },
            ],
        });
    }

    const checkFolderContent = useCallback(async (path: string) => {
        const response = await verifyPath(path, true);
        if ('error' in response){
            return
        }
        if (name === 'results' && !response?.empty) {
            deleteDataFolder(response?.realPath || '');
            return;
        }
        handleFiles(name, response?.realPath || '', index!);
        if (response?.realPath)
            setSelectedPath(response.realPath)
        return;
    }, [handleFiles])

    const getFileTreeData = useCallback(async (isFolder: boolean) => {
        const requestParameters = {
            "fileTreeRequest": {
                "basePathGeneric": basePath,
                "isFolder": isFolder,
                "extension": extension
            }
        }
        const data = await getFileTreeApi(requestParameters);
        return data;
    }, [basePath, extension])

    useEffect(()=> {
        if (manualEdit)
            getFileTreeData(type === INPUT_FILE.dir).then((res) => {
                if (!('error' in res))
                    setFileTreeData(res)
            })
    }, [type, getFileTreeData])

    useEffect(() => {
        setSelectedPath(route)
    }, [route]);

    const onManualEdit = (path: string) => {
        setSelectedPath(path)
        checkFolderContent(path)
    }

    const handleClick = async () => {
        setIsLoadingFileTree(true);
        const isFolder = type === INPUT_FILE.dir;
        const fileTreeDataNew = await getFileTreeData(isFolder)
            .then((res: any) => {
                setIsLoadingFileTree(false);
                return res;
            });
        setFileTreeData(fileTreeDataNew)
        try {
            let modalPath = '';

            setModal({
                ...modal,
                active: true,
                loadingBar: false,
                title: type === INPUT_FILE.dir ? 'Select Folder' : 'Select File',
                size: 'medium',
                component: <FileSystemTree
                    data={fileTreeDataNew.fileSystemTree}
                    onSelectFolder={((path: string) => modalPath =`${fileTreeDataNew?.basePath}${path}`)}
                    type={type}
                />,
                buttons: [
                    {
                        title: 'Cancel',
                        variant: Variant.outlined,
                        testId: 'file-tree-cancel'
                    },
                    {
                        title: 'Select',
                        variant: Variant.contained,
                        action: () => {
                            setSelectedPath(modalPath);
                            checkFolderContent(modalPath);
                        },
                        testId: 'file-tree-select'
                    }
                ]
            });
            // return;
        } catch (error) {
            console.log(error);
        }
    }
    return (
        <DivGrouped sx={styles}>
            {!hideInputPath &&
                <Box flexDirection={'row'} display={'flex'} width={'100%'} marginRight={theme.spacing(4)}>
                    <Autocomplete
                        disabled={buttonDisabled || !manualEdit}
                        sx={{width: '100%'}}
                        value={selectedPath}
                        options={autoCompletePaths}
                        isOptionEqualToValue={(option, value) => option === value}
                        size={'small'}
                        onChange={(_ev , value, reason )=> manualEdit && reason === 'selectOption' && onManualEdit(value || '')}
                               renderInput={(params) =>
                                       <InputFile
                                           {...params}
                                           data-testid={`test-id-input-${name}`}
                                           error={error}
                                           label={type === INPUT_FILE.dir ? 'Load Folder' : 'Load File'}
                                       />}
                    />
                    {endAdornment}
                </Box>
            }
            <LoadingButton loading={isLoadingFileTree} color='primary' variant='contained' disabled={buttonDisabled} data-testid={`test-id-input-button-${name}`} onClick={handleClick}>{buttonLabel}</LoadingButton>
        </DivGrouped>
    )
}

export default UploadFromFileTree;