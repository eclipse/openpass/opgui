/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { useState, useEffect } from 'react';
import FolderTree, {NodeData} from 'react-folder-tree';
import 'react-folder-tree/dist/style.css';
import {StyledBox, StyledTextField} from './fileSystemTree.styles.ts';
import {INPUT_FILE} from "../../../index.d";

type Checked = 0 | 0.5 | 1;

interface TreeNode {
  name: string;
  // id?: Number;
  checked?: Checked;
  isOpen?: boolean;
  children?: TreeNode[];
}

interface FileSystemTreeProps {
  data: TreeNode;
  onSelectFolder: (path: string) => void;
  type?: INPUT_FILE;
}

const FileSystemTree = (props: FileSystemTreeProps) => {
  const {
    data,
    onSelectFolder,
    type = INPUT_FILE.dir
  } = props
  const [fileTreeData, setFileTreeData] = useState<TreeNode>(data);
  const [selectedFile, setSelectedFile] = useState<string>('');

  useEffect(() => {
    const newData = {...data};
    const checkChild = (item: any) => {
      [...item].map((child: any, index: number) => {
        if (child.name === '' && child.children === undefined) {
          item.splice(index, 1);
        } else if (child.children?.length > 0) {
          checkChild(child.children);
        }
      });
      return item;
    }
    const response = newData && newData.children && checkChild(newData.children);
    setFileTreeData({...newData, children: response});
  }, []);

  const getFilePath = (pathArr: any[]) => {
    let filePath = '';
    let dataLevel = data?.children;

    for (let i = 0; i < pathArr.length; i++) {
      const pathIndex = pathArr[i];
      if (dataLevel && dataLevel.length > 0) {
        filePath = filePath + '/' + dataLevel[pathIndex].name;
        if (i !== pathArr.length - 1) {
          dataLevel = dataLevel[pathIndex].children;
        }
      }
    }
    return filePath;
  }

  const handleNameClick = (e: {
    defaultOnClick: () => void;
    nodeData: NodeData;
  }) => {
    const nodeData = e.nodeData;
    const fileTreePath = getFilePath(nodeData.path);
    const isFile = nodeData.isOpen === undefined;

    if ((!isFile && type === INPUT_FILE.dir) || (isFile && type === INPUT_FILE.file)) {
      setSelectedFile(`${data.name}${fileTreePath}`);
      onSelectFolder(fileTreePath);
    }
  }

  return (
      fileTreeData ? <>
        <StyledBox data-testid={"folder-tree-container"} isFolder={(type === INPUT_FILE.dir)}>
          <FolderTree
              data={fileTreeData}
              initOpenStatus="closed"
              showCheckbox={false}
              onNameClick={handleNameClick}
              readOnly
          />
        </StyledBox>
        <StyledTextField
            size="small"
            label={type === INPUT_FILE.dir ? "Select folder" : 'Select File'}
            focused
            value={selectedFile}
            disabled
        />
      </> : <></>
  );
};

export default FileSystemTree;
