/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { styled } from '@mui/material/styles';
import { Box, TextField } from '@mui/material';

type StyledBoxProps = {
    isFolder: boolean
}

export const StyledBox = styled(Box)<StyledBoxProps>(({ theme, isFolder }) => ({
    '.FolderTree': {
        '.TreeNode': {
            '&:active': {
                backgroundColor: theme.palette.primary.light,
            },
        },
    },
    overflow: 'scroll',
    overflowX: 'hidden',
    height: theme.spacing(57),
    borderBottom: `1px solid ${theme.palette.greyScale.main}`,
    '.editableNameContainer:hover': {
        '.displayName': {
            color: isFolder ? '#4d94ff' : 'black',
            cursor: isFolder ? 'not-allowed' : 'pointer'
        }
    },
    '.caretContainer ~ .editableNameContainer:hover': {
        '.displayName': {
            color: isFolder ? '#4d94ff' : 'black',
            cursor: isFolder ? 'pointer' : 'not-allowed'
        }
    }
}));

export const StyledTextField = styled(TextField)(({ theme }) => ({
    width: '100%',
    border: 'none',
    marginTop: '8px',
    input: {
        background: '#fff',
        padding: '2px 0 2px 5px',
        height: '30px',
        '&::before': {
            all: 'unset'
        },

    },
    '.MuiInputBase-root': {
        borderRadius: 0,
        '.MuiOutlinedInput-notchedOutline': {
            borderColor: theme.palette.greyScale.medium,
            WebkitTextFillColor: '#fff'
        },
        '.Mui-disabled': {
            opacity: 1,
            WebkitTextFillColor: theme.palette.secondary.main,
        }
    }
}));