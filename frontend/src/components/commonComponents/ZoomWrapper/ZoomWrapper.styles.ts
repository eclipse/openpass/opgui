/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {ZoomWrapperProps} from "./ZoomWrapper.tsx";

export const ZoomManager = styled('div')(({ zoomScale = 1 } : ZoomWrapperProps) => ({
    width: `calc(100% * ${1 / zoomScale})`,
    height: `calc(100% * ${1 / zoomScale})`,
    transformOrigin: 'top left',
    transform: `scale(${zoomScale})`,
    overflow: 'scroll'
}))

export const MainWrapper = styled('div')(({ width = '100%', height = '100%'}: ZoomWrapperProps) => ({
    width,
    height,
    transition: 'all 0.5s'
}))