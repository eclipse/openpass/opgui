/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {FC, ReactNode, useEffect} from "react";
import {ZoomManager, MainWrapper} from "./ZoomWrapper.styles.ts";

export type ZoomWrapperProps = {
    zoomScale?: number,
    width?: string | number,
    height?: string | number,
    children: ReactNode,
    testId?: string,
}

const ZoomWrapper: FC<ZoomWrapperProps> = ({ children, ...props })  => {
    const { zoomScale = 0.5, width, height, testId } = props

    useEffect(() => {

    }, [zoomScale]);

    return (
        <MainWrapper width={width} height={height} data-testid={testId} {...props}>
            <ZoomManager zoomScale={zoomScale} data-testid={`${testId}-manager`}>
                {children}
            </ZoomManager>
        </MainWrapper>
    )
}

export default ZoomWrapper