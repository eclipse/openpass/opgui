import { createContext, useContext, useEffect, useState } from 'react';
import {getValueList} from "../../../../utils/api.util.ts";

interface IFetchList {
    fetchData: {
        [param: string]: string[]
    };
    fetchListData?(listName: string): Promise<void>;
}

// Create the Context
const SelectorContext = createContext<IFetchList>({
    fetchData: {},
});

// Custom hook to use the data from the context
export const useListData = (listName: string) => {
    const context = useContext(SelectorContext);

    useEffect(() => {
        if (context?.fetchListData && listName)
            context?.fetchListData(listName)
    }, [context?.fetchListData, listName]);
    return context.fetchData[listName] || []
}


// Data provider component (wrap your app with this)
export const DynamicSelectorProvider = ({ children }: { children: any }) => {
    const [fetchData, setFetchData] = useState({})

    const fetchListData = async (listName: string)=> {
        if (listName in fetchData){
            return
        }
        const data = await getValueList(listName)
        if ('error' in data){
            setFetchData((prevState) => {
                return ({
                    ...prevState,
                    [listName]: [],
                })
            })
            return
        }
        setFetchData((prevState) => {
            return ({
                ...prevState,
                [listName]: data.listValues,
            })
        })
    }

    const value: IFetchList = {
        fetchData,
        fetchListData,
    }

    return <SelectorContext.Provider value={value}> {children} </SelectorContext.Provider>;
}