import {IOnChangeValidation} from "./interfaces.ts";


export const sumMustBeValidation = (sum: number, propertyPath: string):IOnChangeValidation => (name, value) => {
    if (typeof value  === 'object' && 'length' in value){
        const array = value as []
        const totalSum = array.reduce((accum, currentValue) => {
            let val = 0;
            if (propertyPath === ''){
                val = currentValue
            }
            else if (propertyPath in currentValue){
                val = currentValue[propertyPath]
            }
            return accum + val
        }, 0)
        if (Math.abs(totalSum - sum) < 0.0001){
            return {
                error: false
            }
        }
        return {
            error: true,
            title: name,
            message: `All values in ${propertyPath} don't add up to exactly 1. Current sum: ${totalSum}`
        }
    }
    if (value === undefined)
        return {
            error: true,
            title: name,
            message: `All values in ${propertyPath} don't add up to exactly 1. Current sum: 0`
        }
    return {
        error: false
    }
}

export const cantRepeatValidation = (propertyPath: string):IOnChangeValidation => (name, value) => {
    if (typeof value  === 'object' && 'length' in value){
        const array = value as []
        const valuesRepeated: never[] = []
        array.forEach((val, i) => {
            const lookForValue = propertyPath === '' ? val : val[propertyPath]
            const similar = array.find((checkVal, checkIndex) => {
                const checkForValue = propertyPath === '' ? checkVal : checkVal[propertyPath]
                return checkForValue === lookForValue && i !== checkIndex
            })
            if (similar && !valuesRepeated.includes(lookForValue)) valuesRepeated.push(lookForValue)
        })
        if (valuesRepeated.length > 0){
            return {
                error: true,
                title: name,
                message: `Property "${propertyPath}" has repeated values: ${valuesRepeated.join(', ')}`
            }
        }
    }
    return {
        error: false
    }
}