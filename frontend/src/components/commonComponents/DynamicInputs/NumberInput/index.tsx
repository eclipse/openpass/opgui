
/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { Box } from "@mui/material";
import {StyledNumberField} from "./index.styles.tsx";
import {ChangeEvent} from "react";
import {IDynamicInputProps} from "../interfaces.ts";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";

const NumberInput = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, onChange, ...other } = props
    const testId = getTestIdFromName(name)

    const handleChange = ({ target } : ChangeEvent<HTMLInputElement>) => {
        onChange && onChange(name, target.value)
    }

    return (
        <Box data-testid={testId} {...other} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
            <StyledLabel>{label}</StyledLabel>
            <StyledNumberField {...inputProps} data-testid={`${testId}-input`} size={'small'} value={value} onChange={handleChange}/>
        </Box>
    )
}

export default NumberInput