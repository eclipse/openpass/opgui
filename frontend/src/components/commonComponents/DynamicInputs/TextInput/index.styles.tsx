import {styled} from "@mui/material/styles";
import {OutlinedInput } from "@mui/material";

export const StyledTextField = styled(OutlinedInput)(() => ({
    height: 25,
    width: 150,
}));