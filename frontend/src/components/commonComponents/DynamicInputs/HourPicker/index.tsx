
/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { Box, MenuItem, SelectChangeEvent } from "@mui/material";
import { StyledSelect } from "./index.styles.tsx";
import {IDynamicInputProps} from "../interfaces.ts";
import { StyledLabel } from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";

const timeRange = Array.from({ length: 24 }, (_val, index)=> index+1)

const TimeRange = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, onChange, ...other } = props
    const testId = getTestIdFromName(name)
    const handleChange = (event: SelectChangeEvent<unknown>) => {
        const selectedValue = (event.target as HTMLInputElement).value;
        onChange && onChange(name, selectedValue)
    }

    return (
        <Box
            display={'flex'} flexDirection={'row'}
            justifyContent={'space-between'} alignItems={'center'}
            width={'100%'}
            data-testid={testId}
            {...other}
        >
            <StyledLabel>{label}</StyledLabel>
            <StyledSelect defaultValue={timeRange[0]} value={value} onChange={handleChange} {...inputProps} data-testid={`${name}-input`} >
                {timeRange.map(time => (
                    <MenuItem key={`${name}-option-${time}`} data-testid={`${name}-option-${time}`} value={time}>
                        {time}h
                    </MenuItem>
                ))}
            </StyledSelect>
        </Box>
    )
}

export default TimeRange