import {styled} from "@mui/material/styles";
import {InputLabel} from "@mui/material";

export const StyledLabel = styled(InputLabel)<{isImportant?: boolean}>(({ theme, isImportant }) => ({
    background: isImportant ? theme.palette.primary.contrastText : undefined,
    color: theme.palette.secondary.main,
    padding: theme.spacing(2, 2),
    flex: 1,
    fontWeight: isImportant ? 'bold' : 'normal',
    userSelect: 'none',
    '& .svg': {
        height: '0.6em',
        width: '0.6em'
    },
    display: 'flex',
    border: isImportant ? `1px solid ${theme.palette.grey[300]}` : '',
}));