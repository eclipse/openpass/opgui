/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {Box, Button, useTheme} from "@mui/material";
import {ReactNode, useEffect, useState} from "react";
import { StyledBox } from "./index.styles.tsx";
import {IDynamicInputProps} from "../interfaces.ts";
import DynamicInputLabel from "../DynamicInputLabel";

interface DynamicInputListProps<T> extends IDynamicInputProps {
    customAmount?: ReactNode;
    constantValues?: T[]
    value: T[];
    defaultValue: T;
    ['data-testid']?: string;
    children(props: IListChildren<T>): ReactNode
}

interface IListChildren<T> {
    values: T[];
    remove(index: number): void;
    add(): void;
}


const ListInput = <T,>(props: DynamicInputListProps<T>) => {
    const theme = useTheme()
    const { name, value, defaultValue, customAmount, error, label, children, onChange, ...rest } = props

    const [listValues, setListValues] = useState<Array<T>>(value || [])
    const [isClosed, setClosed] = useState(false)

    const amount = customAmount || listValues?.length || 0

    const addField = ()=> {
        const newValue = typeof defaultValue === 'object' ? JSON.parse(JSON.stringify(defaultValue)) : defaultValue
        const newList = [...listValues, newValue]
        setListValues(newList)
        onChange(`${name}.${listValues.length}`, newValue)
    }

    const removeField = (index: number)=> {
        listValues.splice(index, 1)
        onChange(`${name}`, [...listValues])
    }

    useEffect(()=> {
        setListValues(value || [])
    }, [value])

    return (
        <Box display={'flex'} flexDirection={'column'} width={'100%'} {...rest}>
            <Box  width={'100%'} display={'flex'} alignItems={'center'} justifyContent={'space-between'} marginBottom={theme.spacing(2)}>
                <DynamicInputLabel error={error} isClosed={isClosed} isImportant onClick={()=>setClosed(!isClosed)}>
                    {label} ({amount})
                </DynamicInputLabel>
                <Button
                    size={'small'} color={'primary'}
                    variant={'contained'} sx={{ marginLeft: theme.spacing(4)}}
                    onClick={addField} data-testid={`${rest['data-testid']}-add`}>
                    Add
                </Button>
            </Box>

            {!isClosed && children && (
                <StyledBox
                    width={`100%`} display={'flex'}
                    flexDirection={'row'} flexWrap={'wrap'}
                    alignItems={'center'}
                    hasContent={!isClosed}
                    justifyContent={'center'}
                    marginBottom={theme.spacing(2)}
                >
                    {children({ values: listValues, remove: removeField, add: addField })}
                </StyledBox>
            )}
        </Box>
    )
}

export default ListInput