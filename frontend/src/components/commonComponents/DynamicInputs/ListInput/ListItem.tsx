import {Button, useTheme} from "@mui/material";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import {StyledItem} from "./index.styles.tsx";
import {FC, ReactNode} from "react";

interface ListItemProps {
    index: number;
    columnsCount?: number;
    onRemove?(index: number): void
    children: ReactNode;
}

const ListItem: FC<ListItemProps> = (props) => {
    const theme = useTheme()
    const { index, columnsCount, onRemove, children } = props

    return (
        <StyledItem
            sx={{background: index % 2 === 0 ? theme.palette.common.white : `${theme.palette.primary.accent1}20`}}
            width={`calc(${100 / (columnsCount || 1)}% - ${theme.spacing((columnsCount || 1 - 1))})`}
        >
            {children}
            <Button size={'small'} variant={'outlined'} color={'primary'}
                    sx={{
                        padding: 0.5,
                        minWidth: 24,
                        marginLeft: theme.spacing(4),
                        opacity: onRemove ? 1: 0,
                    }}
                    disabled={!onRemove}
                    onClick={()=> (onRemove && onRemove(index))}>
                <DeleteOutlinedIcon/>
            </Button>
        </StyledItem>
    )
}

export default ListItem