/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {styled} from "@mui/material/styles";
import {Box} from "@mui/material";

export const StyledItem = styled(Box)<{ isFirst?: boolean, isItem?: boolean }>((
    { theme, isFirst, isItem }) => ({
    display: 'flex',
    alignItems: 'self-start',
    position: 'relative',
    margin: theme.spacing(isItem ? -.25 : .75, 0),
    paddingLeft: !isFirst ? theme.spacing(4) : 0,
}));

export const StyledBox = styled(Box)<{ isItem?: boolean }>(({ theme, isItem }) => ({
    display: 'flex',
    border: isItem ? 'none' : `1px solid ${theme.palette.grey[300]}`,
    borderTop: 'none',
    padding: isItem ? 0 : theme.spacing(2, 6),
}));