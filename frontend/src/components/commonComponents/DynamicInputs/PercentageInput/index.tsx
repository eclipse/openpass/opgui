
/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { Box, InputAdornment } from "@mui/material";
import {StyledTextField} from "./index.styles.tsx";
import {ChangeEvent, useEffect, useState} from "react";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";
import {IDynamicInputProps} from "../interfaces.ts";

const PercentageInputs = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, onChange, ...other } = props
    const [val, setVal] = useState(value || 0)
    const testId = getTestIdFromName(name)

    useEffect(()=> {
        const parsedValue = Number((value*100).toFixed(2))
        setVal(parsedValue)
    }, [value])

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value: newValue } = event.target;
        const validEditRegex = /^-?\d{0,3}([.,]\d{0,2})*$/; // Regex to validate number
        if (validEditRegex.test(newValue)){
            setVal(newValue)

            // if edit was valid we check if the format is valid for numbers
            const regex = /^-?\d{1,3}([.,]\d{1,2})*$/; // Regex to validate number
            // If the new value is empty or matches the regex, call the callback
            if (newValue === '' || regex.test(newValue)) {
                event.target.value = event.target.value.replaceAll(',','.')
                const parsedValue = Number((Number(newValue) / 100).toFixed(4))
                onChange && onChange(name, parsedValue)
            }
        }
    };

    return (
        <Box data-testid={testId} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} {...other}>
            <StyledLabel>{label}</StyledLabel>
            <StyledTextField {...inputProps} data-testid={`${testId}-input`} size={'small'} value={val} onChange={handleChange} endAdornment={<InputAdornment position="end">%</InputAdornment>}/>
        </Box>
    )
}

export default PercentageInputs