
import { Box } from "@mui/material";
import {StyledTextField} from "./index.styles.tsx";
import {ChangeEvent, useEffect, useState} from "react";
import {IDynamicInputProps} from "../interfaces.ts";
import {StyledLabel} from "../DynamicInputLabel/index.styles.tsx";
import getTestIdFromName from "../../../../utils/tests/getTestIdFromName.ts";

const IntInput = (props: IDynamicInputProps)=> {
    const { inputProps, name, value, label, isImportant, onChange, ...other } = props
    const [val, setVal] = useState(value || 0)
    const testId = getTestIdFromName(name)

    useEffect(()=> {
        setVal(value)
    }, [value])

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value: newValue } = event.target;
        const validEditRegex = /^-?\d+$/; // Regex to validate number
        if (validEditRegex.test(newValue)){
            setVal(newValue)
            onChange && onChange(name, newValue)
        }
    };

    return (
        <Box data-testid={testId} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} {...other}>
            <StyledLabel isImportant={isImportant}>{label}</StyledLabel>
            <StyledTextField {...inputProps} data-testId={`${testId}-input`}  size={'small'} value={val} onChange={handleChange} />
        </Box>
    )
}

export default IntInput