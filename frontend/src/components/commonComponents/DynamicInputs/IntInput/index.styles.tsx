import {styled} from "@mui/material/styles";
import {OutlinedInput } from "@mui/material";

export const StyledTextField = styled(OutlinedInput)(({ theme }) => ({
    height: 25,
    width: 80,
    marginLeft: theme.spacing(2),
}));