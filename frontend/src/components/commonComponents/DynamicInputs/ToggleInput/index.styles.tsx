import {styled} from "@mui/material/styles";
import { Switch } from "@mui/material";

export const StyledToggleInput = styled(Switch)<{width?: string | number}>(({ theme, width }) => ({
    margin: theme.spacing(-1, 0),
    width,
}));