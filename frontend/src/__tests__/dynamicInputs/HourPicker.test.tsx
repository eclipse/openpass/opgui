
import {screen} from "@testing-library/react";
import HourPicker from "../../components/commonComponents/DynamicInputs/HourPicker";
import {IDynamicInputProps} from "../../components/commonComponents/DynamicInputs/interfaces.ts";
import getTestIdFromName from "../../utils/tests/getTestIdFromName.ts";
import {RenderInput} from "../../utils/tests/RenderInputTest.tsx";

const name = 'hour'
const testId = getTestIdFromName(name)

describe('Testing Hour Picker', () => {

    it('Should not break with empty minimum props', () => {
        const props: IDynamicInputProps = {
            name,
            onChange: (_inputName: string, _value: any) => {
            },
            value: '5',
            label: '',
        }
        RenderInput(props, HourPicker);
        const input = screen.getByTestId(testId);

        expect(input).toBeInTheDocument();
    });

    it('Should start with initial value set', () => {
        const props: IDynamicInputProps = {
            name,
            onChange: (_inputName: string, _value: any) => {},
            value: '5',
            label: '',
        }
        RenderInput(props, HourPicker);
        const input = screen.getByTestId(testId);
        expect(input).toBeInTheDocument();
        const selector = input.querySelector('input')
        expect(selector).toBeInTheDocument();
        expect(selector).toHaveValue('5')
    });
})
