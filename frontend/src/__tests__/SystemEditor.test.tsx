/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { screen } from "@testing-library/dom";
import {cleanup, fireEvent, waitFor, within} from "@testing-library/react";
import { renderWithProviders } from "../utils/test.utils";
import SystemEditor from "../components/interfaces/SystemEditor/SystemEditor";
import userEvent from '@testing-library/user-event'
import { act } from "react-dom/test-utils";
import fetchMock from "jest-fetch-mock";
import components from "../utils/fakeComponentData.json";
import { FileTreeExtended, ComponentUITypeEnum} from "opGUI-client-frontend";
import handleResponse from "../utils/tests/handleResponse.ts";
import setupMocksCalls, {CallHandler} from "../utils/tests/setupMocksCalls.ts";
import defaultFileTreeResponse from "../utils/tests/mockResponses/defaultFileTreeResponse.ts";
import getDefaultVerifyPathResponse from "../utils/tests/mockResponses/getDefaultVerifyPathResponse.ts";

const openFileTreeSystemButton = 'test-id-input-button-system'

interface SetupMocksParams {
    system: any
    fileTreeResponse?: FileTreeExtended
}

const setupMocks = ({ fileTreeResponse = defaultFileTreeResponse, system }: SetupMocksParams) => {
    const setupCalls: CallHandler[] = [
        {
            url: `api/getFileTree`,
            response: handleResponse(fileTreeResponse)
        },
        {
            url: `api/components`,
            response: handleResponse(components)
        },
        {
            url: `api/verify-path`,
            response: handleResponse(getDefaultVerifyPathResponse(`os/workspace/path/to/folder/test${system.id}`))
        },
        {
            url: `api/getSystem?path=os%2Fworkspace%2Fpath%2Fto%2Ffolder%2Ftest${system.id}`,
            response: handleResponse(system)
        },
        {
            url: `api/deleteSystem?path=os%2Fworkspace%2Fpath%2Fto%2Ffolder%2Ftest`,
            response: handleResponse({
                "response": "System correctly deleted"
            })
        },
    ]
    setupMocksCalls(setupCalls)
}

const renderSystemEditor = ()=> (renderWithProviders(<SystemEditor initialZoomAmount={1}/>))

const SelectSystemFirst = async ({ fileTreeResponse = defaultFileTreeResponse, system }: SetupMocksParams) => {
    setupMocks({ fileTreeResponse, system })
    await renderSystemEditor()
    await SelectAnotherSystem({ fileTreeResponse, system })
}

const SelectAnotherSystem = async ({ fileTreeResponse = defaultFileTreeResponse, system }: SetupMocksParams) => {
    setupMocks({ fileTreeResponse, system })
    const loadSystem = screen.getByTestId('test-id-input-button-system');
    await act(() => {
        fireEvent.click(loadSystem);
    });
    await waitFor(() => expect(screen.getByTestId('test-id-modal-Select File')).toBeInTheDocument())
    const modal = screen.getByTestId('test-id-modal-Select File')
    expect(modal).toBeInTheDocument()
    const selectFileButton = screen.getByTestId('file-tree-select')
    await act(() => {
        fireEvent.click(selectFileButton);
    });
    expect(modal).not.toBeInTheDocument();
}

jest.mock('react-xarrows', () => {
    return {
        __esModule: true,
        default: () => <span />,
        Xarrow: () => <div />,
        useXarrow: () => jest.fn(),
        Xwrapper: () => <div />,
        updateXarrow: () => jest.fn()
    };
});
describe('Render Test for System Editor Component', () => {
    beforeEach(async () => {
        fetchMock.enableMocks();
        fetchMock.mockResponseOnce(JSON.stringify([]));
        await act(() => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
    });

    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('Should render the footer with its content', () => {
        const footer = screen.getByTestId('test-id-footer-systemeditor');
        const clearAllButton = screen.getByTestId('test-id-button-system-clearall');
        const exportButton = screen.getByTestId('test-id-button-system-export');
        const exportAsButton = screen.getByTestId('test-id-button-system-export-as');
        expect(footer).toBeInTheDocument();
        expect(clearAllButton).toBeInTheDocument();
        expect(exportButton).toBeInTheDocument();
        expect(exportAsButton).toBeInTheDocument();
    });
    test('Should render accordion zone', () => {
        const accordionSideBar = screen.getByTestId('test-id-box-accordion-zone');
        expect(accordionSideBar).toBeInTheDocument();
    });
    test('Should render drag and drop zone', () => {
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        expect(droppableZone).toBeInTheDocument();
    });
    test('Should render system editor info icon', async () => {
        const infoIcon = screen.getByTestId('test-id-icon-system-info');
        expect(infoIcon).toBeInTheDocument();
        userEvent.hover(infoIcon);
        const tip = await screen.findByRole('tooltip');
        expect(tip).toBeInTheDocument();
        expect(tip.textContent).toBe('Drag and drop one component to the dashboard to create and editate systems.');
    });
});

describe('Path To Components tests', () => {
    beforeEach(async () => {
        fetchMock.enableMocks();
    });
    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('Given all the components are empty accordion zone items should be empty', async () => {
        fetchMock.mockResponseOnce(
            JSON.stringify([])
        );
        await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
        const accordionComponent = screen.getByTestId('test-id-box-accordion-zone');
        expect(accordionComponent).toBeInTheDocument();
        expect(accordionComponent?.textContent).toBe('');
    });
    test('Given api call brings data when the component is loaded then accordion details should have content', async () => {
        fetchMock.mockResponseOnce(
            JSON.stringify([
                {
                    "id": 0,
                    "inputs": [
                        {
                            "cardinality": 1,
                            "id": 0,
                            "title": "Longitudinal Signal",
                            "type": "LongitudinalSignal",
                            "unit": ""
                        }
                    ],
                    "library": "ActionLongitudinalDriver",
                    "position": {
                        "x": 0,
                        "y": 0
                    },
                    "title": "ActionLongitudinalDriver",
                    "type": "Action"
                },
                {
                    "id": 1,
                    "inputs": [
                        {
                            "cardinality": 1,
                            "id": 0,
                            "title": "Desired trajectory",
                            "type": "TrajectoryEvent",
                            "unit": ""
                        }
                    ],
                    "library": "Algorithm_RouteControl",
                    "outputs": [
                        {
                            "id": 0,
                            "title": "Control",
                            "type": "ControlData",
                            "unit": ""
                        }
                    ],
                    "parameters": [
                    ],
                    "position": {
                        "x": 0,
                        "y": 0
                    },
                    "title": "Algorithm_RouteControl",
                    "type": "Algorithm"
                },
                {
                    "id": 2,
                    "library": "ParametersVehicle",
                    "outputs": [
                        {
                            "id": 1,
                            "title": "Vehicle Parameters",
                            "type": "ParametersVehicleSignal",
                            "unit": ""
                        }
                    ],
                    "position": {
                        "x": 0,
                        "y": 0
                    },
                    "title": "ParametersVehicle",
                    "type": "Sensor"
                },
            ])
        );
        await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
        const accordionActions = screen.getByTestId('test-id-accordion-Action');
        const accordionAlgorithms = screen.getByTestId('test-id-accordion-Algorithm');
        const accordionSensors = screen.getByTestId('test-id-accordion-Sensor');
        expect(accordionAlgorithms).toBeInTheDocument();
        expect(accordionSensors).toBeInTheDocument();
        expect(accordionActions).toBeInTheDocument();
        const actionsAccordionDetails = accordionActions.querySelector('.MuiAccordionDetails-root');
        const algorithmsAccordionDetails = accordionAlgorithms.querySelector('.MuiAccordionDetails-root');
        const sensorsAccordionDetails = accordionSensors.querySelector('.MuiAccordionDetails-root');
        expect(actionsAccordionDetails?.textContent).not.toBe('');
        expect(actionsAccordionDetails?.textContent).toBe('ActionLongitudinalDriver');
        expect(algorithmsAccordionDetails?.textContent).not.toBe('');
        expect(algorithmsAccordionDetails?.textContent).toBe('Algorithm_RouteControl');
        expect(sensorsAccordionDetails?.textContent).not.toBe('');
        expect(sensorsAccordionDetails?.textContent).toBe('ParametersVehicle');
    });
    test(
        'GIVEN interface is loading correctly ' +
        'WHEN the api call fails ' +
        'THEN a GET call is rejected', async () => {
            fetchMock.mockReject(Error('Bad Request'))
            await act(async () => renderWithProviders(<SystemEditor initialZoomAmount={1}/>));
            expect(fetchMock).toHaveBeenCalledTimes(1)
            expect(fetchMock).rejects.toThrowError(Error('Bad Request'))
        });
});

describe('System bar and stepper tests', () => {
    const systems =
        [
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 0,
                "priority": 0,
                "title": "Follow Trajectory",
                "file": "Follow_Trajectory.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 1,
                "priority": 1,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 2,
                "priority": 2,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 3,
                "priority": 3,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 4,
                "priority": 4,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 5,
                "priority": 5,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 6,
                "priority": 6,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
            {
                "components": [
                ],
                "connections": [
                ],
                "id": 7,
                "priority": 7,
                "title": "Copy Trajectory",
                "file": "Agent_Copy.xml"
            },
        ]
    // beforeEach(SelectSystemFirst({ system: systems[0] }));

    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('By default label should say 0 - 0 systems', async () => {
        setupMocks({ system: systems[0] })
        renderSystemEditor()
        expect(await screen.findByText(/0 - 0 Systems/i)).toBeInTheDocument();
    });
    test('By default stepper button should be disabled since no system have been loaded', async () => {
        setupMocks({ system: systems[0] })
        renderSystemEditor()
        const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        expect(stepperBackButton).toBeInTheDocument();
        expect(stepperNextButton).toBeInTheDocument();
        expect(stepperBackButton).toBeDisabled()
        expect(stepperNextButton).toBeDisabled()
    });
    test('Given User click on Load Systems Button system should load', async () => {
        await SelectSystemFirst({ system: systems[0] });
        expect(await screen.findByText(/1 - 1 Systems/i)).toBeInTheDocument();
    });
    test('Given User click on Load Systems and theres 5 or more system stepper should be enabled', async () => {
        await SelectSystemFirst({ system: systems[0] });

        const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        expect(stepperBackButton).toBeInTheDocument();
        expect(stepperNextButton).toBeInTheDocument();
        expect(stepperBackButton).toBeDisabled()
        expect(stepperNextButton).toBeDisabled()
        await SelectAnotherSystem({ system: systems[1] })
        await SelectAnotherSystem({ system: systems[2] })
        await SelectAnotherSystem({ system: systems[3] })
        await SelectAnotherSystem({ system: systems[4] })
        expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
        expect(stepperNextButton).toBeEnabled()
    });

    test('Given User click on Load Systems and theres 5 or more systems when clicking stepper should show 2-5 systems', async () => {
        await SelectSystemFirst({ system: systems[0] });
        await SelectAnotherSystem({ system: systems[1] })
        await SelectAnotherSystem({ system: systems[2] })
        await SelectAnotherSystem({ system: systems[3] })
        await SelectAnotherSystem({ system: systems[4] })
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const system = screen.getByTestId('test-id-system-1');
        expect(system).toBeInTheDocument();
        expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
        expect(stepperNextButton).toBeEnabled()
        act(() => {
            fireEvent.click(stepperNextButton);
        });
        expect(await screen.findByText(/5 - 5 Systems/i)).toBeInTheDocument();
    });

    test('Given User click on Load Systems and theres 8 systems when clicking stepper should show 8-8 systems', async () => {
        await SelectSystemFirst({ system: systems[0] });
        for (let i = 1 ; i<8 ; i++){
            await SelectAnotherSystem({ system: systems[i] })
        }
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const system = screen.getByTestId('test-id-system-1');
        expect(system).toBeInTheDocument();
        expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
        expect(stepperNextButton).toBeEnabled()
        act(() => {
            fireEvent.click(stepperNextButton);
        });
        expect(await screen.findByText(/8 - 8 Systems/i)).toBeInTheDocument();
    });

    test('Given user already advance 1 step in the stepper and theres 5 systems when clicking back should show 4 - 5 Systems again', async () => {
        await SelectSystemFirst({ system: systems[0] });
        const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        expect(stepperBackButton).toBeDisabled();
        for (let i = 1 ; i<5 ; i++){
            await SelectAnotherSystem({ system: systems[i] })
        }
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const system = screen.getByTestId('test-id-system-1');
        expect(system).toBeInTheDocument();
        expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
        expect(stepperNextButton).toBeEnabled()
        act(() => {
            fireEvent.click(stepperNextButton);
        });
        expect(await screen.findByText(/5 - 5 Systems/i)).toBeInTheDocument();
        expect(stepperBackButton).toBeEnabled();
        act(() => {
            fireEvent.click(stepperBackButton);
        });
        expect(await screen.findByText(/4 - 5 Systems/i)).toBeInTheDocument();
        expect(stepperBackButton).toBeDisabled();
    });

    test('Given user already advance 1 step in the stepper and theres 8 systems when clicking back should show 4 - 8 Systems again', async () => {
        await SelectSystemFirst({ system: systems[0] });
        const stepperBackButton = await screen.findByTestId('test-id-button-stepper-back');
        const stepperNextButton = await screen.findByTestId('test-id-button-stepper-next');
        expect(stepperBackButton).toBeDisabled();
        for (let i = 1 ; i<8 ; i++){
            await SelectAnotherSystem({ system: systems[i] })
        }
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const system = screen.getByTestId('test-id-system-1');
        expect(system).toBeInTheDocument();
        expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
        expect(stepperNextButton).toBeEnabled()
        act(() => {
            fireEvent.click(stepperNextButton);
        });
        expect(await screen.findByText(/8 - 8 Systems/i)).toBeInTheDocument();
        expect(stepperBackButton).toBeEnabled();
        act(() => {
            fireEvent.click(stepperBackButton);
        });
        expect(await screen.findByText(/4 - 8 Systems/i)).toBeInTheDocument();
        expect(stepperBackButton).toBeDisabled();
    });
    test('Given a Simulation when clicking on the chevron down icon Dropdown menu should open', async () => {
        await SelectSystemFirst({ system: systems[0] });
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
    });
    test('Given user clicks on add button should add a new system', async () => {
        await renderSystemEditor()
        const addSystemButton = screen.getByTestId('test-id-add-new-system');
        expect(addSystemButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(addSystemButton);
        });
        const systemAdded = await screen.findByText('System 1');
        expect(systemAdded).toBeInTheDocument();
    });
    test('Given user clicks on load button then on clear all and confirms no system should be shown in the screen', async () => {
        await SelectSystemFirst({ system: systems[0] });
        expect(await screen.findByText(/Follow Trajectory/i)).toBeInTheDocument();
        const buttonClearAll = screen.getByTestId('test-id-button-system-clearall');
        act(() => {
            fireEvent.click(buttonClearAll);
        });
        const buttonModalClear = screen.getByTestId('test-id-modalbutton-clearall');
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        act(() => {
            fireEvent.click(buttonModalClear);
        });
        expect(droppableZone).toBeEmptyDOMElement();
        expect(await screen.findByText(/0 - 0 Systems/i)).toBeInTheDocument();
    });
    test('Given user clicks on export button without creating or loading system should show error modal ', async () => {
        await renderSystemEditor()
        const buttonExport = screen.getByTestId('test-id-button-system-export');
        act(() => {
            fireEvent.click(buttonExport);
        });
        expect(await screen.findByText(/No System were loaded or created/i)).toBeInTheDocument();
    });
    test('Given user clicks on export button when renaming a system should show modal error ', async () => {
        await SelectSystemFirst({ system: systems[0] })
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const renameButton = screen.getByTestId('test-id-menu-item-rename');
        expect(renameButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(renameButton);
        });
        const buttonExport = screen.getByTestId('test-id-button-system-export');
        act(() => {
            fireEvent.click(buttonExport);
        });
        expect(await screen.findByText(/You are renaming a system at the moment/i)).toBeInTheDocument();
    });
    test('Given user clicks on export button everything should be ok ', async () => {
        await SelectSystemFirst({ system: systems[0] })
        const system = screen.getByTestId('test-id-system-0');
        expect(system).toHaveClass('selected');
        fetchMock.mockResponseOnce(JSON.stringify({
            "response": "Ok"
        }));
        const buttonExport = screen.getByTestId('test-id-button-system-export');
        act(() => {
            fireEvent.click(buttonExport);
        });
        expect(await screen.findByText(/Systems loaded correctly/i)).toBeInTheDocument();
    });
    test('Given user opens dropdown and click rename then rename input should show and then clicks on the confirm and the system name should change', async () => {
        await SelectSystemFirst({ system: systems[0] })
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const renameButton = screen.getByTestId('test-id-menu-item-rename');
        expect(renameButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(renameButton);
        });
        const renameField = screen.getByTestId('test-id-rename-field');
        expect(renameField).toBeInTheDocument();
        const renameInput = renameField.querySelector('input');
        expect(renameInput).toBeInTheDocument();
        expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
        act(() => {
            fireEvent.change(renameInput!, { target: { value: 'newTitle' } });
        });
        expect(renameInput).toHaveProperty('value', 'newTitle');
        const renameConfirm = screen.getByTestId('test-id-rename-confirm');
        expect(renameConfirm).toBeInTheDocument();
        await act(() => {
            fireEvent.click(renameConfirm);
        });
        expect(await screen.findByText(/newTitle/i)).toBeInTheDocument();
    });
    test(`Given user opens dropdown and click rename then rename 
    input should show and then clicks on the confirm with empty name and should show modal with error`, async () => {
        await SelectSystemFirst({ system: systems[0] })
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const renameButton = screen.getByTestId('test-id-menu-item-rename');
        expect(renameButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(renameButton);
        });
        const renameField = screen.getByTestId('test-id-rename-field');
        expect(renameField).toBeInTheDocument();
        const renameInput = renameField.querySelector('input');
        expect(renameInput).toBeInTheDocument();
        expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
        act(() => {
            fireEvent.change(renameInput!, { target: { value: '' } });
        });
        expect(renameInput).toHaveProperty('value', '');
        const renameConfirm = screen.getByTestId('test-id-rename-confirm');
        expect(renameConfirm).toBeInTheDocument();
        await act(() => {
            fireEvent.click(renameConfirm);
        });
        expect(await screen.findByText(/You have to give a name for the system./i)).toBeInTheDocument();
    });
    test('Given user opens dropdown and click rename then rename input should show and then clicks on the cancel icon the system name should not change', async () => {
        await SelectSystemFirst({ system: systems[0] })
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const renameButton = screen.getByTestId('test-id-menu-item-rename');
        expect(renameButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(renameButton);
        });
        const renameField = screen.getByTestId('test-id-rename-field');
        expect(renameField).toBeInTheDocument();
        const renameInput = renameField.querySelector('input');
        expect(renameInput).toBeInTheDocument();
        expect(renameInput).toHaveProperty('value', 'Follow Trajectory');
        act(() => {
            fireEvent.change(renameInput!, { target: { value: 'newTitle' } });
        });
        expect(renameInput).toHaveProperty('value', 'newTitle');
        const renameCancel = screen.getByTestId('test-id-rename-cancel');
        expect(renameCancel).toBeInTheDocument();
        await act(() => {
            fireEvent.click(renameCancel);
        });
        expect(await screen.findByText(/Follow Trajectory/i)).toBeInTheDocument();
    });
    test('Given user opens dropdown and click rename then click rename again should should modal cannot rename again', async () => {
        await SelectSystemFirst({ system: systems[0] })
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const renameButton = screen.getByTestId('test-id-menu-item-rename');
        expect(renameButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(renameButton);
        });
        act(() => {
            fireEvent.click(renameButton);
        });
        expect(await screen.findByText(/Youre already renaming another system/i)).toBeInTheDocument();
    });
    test('Should hide the accordeon on click ', () => {
        renderSystemEditor()
        const hideAccordeonButton = screen.getByTestId('test-id-button-hide-accordeon');
        act(() => {
            fireEvent.click(hideAccordeonButton);
        });
        const iconHiddenAccorder = screen.getByTestId('test-id-hidden-accordeon');
        expect(iconHiddenAccorder).toBeInTheDocument();
    });
    test('Should hide the connections on click ', () => {
        renderSystemEditor()
        const hideConnectionsButton = screen.getByTestId('test-id-button-hide-connections');
        act(() => {
            fireEvent.click(hideConnectionsButton);
        });
        const iconHideConnections = screen.getByTestId('test-id-hide-connections');
        expect(iconHideConnections).toBeInTheDocument();
    });
    test('Given user opens the dropdown menu and click duplicate the system should duplicate ', async () => {
        await SelectSystemFirst({ system: systems[0]})
        const loadButton = screen.getByTestId(openFileTreeSystemButton);
        await act(() => {
            fireEvent.click(loadButton);
        });
        const icon = screen.getByTestId('test-id-system-open-menu-0');
        expect(icon).toBeInTheDocument();
        act(() => {
            fireEvent.click(icon);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const duplicateButton = screen.getByTestId('test-id-menu-item-duplicate');
        expect(duplicateButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(duplicateButton);
        });
        const newSystems = await screen.findAllByText(/Follow Trajectory/i);
        expect(newSystems.length).toBe(2);
    });
});
describe('Target Zone tests', () => {
    const newDataDropped = JSON.stringify({
        "id": 2,
        "type": "Action",
        "library": "Action_Test",
        "title": "Action_Test",
        "schedule": {
            "offset": 0,
            "cycle": 10,
            "response": 0
        },
        "parameters": [
            {
                "id": 0,
                "type": "double",
                "title": "Spring coefficient",
                "unit": "",
                "value": "1200000"
            },
            {
                "id": 1,
                "type": "double",
                "title": "Damper coefficient",
                "unit": "",
                "value": "12000"
            }
        ],
        "inputs": [
            {
                "id": 0,
                "type": "VectorDouble",
                "title": "InertiaForce",
                "unit": "N",
                "cardinality": 1
            }
        ],
        "outputs": [
            {
                "id": 0,
                "type": "VectorDouble",
                "title": "VerticalForce",
                "unit": "N"
            }
        ],
        "position": {
            "x": 679.1953125,
            "y": 68.734375
        },
        "offsetY": 7.265625,
        "offsetX": 50.8046875,
        "height": 22,
        "width": 269,
        "dropped": false,
        "color": "#1D2D53"
    });
    const systemsWithComponents = [
        {
            "schedule": {
                "cycle": 10,
                "offset": 0,
                "priority": 0,
                "response": 0
            },
            "components": [
                {
                    "id": 0,
                    "inputs": [
                        {
                            "cardinality": 1,
                            "id": 0,
                            "title": "Desired trajectory",
                            "type": "TrajectoryEvent",
                            "unit": ""
                        },
                    ],
                    "library": "Dynamics_CopyTrajectory",
                    "position": {
                        "x": 0,
                        "y": 0
                    },
                    "schedule": {
                        "cycle": 10,
                        "offset": 0,
                        "priority": 0,
                        "response": 0
                    },
                    "parameters": [
                        {
                            "id": 0,
                            "title": "Driver aggressiveness",
                            "type": "double",
                            "unit": "",
                            "value": "1"
                        },
                        {
                            "id": 1,
                            "title": "Max. engine power",
                            "type": "double",
                            "unit": "W",
                            "value": "100000"
                        },
                        {
                            "id": 2,
                            "title": "Boolean Param",
                            "type": "bool",
                            "unit": "",
                            "value": "true"
                        },
                        {
                            "id": 3,
                            "title": "boolVector test",
                            "type": "boolVector",
                            "unit": "",
                            "value": "[true,false,false,true,false]"
                        },
                        {
                            "id": 4,
                            "title": "intVector test",
                            "type": "intVector",
                            "unit": "",
                            "value": "[2,3,4,-2]"
                        },
                        {
                            "id": 5,
                            "title": "intVector test 2",
                            "type": "intVector",
                            "unit": "",
                            "value": "[1000,2000,3000,0]"
                        },
                        {
                            "id": 6,
                            "title": "doubleVector test",
                            "type": "doubleVector",
                            "unit": "",
                            "value": "[1.0,2.0,3.0,0.5]"
                        },
                        {
                            "id": 7,
                            "title": "stringVector test",
                            "type": "stringVector",
                            "unit": "",
                            "value": "[string 1,string 2,string 3,string 4]"
                        },
                        {
                            "id": 8,
                            "title": "String test",
                            "type": "string",
                            "unit": "",
                            "value": "test"
                        },
                        {
                            "id": 8,
                            "title": "double test",
                            "type": "double",
                            "unit": "",
                            "value": "0.123"
                        }
                    ],
                    "title": "Dynamics_CopyTrajectory",
                    "type": ComponentUITypeEnum.Action
                },
                {
                    "id": 1,
                    "library": "OpenScenarioActions",
                    "outputs": [
                        {
                            "id": 0,
                            "title": "Trajectory Event",
                            "type": "TrajectoryEvent",
                            "unit": ""
                        },
                        {
                            "id": 1,
                            "title": "Custom Lane Change Event",
                            "type": "CustomLaneChangeEvent",
                            "unit": ""
                        },
                        {
                            "id": 2,
                            "title": "Gaze Follower Event",
                            "type": "GazeFollowerEvent",
                            "unit": ""
                        },
                        {
                            "id": 3,
                            "title": "Speed Action Event",
                            "type": "SpeedActionEvent",
                            "unit": ""
                        }
                    ],
                    "inputs": [
                        {
                            "cardinality": 1,
                            "id": 0,
                            "title": "Desired trajectory",
                            "type": "TrajectoryEvent",
                            "unit": ""
                        }
                    ],
                    "position": {
                        "x": 0,
                        "y": 0
                    },
                    "schedule": {
                        "cycle": 0,
                        "offset": 0,
                        "priority": 0,
                        "response": 0
                    },
                    "title": "OpenScenarioActions",
                    "type": ComponentUITypeEnum.Algorithm
                }
            ],
            "connections": [
                {
                    "id": 0,
                    "source": {
                        "component": 1,
                        "output": 0
                    },
                    "targets": [{
                        "component": 0,
                        "input": 0
                    }]
                },
                {
                    "id": 1,
                    "source": {
                        "component": 1,
                        "output": 1
                    },
                    "targets": [{
                        "component": 0,
                        "input": 0
                    }]
                }
            ],
            comments: [
                {
                    title: 'Comment',
                    message: 'This is a comment',
                    position: {
                        x: 300,
                        y: 300,
                    }
                }
            ],
            "id": 0,
            "priority": 0,
            "title": "Follow Trajectory",
            "file": "Follow_Trajectory.xml"
        },
    ];
    const getInitialElements = (index: number): number => systemsWithComponents[index].components.length + (systemsWithComponents[index].connections.length > 0 ? 1 : 0)
    const getInitialArrows = (index: number): number => systemsWithComponents[index].connections.length

    beforeEach(async () => SelectSystemFirst({ system: systemsWithComponents[0] }))

    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });

    // COMMENTS
    test('Should render comments', () => {
        const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
        act(() => {
            fireEvent.click(turnOnCommentsButton);
        });
        const component = screen.getByTestId('test-id-comment-0');
        expect(component).toBeInTheDocument();
    });

    test('Comment displays correct title and description', () => {
        const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
        act(() => {
            fireEvent.click(turnOnCommentsButton);
        });
        const component = screen.getByTestId('test-id-comment-0');
        const comment = systemsWithComponents[0].comments[0]
        expect(component).toBeInTheDocument();
        const titleInput = screen.getByTestId('test-id-comment-0-title').querySelector('input');
        expect(titleInput).toBeInTheDocument();
        expect(titleInput).toHaveProperty('value', comment.title);
        const messageInput = screen.getByTestId('test-id-comment-0-message').querySelector('textarea');
        expect(messageInput).toBeInTheDocument();
        expect(messageInput).toHaveDisplayValue(comment.message);
    });

    test('Should be able to edit comment', () => {
        const commentData = systemsWithComponents[0].comments[0]
        const newCommentData = {
            ...commentData,
            title: 'new title',
            message: 'new message'
        }
        const turnOnCommentsButton = screen.getByTestId('test-id-show-comments-button')
        act(() => {
            fireEvent.click(turnOnCommentsButton);
        });
        const component = screen.getByTestId('test-id-comment-0');
        expect(component).toBeInTheDocument();
        const editButton = screen.getByTestId('test-id-comment-0-edit');
        act(() => {
            fireEvent.click(editButton);
        });
        const titleInput = screen.getByTestId('test-id-comment-0-title').querySelector('input');
        expect(titleInput).not.toBe(null)
        expect(titleInput).toBeInTheDocument();
        expect(titleInput).toHaveProperty('value', commentData.title);
        act(() => {
            if (titleInput)
                fireEvent.change(titleInput, { target: { value: newCommentData.title }});
        });
        expect(titleInput).toHaveProperty('value', newCommentData.title);

        const messageInput = screen.getByTestId('test-id-comment-0-message').querySelector('textarea');
        expect(messageInput).toBeInTheDocument();
        expect(messageInput).toHaveProperty('value', commentData.message);
        act(() => {
            if (messageInput)
                fireEvent.change(messageInput, { target: { value: newCommentData.message }});
        });
        expect(messageInput).toHaveProperty('value', newCommentData.message);
    });

    // COMPONENTS
    test('Should render a component with schedule', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
    });
    test('Should change color of component borders', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const colorButton = screen.getByTestId('test-id-color-button-0');
        expect(colorButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(colorButton);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown-color');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const turquoise = screen.getByTestId('test-id-menu-item-0');
        expect(turquoise).toBeInTheDocument();
        act(() => {
            fireEvent.click(turquoise);
        });
    });
    test('Should show delete modal and not delete', async () => {
        const openMenuButton = screen.getByTestId('test-id-open-menu-0');
        expect(openMenuButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(openMenuButton);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const deleteButton = screen.getByTestId('test-id-menu-item-delete');
        expect(deleteButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(deleteButton);
        });
        const modal = screen.getByTestId('test-id-modal-Delete Component');
        expect(modal).toBeInTheDocument();
        const cancelButton = await screen.findByText(/Cancel/i);
        expect(cancelButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(cancelButton);
        });
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
    });
    test('Should show delete modal and delete', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const openMenuButton = screen.getByTestId('test-id-open-menu-0');
        expect(openMenuButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(openMenuButton);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const deleteButton = screen.getByTestId('test-id-menu-item-delete');
        expect(deleteButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(deleteButton);
        });
        const modal = screen.getByTestId('test-id-modal-Delete Component');
        expect(modal).toBeInTheDocument();
        const deleteComponentButton = screen.getByTestId('test-id-modalbutton-delete-component');
        expect(deleteComponentButton).toBeInTheDocument();
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        expect(droppableZone.childElementCount).toBe(getInitialElements(0));
        act(() => {
            fireEvent.click(deleteComponentButton);
        });
        // we write -2 because the element had all connections assigned to it and they have also been deleted
        expect(droppableZone.childElementCount).toBe(getInitialElements(0)-2);
    });
    test('Should be able to duplicate component', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const openMenuButton = screen.getByTestId('test-id-open-menu-0');
        expect(openMenuButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(openMenuButton);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-draggale-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const duplicateButton = screen.getByTestId('test-id-menu-item-duplicate');
        expect(duplicateButton).toBeInTheDocument();
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        expect(droppableZone.childElementCount).toBe(getInitialElements(0));
        await act(() => {
            fireEvent.click(duplicateButton);
        });
        expect(droppableZone.childElementCount).toBe(getInitialElements(0)+1);
    });
    test('Should change the value of the schedule of a component', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const scheduleElement1 = screen.getByTestId('schedule-input-offset-0').querySelector('input');
        expect(scheduleElement1).toBeInTheDocument();
        act(() => {
            fireEvent.change(scheduleElement1!, { target: { value: '20' } });
        });
        expect(scheduleElement1).toHaveProperty('value', '20');
    });
    test('Should be empty when changing since its not a number', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const scheduleElement1 = screen.getByTestId('schedule-input-offset-0').querySelector('input');
        expect(scheduleElement1).toBeInTheDocument();
        act(() => {
            fireEvent.input(scheduleElement1!, { target: { value: 'dsadsa' } });
        });
        expect(scheduleElement1).toHaveProperty('value', '');
    });
    test('Should change the value of the int parameter of a component', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const parameter1 = screen.getByTestId('test-id-parameter-0-Driver aggressiveness').querySelector('input');
        expect(parameter1).toBeInTheDocument();
        act(() => {
            fireEvent.change(parameter1!, { target: { value: '20' } });
        });
        expect(parameter1).toHaveProperty('value', '20');
    });
    test('Should change the value of the string parameter of a component', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const parameter1 = screen.getByTestId('test-id-parameter-0-String test').querySelector('input');
        expect(parameter1).toBeInTheDocument();
        act(() => {
            fireEvent.change(parameter1!, { target: { value: 'test after change' } });
        });
        expect(parameter1).toHaveProperty('value', 'test after change');
    });
    test('Should change the value of the double parameter of a component', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const parameter1 = screen.getByTestId('test-id-parameter-0-String test').querySelector('input');
        expect(parameter1).toBeInTheDocument();
        act(() => {
            fireEvent.change(parameter1!, { target: { value: '1000.25' } });
        });
        expect(parameter1).toHaveProperty('value', '1000.25');
    });
    test('Should change the value of the boolean parameter of a component', () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        const parameterBoolean = screen.getByTestId('test-id-parameter-0-Boolean Param').querySelector('input');
        expect(parameterBoolean).toBeInTheDocument();
        act(() => {
            fireEvent.change(parameterBoolean!, { target: { value: 'true' } });
        });
        expect(parameterBoolean).toHaveProperty('value', 'true');
    });    
    test('Should open the modal to edit intVector parameter and save changes', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the intVector parameter
        const intVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
        expect(intVectorElem).toBeInTheDocument();

        const editButton = intVectorElem.nextElementSibling; // or previousElementSibling, if applicable
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const intVectorModal = screen.getByRole('presentation');
        expect(intVectorModal).toBeInTheDocument();
          
        const modalInputs = within(intVectorModal).getAllByRole('textbox');
        expect(modalInputs.length).toBe(4);

        const newValues = ['-5', '3', '8','-1'];

        for (let i = 0; i < modalInputs.length; i++) {
            // Ensure that modalInputs[i] is treated as an HTMLInputElement
            const inputElement = modalInputs[i] as HTMLInputElement;
        
            // Clear the existing value in the input
            userEvent.clear(inputElement);
        
            // Type the new value into the input
            userEvent.type(inputElement, newValues[i]);
        
            await waitFor(() => {
                // Access the 'value' property of the input element for comparison
                expect(inputElement.value).toBe(newValues[i]);
            });
        }
        const inputElement = modalInputs[0] as HTMLInputElement;
        userEvent.type(inputElement, newValues[0]);
        
        const saveButton = within(intVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        expect(intVectorModal).not.toBeInTheDocument();

        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[-5,3,8,-1]');
        });
        
    });
    test('Should open the modal to edit doubleVector parameter and save changes', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the boolVector parameter
        const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
        expect(doubleVectorElem).toBeInTheDocument();


        const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const doubleVectorModal = screen.getByRole('presentation');
        expect(doubleVectorModal).toBeInTheDocument();
          
        const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputs.length).toBe(4);

        const newValues = ['0.1','-2.0','1000.13','0'];

        for (let i = 0; i < modalInputs.length; i++) {
            // Ensure that modalInputs[i] is treated as an HTMLInputElement
            const inputElement = modalInputs[i] as HTMLInputElement;
        
            // Clear the existing value in the input
            userEvent.clear(inputElement);
        
            // Type the new value into the input
            userEvent.type(inputElement, newValues[i]);
        
            await waitFor(() => {
                // Access the 'value' property of the input element for comparison
                expect(inputElement.value).toBe(newValues[i]);
            });
        }

        const saveButton = within(doubleVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        expect(doubleVectorModal).not.toBeInTheDocument();

        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[0.1,-2.0,1000.13,0]');
        });
    });
    test('Should open the modal to edit stringvector parameter and save changes', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the intVector parameter
        const stringVectorElem = screen.getByTestId('test-id-parameter-0-stringVector test');
        expect(stringVectorElem).toBeInTheDocument();

        const editButton = stringVectorElem.nextElementSibling;
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const stringVectorModal = screen.getByRole('presentation');
        expect(stringVectorModal).toBeInTheDocument();
          
        const modalInputs = within(stringVectorModal).getAllByRole('textbox');
        expect(modalInputs.length).toBe(4);

        const newValues = ["bla", "test val2", "test val 3 longer","123"];

        for (let i = 0; i < modalInputs.length; i++) {
            // Ensure that modalInputs[i] is treated as an HTMLInputElement
            const inputElement = modalInputs[i] as HTMLInputElement;
        
            // Clear the existing value in the input
            userEvent.clear(inputElement);
        
            // Type the new value into the input
            userEvent.type(inputElement, newValues[i]);
        
            await waitFor(() => {
                // Access the 'value' property of the input element for comparison
                expect(inputElement.value).toBe(newValues[i]);
            });
        }
        const inputElement = modalInputs[0] as HTMLInputElement;
        userEvent.clear(inputElement);
        userEvent.type(inputElement, newValues[0]);
        
        const saveButton = within(stringVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        expect(stringVectorModal).not.toBeInTheDocument();

        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-stringVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[bla,test val2,test val 3 longer,123]');
        });
    });
    test('Should open the modal to edit boolvector parameter and save changes', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the intVector parameter
        const boolVectorElem = screen.getByTestId('test-id-parameter-0-boolVector test');
        expect(boolVectorElem).toBeInTheDocument();

        const editButton = boolVectorElem.nextElementSibling;
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within boolVector');
          }

        const boolVectorModal = screen.getByRole('presentation');
        expect(boolVectorModal).toBeInTheDocument();
          
        const modalInputs = within(boolVectorModal).getAllByRole('checkbox');
        expect(modalInputs.length).toBe(5);

        const newValues = [true,false,true,true,true];

        for (let i = 0; i < modalInputs.length; i++) {
            const inputElement = modalInputs[i] as HTMLInputElement; // Ensure it's treated as HTMLInputElement

                // If the element is a checkbox, set its checked state based on newValues[i]
                const shouldBeChecked = newValues[i] == true; // Assuming newValues[i] is 'true' or 'false' string
                if (inputElement.checked !== shouldBeChecked) {
                    userEvent.click(inputElement); // Toggle the checkbox state
                }
        
                // Verify the checkbox's state
                await waitFor(() => {
                    expect(inputElement.checked).toBe(shouldBeChecked);
                });
        }
    
        const saveButton = within(boolVectorModal).getByText('Save', { exact: false });

        act(() => {
            fireEvent.click(saveButton);
        });
    
        //exact false can access not direct children
        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-boolVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[true,false,true,true,true]');
        });
    });
    test('Should add one element to a vector modal', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the boolVector parameter
        const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
        expect(doubleVectorElem).toBeInTheDocument();


        const editButton = doubleVectorElem.nextElementSibling; 
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const doubleVectorModal = screen.getByRole('presentation');
        expect(doubleVectorModal).toBeInTheDocument();
          
          
        const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputs.length).toBe(4);

        //as first input does not have icon, 2nd element is removed
        const addButton = within(doubleVectorModal).getByText('Add');
        if (addButton instanceof HTMLButtonElement) { 
            fireEvent.click(addButton);
        }

        const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputsAfter.length).toBe(5);

        const newValues = ['1.0','2.0','3.0','0.5','0.10'];

        for (let i = 0; i < modalInputsAfter.length; i++) {
            // Ensure that modalInputs[i] is treated as an HTMLInputElement
            const inputElement = modalInputsAfter[i] as HTMLInputElement;
        
            // Clear the existing value in the input
            userEvent.clear(inputElement);
        
            // Type the new value into the input
            userEvent.type(inputElement, newValues[i]);
        
            await waitFor(() => {
                // Access the 'value' property of the input element for comparison
                expect(inputElement.value).toBe(newValues[i]);
            });
        }
        const inputElement = modalInputs[0] as HTMLInputElement;
        userEvent.clear(inputElement);
        userEvent.type(inputElement, newValues[0]);

        const saveButton = within(doubleVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        //exact false can access not direct children
        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[1.0,2.0,3.0,0.5,0.10]');
        });
    });
    test('Should remove one element from a vector modal and save it', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the boolVector parameter
        const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
        expect(doubleVectorElem).toBeInTheDocument();


        const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const doubleVectorModal = screen.getByRole('presentation');
        expect(doubleVectorModal).toBeInTheDocument();
          
          
        const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputs.length).toBe(4);

        const deleteIcons = within(doubleVectorModal).getAllByTestId('DeleteOutlinedIcon');

        //as first input does not have icon, 2nd element is removed
        const buttonOfFirstIcon = deleteIcons[0].parentNode; 
        if (buttonOfFirstIcon instanceof HTMLButtonElement) { 
            fireEvent.click(buttonOfFirstIcon);
        }

        const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputsAfter.length).toBe(3);

        const saveButton = within(doubleVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        //exact false can access not direct children
        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[1.0,3.0,0.5]');
        });
    });
    test('Should remove one element from a vector modal and leave it unchanged when canceling', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the boolVector parameter
        const doubleVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
        expect(doubleVectorElem).toBeInTheDocument();


        const editButton = doubleVectorElem.nextElementSibling; // or previousElementSibling, if applicable
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const doubleVectorModal = screen.getByRole('presentation');
        expect(doubleVectorModal).toBeInTheDocument();
          
          
        const modalInputs =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputs.length).toBe(4);

        const deleteIcons = within(doubleVectorModal).getAllByTestId('DeleteOutlinedIcon');

        //as first input does not have icon, 2nd element is removed
        const buttonOfFirstIcon = deleteIcons[0].parentNode; 
        if (buttonOfFirstIcon instanceof HTMLButtonElement) { 
            fireEvent.click(buttonOfFirstIcon);
        }

        const modalInputsAfter =doubleVectorModal.querySelectorAll('input[type="number"]');
        expect(modalInputsAfter.length).toBe(3);

        const cancelButton = within(doubleVectorModal).getByText('Cancel');
        expect(cancelButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(cancelButton);
        });

        //exact false can access not direct children
        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-doubleVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[1.0,2.0,3.0,0.5]');
        });
    });
    test('Should open the modal to edit intVector parameter, save changes and leave other intVectors unchanged', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();

        // Trigger the edit action for the intVector parameter
        const intVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
        expect(intVectorElem).toBeInTheDocument();

        const editButton = intVectorElem.nextElementSibling; 
        expect(editButton).toBeInTheDocument();
        expect(editButton?.tagName).toBe('BUTTON');

        if (editButton) {
            act(() => {
              fireEvent.click(editButton);
            });
          } else {
            throw new Error('Edit button not found within intVector');
          }

        const intVectorModal = screen.getByRole('presentation');
        expect(intVectorModal).toBeInTheDocument();
          
        const modalInputs = within(intVectorModal).getAllByRole('textbox');
        expect(modalInputs.length).toBe(4);

        const newValues = ['-5', '3', '8','-1'];

        for (let i = 0; i < modalInputs.length; i++) {
            // Ensure that modalInputs[i] is treated as an HTMLInputElement
            const inputElement = modalInputs[i] as HTMLInputElement;
        
            // Clear the existing value in the input
            userEvent.clear(inputElement);
        
            // Type the new value into the input
            userEvent.type(inputElement, newValues[i]);
        
            await waitFor(() => {
                // Access the 'value' property of the input element for comparison
                expect(inputElement.value).toBe(newValues[i]);
            });
        }
        const inputElement = modalInputs[0] as HTMLInputElement;
        userEvent.type(inputElement, newValues[0]);
        
        const saveButton = within(intVectorModal).getByText('Save');
        expect(saveButton).toBeInTheDocument();

        act(() => {
            fireEvent.click(saveButton);
        });

        expect(intVectorModal).not.toBeInTheDocument();

        await waitFor(() => {
            const updatedVectorElem = screen.getByTestId('test-id-parameter-0-intVector test');
            expect(updatedVectorElem).toBeInTheDocument();
            const inputElem = updatedVectorElem.querySelector('input');
            expect(inputElem).toBeInTheDocument();
            const inputValue = inputElem?.value;
            expect(inputValue).toBe('[-5,3,8,-1]');

            const vectorElementStatic = screen.getByTestId('test-id-parameter-0-intVector test 2');
            expect(vectorElementStatic).toBeInTheDocument();
            const inputElemStatic = vectorElementStatic.querySelector('input');
            expect(inputElemStatic).toBeInTheDocument();
            const inputValueStatic = inputElemStatic?.value;
            expect(inputValueStatic).toBe('[1000,2000,3000,0]');
        });
    });
    test('Should delete a connection with modal', async () => {
        const component = screen.getByTestId('test-id-draggable-0');
        expect(component).toBeInTheDocument();
        await new Promise((r) => setTimeout(r, 2000));
        const connection = screen.getByTestId('test-id-connection-0');
        expect(connection).toBeInTheDocument();
        act(() => {
            if (connection) fireEvent.click(connection);
        });
        const modal = screen.getByTestId('test-id-modal-Delete Connection');
        expect(modal).toBeInTheDocument();
        const deleteConnectionButton = screen.getByTestId('test-id-delete-connection-button');
        expect(deleteConnectionButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(deleteConnectionButton);
        });
        await waitFor(() => {
            const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
            expect(droppableZone.childElementCount).toBe(3);
        });
    });
    test('Should cancel deletion of connection with modal', async () => {
        const connection = screen.getByTestId('test-id-connection-0');
        expect(connection).toBeInTheDocument();
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        act(() => {
            fireEvent.click(connection);
        });
        const modal = screen.getByTestId('test-id-modal-Delete Connection');
        expect(modal).toBeInTheDocument();
        const cancelButton = await screen.findByText(/Cancel/i);
        expect(cancelButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(cancelButton);
        });
        expect(droppableZone.childElementCount).toBe(getInitialElements(0));
    });
    test('Shoud start a connection', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        expect(outputButton).toHaveClass('selectedConnection')
    });
    test('Should remove the connection start if click two times on it', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        expect(outputButton).toHaveClass('selectedConnection');
        act(() => {
            fireEvent.click(outputButton);
        });
        expect(outputButton).not.toHaveClass('selectedConnection');
    });
    test('Should show modal of connection already started if click on two different outputs without finishing a connection', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
        const outputButtonTwo = screen.getByTestId('test-id-output-button-1-Custom Lane Change Event');
        act(() => {
            fireEvent.click(outputButton);
        });
        expect(outputButton).toHaveClass('selectedConnection');
        act(() => {
            fireEvent.click(outputButtonTwo);
        });
        expect(outputButtonTwo).not.toHaveClass('outputButtonTwo');
        const modal = screen.getByTestId('test-id-modal-Connection Already Started');
        expect(modal).toBeInTheDocument();
    });

    test('Should show modal that says cannot start connection from an input', () => {
        const inputButton = screen.getByTestId('test-id-input-button-1-Desired trajectory');
        expect(inputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(inputButton);
        });
        const modal = screen.getByTestId('test-id-modal-Need to select an output first to start a connection');
        expect(modal).toBeInTheDocument();
    });
    test('Should show modal that says cannot create a connection with the same component', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        const inputButton = screen.getByTestId('test-id-input-button-1-Desired trajectory');
        expect(inputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(inputButton);
        });
        const modal = screen.getByTestId('test-id-modal-Cannot create a connection with the same component.');
        expect(modal).toBeInTheDocument();
    });
    test('Should show modal that says that there is already a connection on the same output and input', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Trajectory Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
        expect(inputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(inputButton);
        });
        const modal = screen.getByTestId('test-id-modal-Theres already a connection between these 2 components');
        expect(modal).toBeInTheDocument();
    });
    test('Should create a new connection', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Gaze Follower Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
        expect(inputButton).toBeInTheDocument();
        let arrowsContainer = screen.getByTestId('test-id-arrows-container');
        expect(arrowsContainer.childElementCount).toBe(getInitialArrows(0));
        act(() => {
            fireEvent.click(inputButton);
        });
        arrowsContainer = screen.getByTestId('test-id-arrows-container');
        expect(arrowsContainer.childElementCount).toBe(getInitialArrows(0)+1);
    });
    test('Should display an alert if connection is not valid', () => {
        const outputButton = screen.getByTestId('test-id-output-button-1-Custom Lane Change Event');
        expect(outputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(outputButton);
        });
        const inputButton = screen.getByTestId('test-id-input-button-0-Desired trajectory');
        expect(inputButton).toBeInTheDocument();
        act(() => {
            fireEvent.click(inputButton);
        });
        const invalidIcon = screen.getByTestId('1-invalid-icon');
        expect(invalidIcon).toBeInTheDocument();
    });


    test('Should move an element inside the droppable zone', () => {
        const testingElement = screen.getByTestId('test-id-draggable-ActionLongitudinalDriver')
        const mockDataTransfer = {
            clearData: jest.fn(),
            setData: jest.fn(),
        };
        act(() => {
            fireEvent.dragStart(testingElement, { dataTransfer: mockDataTransfer });
        });
        const testingElementDraggable = screen.getByTestId('test-id-draggable-0')
        act(() => {
            fireEvent.dragStart(testingElementDraggable, { dataTransfer: mockDataTransfer });
        });
        const dataToBeReturnedByGetData = JSON.stringify({
            "id": 27,
            "type": "Algorithm",
            "library": "OpenScenarioActions",
            "title": "OpenScenarioActions",
            "schedule": {
                "offset": 0,
                "cycle": 0,
                "response": 0,
                "priority": 0
            },
            "outputs": [
                {
                    "id": 0,
                    "type": "TrajectoryEvent",
                    "title": "Trajectory Event",
                    "unit": ""
                },
                {
                    "id": 1,
                    "type": "CustomLaneChangeEvent",
                    "title": "Custom Lane Change Event",
                    "unit": ""
                },
                {
                    "id": 2,
                    "type": "GazeFollowerEvent",
                    "title": "Gaze Follower Event",
                    "unit": ""
                },
                {
                    "id": 3,
                    "type": "SpeedActionEvent",
                    "title": "Speed Action Event",
                    "unit": ""
                }
            ],
            "position": {
                "x": 19,
                "y": 31
            },
            "dropped": true,
            "color": "#1D2D53",
            "index": 1,
            "offsetY": 28,
            "offsetX": 134,
            "height": 289,
            "width": 318,
            "droppped": true
        });
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        const mockDataTransferDrop = {
            getData: jest.fn().mockReturnValue(dataToBeReturnedByGetData),
            clearData: jest.fn(),
            setData: jest.fn(),
        };
        act(() => {
            fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
        });
        const element = screen.getByTestId('test-id-draggable-1');
        expect(element.style.left).toBe("946px");
        expect(element.style.top).toBe("276px");
    });
    test('Should create a new element when dropped', async () => {
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        const mockDataTransferDrop = {
            getData: jest.fn().mockReturnValue(newDataDropped),
            clearData: jest.fn(),
            setData: jest.fn(),
        };
        expect(droppableZone.childElementCount).toBe(getInitialElements(0));
        await act(() => {
            fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
        });
        expect(droppableZone.childElementCount).toBe(getInitialElements(0)+1);
    });
    test('Should show modal for dropping elements without selecting a system', async() => { 
        const system = screen.getByTestId('test-id-system-text-0');
        act(() => {
            fireEvent.click(system);
        });
        const droppableZone = screen.getByTestId('test-id-box-droppable-zone');
        const mockDataTransferDrop = {
            getData: jest.fn().mockReturnValue(newDataDropped),
            clearData: jest.fn(),
            setData: jest.fn(),
        };
        await act(() => {
            fireEvent.drop(droppableZone, { dataTransfer: mockDataTransferDrop, });
        });
        const modal = screen.getByTestId('test-id-modal-Select A System');
        expect(modal).toBeInTheDocument();
     });
});

