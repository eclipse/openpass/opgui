/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {fireEvent, waitForElementToBeRemoved} from "@testing-library/react";
import {renderWithProviders} from "../utils/test.utils.tsx";
import NewConfig from "../components/interfaces/SetupConfigs/setups/NewConfig";
import {hexagonsData} from "../components/interfaces/SetupConfigs/setups/NewConfig/hexagonsData";
import {act} from "react-dom/test-utils";
import setupMocksCalls, {CallHandler} from "../utils/tests/setupMocksCalls.ts";
import handleResponse from "../utils/tests/handleResponse.ts";
import {DYNAMIC_SELECTORS} from "../components/commonComponents/DynamicInputs/interfaces.ts";

const mockedUsedNavigate = jest.fn();
jest.mock("react-router", () => ({
    ...(jest.requireActual("react-router") as any),
    useNavigate: () => mockedUsedNavigate
}));
const setupValueLists = ()=> {

    const setupCalls: CallHandler[] = [
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.TRAFFIC_RULES}`,
            response: handleResponse({
                listValues: ['DE']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.DATA_BUFFER_LIBRARIES}`,
            response: handleResponse({
                listValues: ['DataBuffer']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.WORLD_LIBRARIES}`,
            response: handleResponse({
                listValues: ['WORLD', 'WORLD_OSI']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.STOCHASTIC_LIBRARIES}`,
            response: handleResponse({
                listValues: ['Stochastics']
            })
        },
        {
            url: `api/getValueList?name=${DYNAMIC_SELECTORS.DEFAULT_LOGGING_GROUPS}`,
            response: handleResponse({
                listValues: ["Trace", "RoadPosition", "RoadPositionExtended", "Sensor", "Vehicle", "Visualization"]
            })
        },
    ]
    setupMocksCalls(setupCalls)
}

describe('Render test for New Configs Screen', () => {

    afterEach(() => {
        jest.restoreAllMocks();
    });

    test('It should render all hexagons ', () => {
        const { getByTestId } = renderWithProviders(<NewConfig />)
        for (let i= 0; i < hexagonsData.length; i++){
            const component = getByTestId(`hexagon-${i}`);
            expect(component).toBeInTheDocument();
            expect(component).not.toBeEmptyDOMElement();
        }
    });

    test('It should show modal when trying to switch hexagons', async () => {
        setupValueLists()
        const { getByTestId } = renderWithProviders(<NewConfig />)
        const hexagonToSelect = getByTestId(`hexagon-${2}`)
        await waitForElementToBeRemoved(()=> getByTestId('testid-fetching-groups'))
        act(()=> {
            fireEvent.click(hexagonToSelect)
        })
        const modal = getByTestId(`test-id-modal-Local changes not saved!`)
        expect(modal).toBeInTheDocument()
    });
});