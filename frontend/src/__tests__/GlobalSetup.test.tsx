/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { screen } from "@testing-library/react";
import GlobalSetup from "../components/interfaces/GlobalSetup/GlobalSetup";
import { renderWithProviders } from "../utils/test.utils";

describe('Render Test for Global Setup Interface', () => {
  beforeEach(() => {
    renderWithProviders(<GlobalSetup />);
  });

  test("renders FooterComponent with Save Changes button", () => {
    const footer = screen.getByTestId("test-id-footer-setup");
    expect(footer).toBeInTheDocument();
    expect(footer).not.toBeEmptyDOMElement();

    const saveChangesButton = screen.getByTestId("test-id-button-saveChanges");
    expect(saveChangesButton).toBeInTheDocument();
    expect(saveChangesButton).toHaveTextContent("Save Changes");
  });


});