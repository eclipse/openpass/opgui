/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import {ComponentUI} from "opGUI-client-frontend";
import {updateComponentIds} from "../components/interfaces/SystemEditor/EditorUtils.ts";
import { System } from "../components/interfaces/SystemEditor/systemEditor.d";

const frontEndComponents: ComponentUI[] = [
    {
        id: 10,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 0',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        outputs: [{
            id: 0,
            title: 'Output 0',
            type: 'type',
            unit: 'seconds',
        }]
    },
    {
        id: 5,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 1',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        inputs: [
            {
                id: 0,
                title: 'Input 0',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            },
            {
                id: 1,
                title: 'Input 1',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            }
        ],
        outputs: [{
            id: 0,
            title: 'Output 0',
            type: 'type',
            unit: 'seconds',
        }]
    },
    {
        id: 15,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 2',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        inputs: [
            {
                id: 0,
                title: 'Input 0',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            },
        ],
    },
]
const frontEndComponentsOrdered: ComponentUI[] = frontEndComponents.map(((c,i) => ({
    ...c,
    id: i,
})))

const systemConnections = [
    {
        id: 24,
        source: {
            component: frontEndComponents[0].id,
            output: 0
        },
        target: {
            component: frontEndComponents[1].id,
            input: 0,
        },
    },
    {
        id: 16,
        source: {
            component: frontEndComponents[0].id,
            output: 0
        },
        target: {
            component: frontEndComponents[1].id,
            input: 1,
        }
    },
    {
        id: 56,
        source: {
            component: frontEndComponents[0].id,
            output: 0
        },
        target: {
            component: frontEndComponents[2].id,
            input: 0,
        }
    },
]
const systemConnectionsOrdered = [
    {
        id: 0,
        source: {
            component: frontEndComponentsOrdered[0].id,
            output: 0
        },
        target: {
            component: frontEndComponentsOrdered[1].id,
            input: 0,
        },
    },
    {
        id: 1,
        source: {
            component: frontEndComponentsOrdered[0].id,
            output: 0
        },
        target: {
            component: frontEndComponentsOrdered[1].id,
            input: 1,
        }
    },
    {
        id: 2,
        source: {
            component: frontEndComponentsOrdered[0].id,
            output: 0
        },
        target: {
            component: frontEndComponentsOrdered[2].id,
            input: 0,
        }
    },
]

const systemUnordered: System = {
    id: 0,
    path: '',
    title: 'Title',
    priority: 0,
    anchorEl: null,
    components: frontEndComponents,
    connections: systemConnections,
    comments: [],
    file: '',
}
const systemOrdered: System = {
    id: 0,
    path: '',
    title: 'Title',
    priority: 0,
    anchorEl: null,
    components: frontEndComponentsOrdered,
    connections: systemConnectionsOrdered,
    comments: [],
    file: '',
}

describe('Testing Editor Utils', () => {
    it('Makes component and connection ids be ordered and sequential', () => {
        const updatedIdsSystem = updateComponentIds(systemUnordered)
        expect(JSON.stringify(updatedIdsSystem)).toBe(JSON.stringify(systemOrdered))
    });
})