/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import FileSystemTree from "../components/commonComponents/fileSystemTree/FileSystemTree.tsx";
import {renderWithProviders} from "../utils/test.utils.tsx";
import { screen } from "@testing-library/react";

describe('Testing FileSystemTree Component', () => {
    const testData = {
        name: 'workspace',
        children: [{
            name: 'folderTest1',
            children: [{
                name: '',
                children: undefined
            }]
        }, {
            name: 'results',
            children: [{
                name: 'emptyFolder',
                children: [{
                    name: '',
                    children: undefined
                }]
            }]
        }]
    }

    const props = {
        data: testData,
        onSelectFolder: jest.fn()
    }

    beforeEach(() => {
        renderWithProviders(<FileSystemTree {...props} />);
    });

    it('WHEN the component renders THEN the FolderTree should show', () => {
        const folderTree = screen.getByTestId('folder-tree-container');

        expect(folderTree).toBeInTheDocument();
    });
});