/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */


import {cleanup, fireEvent, render, screen, waitFor, within} from "@testing-library/react";
import SetupConfig from "../components/interfaces/SetupConfigs/SetupConfig";
import { renderWithProviders } from "../utils/test.utils";
import { act } from "react-dom/test-utils";
import fetchMock from "jest-fetch-mock";
import handleResponse from "../utils/tests/handleResponse.ts";
import defaultFileTreeResponse from "../utils/tests/mockResponses/defaultFileTreeResponse.ts";
import { FileTreeExtended } from "opGUI-client-frontend";
import setupMocksCalls, {CallHandler} from "../utils/tests/setupMocksCalls.ts";
import getDefaultVerifyPathResponse from "../utils/tests/mockResponses/getDefaultVerifyPathResponse.ts";
// import type {ExportedSimConfigSettings} from "opGUI-client-frontend/src/models";
import selectFileInFileTree from "../utils/tests/selectFromFileTree.ts";
import Hexagon from "../components/interfaces/SetupConfigs/setups/NewConfig/Hexagon.tsx";
import { ThemeProvider } from "@mui/material";
import {theme} from "../config/themeProvider.ts";

const defaultVerifyPathUrl = `os/workspace/path/to/folder/test`

interface SetupMocksParams {
    fileTreeResponse?: FileTreeExtended
    verifyPathResponse?: {
        "ok": boolean,
        "realPath": string,
        "empty": boolean
    },
    pcmFileResponse?: string[]
    convertToConfigResponse?: {
        response: string;
    }
    exportToSimulationResponse?: any
}

const setupMockResponses = ({
    fileTreeResponse = defaultFileTreeResponse,
    verifyPathResponse,
    pcmFileResponse,
    convertToConfigResponse,
    exportToSimulationResponse,
}: SetupMocksParams = {}) => {
    const setupCalls: CallHandler[] = [
        {
            url: `api/getFileTree`,
            response: handleResponse(fileTreeResponse)
        },
        {
            url: `api/verify-path`,
            response: handleResponse(verifyPathResponse || getDefaultVerifyPathResponse(defaultVerifyPathUrl))
        },
        {
            url: `api/sendPCMFile`,
            response: handleResponse({
                selectedExperiments: pcmFileResponse || ['1','2','3','4']
            })
        },
        {
            url: `api/convertToConfigs`,
            response: handleResponse(convertToConfigResponse || { response: 'Configuration data correctly generated.'})
        },
        {
            url: 'api/exportToSimulations',
            response: handleResponse(exportToSimulationResponse || {
                path: 'workspace/test/example',
                selectedExperiments: pcmFileResponse || ['1','2','3','4'],
                xmlContent: 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxvcFNpbXVsYXRpb25NYW5hZ2VyPg0KICAgIDxsb2dMZXZlbD4zPC9sb2dMZXZlbD4NCiAgICA8bG9nRmlsZVNpbXVsYXRpb25NYW5hZ2VyPi9ob21lL29wZ3VpLXVzZXIvRG9jdW1lbnRzL3dvcmtzcGFjZXMvb3BHVUkvd29ya3NwYWNlMS90ZXN0L29wU2ltdWxhdGlvbk1hbmFnZXIubG9nPC9sb2dGaWxlU2ltdWxhdGlvbk1hbmFnZXI+DQogICAgPGxpYnJhcmllcz5tb2R1bGVzPC9saWJyYXJpZXM+DQogICAgPHNpbXVsYXRpb25Db25maWdzPg0KICAgICAgICA8c2ltdWxhdGlvbkNvbmZpZz4NCiAgICAgICAgICAgIDxjb25maWd1cmF0aW9ucz4vaG9tZS9vcGd1aS11c2VyL0RvY3VtZW50cy93b3Jrc3BhY2VzL29wR1VJL3dvcmtzcGFjZTEvdGVzdC8xMTExLzAtMC0wL2NvbmZpZ3M8L2NvbmZpZ3VyYXRpb25zPg0KICAgICAgICAgICAgPGxvZ0ZpbGVTaW11bGF0aW9uPi9ob21lL29wZ3VpLXVzZXIvRG9jdW1lbnRzL3dvcmtzcGFjZXMvb3BHVUkvd29ya3NwYWNlMS90ZXN0LzExMTEvMC0wLTAvb3BTaW11bGF0aW9uLmxvZzwvbG9nRmlsZVNpbXVsYXRpb24+DQogICAgICAgICAgICA8cmVzdWx0cz4vaG9tZS9vcGd1aS11c2VyL0RvY3VtZW50cy93b3Jrc3BhY2VzL29wR1VJL3dvcmtzcGFjZTEvdGVzdC8xMTExLzAtMC0wL3Jlc3VsdHM8L3Jlc3VsdHM+DQogICAgICAgIDwvc2ltdWxhdGlvbkNvbmZpZz4NCiAgICAgICAgPHNpbXVsYXRpb25Db25maWc+DQogICAgICAgICAgICA8Y29uZmlndXJhdGlvbnM+L2hvbWUvb3BndWktdXNlci9Eb2N1bWVudHMvd29ya3NwYWNlcy9vcEdVSS93b3Jrc3BhY2UxL3Rlc3QvMjIyMi8wLTAtMC9jb25maWdzPC9jb25maWd1cmF0aW9ucz4NCiAgICAgICAgICAgIDxsb2dGaWxlU2ltdWxhdGlvbj4vaG9tZS9vcGd1aS11c2VyL0RvY3VtZW50cy93b3Jrc3BhY2VzL29wR1VJL3dvcmtzcGFjZTEvdGVzdC8yMjIyLzAtMC0wL29wU2ltdWxhdGlvbi5sb2c8L2xvZ0ZpbGVTaW11bGF0aW9uPg0KICAgICAgICAgICAgPHJlc3VsdHM+L2hvbWUvb3BndWktdXNlci9Eb2N1bWVudHMvd29ya3NwYWNlcy9vcEdVSS93b3Jrc3BhY2UxL3Rlc3QvMjIyMi8wLTAtMC9yZXN1bHRzPC9yZXN1bHRzPg0KICAgICAgICA8L3NpbXVsYXRpb25Db25maWc+DQogICAgICAgIDxzaW11bGF0aW9uQ29uZmlnPg0KICAgICAgICAgICAgPGNvbmZpZ3VyYXRpb25zPi9ob21lL29wZ3VpLXVzZXIvRG9jdW1lbnRzL3dvcmtzcGFjZXMvb3BHVUkvd29ya3NwYWNlMS90ZXN0LzMzMzMvMC0wLTAvY29uZmlnczwvY29uZmlndXJhdGlvbnM+DQogICAgICAgICAgICA8bG9nRmlsZVNpbXVsYXRpb24+L2hvbWUvb3BndWktdXNlci9Eb2N1bWVudHMvd29ya3NwYWNlcy9vcEdVSS93b3Jrc3BhY2UxL3Rlc3QvMzMzMy8wLTAtMC9vcFNpbXVsYXRpb24ubG9nPC9sb2dGaWxlU2ltdWxhdGlvbj4NCiAgICAgICAgICAgIDxyZXN1bHRzPi9ob21lL29wZ3VpLXVzZXIvRG9jdW1lbnRzL3dvcmtzcGFjZXMvb3BHVUkvd29ya3NwYWNlMS90ZXN0LzMzMzMvMC0wLTAvcmVzdWx0czwvcmVzdWx0cz4NCiAgICAgICAgPC9zaW11bGF0aW9uQ29uZmlnPg0KICAgIDwvc2ltdWxhdGlvbkNvbmZpZ3M+DQo8L29wU2ltdWxhdGlvbk1hbmFnZXI+'
            })
        },
        {
            url: `/api/pathToConvertedCases`,
            response: handleResponse({
                response: `PATH_CONVERTED_CASES has been correctly set.`
            })

        }
    ]
    setupMocksCalls(setupCalls)
}

const clickSystemIdCheckboxes = (ids: string[], expectToBecomeChecked = true) => {
    for(let i = 0; i < ids.length; i++){
        const checkbox = within(screen.getByTestId(`test-simid-checkbox-${ids[i]}`)).getByRole('checkbox')
        expect(checkbox).toBeInTheDocument();
        fireEvent.click(checkbox)
        if (expectToBecomeChecked){
            expect(checkbox).toBeChecked()
        }
        else{
            expect(checkbox).not.toBeChecked()
        }
    }
}

describe('Render Tests for SetupConfigs interface', () => {
    beforeEach(async () => {
        fetchMock.enableMocks();
        await act(() => renderWithProviders(<SetupConfig />));
    });
    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('Given its the first time they enter should render the component', async () => {
        const mainComponent = screen.getByTestId('test-id-interface-setupconfigs');
        expect(mainComponent).toBeInTheDocument();
    });
    test('Should render path to Pcm database option by default', () => {
        const uploadFilePcm = screen.getByTestId('test-id-input-pathToPcm').querySelector('input');
        expect(uploadFilePcm).toBeInTheDocument();
        expect(uploadFilePcm?.value).toBe('');
    });
    test('should render at least 1 agent as the initial state', () => {
        const agentContainer = screen.getByTestId('test-id-grid-agent-container');
        expect(agentContainer).toBeInTheDocument();
        expect(agentContainer).not.toBeEmptyDOMElement();
    });
    test('Should render no id found as the initial state', () => {
        const emptyIdText = screen.queryByText('No IDs loaded.');
        expect(emptyIdText).toBeInTheDocument();
    });
    test('Should render sticky div section complete', () => {
        const checkboxFilter = screen.getByTestId('test-id-checkbox-setup-filter');
        const uploadFileFilter = screen.getByTestId('test-id-input-fileFromList').querySelector('input');
        const searchBarFilter = screen.getByTestId('test-id-input-setup-filter').querySelector('input');
        expect(checkboxFilter).toBeInTheDocument();
        expect(checkboxFilter).not.toBeChecked();
        expect(searchBarFilter).toBeInTheDocument();
        expect(searchBarFilter?.value).toBe('');
        expect(uploadFileFilter).toBeInTheDocument();
        expect(searchBarFilter?.value).toBe('');
    });
    test('Should render footer with all of its options', () => {
        const footer = screen.getByTestId('test-id-footer-setup');
        const convertToConfigsButton = screen.getByTestId('test-id-button-convert');
        const uploadFileFooter = screen.getByTestId('test-id-input-pathToConvertedCases');
        const exportToSimulationButton = screen.getByTestId('test-id-button-export');
        expect(footer).toBeInTheDocument();
        expect(footer).not.toBeEmptyDOMElement();
        expect(convertToConfigsButton).toBeInTheDocument();
        expect(convertToConfigsButton).toBeDisabled();
        expect(exportToSimulationButton).toBeInTheDocument();
        expect(exportToSimulationButton).toBeDisabled();
        expect(uploadFileFooter).toBeInTheDocument();
    });
});
describe('Functional Test for Setup Configs Component', () => {
    beforeEach(async () => {
        fetchMock.enableMocks();
        await act(() => renderWithProviders(<SetupConfig />));
    });
    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });
    test('Given User select a .db file input should not be empty and should not have class Mui-error ', async () => {
        const fileName = 'os/workspace/path/to/folder/dummy.db';
        // Mocking the response for the verifyPath function
        setupMockResponses({
            verifyPathResponse: getDefaultVerifyPathResponse(
                fileName,
            )
        })
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        const inputContainer = screen.getByTestId('test-id-input-pathToPcm');
        const inputFile = inputContainer.querySelector('input');

        await waitFor(() => {
            expect(inputContainer).not.toHaveClass('Mui-error');
            expect(inputFile).toHaveAttribute('value', fileName);
        });
    });
    test('Given User select a .xml file for an agent input should not be empty and should not have class Mui-error ', async () => {
        const fileName = 'os/workspace/path/to/folder/dummy.xml';
        // Mocking the response for the verifyPath function
        setupMockResponses({
            verifyPathResponse: getDefaultVerifyPathResponse(
                fileName,
                false,
            )
        })
        await selectFileInFileTree('test-id-input-button-agentFile-0')

        const inputContainer = screen.getByTestId('test-id-input-agentFile-0');
        const inputFile = inputContainer.querySelector('input');

        expect(inputContainer).not.toHaveClass('Mui-error');
        expect(inputFile).toHaveAttribute('value', fileName);
    });
    test('Given user clicks on checkbox should open dropdown menu ', () => { 
        const dropdownSelector = screen.getByTestId('test-id-box-dropdown-selector');
        expect(dropdownSelector).toBeInTheDocument();
        act(() => {
            fireEvent.click(dropdownSelector);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
     });
    test('Given user clicks on checkbox should open dropdown menu and select select all options all experiments should be selected', async () => { 
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse: pcmFileResponse
        })

        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        const emptyState = screen.getByTestId('test-id-label-empty-ids');
        expect(emptyState).toBeInTheDocument();

        expect(simulationIdContainer).toBeEmptyDOMElement();
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        await waitFor(() => {
            expect(simulationIdContainer).not.toBeEmptyDOMElement();
        });
        const dropdownSelector = screen.getByTestId('test-id-box-dropdown-selector');
        expect(dropdownSelector).toBeInTheDocument();
        act(() => {
            fireEvent.click(dropdownSelector);
        });
        const menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const selectAll = screen.getByTestId('test-id-menu-item-select-all');
        act(() => {
            fireEvent.click(selectAll);
        });
        for (let i=0; i< pcmFileResponse.length; i++){
            const checkbox = within(screen.getByTestId(`test-simid-checkbox-${pcmFileResponse[i]}`)).getByRole('checkbox', { hidden: true })
            expect(checkbox).toBeInTheDocument();
            expect(checkbox).toBeChecked()
        }
     });
    test('Given user clicks on checkbox should open dropdown menu and select unselect all options all experiments should be empty', async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse: pcmFileResponse
        })

        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        const emptyState = screen.getByTestId('test-id-label-empty-ids');
        expect(emptyState).toBeInTheDocument();

        expect(simulationIdContainer).toBeEmptyDOMElement();
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        await waitFor(() => {
            expect(simulationIdContainer).not.toBeEmptyDOMElement();
        });
        const dropdownSelector = screen.getByTestId('test-id-box-dropdown-selector');
        expect(dropdownSelector).toBeInTheDocument();
        fireEvent.click(dropdownSelector);

        let menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        expect(menuDropdown).toBeInTheDocument();
        expect(menuDropdown).not.toBeEmptyDOMElement();
        const selectAll = screen.getByTestId('test-id-menu-item-select-all');
        fireEvent.click(selectAll);
        for (let i=0; i< pcmFileResponse.length; i++){
            const checkbox = within(screen.getByTestId(`test-simid-checkbox-${pcmFileResponse[i]}`)).getByRole('checkbox', { hidden: true })
            expect(checkbox).toBeInTheDocument();
            expect(checkbox).toBeChecked()
        }
        // unselect
        fireEvent.click(dropdownSelector);
        menuDropdown = screen.getByTestId('test-id-menu-dropdown');
        const unselectAll = screen.getByTestId('test-id-menu-item-unselect-all');
        fireEvent.click(unselectAll);
        for (let i=0; i< pcmFileResponse.length; i++){
            const checkbox = within(screen.getByTestId(`test-simid-checkbox-${pcmFileResponse[i]}`)).getByRole('checkbox', { hidden: true })
            expect(checkbox).toBeInTheDocument();
            expect(checkbox).not.toBeChecked()
        }
     });
    test('Given .db file is selected should render multiple Simulation Id Component', async () => {
        const pcmFileResponse = ['1','2']
        const fileName = 'os/workspace/path/to/folder/dummy.db';
        // Mocking the response for the verifyPath function
        setupMockResponses({
            verifyPathResponse: getDefaultVerifyPathResponse(
                fileName,
            ),
            pcmFileResponse
        })
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        expect(simulationIdContainer).not.toBeEmptyDOMElement();
    });
    test('Given .db file and file is empty should render empty list', async () => {
        const pcmFileResponse: string[] = []
        const fileName = 'os/workspace/path/to/folder/dummy.db';
        // Mocking the response for the verifyPath function
        setupMockResponses({
            verifyPathResponse: getDefaultVerifyPathResponse(
                fileName,
            ),
            pcmFileResponse
        })
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        expect(simulationIdContainer).toBeEmptyDOMElement();
    });
    test('Given simulation are charged with the pcm database when clicking on one should be selected', async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse
        })

        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        expect(simulationIdContainer).toBeEmptyDOMElement();

        await selectFileInFileTree('test-id-input-button-pathToPcm')
        expect(simulationIdContainer).not.toBeEmptyDOMElement();

        clickSystemIdCheckboxes([pcmFileResponse[0]])
    });
    test(`Given .db file is selected and at least 1 sim is select 
        and path to converted cases is selected actions buttons should be enabled`, async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse
        })

        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        expect(simulationIdContainer).toBeEmptyDOMElement();

        await selectFileInFileTree('test-id-input-button-pathToPcm')
        expect(simulationIdContainer).not.toBeEmptyDOMElement();

        const convertButton = screen.getByTestId('test-id-button-convert');
        expect(convertButton).toBeInTheDocument();

        clickSystemIdCheckboxes([pcmFileResponse[0]])
        // Check for convert button availability
        expect(convertButton).toBeEnabled();
    });
    test(`Given .db file is selected and at least 1 sim is select 
        and path to converted cases is selected actions buttons should be enabled and should show modal error because theres no agents selected.`, async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse
        })
        const agentCheckbox = within(screen.getByTestId('test-id-checkbox-agent-Agent 1')).getByRole('checkbox')
        expect(agentCheckbox).not.toBeChecked()

        await selectFileInFileTree('test-id-input-button-pathToPcm')

        clickSystemIdCheckboxes([pcmFileResponse[0]])

        const convertButton = screen.getByTestId('test-id-button-convert');
        fireEvent.click(convertButton)

        const incompleteDataModal = screen.getByTestId('test-id-modal-Incomplete Data')
        expect(incompleteDataModal).toBeInTheDocument()
    });
    test(`Given .db file is selected and at least 1 sim is select 
        and path to converted cases is selected actions buttons should be enabled and should show modal error because theres no agents selected file path is selected.`, async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse
        })
        const agentCheckbox = within(screen.getByTestId('test-id-checkbox-agent-Agent 2')).getByRole('checkbox')
        expect(agentCheckbox).not.toBeChecked()

        await selectFileInFileTree('test-id-input-button-pathToPcm')

        const convertButton = screen.getByTestId('test-id-button-convert');
        clickSystemIdCheckboxes([pcmFileResponse[0]])
        fireEvent.click(agentCheckbox)
        fireEvent.click(convertButton)

        expect(agentCheckbox).toBeChecked()

        const incompleteDataModal = screen.getByTestId('test-id-modal-Incomplete Data')
        expect(incompleteDataModal).toBeInTheDocument()
        const incompleteDataModalText = incompleteDataModal.querySelector('#modal-modal-description')
        expect(incompleteDataModalText).toHaveTextContent('The selected agents must have a file loaded.');
    });
    const enableButton = async (pcmFileResponse: string[]) => {
        const agentCheckbox = within(screen.getByTestId('test-id-checkbox-agent-Agent 2')).getByRole('checkbox')
        await act(()=> fireEvent.click(agentCheckbox))
        await selectFileInFileTree('test-id-input-button-agentFile-1')

        await selectFileInFileTree('test-id-input-button-pathToPcm')

        clickSystemIdCheckboxes([pcmFileResponse[0]])
    }
    test('Given data is complete config modal should open ', async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse,
            convertToConfigResponse: {
                response: "location set correctly"
            }
        })
        await enableButton(pcmFileResponse);

        const convertButton = screen.getByTestId('test-id-button-convert');
        expect(convertButton).toBeInTheDocument();
        expect(convertButton).toBeEnabled();
        await act(()=> fireEvent.click(convertButton));
        
        await waitFor(() => {
            const modal = screen.getByTestId('test-id-modal-Convert To Configs');
            expect(modal).toBeInTheDocument();
        });
    });
    test('Given data is complete export modal should open ', async () => {
        const pcmFileResponse = ['1','2']
        setupMockResponses({
            pcmFileResponse,
            convertToConfigResponse: {
                response: "location set correctly"
            }
        })
        const exportButton = screen.getByTestId('test-id-button-export');
        expect(exportButton).toBeInTheDocument();
        expect(exportButton).toBeDisabled()

        await enableButton(pcmFileResponse);

        const convertButton = screen.getByTestId('test-id-button-convert');
        expect(convertButton).toBeInTheDocument();
        expect(convertButton).toBeEnabled();
        await act(()=> fireEvent.click(convertButton))
        await waitFor(() => {
            const modal = screen.getByTestId('test-id-modal-Convert To Configs');
            expect(modal).toBeInTheDocument();
        });

        expect(exportButton).toBeEnabled();
        await act(() =>
            fireEvent.click(exportButton)
        );

        const modal = screen.getByTestId('test-id-modal-Export');
        expect(modal).toBeInTheDocument();
    });
    test('filterData is called when searchTerm changes', () => {
        const searchTermInput = screen.getByTestId('test-id-input-setup-filter').querySelector('input');
        act(() => {
            fireEvent.change(searchTermInput!, { target: { value: '1123' } });
        });
        expect(searchTermInput).toHaveProperty('value', '1123');

        act(() => {
            fireEvent.change(searchTermInput!, { target: { value: '1123, 1098' } });
        });
        expect(searchTermInput).toHaveProperty('value', '1123, 1098');
    });
    test('Given user input filter SimulationIdComponent should change', async () => {
        const pcmFileResponse = ['1123','2']
        setupMockResponses({
            pcmFileResponse,
            convertToConfigResponse: {
                response: "location set correctly"
            }
        })
        await selectFileInFileTree('test-id-input-button-pathToPcm')
        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        const searchTermInput = screen.getByTestId('test-id-input-setup-filter').querySelector('input');

        await waitFor(() => {
            expect(simulationIdContainer).not.toBeEmptyDOMElement();
        });
        expect(simulationIdContainer.childElementCount).toBe(2);
        act(() => {
            fireEvent.change(searchTermInput!, { target: { value: '1123' } });
        });
        expect(searchTermInput).toHaveProperty('value', '1123');
        expect(simulationIdContainer.childElementCount).toBe(1);
    });
    test('Given no .db file is selected filter from list button should be disabled', () => {
        const uploadFileFilter = screen.getByTestId('test-id-input-fileFromList').querySelector('input');
        const uploadButton = screen.getByTestId('test-id-input-button-fileFromList');
        expect(uploadFileFilter).toBeInTheDocument();
        expect(uploadButton).toBeDisabled();
        expect(uploadButton).toBeInTheDocument();
    });
    test('Given There are simulations in the csv file, they should be put into the filter input as comma separated', async () => {
        // TODO: Add test when cvs support comes back
        /*fetchMock.mockResponseOnce(JSON.stringify({
            "ok": true,
            "realPath": "os/workspace/path/to/folder/dummy.db"
        }));
        // Mocking the response for the sendPCMFile function
        fetchMock.mockResponseOnce(JSON.stringify({
            "selectedExperiments": ["1123", "1098", "2000"]
        }));
        const simulationIdContainer = screen.getByTestId('test-id-container-simulation-id');
        const fileName = 'os/workspace/path/to/folder/dummy.db';
        const inputFileHidden = screen.getByTestId('test-id-input-hidden-pathToPcm');
        act(() => {
            fireEvent.change(inputFileHidden, { target: { files: [{ name: fileName }] } });
        });
        await waitFor(() => {
            expect(simulationIdContainer).not.toBeEmptyDOMElement();
        });
        expect(simulationIdContainer.childElementCount).toBe(3);
        const inputFileHiddenFilter = screen.getByTestId('test-id-input-hidden-fileFromList');
        const file = new File(['simulations\n1123\n1098'], 'dummy.csv', { type: 'text/csv' });
        act(() => {
            fireEvent.change(inputFileHiddenFilter, { target: { files: [file] } });
        });

        const searchTermInput = screen.getByTestId('test-id-input-setup-filter').querySelector('input');

        await waitFor(() => {
            expect(searchTermInput).toHaveValue('1123,1098');
        });*/

    });
});

describe('Functional Test for Setup Configs Component', () => {

    beforeEach(async () => {
        fetchMock.enableMocks();
        await act(() => renderWithProviders(<SetupConfig />));
    });

    afterEach(() => {
        fetchMock.resetMocks();
        cleanup();
    });

    test('Given path to convert cases input is clicked and folder is selected input should not be empty  ', async () => {
        const testPath = 'os/test/path'
        setupMockResponses({
            verifyPathResponse: getDefaultVerifyPathResponse(testPath)
        })
        await selectFileInFileTree('test-id-input-button-pathToConvertedCases', true)

        await waitFor(() => {
            const inputFile = screen.getByTestId('test-id-input-pathToConvertedCases').querySelector('input');
            expect(inputFile).toHaveAttribute('value', testPath);
        });
    });

    test('Hexagon is rendered with correct default styles', async () => {
        const { getByTestId } = render(
            <ThemeProvider theme={theme}>
                <Hexagon data-testid={'hexagon'} title={'title'} />
            </ThemeProvider>)
        const hex = getByTestId('hexagon')
        const hexContent = getByTestId('hexagon-content')

        expect(hex).toBeInTheDocument()
        expect(hex).toHaveStyle(`background: #000`)
        expect(hexContent).toHaveStyle(`background: #fff`)
        expect(hexContent).toHaveStyle(`color: ${theme.palette.primary.main}`)
    });

    test('Hexagon is marked as selected', async () => {
        const { getByTestId } = render(
            <ThemeProvider theme={theme}>
                <Hexagon data-testid={'hexagon'} title={'title'} selected />
            </ThemeProvider>)
        const hex = getByTestId('hexagon')
        const hexContent = getByTestId('hexagon-content')

        expect(hex).toBeInTheDocument()
        expect(hex).toHaveStyle(`background: ${theme.palette.primary.contrastText}`)
        expect(hexContent).toHaveStyle(`background: ${theme.palette.primary.main}`)
        expect(hexContent).toHaveStyle(`color: ${theme.palette.primary.contrastText}`)
    });

    test('Hexagon is marked as disabled', async () => {
        const { getByTestId } = render(
            <ThemeProvider theme={theme}>
                <Hexagon data-testid={'hexagon'} title={'title'} disabled />
            </ThemeProvider>)
        const hex = getByTestId('hexagon')
        const hexContent = getByTestId('hexagon-content')

        expect(hex).toBeInTheDocument()
        expect(hex).toHaveStyle(`background: #444`)
        expect(hexContent).toHaveStyle(`background: #ddd`)
        expect(hexContent).toHaveStyle(`color: ${theme.palette.primary.main}`)
    });
});