/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { System } from "../components/interfaces/SystemEditor/systemEditor.d";
import { Connection, SystemUI, ComponentUI } from "opGUI-client-frontend";
import {Connections} from "../components/interfaces/SystemEditor/systemEditor.d";
import {systemToBackend, systemToFront} from "../utils/apiConverters.ts";

const frontEndComponents: ComponentUI[] = [
    {
        id: 0,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 0',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        outputs: [{
            id: 0,
            title: 'Output 0',
            type: 'type',
            unit: 'seconds',
        }]
    },
    {
        id: 1,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 1',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        inputs: [
            {
                id: 0,
                title: 'Input 0',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            },
            {
                id: 1,
                title: 'Input 1',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            }
        ],
        outputs: [{
            id: 0,
            title: 'Output 0',
            type: 'type',
            unit: 'seconds',
        }]
    },
    {
        id: 2,
        type: 'Sensor',
        library: 'ComponentLibrary',
        title: 'Component Library 2',
        schedule: { offset: 0, cycle: 0, response: 0, priority: 0},
        inputs: [
            {
                id: 0,
                title: 'Input 0',
                type: 'type',
                unit: 'seconds',
                cardinality: 0
            },
        ],
    },
]

// Frontend
const frontEndConnectionsUnordered = (isOrdered: boolean): Connections[] => [
    {
        id: isOrdered ? 0: 24,
        source: {
            component: 0,
            output: 0
        },
        target: {
            component: 1,
            input: 0,
        },
    },
    {
        id: isOrdered ? 1: 16,
        source: {
            component: 0,
            output: 0
        },
        target: {
            component: 1,
            input: 1,
        }
    },
    {
        id: isOrdered ? 2 : 56,
        source: {
            component: 1,
            output: 0
        },
        target: {
            component: 2,
            input: 0,
        }
    },
]

const frontEndSystemUnordered: System = {
    id: 0,
    path: '',
    title: 'Title',
    priority: 0,
    anchorEl: null,
    components: frontEndComponents,
    connections: frontEndConnectionsUnordered(false),
    comments: [],
    file: '',
}

const frontEndSystemOrdered: System = {
    id: 0,
    title: 'Title',
    priority: 0,
    file: '',
    comments: [],
    components: frontEndComponents,
    connections: frontEndConnectionsUnordered(true),
    path: '',
    anchorEl: null,
}

// Backend
const backendConnections: Connection[] = [
    {
        id: 0,
        source: {
            component: 0,
            output: 0
        },
        targets: [
            {
                component: 1,
                input: 0
            },
            {
                component: 1,
                input: 1
            },
        ]
    },
    {
        id: 1,
        source: {
            component: 1,
            output: 0
        },
        targets: [
            {
                component: 2,
                input: 0
            }
        ]
    }
]

const backendSystem: SystemUI = {
    id: 0,
    title: 'Title',
    priority: 0,
    file: '',
    comments: [],
    components: frontEndComponents,
    connections: backendConnections,
}

describe('Testing Api Converter for systems', () => {
    it('Converts System FRONT with unordered connection into system BACK with ordered connections', () => {
        const backendSystemConverted = systemToBackend(frontEndSystemUnordered)
        expect(JSON.stringify(backendSystemConverted)).toBe(JSON.stringify(backendSystem))
    });
    it('Converts System BACK into system FRONT with ordered connections', () => {
        const frontendSystemConverted = systemToFront(backendSystem)
        expect(JSON.stringify(frontendSystemConverted)).toBe(JSON.stringify(frontEndSystemOrdered))
    });
})