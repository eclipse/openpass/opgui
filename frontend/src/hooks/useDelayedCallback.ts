/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import { useState, useEffect, useRef, Dispatch, SetStateAction } from 'react';

const useDelayedCallback = <T>(
    callback: (value: T) => void,
    delay: number
): Dispatch<SetStateAction<T>> => {
    const [value, setValue] = useState<T | null>(null);
    const timeoutRef = useRef<number | null>(null);

    useEffect(() => {
        if (timeoutRef.current !== null) {
            clearTimeout(timeoutRef.current);
        }

        timeoutRef.current = window.setTimeout(() => {
            if (value !== null) {
                callback(value);
            }
        }, delay);

        return () => {
            if (timeoutRef.current !== null) {
                clearTimeout(timeoutRef.current);
            }
        };
    }, [value, callback, delay]);

    const delayedSetter: Dispatch<SetStateAction<T>> = (newValue: SetStateAction<T>) => {
        setValue((prevValue) => {
            const newValueToSet = typeof newValue === 'function' ? (newValue as (prevState: T | null) => T | null)(prevValue) : newValue;
            return newValueToSet as T; // Ensure the type is correct
        });
    };

    return delayedSetter;
}

  export default useDelayedCallback
