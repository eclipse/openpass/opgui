"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertToConfigsApi = void 0;
const runtime = require("../runtime");
const models_1 = require("../models");
/**
 *
 */
class ConvertToConfigsApi extends runtime.BaseAPI {
    /**
     * send the IDs to convert into EXAMPLEID/configs for each selected experiment and also send the system paths that will be used
     * convert the selected PCM cases into configuration folders
     */
    apiConvertToConfigsPostRaw(requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.configsRequest === null || requestParameters.configsRequest === undefined) {
                throw new runtime.RequiredError('configsRequest', 'Required parameter requestParameters.configsRequest was null or undefined when calling apiConvertToConfigsPost.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            const response = yield this.request({
                path: `/api/convertToConfigs`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: (0, models_1.ConfigsRequestToJSON)(requestParameters.configsRequest),
            }, initOverrides);
            return new runtime.JSONApiResponse(response, (jsonValue) => (0, models_1.Default200ResponseFromJSON)(jsonValue));
        });
    }
    /**
     * send the IDs to convert into EXAMPLEID/configs for each selected experiment and also send the system paths that will be used
     * convert the selected PCM cases into configuration folders
     */
    apiConvertToConfigsPost(requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.apiConvertToConfigsPostRaw(requestParameters, initOverrides);
            return yield response.value();
        });
    }
}
exports.ConvertToConfigsApi = ConvertToConfigsApi;
