"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExportToSimulationApi = void 0;
const runtime = require("../runtime");
const models_1 = require("../models");
/**
 *
 */
class ExportToSimulationApi extends runtime.BaseAPI {
    /**
     * send the selected IDs to the pcm simulation manager and launch the conversion process, once this has finished all the results will be stored on the pcm-results folder
     * execute pcm simulation manager and store the results
     */
    apiExportToSimulationsPostRaw(requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.selectedExperimentsRequest === null || requestParameters.selectedExperimentsRequest === undefined) {
                throw new runtime.RequiredError('selectedExperimentsRequest', 'Required parameter requestParameters.selectedExperimentsRequest was null or undefined when calling apiExportToSimulationsPost.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            const response = yield this.request({
                path: `/api/exportToSimulations`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: (0, models_1.SelectedExperimentsRequestToJSON)(requestParameters.selectedExperimentsRequest),
            }, initOverrides);
            return new runtime.JSONApiResponse(response, (jsonValue) => (0, models_1.ExportedSimConfigSettingsFromJSON)(jsonValue));
        });
    }
    /**
     * send the selected IDs to the pcm simulation manager and launch the conversion process, once this has finished all the results will be stored on the pcm-results folder
     * execute pcm simulation manager and store the results
     */
    apiExportToSimulationsPost(requestParameters, initOverrides) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.apiExportToSimulationsPostRaw(requestParameters, initOverrides);
            return yield response.value();
        });
    }
}
exports.ExportToSimulationApi = ExportToSimulationApi;
