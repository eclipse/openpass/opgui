/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import type { Friction } from './Friction';
import {
    FrictionFromJSON,
    FrictionFromJSONTyped,
    FrictionToJSON,
} from './Friction';
import type { TimeOfDay } from './TimeOfDay';
import {
    TimeOfDayFromJSON,
    TimeOfDayFromJSONTyped,
    TimeOfDayToJSON,
} from './TimeOfDay';
import type { VisibilityDistance } from './VisibilityDistance';
import {
    VisibilityDistanceFromJSON,
    VisibilityDistanceFromJSONTyped,
    VisibilityDistanceToJSON,
} from './VisibilityDistance';
import type { Weather } from './Weather';
import {
    WeatherFromJSON,
    WeatherFromJSONTyped,
    WeatherToJSON,
} from './Weather';

/**
 * 
 * @export
 * @interface Environment
 */
export interface Environment {
    /**
     * 
     * @type {Array<TimeOfDay>}
     * @memberof Environment
     */
    timeOfDays?: Array<TimeOfDay>;
    /**
     * 
     * @type {Array<VisibilityDistance>}
     * @memberof Environment
     */
    visibilityDistances?: Array<VisibilityDistance>;
    /**
     * 
     * @type {Array<Friction>}
     * @memberof Environment
     */
    frictions?: Array<Friction>;
    /**
     * 
     * @type {Array<Weather>}
     * @memberof Environment
     */
    weathers?: Array<Weather>;
    /**
     * 
     * @type {string}
     * @memberof Environment
     */
    trafficRules?: string;
}

/**
 * Check if a given object implements the Environment interface.
 */
export function instanceOfEnvironment(value: object): boolean {
    let isInstance = true;

    return isInstance;
}

export function EnvironmentFromJSON(json: any): Environment {
    return EnvironmentFromJSONTyped(json, false);
}

export function EnvironmentFromJSONTyped(json: any, ignoreDiscriminator: boolean): Environment {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'timeOfDays': !exists(json, 'timeOfDays') ? undefined : ((json['timeOfDays'] as Array<any>).map(TimeOfDayFromJSON)),
        'visibilityDistances': !exists(json, 'visibilityDistances') ? undefined : ((json['visibilityDistances'] as Array<any>).map(VisibilityDistanceFromJSON)),
        'frictions': !exists(json, 'frictions') ? undefined : ((json['frictions'] as Array<any>).map(FrictionFromJSON)),
        'weathers': !exists(json, 'weathers') ? undefined : ((json['weathers'] as Array<any>).map(WeatherFromJSON)),
        'trafficRules': !exists(json, 'trafficRules') ? undefined : json['trafficRules'],
    };
}

export function EnvironmentToJSON(value?: Environment | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'timeOfDays': value.timeOfDays === undefined ? undefined : ((value.timeOfDays as Array<any>).map(TimeOfDayToJSON)),
        'visibilityDistances': value.visibilityDistances === undefined ? undefined : ((value.visibilityDistances as Array<any>).map(VisibilityDistanceToJSON)),
        'frictions': value.frictions === undefined ? undefined : ((value.frictions as Array<any>).map(FrictionToJSON)),
        'weathers': value.weathers === undefined ? undefined : ((value.weathers as Array<any>).map(WeatherToJSON)),
        'trafficRules': value.trafficRules,
    };
}

