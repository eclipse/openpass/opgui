#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

if [ $# -lt 2 ]; then
  echo "Usage: $0 <target artifact path> <files/dirs to add...>"
  exit 1
fi

ARTIFACT_PATH="$(readlink -f $1)"
shift

SRCFILES=("$@")

echo "Archiving into $ARTIFACT_PATH"
echo "Files/Directories to archive: ${SRCFILES[@]}"

# Verify files/directories existence
for item in "${SRCFILES[@]}"; do
    if [ -e "$item" ]; then
        echo "Found $item"
    else
        echo "Not found $item"
    fi
done

echo "Zip contents:"
zip -r -9 "$ARTIFACT_PATH" "${SRCFILES[@]}"