#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script just wraps the call to tar by adding common flags
################################################################################

echo "Input path: $1"
ARTIFACT_PATH="$(readlink -f $1)"
echo "Resolved artifact path: $ARTIFACT_PATH"

if [ -z "$ARTIFACT_PATH" ]; then
  echo "Error: Artifact path is empty."
  exit 1
fi

shift
SRCFILES=("$@")

echo "Archiving into $ARTIFACT_PATH: ${SRCFILES[@]}"
tar zcvf "$ARTIFACT_PATH" ${SRCFILES[@]}