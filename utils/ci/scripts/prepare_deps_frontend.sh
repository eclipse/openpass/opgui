#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Define URLs and file names
NODEJS_MSI_URL="https://nodejs.org/dist/v18.13.0/node-v18.13.0-x64.msi"
NODEJS_MSI_FILE="nodejs.msi"
YARN_MSI_URL="https://classic.yarnpkg.com/latest.msi"
YARN_MSI_FILE="yarn.msi"
NODE_DIR="/c/Program Files/nodejs"
YARN_DIR="/c/Program Files (x86)/Yarn/bin" 


# Add Node.js and Yarn to PATH
export PATH=$PATH:$NODE_PATH:$YARN_PATH

# Check for msiexec.exe
if [ ! -f "/c/Windows/System32/msiexec.exe" ]; then
    echo "msiexec.exe not found"
    exit 1
else
    echo "msiexec.exe found"
fi

# Function to download and install MSI packages
install_msi () {
    local url=$1
    local file=$2

    # Use PowerShell to download MSI
    powershell -Command "Invoke-WebRequest -Uri $url -OutFile $file"
    
    # Check if download was successful
    if [ ! -f "$file" ]; then
        echo "$file download failed"
        exit 1
    else
        echo "$file download succeeded"
    fi

   # Install MSI with PowerShell command split for readability
    powershell -Command "\$installResult = Start-Process -FilePath 'msiexec.exe' -ArgumentList '/qn /i $file' -Wait -PassThru; \
                          if (\$installResult.ExitCode -eq 0) { \
                              Write-Host '$file installation successful.' \
                          } else { \
                              Write-Host '$file installation failed with error code ' + \$installResult.ExitCode; \
                              exit 1 \
                          }"
}

# Function to check if the directory exists and add it to the PATH
add_to_path_if_exists() {
    local dir_path="$1"

    # Check if the directory exists
    if [ -d "$dir_path" ]; then
        echo "Directory found: $dir_path"
        # Add it to the PATH
        export PATH="$dir_path:$PATH"
        echo "Added $dir_path to PATH"
    else
        echo "Directory not found: $dir_path"
    fi
}


# Install Node.js
# included in windows machine
# install_msi $NODEJS_MSI_URL $NODEJS_MSI_FILE

# Install Yarn
#install_msi $YARN_MSI_URL $YARN_MSI_FILE

# Add directories to PATH if they exist
add_to_path_if_exists "$NODE_DIR"
add_to_path_if_exists "$YARN_DIR"

# Now you can directly check versions to verify
node --version
yarn --version