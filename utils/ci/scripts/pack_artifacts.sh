#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Script to pack the backend artifacts

MYDIR="$(dirname "$(readlink -f $0)")"
BUILD_DIR="$MYDIR/../../../backend/build"

# Navigate to the build directory
cd "$BUILD_DIR" || exit 1
echo "Current directory: $(pwd)"

# Name and dir of the artifact
ARTIFACT_NAME="opGUI"
ARTIFACT_DIR="$MYDIR/../../../../artifacts"

# Check if the artifacts directory exists and create if it does not
if [ ! -d "$ARTIFACT_DIR" ]; then
  echo "Creating artifact directory: $ARTIFACT_DIR"
  mkdir -p "$ARTIFACT_DIR"
fi

if [[ "${OSTYPE}" = "msys" ]]; then
    EXECUTABLE_NAME="OPGUICore.exe"
    ARTIFACT_PATH="$ARTIFACT_DIR/${ARTIFACT_NAME}.zip"
else
    EXECUTABLE_NAME="OPGUICore"
    ARTIFACT_PATH="$ARTIFACT_DIR/${ARTIFACT_NAME}.tar.gz"
fi

# Print the complete path where the artifact will be stored
echo "Artifact will be stored as folder: $ARTIFACT_PATH"

# Initial checks
echo "Checking if files/folders to be packed exist:"
[[ -d "dist" ]] && echo "dist directory exists" || echo "dist directory does not exist"
[[ -d "doc/html" ]] && echo "doc/html directory exists" || echo "doc/html directory does not exist"
[[ -f "$EXECUTABLE_NAME" ]] && echo "$EXECUTABLE_NAME exists" || echo "$EXECUTABLE_NAME does not exist"
[[ -f "../doc/pdf/opGUI.pdf" ]] && echo "../doc/pdf/opGUI.pdf file exists" || echo "../doc/pdf/opGUI.pdf file does not exist"
[[ -f "../../backendConfig.json" ]] && echo "../../backendConfig.json file exists" || echo "../../backendConfig.json file does not exist"

# Create a staging directory for packaging
echo "Creating staging directory: $MYDIR/staging"
mkdir -p "$MYDIR/staging"
echo "Staging directory created."

# Copy exe file
echo "Copying executable and dist folder to staging directory."
cp -R "$EXECUTABLE_NAME" "dist" "$MYDIR/staging"
echo "Executable and dist folder copied."

# Ensure html and pdf directories exist within doc in staging
echo "Creating html and pdf directories in staging/doc."
mkdir -p "$MYDIR/staging/doc/html"
mkdir -p "$MYDIR/staging/doc/pdf"
echo "html and pdf directories created."

# Copy docu related files
echo "Copying HTML documentation to staging/doc/html."
cp -R "doc/html/"* "$MYDIR/staging/doc/html/"
echo "HTML documentation copied."

echo "Copying PDF documentation to staging/doc/pdf."
cp "../doc/pdf/opGUI.pdf" "$MYDIR/staging/doc/pdf/"
echo "PDF documentation copied."

# Copy config files
echo "Copying backendConfig.json to staging."
cp "../../backendConfig.json" "$MYDIR/staging/backendConfig.json"
echo "backendConfig.json copied."

# Verify files and directories in staging
echo "Verifying staging directory contents:"
ls -R "$MYDIR/staging"

# Change directory to staging
echo "Changing directory to staging: $MYDIR/staging"
cd "$MYDIR/staging"
echo "Current directory: $(pwd)"

# Pack items into a zip or tar file
if [[ "${OSTYPE}" = "msys" ]]; then
  echo "Creating zip archive."
  $MYDIR/util_zip.sh "$ARTIFACT_PATH" "$EXECUTABLE_NAME" "dist" "backendConfig.json" "doc"
else
  echo "Creating tar archive."
  $MYDIR/util_tar.sh "$ARTIFACT_PATH" "$EXECUTABLE_NAME" "dist" "backendConfig.json" "doc"
fi

echo "Packaging complete."

# Clean up the staging directory
cd ..
rm -rf "$MYDIR/staging"

# Check if the artifact has been created and report
if [ -f "$ARTIFACT_PATH" ]; then
  echo "Artifact created successfully."
else
  echo "Failed to create artifact."
fi
