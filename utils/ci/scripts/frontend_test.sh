#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Determine the script's directory
NODE_PATH="/c/Program Files/nodejs"
YARN_PATH="/c/Program Files (x86)/Yarn/bin"

# Add Node.js and Yarn to the PATH
export PATH="$NODE_PATH:$YARN_PATH:$PATH"

MYDIR="$(dirname "$(readlink -f "$0")")"

# Navigate up three directories to get to the project root
DIR_PROJECT="$MYDIR/../../.."

# Function to handle command failure
handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

# Navigate back to the frontend directory to build the frontend
cd "$DIR_PROJECT/frontend" || handle_failure "Failed to change directory to frontend"

yarn test || handle_failure "Frontend test failed"
