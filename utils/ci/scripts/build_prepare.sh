#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the dependencies for building and testing
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR" || exit 1
echo The dir is $MYDIR

# export CCACHE_DIR=$MYDIR/../../../../ccache

for SCRIPT in prepare_system.sh prepare_deps_backend.sh prepare_deps_frontend.sh configure.sh; do
  echo
  echo "======================================================================="
  echo "Executing ${SCRIPT}..."
  echo "======================================================================="
  echo

  ./$SCRIPT || exit 1
done