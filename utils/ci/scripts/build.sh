#!/bin/bash

################################################################################
# Copyright (c) 2024 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Determine the script's directory
NODE_PATH="/c/Program Files/nodejs"
YARN_PATH="/c/Program Files (x86)/Yarn/bin"

# Add Node.js and Yarn to the PATH
export PATH="$NODE_PATH:$YARN_PATH:$PATH"

MYDIR="$(dirname "$(readlink -f "$0")")"

# Navigate up three directories to get to the project root
DIR_PROJECT="$MYDIR/../../.."

# Function to handle command failure
handle_failure() {
    echo "Error: $1"  # $1 can be an exit code or a message
    exit ${2:-1}  # Exit with the provided code, or default to 1
}

# Attempt to navigate to directories or exit with a message
cd "$DIR_PROJECT/frontend/openAPI/generateApiClientFrontend" || handle_failure "Failed to change directory to generateApiClientFrontend"

yarn install && yarn build || handle_failure "Yarn install or build failed in generateApiClientFrontend"

# Navigate back to the frontend directory to build the frontend
cd "$DIR_PROJECT/frontend" || handle_failure "Failed to change directory to frontend"

yarn install && yarn build || handle_failure "Yarn install or build failed in frontend"

# Copy the dist folder into the backend build folder
cp -r dist "$DIR_PROJECT/backend/build" || handle_failure "Failed to copy dist folder into backend build"

#Run cmake at the project root directory
cd "$DIR_PROJECT/backend/build" || handle_failure "Failed to change directory to backend build"

if hash nproc 2>/dev/null; then
  # calculation is kept for reference
  MAKE_JOB_COUNT=$(($(nproc)/4))
else
  # fallback, if nproc doesn't exist
  MAKE_JOB_COUNT=1
fi

make -j$MAKE_JOB_COUNT

# Print the contents of the project directory
echo "Listing contents of the project directory in build script:"
ls "$DIR_PROJECT"

# Print the contents of the project directory
echo "Listing contents of the backend directory in build script::"
ls "$DIR_PROJECT/backend"

# Navigate to the backend build directory
cd "$DIR_PROJECT/backend/build" || handle_failure "Failed to change directory to backend build"
echo "Changed to backend build directory in build script:."

# List the contents of the backend build directory
echo "Listing contents of backend build directory in build script::"
ls -la
