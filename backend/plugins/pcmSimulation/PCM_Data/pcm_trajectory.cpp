/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "pcm_trajectory.h"
#include <cmath>

PCM_Trajectory::PCM_Trajectory(std::vector<double> *timeVec, std::vector<double> *xPosVec, std::vector<double> *yPosVec, std::vector<double> *uVelVec, std::vector<double> *vVelVec, std::vector<double> *psiVec) :
    timeVec(timeVec),
    xPosVec(xPosVec),
    yPosVec(yPosVec),
    uVelVec(uVelVec),
    vVelVec(vVelVec),
    psiVec(psiVec)
{
}

PCM_Trajectory::~PCM_Trajectory()
{
    Clear();
}

double PCM_Trajectory::GetEndTime() const
{
    return timeVec->back();
}

double PCM_Trajectory::GetStartVelocity() const
{
    return uVelVec->front();
}

std::vector<double> *PCM_Trajectory::GetTimeVec() const
{
    return timeVec;
}

std::vector<double> *PCM_Trajectory::GetXPosVec() const
{
    return xPosVec;
}

std::vector<double> *PCM_Trajectory::GetYPosVec() const
{
    return yPosVec;
}

std::vector<double> *PCM_Trajectory::GetUVelVec() const
{
    return uVelVec;
}

std::vector<double> *PCM_Trajectory::GetVVelVec() const
{
    return vVelVec;
}

std::vector<double> *PCM_Trajectory::GetPsiVec() const
{
    return psiVec;
}

void PCM_Trajectory::ShiftForward(double distance) const
{
    for (unsigned int i = 0; i < timeVec->size(); ++i)
    {
        xPosVec->at(i) += distance * std::cos(psiVec->at(i));
        yPosVec->at(i) += distance * std::sin(psiVec->at(i));
    }
}

void PCM_Trajectory::Clear()
{
    if (timeVec != nullptr)
    {
        delete timeVec ;
        timeVec = nullptr;
    }
    if (xPosVec != nullptr)
    {
        delete xPosVec ;
        xPosVec = nullptr;
    }
    if (yPosVec != nullptr)
    {
        delete yPosVec ;
        yPosVec = nullptr;
    }
    if (uVelVec != nullptr)
    {
        delete uVelVec ;
        uVelVec = nullptr;
    }
    if (vVelVec != nullptr)
    {
        delete vVelVec ;
        vVelVec = nullptr;
    }
    if (psiVec != nullptr)
    {
        delete psiVec ;
        psiVec = nullptr;
    }
}
