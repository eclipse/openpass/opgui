/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef DATABASEREADER_H
#define DATABASEREADER_H

#include <iostream>
#include <memory>

#include <QStringList>
#include <QtSql>

#include "pcm_definitions.h"
#include "pcm_simulationSet.h"

class DatabaseReader
{
public:
    DatabaseReader();

    virtual ~DatabaseReader();

    bool SetDatabase(QString &dbName);

    bool OpenDataBase();
    bool CloseDataBase();
    bool IsDataBaseOpen();

    bool ReadCaseList(QStringList &caseList);

    PCM_SimulationSet *Read(const QString &pcmCase,QString &errorMsg);

    bool ReadParticipantData(const QString &pcmCase,
                             std::vector<PCM_ParticipantData *> &participants);
    bool ReadDynamicsData(const QString &pcmCase,
                          std::vector<PCM_InitialValues *> &initials);
    bool ReadTrajectoryData(const QString &pcmCase,
                            std::vector<PCM_Trajectory *> &trajectories);
private:
    QString databaseName = "";
    QSqlDatabase db;
    QString connection;
};

#endif // DATABASEREADER_H
