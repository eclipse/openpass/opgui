/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <QDebug>

#include "ConfigWriter.h"
#include "GUI_Definitions.h"
#include "helpers.h"



ConfigWriter::ConfigWriter(const QString &baseFolder)
{
    baseDirectory.setPath(baseFolder);
}

const QString ConfigWriter::CreateSimulationConfiguration(const QString &configPath,
                                                     const PCM_SimulationSet *simSet,
                                                     const QString &resultFolderName,
                                                     const QString &pcmCase,
                                                     const int randomSeed)
{

    const std::vector<PCM_Trajectory *> &trajectories = simSet->GetTrajectories();
    
    XmlSimulationConfig runConfig(1, randomSeed);

    return WriteSimulationConfiguration(runConfig, configPath);
}

const QString ConfigWriter::CreateProfilesCatalog(const QString &configPath,
                                                  const PCM_SimulationSet *simSet,
                                                  const QString &resultFolderName,
                                                  const QString &pcmCase,
                                                  const int randomSeed)
{
    XmlProfilesConfig profilesConfig;

    const std::vector<PCM_ParticipantData *> &participants = simSet->GetParticipants();

    for (uint i = 0; i < participants.size(); i++)
    {
        profilesConfig.AddModelId(i);
    }

    return WriteProfilesCatalog(profilesConfig, configPath);
}

const QString ConfigWriter::CreateModelsVehicle(const QString &configPath,
                                                const PCM_SimulationSet *simSet,
                                                const QString &resultFolderName,
                                                const QString &pcmCase,
                                                const int randomSeed)
{

    XmlModelsConfig modelsConfig;

    const std::vector<PCM_ParticipantData *> &participants = simSet->GetParticipants();

    for (uint i = 0; i < participants.size(); i++)
    {
        modelsConfig.AddAgent(i, i, participants.at(i));
    }

    return WriteModelsVehicle(modelsConfig, configPath);
}

const QString ConfigWriter::CreateModelsPedestrian(const QString &configPath,
                                                const PCM_SimulationSet *simSet,
                                                const QString &resultFolderName,
                                                const QString &pcmCase,
                                                const int randomSeed)
{

    XmlModelsConfig modelsConfig;

    const std::vector<PCM_ParticipantData *> &participants = simSet->GetParticipants();

    for (uint i = 0; i < participants.size(); i++)
    {
        modelsConfig.AddAgent(i, i, participants.at(i));
    }

    return WriteModelsPedestrian(modelsConfig, configPath);
}

const QString ConfigWriter::CreateSystemConfiguration(const QString &caseOutputFolder,
                                                      const QString &otherSystemFile,
                                                      const QString &car1SystemFile,
                                                      const QString &car2SystemFile,
                                                      const PCM_SimulationSet *simSet)
{
    QString systemConfigFile = caseOutputFolder + "/" + FILENAME_SYSTEM_CONFIG;

    QString agent_OtherSystemFile = "Systems/agent_NoDynamic.xml";
    if (otherSystemFile != "")
    {
        agent_OtherSystemFile = otherSystemFile;
    }

    QString agent_Car1SystemFile = "Systems/agent_TwoTrackModel.xml";
    if (car1SystemFile != "")
    {
        agent_Car1SystemFile = car1SystemFile;
    }

    QString agent_Car2SystemFile = "Systems/agent_TwoTrackModel.xml";
    if (car2SystemFile != "")
    {
        agent_Car2SystemFile = car2SystemFile;
    }
    else
    {
        agent_Car2SystemFile = agent_Car1SystemFile;
    }

    const std::vector<PCM_ParticipantData *> &participants = simSet->GetParticipants();

    if (participants.size() < 1)
    {
        return "";
    }

    if (QFile::exists(systemConfigFile))
    {
        QFile::remove(systemConfigFile);
    }

    QString firstAgentFile;
    bool firstAgentFileIsSet = false; //only the first agent which is a car will get agent_Car1SystemFile

    if ((participants.at(0)->GetType() == "car") || (participants.at(0)->GetType() == "truck"))
    {
        firstAgentFile = agent_Car1SystemFile;
        firstAgentFileIsSet = true;
    }
    else
    {
        firstAgentFile = agent_OtherSystemFile;
    }

    QDomDocument systemDocument;
    if (!XmlMergeHelper::loadDomDocument(firstAgentFile, systemDocument))
    {
        return "";
    }

    QDomElement systemRoot = systemDocument.documentElement();
    XmlMergeHelper::setIdOfSystem(systemRoot, 0);

    for (size_t i = 1; i < participants.size(); i++)
    {
        QString agentFile;
        if ((participants.at(i)->GetType() == "car") || (participants.at(i)->GetType() == "truck"))
        {
            if (firstAgentFileIsSet)
            {
                agentFile = agent_Car2SystemFile;
            }
            else
            {
                agentFile = agent_Car1SystemFile;
            }
        }
        else
        {
            agentFile = agent_OtherSystemFile;
        }

        QDomDocument agentDocument;
        if (!XmlMergeHelper::loadDomDocument(agentFile, agentDocument))
        {
            return "";
        }
        QDomElement agentRoot = agentDocument.documentElement();
        XmlMergeHelper::setIdOfSystem(agentRoot, i);


        systemRoot.appendChild(agentRoot.firstChildElement("system"));

    }

    std::locale::global(std::locale("C"));

    QFile systemXmlFile(systemConfigFile); // automatic object will be closed on destruction
    if (!systemXmlFile.open(QIODevice::WriteOnly))
    {
        std::cout << "Error (ConfigGenerator): could not open xmlFile: " << systemConfigFile.toStdString()
                  <<
                  std::endl;
        return "";
    }

    QTextStream stream(&systemXmlFile);
    stream << systemDocument.toString();

    systemXmlFile.close();
    return systemConfigFile;
}

const QString ConfigWriter::CreateParkingConfiguration(const QString &configPath)
{
    return WriteParkingConfiguration(configPath);
}

const QString ConfigWriter::CreateFrameworkConfiguration(const QString frameworkConfigPath,
                                                         QList<QMap<QString, QString> > configList,
                                                         const int logLevel, const QString originalOpSimExe, const QString originalLibraries)
{
    QString frameworkConfigFile = frameworkConfigPath + "/" + FILENAME_FRAMEWORK_CONFIG;
    QFile file(frameworkConfigFile);

    QString libraries = Helpers::normalizePathToUnix(originalLibraries);
    QString opSimExe = Helpers::normalizePathToUnix(originalOpSimExe);

    // open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): "
                  "could not open opSimulationManager.xml" << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("opSimulationManager");
    xmlWriter.writeTextElement("logLevel", QString::number(logLevel));

    xmlWriter.writeTextElement("simulation",opSimExe);
    
    QString SUBDIR_LOG = frameworkConfigPath + "/" + FILENAME_OPSIMULATIONMANAGER_LOG;
    xmlWriter.writeTextElement("logFileSimulationManager", SUBDIR_LOG);

    xmlWriter.writeTextElement("libraries", !libraries.isEmpty() ? libraries : QString(SUBDIR_LIB_MODULES));

    xmlWriter.writeStartElement("simulationConfigs");

    for (QMap<QString, QString> configSet : configList)
    {
        xmlWriter.writeStartElement("simulationConfig");
        QMapIterator<QString, QString> configSetIterator(configSet);
        while (configSetIterator.hasNext())
        {
            configSetIterator.next();
            xmlWriter.writeTextElement(configSetIterator.key(), configSetIterator.value());
        }
        xmlWriter.writeEndElement(); // simulationConfig
    }

    xmlWriter.writeEndElement(); // simulationConfigs
    xmlWriter.writeEndElement(); // opSimulationManager

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    return frameworkConfigFile;
}

const QString ConfigWriter::WriteSimulationConfiguration(XmlSimulationConfig &simulationConfig,
                                                    const QString &configPath)
{
    // Create the xml with the chosen cases
    QString simulationConfigFile = configPath + "/" + FILENAME_SIMULATION_CONFIG;
    QFile file(simulationConfigFile);

    // open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): could not open "
                  << FILENAME_SIMULATION_CONFIG << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("simulationConfig");

    xmlWriter.writeAttribute("SchemaVersion","0.8.2");

    bool success = simulationConfig.WriteToXml(&xmlWriter);

        
    xmlWriter.writeEndElement(); //SimulationConfig

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    if (success)
    {
        return simulationConfigFile;
    }
    else
    {
        return "";
    }
}

const QString ConfigWriter::WriteProfilesCatalog(XmlProfilesConfig &profilesConfig,
                                                 const QString &configPath)
{
    // Create the xml with the chosen cases
    QString profilesConfigFile = configPath + "/" + FILENAME_PROFILES_CONFIG;
    QFile file(profilesConfigFile);

    // open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): could not open "
                  << FILENAME_PROFILES_CONFIG << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("ProfilesCatalog");

    xmlWriter.writeAttribute("SchemaVersion","0.6.0");
    

    bool success = profilesConfig.WriteToXml(&xmlWriter);

    xmlWriter.writeEndElement(); //Profiles

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    if (success)
    {
        return profilesConfigFile;
    }
    else
    {
        return "";
    }
}

const QString ConfigWriter::WriteModelsVehicle(XmlModelsConfig &modelsConfig,
                                               const QString &configPath)
{    

    QString vehicleDirPath = configPath + "/Vehicles";
    QString vehiclesConfigFile = vehicleDirPath + "/" + VEHICLE_MODELS_CONFIG;

    // Check if the directory exists
    QDir vehicleDir(vehicleDirPath);
    if (!vehicleDir.exists()) {
        std::cout << "Error: The directory does not exist - " << vehicleDirPath.toStdString() << std::endl;
        return "";
    }
    //

    QFile file(vehiclesConfigFile);

    // open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): could not open "
                  << VEHICLE_MODELS_CONFIG << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("OpenSCENARIO");

    bool success = modelsConfig.WriteVehiclesToXml(&xmlWriter);

    xmlWriter.writeEndElement(); //OpenSCENARIO

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    if (success)
    {
        return vehiclesConfigFile;
    }
    else
    {
        return "";
    }
}

const QString ConfigWriter::WriteModelsPedestrian(XmlModelsConfig &modelsConfig,
                                                  const QString &configPath)
{    

    QString pedestriansDirPath = configPath + "/Vehicles";
    QString pedestriansConfigFile = pedestriansDirPath + "/" + PEDESTRIAN_MODELS_CONFIG;

    // Check if the directory exists
    QDir pedestrianDir(pedestriansDirPath);
    if (!pedestrianDir.exists()) {
        std::cout << "Error: The directory does not exist - " << pedestriansDirPath.toStdString() << std::endl;
        return "";
    }
    //

    QFile file(pedestriansConfigFile);

    // open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): could not open "
                  << PEDESTRIAN_MODELS_CONFIG << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("OpenSCENARIO");

    bool success = modelsConfig.WritePedestriansToXml(&xmlWriter);

    xmlWriter.writeEndElement(); //OpenSCENARIO

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    if (success)
    {
        return pedestriansConfigFile;
    }
    else
    {
        return "";
    }
}

const QString ConfigWriter::WriteParkingConfiguration(const QString &configPath)
{
    
    // write the xml agent file
    QString sceneryConfigFile = configPath + "/" + FILENAME_PARKING_CONFIG;
    QFile file(sceneryConfigFile);
    //open file
    if (!file.open(QIODevice::WriteOnly))
    {
        // show error message if not able to open file
        std::cout << "Error (ConfigGenerator): could not open "
                  << FILENAME_PARKING_CONFIG << std::endl;
        return "";
    }

    // if file is successfully opened, create XML
    QXmlStreamWriter xmlWriter(&file);

    xmlWriter.setAutoFormatting(true);

    xmlWriter.writeStartDocument();

    QXmlStreamAttributes attrib;
    attrib.append("name","Parking");
    attrib.append("north","10000");
    attrib.append("east","10000");
    attrib.append("south","-10000");
    attrib.append("west","-10000");
    attrib.append("revMajor","1");
    attrib.append("revMinor","1");
    attrib.append("date","2020-01-01T00:00:00");
    attrib.append("version","1");

    xmlWriter.writeStartElement("OpenDRIVE");

    xmlWriter.writeStartElement("header");
    xmlWriter.writeAttributes(attrib);
    xmlWriter.writeEndElement(); // header

    xmlWriter.writeStartElement("road");
    xmlWriter.writeAttribute("name","unnamed");
    xmlWriter.writeAttribute("junction","-1");
    xmlWriter.writeAttribute("length","10000");
    xmlWriter.writeAttribute("id","1");
    xmlWriter.writeEmptyElement("link");
    xmlWriter.writeStartElement("planView");
    xmlWriter.writeStartElement("geometry");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeAttribute("hdg","0");
    xmlWriter.writeAttribute("x","-5000");
    xmlWriter.writeAttribute("y","5000");
    xmlWriter.writeAttribute("length","10000");
    xmlWriter.writeEmptyElement("line");
    xmlWriter.writeEndElement(); // geometry
    xmlWriter.writeEndElement(); // planView
    xmlWriter.writeStartElement("elevationProfile");
    xmlWriter.writeStartElement("elevation");
    xmlWriter.writeAttribute("a","0");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeAttribute("b","0");
    xmlWriter.writeAttribute("c","0");
    xmlWriter.writeAttribute("d","0");
    xmlWriter.writeEndElement(); // elevation
    xmlWriter.writeEndElement(); // elevationProfile
    xmlWriter.writeStartElement("lateralProfile");
    xmlWriter.writeStartElement("superelevation");
    xmlWriter.writeAttribute("a","0");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeAttribute("b","0");
    xmlWriter.writeAttribute("c","0");
    xmlWriter.writeAttribute("d","0");
    xmlWriter.writeEndElement(); // superelevation
    xmlWriter.writeStartElement("crossfall");
    xmlWriter.writeAttribute("side","both");
    xmlWriter.writeAttribute("a","0");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeAttribute("b","0");
    xmlWriter.writeAttribute("c","0");
    xmlWriter.writeAttribute("d","0");
    xmlWriter.writeEndElement(); // crossfall
    xmlWriter.writeEndElement(); // lateralProfile


    xmlWriter.writeStartElement("lanes");
    xmlWriter.writeStartElement("laneSection");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeStartElement("right");
    xmlWriter.writeStartElement("lane");
    xmlWriter.writeAttribute("level","1");
    xmlWriter.writeAttribute("type","driving");
    xmlWriter.writeAttribute("id","-2");
    xmlWriter.writeStartElement("width");
    xmlWriter.writeAttribute("a","10000");
    xmlWriter.writeAttribute("b","0");
    xmlWriter.writeAttribute("c","0");
    xmlWriter.writeAttribute("d","0");
    xmlWriter.writeAttribute("sOffset","0");
    xmlWriter.writeEndElement(); // width
    xmlWriter.writeStartElement("roadMark");
    xmlWriter.writeAttribute("color","standard");
    xmlWriter.writeAttribute("width","0.12");
    xmlWriter.writeAttribute("weight","standard");
    xmlWriter.writeAttribute("sOffset","0");
    xmlWriter.writeAttribute("type","broken");
    xmlWriter.writeAttribute("laneChange","both");
    xmlWriter.writeEndElement(); // roadMark
    xmlWriter.writeEndElement(); // lane
    xmlWriter.writeStartElement("lane");
    xmlWriter.writeAttribute("level","1");
    xmlWriter.writeAttribute("type","driving");
    xmlWriter.writeAttribute("id","-1");
    xmlWriter.writeStartElement("width");
    xmlWriter.writeAttribute("a","0");
    xmlWriter.writeAttribute("b","0");
    xmlWriter.writeAttribute("c","0");
    xmlWriter.writeAttribute("d","0");
    xmlWriter.writeAttribute("sOffset","0");
    xmlWriter.writeEndElement(); // width
    xmlWriter.writeStartElement("roadMark");
    xmlWriter.writeAttribute("color","standard");
    xmlWriter.writeAttribute("width","0.12");
    xmlWriter.writeAttribute("weight","standard");
    xmlWriter.writeAttribute("sOffset","0");
    xmlWriter.writeAttribute("type","broken");
    xmlWriter.writeAttribute("laneChange","both");
    xmlWriter.writeEndElement(); // roadMark
    xmlWriter.writeEndElement(); // lane
    xmlWriter.writeEndElement(); // right
    xmlWriter.writeStartElement("center");
    xmlWriter.writeStartElement("lane");
    xmlWriter.writeAttribute("level","1");
    xmlWriter.writeAttribute("type","border");
    xmlWriter.writeAttribute("id","0");
    xmlWriter.writeStartElement("roadMark");
    xmlWriter.writeAttribute("color","standard");
    xmlWriter.writeAttribute("width","0.12");
    xmlWriter.writeAttribute("weight","standard");
    xmlWriter.writeAttribute("sOffset","0");
    xmlWriter.writeAttribute("type","solid");
    xmlWriter.writeAttribute("laneChange","both");
    xmlWriter.writeEndElement(); // roadMark
    xmlWriter.writeEndElement(); // lane
    xmlWriter.writeEndElement(); // center
    xmlWriter.writeEndElement(); // laneSection
    xmlWriter.writeEndElement(); // lanes

    xmlWriter.writeStartElement("type");
    xmlWriter.writeAttribute("s","0");
    xmlWriter.writeAttribute("type","unknown");
    xmlWriter.writeEndElement(); // type

    xmlWriter.writeEndElement(); // road

    xmlWriter.writeEndElement(); // OpenDRIVE

    xmlWriter.writeEndDocument();

    file.close();
    file.flush();

    return sceneryConfigFile;
}
