/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "DatabaseReaderPcm.h"

DatabaseReader::DatabaseReader()
{
}

DatabaseReader::~DatabaseReader()
{
    CloseDataBase();
}

bool DatabaseReader::SetDatabase(QString &dbName)
{
    if (!QFileInfo::exists(dbName))
    {
        databaseName = "";
        return false;
    }

    if (IsDataBaseOpen())
    {
        CloseDataBase();
    }

    databaseName = dbName;

    return true;
}

bool DatabaseReader::OpenDataBase()
{
    if (!QFileInfo::exists(databaseName))
    {
        std::cout<<"[ERROR]DB FILE NOT FOUND : "<<databaseName.toStdString()<<std::endl;
        return false;
    }

    bool success = true;

    if (IsDataBaseOpen())
    {
        CloseDataBase();
    }

    //SQLITE JEM
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(databaseName);
    connection = db.connectionName();

	success = db.open();

    // MDB
    //db = QSqlDatabase::addDatabase("QODBC");
    // QString dbString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ="
     //                 + databaseName;
    // db.setDatabaseName(dbString);
    //connection = db.connectionName();

    //success = db.open();
    if (!success)
    {
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(connection);
        connection = "";
        return false;
    }

    //add extra checks for db integrity and emptiness
    QFileInfo fileInfo(databaseName);
    if (fileInfo.size() == 0) {
        std::cout << "Database file is empty." << std::endl;
        return false;
    }

    QSqlQuery testQuery(db);
    if (!testQuery.exec("PRAGMA integrity_check")) {
        std::cout << "Database might be corrupt or not a valid SQLite database:" << testQuery.lastError().text().toStdString() << std::endl;
        return false;
    }

    return success;
}

bool DatabaseReader::CloseDataBase()
{
    if (IsDataBaseOpen())
    {
        db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(connection);
        connection = "";
    }

    return true;
}

bool DatabaseReader::IsDataBaseOpen()
{
    return db.isOpen();
}

bool DatabaseReader::ReadCaseList(QStringList &caseList)
{
    if (!IsDataBaseOpen())
    {
        return false;
    }

    // Read the Cases from the table "participant_data"
    QSqlQuery query;

    //query.exec("SELECT DISTINCT FALL FROM participant_data ORDER BY FALL ASC");

    //PCM 5.0 JEM
    query.exec("SELECT DISTINCT CASEID FROM participant_data ORDER BY CASEID ASC");

    while (query.next())
    {

        caseList.append(QString("%1").arg(query.value(0).toInt()));
    }

    query.clear();

    return true;
}

PCM_SimulationSet *DatabaseReader::Read(const QString &pcmCase,QString &errorMsg)
{
    std::vector<PCM_ParticipantData *> participants;
    std::vector<PCM_InitialValues *> initials;
    std::vector<PCM_Trajectory *> trajectories;

    if (!OpenDataBase())
    {
        errorMsg= "Could not open database.";
        std::cout << errorMsg.toStdString() << std::endl;
        return nullptr;
    }

    if (!ReadParticipantData(pcmCase,
                             participants))
    {
        errorMsg= "Could not read participant data.";
        std::cout << errorMsg.toStdString() << std::endl;
        return nullptr;
    }

    if (!ReadDynamicsData(pcmCase,
                          initials))
    {
        errorMsg= "Could not read dynamics data.";
        std::cout << errorMsg.toStdString() << std::endl;
        return nullptr;
    }

    if (!ReadTrajectoryData(pcmCase,
                            trajectories))
    {
        errorMsg= "Could not read trajectory data.";
        std::cout << errorMsg.toStdString() << std::endl;
        return nullptr;
    }
	// JEM PCM 5.0
    /*
    if (!ReadMarksData(pcmCase,
                       pcmData))
    {
        std::cout << "Error (ConfigGenerator): could not read marks data" << std::endl;
        return nullptr;
    }

    if (!ReadObjectsData(pcmCase,
                         pcmData))
    {
        std::cout << "Error (ConfigGenerator): could not read object data" << std::endl;
        return nullptr;
    }

    if (!ReadViewObjectsData(pcmCase,
                             pcmData))
    {
        std::cout << "Error (ConfigGenerator): could not read viewObject data" << std::endl;
        return nullptr;
    }

    if (!ReadIntendedCourseData(pcmCase,
                                pcmData))
    {
        std::cout << "Error (ConfigGenerator): could not read intendedCourses data" << std::endl;
        return nullptr;
    }

    if (!ReadGlobalData(pcmCase,
                        pcmData))
    {
        std::cout << "Error (ConfigGenerator): could not read intendedCourses data" << std::endl;
        return nullptr;
    }
	*/

    CloseDataBase();

   return new PCM_SimulationSet(participants,
                                 initials,
                                 trajectories);
}

bool DatabaseReader::ReadParticipantData(const QString &pcmCase,
                                         std::vector<PCM_ParticipantData *> &participants)
{
    if (!IsDataBaseOpen())
    {
        return false;
    }

    QSqlQuery query;
    //query.exec("SELECT TYPEPCM,WIDTH,LENGTH,DISTCGFA,WEIGHT,HEIGHTCG,WHEELBASE,IXX,IYY,IZZ,MUE,TRACKWIDTH,HEIGHT,CGFRONT FROM participant_data WHERE FALL = "
    //           + pcmCase);

    //JEM PCM 5.0
    query.exec("SELECT PARTTYPE,WIDTH,LENGTH,FRONTAXLEX,WEIGHT,COGZ,WHEELBASE,IXX,IYY,IZZ,TRACKWIDTH,HEIGHT,PARTID FROM participant_data WHERE CASEID = " + pcmCase);

    while (query.next())
    {
        //JEM PCM 5.0
        QSqlQuery query_shape;
        query_shape.exec("SELECT X FROM participant_shape WHERE CASEID = " + pcmCase +" AND PARTID =" + query.value(12).toString());

        double x_max = 0;
        while (query_shape.next())
        {
            if (query_shape.value(0).toDouble() > x_max)
                x_max = query_shape.value(0).toDouble();

        }

		//JEM PCM 5.0 MUE
		QSqlQuery query_mue;
        query_mue.exec("SELECT MUE FROM dynamics WHERE CASEID = " + pcmCase + " AND PARTID =" + query.value(12).toString());

		double mue = 0.9;
        while (query_mue.next())
        {
                mue = query_mue.value(0).toDouble();
            break;
        }



		/////////////////////////////////
        participants.push_back(new PCM_ParticipantData(query.value(0).toString(),
                                                       query.value(1).toString(),
                                                       query.value(2).toString(),
                                                       query.value(3).toString(),
                                                       query.value(4).toString(),
                                                       query.value(5).toString(),
                                                       query.value(6).toString(),
                                                       query.value(7).toString(),
                                                       query.value(8).toString(),
                                                       query.value(9).toString(),
                                                       QString::number(mue),
                                                       query.value(10).toString(),
                                                       query.value(11).toString(),
                                                       QString::number(x_max)));
    }

    query.clear();

    return !(participants.empty());
}

bool DatabaseReader::ReadDynamicsData(const QString &pcmCase,
                                      std::vector<PCM_InitialValues *> &initials)
{
    if (!IsDataBaseOpen())
    {
        return false;
    }

    QSqlQuery query;
    //query.exec("SELECT XPOS,YPOS,VX,VY,AX,AY,PSI FROM dynamics WHERE STEP = 0 AND FALL = " + pcmCase);

    // PCM 5.0 JEM
    query.exec("SELECT POSX,POSY,VX,VY,AX,AY,POSPSI FROM dynamics WHERE TIME = 0 AND CASEID =" + pcmCase);

    while (query.next())
    {
        initials.push_back(new PCM_InitialValues(query.value(0).toString(),
                                                 query.value(1).toString(),
                                                 query.value(2).toString(),
                                                 query.value(3).toString(),
                                                 query.value(4).toString(),
                                                 query.value(5).toString(),
                                                 query.value(6).toString()));
    }

    query.clear();

    return !(initials.empty());
}

bool DatabaseReader::ReadTrajectoryData(const QString &pcmCase,
                                        std::vector<PCM_Trajectory *> &trajectories)
{
    if (!IsDataBaseOpen())
    {
        return false;
    }

    QSqlQuery query;
    //query.exec("SELECT BETNR FROM dynamics WHERE FALL = " + pcmCase + " GROUP BY BETNR");

    //PCM 5.0 JEM
    query.exec("SELECT PARTID FROM dynamics WHERE CASEID =" + pcmCase + " GROUP BY PARTID");

    std::vector<QString> betNr;
    while (query.next())
    {
        betNr.push_back(query.value(0).toString());
        

    }

    query.clear();

    for (size_t i = 0; i < betNr.size(); i++)
    {
        //query.exec("SELECT STEP,XPOS,YPOS,VX,VY,PSI FROM dynamics WHERE BETNR = " + betNr.at(i) + "AND FALL = " + pcmCase + " ORDER BY STEP ASC");

		//PCM 5.0 JEM
        query.exec("SELECT TIME,POSX,POSY,VX,VY,POSPSI FROM dynamics WHERE PARTID = " + betNr.at(i) + " AND CASEID = " + pcmCase + " ORDER BY TIME ASC");


	

        std::vector<double> *timeVec = new std::vector<double>();
        std::vector<double> *xPosVec = new std::vector<double>();
        std::vector<double> *yPosVec = new std::vector<double>();
        std::vector<double> *uVelVec = new std::vector<double>();
        std::vector<double> *vVelVec = new std::vector<double>();
        std::vector<double> *psiVec = new std::vector<double>();

        while (query.next())
        {
            timeVec->push_back(query.value(0).toDouble());
            xPosVec->push_back(query.value(1).toDouble());
            yPosVec->push_back(query.value(2).toDouble());
            uVelVec->push_back(query.value(3).toDouble());
            vVelVec->push_back(query.value(4).toDouble());
            psiVec->push_back(query.value(5).toDouble());

				

        }

        trajectories.push_back(new PCM_Trajectory(timeVec,
                                                  xPosVec,
                                                  yPosVec,
                                                  uVelVec,
                                                  vVelVec,
                                                  psiVec));
    }

    query.clear();

    return !(trajectories.empty());
}
