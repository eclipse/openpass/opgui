/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "XmlAgent.h"

class XmlProfilesConfig: public XmlBaseClass
{
public:
    XmlProfilesConfig();

    ~XmlProfilesConfig();

    bool WriteToXml( QXmlStreamWriter *xmlWriter );

    void AddModelId(int id);

private:
    std::vector<int> modelIds;                 //!< vector of all model ids
};

