/********************************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef XMLSIMULATIONCONFIG_H
#define XMLSIMULATIONCONFIG_H

#include "XmlAgent.h"

class XmlSimulationConfig : public XmlBaseClass
{
public:
    XmlSimulationConfig(int nInvocations,int randomSeed);

    virtual ~XmlSimulationConfig();

    bool WriteToXml( QXmlStreamWriter *xmlWriter );

    void AddAgent(int id, int agentTypeRef, PCM_ParticipantData *participant);

private:
    int numberInvocations =
        0;     //!< number of repetitions of the same situation described with this file
    int randomSeed =
        -1;           //!< random seed of the situation for initialisation of non-deterministic modules
    QString worldLibrary = "World_OSI";//!< World_OSI.dll
    QString scenarioFile = "Scenario.xosc";//!< World_OSI.dll
   
    std::vector<XmlAgent>       agents;        //!< vector of all agents of the situation
};

#endif // XMLRUNCONFIG_H
