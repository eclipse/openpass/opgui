/********************************************************************************
 * Copyright (c) 2017-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "XmlAgent.h"

class XmlModelsConfig: public XmlBaseClass
{
public:
    XmlModelsConfig();

    ~XmlModelsConfig();

    bool WriteToXml( QXmlStreamWriter *xmlWriter );

    bool WriteVehiclesToXml( QXmlStreamWriter *xmlWriter );

    bool WritePedestriansToXml(QXmlStreamWriter *xmlWriter);

    void AddAgent(int id, int agentTypeRef, PCM_ParticipantData *participant);

private:
    std::vector<XmlAgent>       agents;        //!< vector of all agents of the situation
};

