/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIGetValueListApiRequest.h"

namespace OpenAPI {

OAIGetValueListApiRequest::OAIGetValueListApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIGetValueListApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIGetValueListApiRequest::~OAIGetValueListApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIGetValueListApiRequest::~OAIGetValueListApiRequest()";
}

QMap<QString, QString>
OAIGetValueListApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIGetValueListApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIGetValueListApiRequest::getRawSocket(){
    return socket;
}


void OAIGetValueListApiRequest::apiGetValueListGetRequest(){
    qDebug() << "/api/getValueList";
    connect(this, &OAIGetValueListApiRequest::apiGetValueListGet, handler.data(), &OAIGetValueListApiHandler::apiGetValueListGet);

    
    QString name;
    if(socket->queryString().keys().contains("name")){
        fromStringValue(socket->queryString().value("name"), name);
    }
    


    emit apiGetValueListGet(name);
}



void OAIGetValueListApiRequest::apiGetValueListGetResponse(const OAI_api_getValueList_get_200_response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetValueListApiRequest::apiGetValueListGetError(const OAI_api_getValueList_get_200_response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIGetValueListApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIGetValueListApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
