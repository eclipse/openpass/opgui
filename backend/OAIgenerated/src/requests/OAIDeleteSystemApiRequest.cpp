/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIHelpers.h"
#include "OAIDeleteSystemApiRequest.h"

namespace OpenAPI {

OAIDeleteSystemApiRequest::OAIDeleteSystemApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAIDeleteSystemApiHandler> hdl) : QObject(s), socket(s), handler(hdl) {
    auto headers = s->headers();
    for(auto itr = headers.begin(); itr != headers.end(); itr++) {
        requestHeaders.insert(QString(itr.key()), QString(itr.value()));
    }
}

OAIDeleteSystemApiRequest::~OAIDeleteSystemApiRequest(){
    disconnect(this, nullptr, nullptr, nullptr);
    qDebug() << "OAIDeleteSystemApiRequest::~OAIDeleteSystemApiRequest()";
}

QMap<QString, QString>
OAIDeleteSystemApiRequest::getRequestHeaders() const {
    return requestHeaders;
}

void OAIDeleteSystemApiRequest::setResponseHeaders(const QMultiMap<QString, QString>& headers){
    for(auto itr = headers.begin(); itr != headers.end(); ++itr) {
        responseHeaders.insert(itr.key(), itr.value());
    }
}


QHttpEngine::Socket* OAIDeleteSystemApiRequest::getRawSocket(){
    return socket;
}


void OAIDeleteSystemApiRequest::apiDeleteSystemDeleteRequest(){
    qDebug() << "/api/deleteSystem";
    connect(this, &OAIDeleteSystemApiRequest::apiDeleteSystemDelete, handler.data(), &OAIDeleteSystemApiHandler::apiDeleteSystemDelete);

    
    QString path;
    if(socket->queryString().keys().contains("path")){
        fromStringValue(socket->queryString().value("path"), path);
    }
    


    emit apiDeleteSystemDelete(path);
}



void OAIDeleteSystemApiRequest::apiDeleteSystemDeleteResponse(const OAIDefault200Response& res){
    setSocketResponseHeaders();
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIDeleteSystemApiRequest::apiDeleteSystemDeleteError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str){
    Q_UNUSED(error_type); // TODO: Remap error_type to QHttpEngine::Socket errors
    setSocketResponseHeaders();
    Q_UNUSED(error_str);  // response will be used instead of error string
    QJsonDocument resDoc(::OpenAPI::toJsonValue(res).toObject());
    socket->writeJson(resDoc);
    if(socket->isOpen()){
        socket->close();
    }
}


void OAIDeleteSystemApiRequest::sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type); // TODO
    socket->write(res);
    if(socket->isOpen()){
        socket->close();
    }
}

void OAIDeleteSystemApiRequest::sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type){
    Q_UNUSED(error_type);  // TODO
    socket->write(res->readAll());
    if(socket->isOpen()){
        socket->close();
    }
}

}
