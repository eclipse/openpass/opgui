/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISaveSystemApiRequest_H
#define OAI_OAISaveSystemApiRequest_H

#include <QObject>
#include <QStringList>
#include <QMultiMap>
#include <QNetworkReply>
#include <QSharedPointer>

#include <qhttpengine/socket.h>
#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_saveSystem_post_request.h"
#include <QString>
#include "OAISaveSystemApiHandler.h"

namespace OpenAPI {

class OAISaveSystemApiRequest : public QObject
{
    Q_OBJECT

public:
    OAISaveSystemApiRequest(QHttpEngine::Socket *s, QSharedPointer<OAISaveSystemApiHandler> handler);
    virtual ~OAISaveSystemApiRequest();

    void apiSaveSystemPostRequest();
    

    void apiSaveSystemPostResponse(const OAIDefault200Response& res);
    

    void apiSaveSystemPostError(const OAIDefault200Response& res, QNetworkReply::NetworkError error_type, QString& error_str);
    

    void sendCustomResponse(QByteArray & res, QNetworkReply::NetworkError error_type);

    void sendCustomResponse(QIODevice *res, QNetworkReply::NetworkError error_type);

    QMap<QString, QString> getRequestHeaders() const;

    QHttpEngine::Socket* getRawSocket();

    void setResponseHeaders(const QMultiMap<QString,QString>& headers);

signals:
    void apiSaveSystemPost(OAI_api_saveSystem_post_request oai_api_save_system_post_request);
    

private:
    QMap<QString, QString> requestHeaders;
    QMap<QString, QString> responseHeaders;
    QHttpEngine::Socket  *socket;
    QSharedPointer<OAISaveSystemApiHandler> handler;

    inline void setSocketResponseHeaders(){
        QHttpEngine::Socket::HeaderMap resHeaders;
        for(auto itr = responseHeaders.begin(); itr != responseHeaders.end(); ++itr) {
            resHeaders.insert(itr.key().toUtf8(), itr.value().toUtf8());
        }
        socket->setHeaders(resHeaders);
    }
};

}

#endif // OAI_OAISaveSystemApiRequest_H
