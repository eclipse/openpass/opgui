/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIFileTreeRequest.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIFileTreeRequest::OAIFileTreeRequest(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIFileTreeRequest::OAIFileTreeRequest() {
    this->initializeModel();
}

OAIFileTreeRequest::~OAIFileTreeRequest() {}

void OAIFileTreeRequest::initializeModel() {

    m_base_path_generic_isSet = false;
    m_base_path_generic_isValid = false;

    m_is_folder_isSet = false;
    m_is_folder_isValid = false;

    m_extension_isSet = false;
    m_extension_isValid = false;
}

void OAIFileTreeRequest::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIFileTreeRequest::fromJsonObject(QJsonObject json) {

    m_base_path_generic_isValid = ::OpenAPI::fromJsonValue(base_path_generic, json[QString("basePathGeneric")]);
    m_base_path_generic_isSet = !json[QString("basePathGeneric")].isNull() && m_base_path_generic_isValid;

    m_is_folder_isValid = ::OpenAPI::fromJsonValue(is_folder, json[QString("isFolder")]);
    m_is_folder_isSet = !json[QString("isFolder")].isNull() && m_is_folder_isValid;

    m_extension_isValid = ::OpenAPI::fromJsonValue(extension, json[QString("extension")]);
    m_extension_isSet = !json[QString("extension")].isNull() && m_extension_isValid;
}

QString OAIFileTreeRequest::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIFileTreeRequest::asJsonObject() const {
    QJsonObject obj;
    if (m_base_path_generic_isSet) {
        obj.insert(QString("basePathGeneric"), ::OpenAPI::toJsonValue(base_path_generic));
    }
    if (m_is_folder_isSet) {
        obj.insert(QString("isFolder"), ::OpenAPI::toJsonValue(is_folder));
    }
    if (m_extension_isSet) {
        obj.insert(QString("extension"), ::OpenAPI::toJsonValue(extension));
    }
    return obj;
}

QString OAIFileTreeRequest::getBasePathGeneric() const {
    return base_path_generic;
}
void OAIFileTreeRequest::setBasePathGeneric(const QString &base_path_generic) {
    this->base_path_generic = base_path_generic;
    this->m_base_path_generic_isSet = true;
}

bool OAIFileTreeRequest::is_base_path_generic_Set() const{
    return m_base_path_generic_isSet;
}

bool OAIFileTreeRequest::is_base_path_generic_Valid() const{
    return m_base_path_generic_isValid;
}

bool OAIFileTreeRequest::isIsFolder() const {
    return is_folder;
}
void OAIFileTreeRequest::setIsFolder(const bool &is_folder) {
    this->is_folder = is_folder;
    this->m_is_folder_isSet = true;
}

bool OAIFileTreeRequest::is_is_folder_Set() const{
    return m_is_folder_isSet;
}

bool OAIFileTreeRequest::is_is_folder_Valid() const{
    return m_is_folder_isValid;
}

QString OAIFileTreeRequest::getExtension() const {
    return extension;
}
void OAIFileTreeRequest::setExtension(const QString &extension) {
    this->extension = extension;
    this->m_extension_isSet = true;
}

bool OAIFileTreeRequest::is_extension_Set() const{
    return m_extension_isSet;
}

bool OAIFileTreeRequest::is_extension_Valid() const{
    return m_extension_isValid;
}

bool OAIFileTreeRequest::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_base_path_generic_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_is_folder_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_extension_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIFileTreeRequest::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_is_folder_isValid && m_extension_isValid && true;
}

} // namespace OpenAPI
