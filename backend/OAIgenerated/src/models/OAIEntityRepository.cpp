/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIEntityRepository.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIEntityRepository::OAIEntityRepository(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIEntityRepository::OAIEntityRepository() {
    this->initializeModel();
}

OAIEntityRepository::~OAIEntityRepository() {}

void OAIEntityRepository::initializeModel() {

    m_file_name_prefix_isSet = false;
    m_file_name_prefix_isValid = false;

    m_write_persistent_entities_isSet = false;
    m_write_persistent_entities_isValid = false;
}

void OAIEntityRepository::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIEntityRepository::fromJsonObject(QJsonObject json) {

    m_file_name_prefix_isValid = ::OpenAPI::fromJsonValue(file_name_prefix, json[QString("fileNamePrefix")]);
    m_file_name_prefix_isSet = !json[QString("fileNamePrefix")].isNull() && m_file_name_prefix_isValid;

    m_write_persistent_entities_isValid = ::OpenAPI::fromJsonValue(write_persistent_entities, json[QString("writePersistentEntities")]);
    m_write_persistent_entities_isSet = !json[QString("writePersistentEntities")].isNull() && m_write_persistent_entities_isValid;
}

QString OAIEntityRepository::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIEntityRepository::asJsonObject() const {
    QJsonObject obj;
    if (m_file_name_prefix_isSet) {
        obj.insert(QString("fileNamePrefix"), ::OpenAPI::toJsonValue(file_name_prefix));
    }
    if (m_write_persistent_entities_isSet) {
        obj.insert(QString("writePersistentEntities"), ::OpenAPI::toJsonValue(write_persistent_entities));
    }
    return obj;
}

QString OAIEntityRepository::getFileNamePrefix() const {
    return file_name_prefix;
}
void OAIEntityRepository::setFileNamePrefix(const QString &file_name_prefix) {
    this->file_name_prefix = file_name_prefix;
    this->m_file_name_prefix_isSet = true;
}

bool OAIEntityRepository::is_file_name_prefix_Set() const{
    return m_file_name_prefix_isSet;
}

bool OAIEntityRepository::is_file_name_prefix_Valid() const{
    return m_file_name_prefix_isValid;
}

QString OAIEntityRepository::getWritePersistentEntities() const {
    return write_persistent_entities;
}
void OAIEntityRepository::setWritePersistentEntities(const QString &write_persistent_entities) {
    this->write_persistent_entities = write_persistent_entities;
    this->m_write_persistent_entities_isSet = true;
}

bool OAIEntityRepository::is_write_persistent_entities_Set() const{
    return m_write_persistent_entities_isSet;
}

bool OAIEntityRepository::is_write_persistent_entities_Valid() const{
    return m_write_persistent_entities_isValid;
}

bool OAIEntityRepository::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_file_name_prefix_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_write_persistent_entities_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIEntityRepository::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_file_name_prefix_isValid && m_write_persistent_entities_isValid && true;
}

} // namespace OpenAPI
