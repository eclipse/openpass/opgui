/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIFileTreeExtended.h
 *
 * 
 */

#ifndef OAIFileTreeExtended_H
#define OAIFileTreeExtended_H

#include <QJsonObject>

#include "OAITreeNode.h"
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIFileTreeExtended : public OAIObject {
public:
    OAIFileTreeExtended();
    OAIFileTreeExtended(QString json);
    ~OAIFileTreeExtended() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getBasePath() const;
    void setBasePath(const QString &base_path);
    bool is_base_path_Set() const;
    bool is_base_path_Valid() const;

    OAITreeNode getFileSystemTree() const;
    void setFileSystemTree(const OAITreeNode &file_system_tree);
    bool is_file_system_tree_Set() const;
    bool is_file_system_tree_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString base_path;
    bool m_base_path_isSet;
    bool m_base_path_isValid;

    OAITreeNode file_system_tree;
    bool m_file_system_tree_isSet;
    bool m_file_system_tree_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIFileTreeExtended)

#endif // OAIFileTreeExtended_H
