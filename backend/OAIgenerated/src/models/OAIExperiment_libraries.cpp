/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIExperiment_libraries.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIExperiment_libraries::OAIExperiment_libraries(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIExperiment_libraries::OAIExperiment_libraries() {
    this->initializeModel();
}

OAIExperiment_libraries::~OAIExperiment_libraries() {}

void OAIExperiment_libraries::initializeModel() {

    m_world_library_isSet = false;
    m_world_library_isValid = false;

    m_data_buffer_library_isSet = false;
    m_data_buffer_library_isValid = false;

    m_stochastics_library_isSet = false;
    m_stochastics_library_isValid = false;
}

void OAIExperiment_libraries::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIExperiment_libraries::fromJsonObject(QJsonObject json) {

    m_world_library_isValid = ::OpenAPI::fromJsonValue(world_library, json[QString("worldLibrary")]);
    m_world_library_isSet = !json[QString("worldLibrary")].isNull() && m_world_library_isValid;

    m_data_buffer_library_isValid = ::OpenAPI::fromJsonValue(data_buffer_library, json[QString("dataBufferLibrary")]);
    m_data_buffer_library_isSet = !json[QString("dataBufferLibrary")].isNull() && m_data_buffer_library_isValid;

    m_stochastics_library_isValid = ::OpenAPI::fromJsonValue(stochastics_library, json[QString("stochasticsLibrary")]);
    m_stochastics_library_isSet = !json[QString("stochasticsLibrary")].isNull() && m_stochastics_library_isValid;
}

QString OAIExperiment_libraries::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIExperiment_libraries::asJsonObject() const {
    QJsonObject obj;
    if (m_world_library_isSet) {
        obj.insert(QString("worldLibrary"), ::OpenAPI::toJsonValue(world_library));
    }
    if (m_data_buffer_library_isSet) {
        obj.insert(QString("dataBufferLibrary"), ::OpenAPI::toJsonValue(data_buffer_library));
    }
    if (m_stochastics_library_isSet) {
        obj.insert(QString("stochasticsLibrary"), ::OpenAPI::toJsonValue(stochastics_library));
    }
    return obj;
}

QString OAIExperiment_libraries::getWorldLibrary() const {
    return world_library;
}
void OAIExperiment_libraries::setWorldLibrary(const QString &world_library) {
    this->world_library = world_library;
    this->m_world_library_isSet = true;
}

bool OAIExperiment_libraries::is_world_library_Set() const{
    return m_world_library_isSet;
}

bool OAIExperiment_libraries::is_world_library_Valid() const{
    return m_world_library_isValid;
}

QString OAIExperiment_libraries::getDataBufferLibrary() const {
    return data_buffer_library;
}
void OAIExperiment_libraries::setDataBufferLibrary(const QString &data_buffer_library) {
    this->data_buffer_library = data_buffer_library;
    this->m_data_buffer_library_isSet = true;
}

bool OAIExperiment_libraries::is_data_buffer_library_Set() const{
    return m_data_buffer_library_isSet;
}

bool OAIExperiment_libraries::is_data_buffer_library_Valid() const{
    return m_data_buffer_library_isValid;
}

QString OAIExperiment_libraries::getStochasticsLibrary() const {
    return stochastics_library;
}
void OAIExperiment_libraries::setStochasticsLibrary(const QString &stochastics_library) {
    this->stochastics_library = stochastics_library;
    this->m_stochastics_library_isSet = true;
}

bool OAIExperiment_libraries::is_stochastics_library_Set() const{
    return m_stochastics_library_isSet;
}

bool OAIExperiment_libraries::is_stochastics_library_Valid() const{
    return m_stochastics_library_isValid;
}

bool OAIExperiment_libraries::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_world_library_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_data_buffer_library_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_stochastics_library_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIExperiment_libraries::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_world_library_isValid && m_data_buffer_library_isValid && m_stochastics_library_isValid && true;
}

} // namespace OpenAPI
