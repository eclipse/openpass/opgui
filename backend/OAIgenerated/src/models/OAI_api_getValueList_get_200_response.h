/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAI_api_getValueList_get_200_response.h
 *
 * 
 */

#ifndef OAI_api_getValueList_get_200_response_H
#define OAI_api_getValueList_get_200_response_H

#include <QJsonObject>

#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAI_api_getValueList_get_200_response : public OAIObject {
public:
    OAI_api_getValueList_get_200_response();
    OAI_api_getValueList_get_200_response(QString json);
    ~OAI_api_getValueList_get_200_response() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QList<QString> getListValues() const;
    void setListValues(const QList<QString> &list_values);
    bool is_list_values_Set() const;
    bool is_list_values_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QList<QString> list_values;
    bool m_list_values_isSet;
    bool m_list_values_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAI_api_getValueList_get_200_response)

#endif // OAI_api_getValueList_get_200_response_H
