/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIExperiment_libraries.h
 *
 * 
 */

#ifndef OAIExperiment_libraries_H
#define OAIExperiment_libraries_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIExperiment_libraries : public OAIObject {
public:
    OAIExperiment_libraries();
    OAIExperiment_libraries(QString json);
    ~OAIExperiment_libraries() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getWorldLibrary() const;
    void setWorldLibrary(const QString &world_library);
    bool is_world_library_Set() const;
    bool is_world_library_Valid() const;

    QString getDataBufferLibrary() const;
    void setDataBufferLibrary(const QString &data_buffer_library);
    bool is_data_buffer_library_Set() const;
    bool is_data_buffer_library_Valid() const;

    QString getStochasticsLibrary() const;
    void setStochasticsLibrary(const QString &stochastics_library);
    bool is_stochastics_library_Set() const;
    bool is_stochastics_library_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString world_library;
    bool m_world_library_isSet;
    bool m_world_library_isValid;

    QString data_buffer_library;
    bool m_data_buffer_library_isSet;
    bool m_data_buffer_library_isValid;

    QString stochastics_library;
    bool m_stochastics_library_isSet;
    bool m_stochastics_library_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIExperiment_libraries)

#endif // OAIExperiment_libraries_H
