/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAITreeNode.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAITreeNode::OAITreeNode(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAITreeNode::OAITreeNode() {
    this->initializeModel();
}

OAITreeNode::~OAITreeNode() {}

void OAITreeNode::initializeModel() {

    m_name_isSet = false;
    m_name_isValid = false;

    m_children_isSet = false;
    m_children_isValid = false;
}

void OAITreeNode::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAITreeNode::fromJsonObject(QJsonObject json) {

    m_name_isValid = ::OpenAPI::fromJsonValue(name, json[QString("name")]);
    m_name_isSet = !json[QString("name")].isNull() && m_name_isValid;

    m_children_isValid = ::OpenAPI::fromJsonValue(children, json[QString("children")]);
    m_children_isSet = !json[QString("children")].isNull() && m_children_isValid;
}

QString OAITreeNode::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAITreeNode::asJsonObject() const {
    QJsonObject obj;
    if (m_name_isSet) {
        obj.insert(QString("name"), ::OpenAPI::toJsonValue(name));
    }
    if (children.size() > 0) {
        obj.insert(QString("children"), ::OpenAPI::toJsonValue(children));
    }
    return obj;
}

QString OAITreeNode::getName() const {
    return name;
}
void OAITreeNode::setName(const QString &name) {
    this->name = name;
    this->m_name_isSet = true;
}

bool OAITreeNode::is_name_Set() const{
    return m_name_isSet;
}

bool OAITreeNode::is_name_Valid() const{
    return m_name_isValid;
}

QList<OAITreeNode> OAITreeNode::getChildren() const {
    return children;
}
void OAITreeNode::setChildren(const QList<OAITreeNode> &children) {
    this->children = children;
    this->m_children_isSet = true;
}

bool OAITreeNode::is_children_Set() const{
    return m_children_isSet;
}

bool OAITreeNode::is_children_Valid() const{
    return m_children_isValid;
}

bool OAITreeNode::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_name_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (children.size() > 0) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAITreeNode::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_name_isValid && true;
}

} // namespace OpenAPI
