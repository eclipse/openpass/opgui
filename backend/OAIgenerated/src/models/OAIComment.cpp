/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIComment.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIComment::OAIComment(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIComment::OAIComment() {
    this->initializeModel();
}

OAIComment::~OAIComment() {}

void OAIComment::initializeModel() {

    m_title_isSet = false;
    m_title_isValid = false;

    m_message_isSet = false;
    m_message_isValid = false;

    m_position_isSet = false;
    m_position_isValid = false;
}

void OAIComment::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIComment::fromJsonObject(QJsonObject json) {

    m_title_isValid = ::OpenAPI::fromJsonValue(title, json[QString("title")]);
    m_title_isSet = !json[QString("title")].isNull() && m_title_isValid;

    m_message_isValid = ::OpenAPI::fromJsonValue(message, json[QString("message")]);
    m_message_isSet = !json[QString("message")].isNull() && m_message_isValid;

    m_position_isValid = ::OpenAPI::fromJsonValue(position, json[QString("position")]);
    m_position_isSet = !json[QString("position")].isNull() && m_position_isValid;
}

QString OAIComment::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIComment::asJsonObject() const {
    QJsonObject obj;
    if (m_title_isSet) {
        obj.insert(QString("title"), ::OpenAPI::toJsonValue(title));
    }
    if (m_message_isSet) {
        obj.insert(QString("message"), ::OpenAPI::toJsonValue(message));
    }
    if (position.isSet()) {
        obj.insert(QString("position"), ::OpenAPI::toJsonValue(position));
    }
    return obj;
}

QString OAIComment::getTitle() const {
    return title;
}
void OAIComment::setTitle(const QString &title) {
    this->title = title;
    this->m_title_isSet = true;
}

bool OAIComment::is_title_Set() const{
    return m_title_isSet;
}

bool OAIComment::is_title_Valid() const{
    return m_title_isValid;
}

QString OAIComment::getMessage() const {
    return message;
}
void OAIComment::setMessage(const QString &message) {
    this->message = message;
    this->m_message_isSet = true;
}

bool OAIComment::is_message_Set() const{
    return m_message_isSet;
}

bool OAIComment::is_message_Valid() const{
    return m_message_isValid;
}

OAIPosition OAIComment::getPosition() const {
    return position;
}
void OAIComment::setPosition(const OAIPosition &position) {
    this->position = position;
    this->m_position_isSet = true;
}

bool OAIComment::is_position_Set() const{
    return m_position_isSet;
}

bool OAIComment::is_position_Valid() const{
    return m_position_isValid;
}

bool OAIComment::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_title_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_message_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (position.isSet()) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIComment::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_title_isValid && m_message_isValid && m_position_isValid && true;
}

} // namespace OpenAPI
