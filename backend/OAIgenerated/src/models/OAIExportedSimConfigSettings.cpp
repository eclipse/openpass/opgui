/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIExportedSimConfigSettings.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIExportedSimConfigSettings::OAIExportedSimConfigSettings(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIExportedSimConfigSettings::OAIExportedSimConfigSettings() {
    this->initializeModel();
}

OAIExportedSimConfigSettings::~OAIExportedSimConfigSettings() {}

void OAIExportedSimConfigSettings::initializeModel() {

    m_path_isSet = false;
    m_path_isValid = false;

    m_selected_experiments_isSet = false;
    m_selected_experiments_isValid = false;

    m_xml_content_isSet = false;
    m_xml_content_isValid = false;
}

void OAIExportedSimConfigSettings::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIExportedSimConfigSettings::fromJsonObject(QJsonObject json) {

    m_path_isValid = ::OpenAPI::fromJsonValue(path, json[QString("path")]);
    m_path_isSet = !json[QString("path")].isNull() && m_path_isValid;

    m_selected_experiments_isValid = ::OpenAPI::fromJsonValue(selected_experiments, json[QString("selectedExperiments")]);
    m_selected_experiments_isSet = !json[QString("selectedExperiments")].isNull() && m_selected_experiments_isValid;

    m_xml_content_isValid = ::OpenAPI::fromJsonValue(xml_content, json[QString("xmlContent")]);
    m_xml_content_isSet = !json[QString("xmlContent")].isNull() && m_xml_content_isValid;
}

QString OAIExportedSimConfigSettings::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIExportedSimConfigSettings::asJsonObject() const {
    QJsonObject obj;
    if (m_path_isSet) {
        obj.insert(QString("path"), ::OpenAPI::toJsonValue(path));
    }
    if (selected_experiments.size() > 0) {
        obj.insert(QString("selectedExperiments"), ::OpenAPI::toJsonValue(selected_experiments));
    }
    if (m_xml_content_isSet) {
        obj.insert(QString("xmlContent"), ::OpenAPI::toJsonValue(xml_content));
    }
    return obj;
}

QString OAIExportedSimConfigSettings::getPath() const {
    return path;
}
void OAIExportedSimConfigSettings::setPath(const QString &path) {
    this->path = path;
    this->m_path_isSet = true;
}

bool OAIExportedSimConfigSettings::is_path_Set() const{
    return m_path_isSet;
}

bool OAIExportedSimConfigSettings::is_path_Valid() const{
    return m_path_isValid;
}

QList<QString> OAIExportedSimConfigSettings::getSelectedExperiments() const {
    return selected_experiments;
}
void OAIExportedSimConfigSettings::setSelectedExperiments(const QList<QString> &selected_experiments) {
    this->selected_experiments = selected_experiments;
    this->m_selected_experiments_isSet = true;
}

bool OAIExportedSimConfigSettings::is_selected_experiments_Set() const{
    return m_selected_experiments_isSet;
}

bool OAIExportedSimConfigSettings::is_selected_experiments_Valid() const{
    return m_selected_experiments_isValid;
}

QString OAIExportedSimConfigSettings::getXmlContent() const {
    return xml_content;
}
void OAIExportedSimConfigSettings::setXmlContent(const QString &xml_content) {
    this->xml_content = xml_content;
    this->m_xml_content_isSet = true;
}

bool OAIExportedSimConfigSettings::is_xml_content_Set() const{
    return m_xml_content_isSet;
}

bool OAIExportedSimConfigSettings::is_xml_content_Valid() const{
    return m_xml_content_isValid;
}

bool OAIExportedSimConfigSettings::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_path_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (selected_experiments.size() > 0) {
            isObjectUpdated = true;
            break;
        }

        if (m_xml_content_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIExportedSimConfigSettings::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_path_isValid && m_selected_experiments_isValid && m_xml_content_isValid && true;
}

} // namespace OpenAPI
