/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAI_api_getValueList_get_200_response.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAI_api_getValueList_get_200_response::OAI_api_getValueList_get_200_response(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAI_api_getValueList_get_200_response::OAI_api_getValueList_get_200_response() {
    this->initializeModel();
}

OAI_api_getValueList_get_200_response::~OAI_api_getValueList_get_200_response() {}

void OAI_api_getValueList_get_200_response::initializeModel() {

    m_list_values_isSet = false;
    m_list_values_isValid = false;
}

void OAI_api_getValueList_get_200_response::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAI_api_getValueList_get_200_response::fromJsonObject(QJsonObject json) {

    m_list_values_isValid = ::OpenAPI::fromJsonValue(list_values, json[QString("listValues")]);
    m_list_values_isSet = !json[QString("listValues")].isNull() && m_list_values_isValid;
}

QString OAI_api_getValueList_get_200_response::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAI_api_getValueList_get_200_response::asJsonObject() const {
    QJsonObject obj;
    if (list_values.size() > 0) {
        obj.insert(QString("listValues"), ::OpenAPI::toJsonValue(list_values));
    }
    return obj;
}

QList<QString> OAI_api_getValueList_get_200_response::getListValues() const {
    return list_values;
}
void OAI_api_getValueList_get_200_response::setListValues(const QList<QString> &list_values) {
    this->list_values = list_values;
    this->m_list_values_isSet = true;
}

bool OAI_api_getValueList_get_200_response::is_list_values_Set() const{
    return m_list_values_isSet;
}

bool OAI_api_getValueList_get_200_response::is_list_values_Valid() const{
    return m_list_values_isValid;
}

bool OAI_api_getValueList_get_200_response::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (list_values.size() > 0) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAI_api_getValueList_get_200_response::isValid() const {
    // only required properties are required for the object to be considered valid
    return true;
}

} // namespace OpenAPI
