/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIComment.h
 *
 * 
 */

#ifndef OAIComment_H
#define OAIComment_H

#include <QJsonObject>

#include "OAIPosition.h"
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIComment : public OAIObject {
public:
    OAIComment();
    OAIComment(QString json);
    ~OAIComment() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QString getTitle() const;
    void setTitle(const QString &title);
    bool is_title_Set() const;
    bool is_title_Valid() const;

    QString getMessage() const;
    void setMessage(const QString &message);
    bool is_message_Set() const;
    bool is_message_Valid() const;

    OAIPosition getPosition() const;
    void setPosition(const OAIPosition &position);
    bool is_position_Set() const;
    bool is_position_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QString title;
    bool m_title_isSet;
    bool m_title_isValid;

    QString message;
    bool m_message_isSet;
    bool m_message_isValid;

    OAIPosition position;
    bool m_position_isSet;
    bool m_position_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIComment)

#endif // OAIComment_H
