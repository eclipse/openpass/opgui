/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIExperiment.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIExperiment::OAIExperiment(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIExperiment::OAIExperiment() {
    this->initializeModel();
}

OAIExperiment::~OAIExperiment() {}

void OAIExperiment::initializeModel() {

    m_experiment_id_isSet = false;
    m_experiment_id_isValid = false;

    m_number_of_invocations_isSet = false;
    m_number_of_invocations_isValid = false;

    m_random_seed_isSet = false;
    m_random_seed_isValid = false;

    m_libraries_isSet = false;
    m_libraries_isValid = false;
}

void OAIExperiment::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIExperiment::fromJsonObject(QJsonObject json) {

    m_experiment_id_isValid = ::OpenAPI::fromJsonValue(experiment_id, json[QString("experimentID")]);
    m_experiment_id_isSet = !json[QString("experimentID")].isNull() && m_experiment_id_isValid;

    m_number_of_invocations_isValid = ::OpenAPI::fromJsonValue(number_of_invocations, json[QString("numberOfInvocations")]);
    m_number_of_invocations_isSet = !json[QString("numberOfInvocations")].isNull() && m_number_of_invocations_isValid;

    m_random_seed_isValid = ::OpenAPI::fromJsonValue(random_seed, json[QString("randomSeed")]);
    m_random_seed_isSet = !json[QString("randomSeed")].isNull() && m_random_seed_isValid;

    m_libraries_isValid = ::OpenAPI::fromJsonValue(libraries, json[QString("libraries")]);
    m_libraries_isSet = !json[QString("libraries")].isNull() && m_libraries_isValid;
}

QString OAIExperiment::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIExperiment::asJsonObject() const {
    QJsonObject obj;
    if (m_experiment_id_isSet) {
        obj.insert(QString("experimentID"), ::OpenAPI::toJsonValue(experiment_id));
    }
    if (m_number_of_invocations_isSet) {
        obj.insert(QString("numberOfInvocations"), ::OpenAPI::toJsonValue(number_of_invocations));
    }
    if (m_random_seed_isSet) {
        obj.insert(QString("randomSeed"), ::OpenAPI::toJsonValue(random_seed));
    }
    if (libraries.isSet()) {
        obj.insert(QString("libraries"), ::OpenAPI::toJsonValue(libraries));
    }
    return obj;
}

qint32 OAIExperiment::getExperimentId() const {
    return experiment_id;
}
void OAIExperiment::setExperimentId(const qint32 &experiment_id) {
    this->experiment_id = experiment_id;
    this->m_experiment_id_isSet = true;
}

bool OAIExperiment::is_experiment_id_Set() const{
    return m_experiment_id_isSet;
}

bool OAIExperiment::is_experiment_id_Valid() const{
    return m_experiment_id_isValid;
}

qint32 OAIExperiment::getNumberOfInvocations() const {
    return number_of_invocations;
}
void OAIExperiment::setNumberOfInvocations(const qint32 &number_of_invocations) {
    this->number_of_invocations = number_of_invocations;
    this->m_number_of_invocations_isSet = true;
}

bool OAIExperiment::is_number_of_invocations_Set() const{
    return m_number_of_invocations_isSet;
}

bool OAIExperiment::is_number_of_invocations_Valid() const{
    return m_number_of_invocations_isValid;
}

qint32 OAIExperiment::getRandomSeed() const {
    return random_seed;
}
void OAIExperiment::setRandomSeed(const qint32 &random_seed) {
    this->random_seed = random_seed;
    this->m_random_seed_isSet = true;
}

bool OAIExperiment::is_random_seed_Set() const{
    return m_random_seed_isSet;
}

bool OAIExperiment::is_random_seed_Valid() const{
    return m_random_seed_isValid;
}

OAIExperiment_libraries OAIExperiment::getLibraries() const {
    return libraries;
}
void OAIExperiment::setLibraries(const OAIExperiment_libraries &libraries) {
    this->libraries = libraries;
    this->m_libraries_isSet = true;
}

bool OAIExperiment::is_libraries_Set() const{
    return m_libraries_isSet;
}

bool OAIExperiment::is_libraries_Valid() const{
    return m_libraries_isValid;
}

bool OAIExperiment::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_experiment_id_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_number_of_invocations_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (m_random_seed_isSet) {
            isObjectUpdated = true;
            break;
        }

        if (libraries.isSet()) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIExperiment::isValid() const {
    // only required properties are required for the object to be considered valid
    return m_experiment_id_isValid && m_number_of_invocations_isValid && m_random_seed_isValid && m_libraries_isValid && true;
}

} // namespace OpenAPI
