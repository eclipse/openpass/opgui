/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAILoggingGroup.h
 *
 * 
 */

#ifndef OAILoggingGroup_H
#define OAILoggingGroup_H

#include <QJsonObject>

#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAILoggingGroup : public OAIObject {
public:
    OAILoggingGroup();
    OAILoggingGroup(QString json);
    ~OAILoggingGroup() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    bool isEnabled() const;
    void setEnabled(const bool &enabled);
    bool is_enabled_Set() const;
    bool is_enabled_Valid() const;

    QString getGroup() const;
    void setGroup(const QString &group);
    bool is_group_Set() const;
    bool is_group_Valid() const;

    QString getValues() const;
    void setValues(const QString &values);
    bool is_values_Set() const;
    bool is_values_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    bool enabled;
    bool m_enabled_isSet;
    bool m_enabled_isValid;

    QString group;
    bool m_group_isSet;
    bool m_group_isValid;

    QString values;
    bool m_values_isSet;
    bool m_values_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAILoggingGroup)

#endif // OAILoggingGroup_H
