/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIEnvironment.h
 *
 * 
 */

#ifndef OAIEnvironment_H
#define OAIEnvironment_H

#include <QJsonObject>

#include "OAIFriction.h"
#include "OAITimeOfDay.h"
#include "OAIVisibilityDistance.h"
#include "OAIWeather.h"
#include <QList>
#include <QString>

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIEnvironment : public OAIObject {
public:
    OAIEnvironment();
    OAIEnvironment(QString json);
    ~OAIEnvironment() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    QList<OAITimeOfDay> getTimeOfDays() const;
    void setTimeOfDays(const QList<OAITimeOfDay> &time_of_days);
    bool is_time_of_days_Set() const;
    bool is_time_of_days_Valid() const;

    QList<OAIVisibilityDistance> getVisibilityDistances() const;
    void setVisibilityDistances(const QList<OAIVisibilityDistance> &visibility_distances);
    bool is_visibility_distances_Set() const;
    bool is_visibility_distances_Valid() const;

    QList<OAIFriction> getFrictions() const;
    void setFrictions(const QList<OAIFriction> &frictions);
    bool is_frictions_Set() const;
    bool is_frictions_Valid() const;

    QList<OAIWeather> getWeathers() const;
    void setWeathers(const QList<OAIWeather> &weathers);
    bool is_weathers_Set() const;
    bool is_weathers_Valid() const;

    QString getTrafficRules() const;
    void setTrafficRules(const QString &traffic_rules);
    bool is_traffic_rules_Set() const;
    bool is_traffic_rules_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    QList<OAITimeOfDay> time_of_days;
    bool m_time_of_days_isSet;
    bool m_time_of_days_isValid;

    QList<OAIVisibilityDistance> visibility_distances;
    bool m_visibility_distances_isSet;
    bool m_visibility_distances_isValid;

    QList<OAIFriction> frictions;
    bool m_frictions_isSet;
    bool m_frictions_isValid;

    QList<OAIWeather> weathers;
    bool m_weathers_isSet;
    bool m_weathers_isValid;

    QString traffic_rules;
    bool m_traffic_rules_isSet;
    bool m_traffic_rules_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIEnvironment)

#endif // OAIEnvironment_H
