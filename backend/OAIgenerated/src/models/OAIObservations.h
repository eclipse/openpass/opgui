/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

/*
 * OAIObservations.h
 *
 * 
 */

#ifndef OAIObservations_H
#define OAIObservations_H

#include <QJsonObject>

#include "OAIEntityRepository.h"
#include "OAILog.h"

#include "OAIEnum.h"
#include "OAIObject.h"

namespace OpenAPI {

class OAIObservations : public OAIObject {
public:
    OAIObservations();
    OAIObservations(QString json);
    ~OAIObservations() override;

    QString asJson() const override;
    QJsonObject asJsonObject() const override;
    void fromJsonObject(QJsonObject json) override;
    void fromJson(QString jsonString) override;

    OAILog getLog() const;
    void setLog(const OAILog &log);
    bool is_log_Set() const;
    bool is_log_Valid() const;

    OAIEntityRepository getEntityRepository() const;
    void setEntityRepository(const OAIEntityRepository &entity_repository);
    bool is_entity_repository_Set() const;
    bool is_entity_repository_Valid() const;

    virtual bool isSet() const override;
    virtual bool isValid() const override;

private:
    void initializeModel();

    OAILog log;
    bool m_log_isSet;
    bool m_log_isValid;

    OAIEntityRepository entity_repository;
    bool m_entity_repository_isSet;
    bool m_entity_repository_isValid;
};

} // namespace OpenAPI

Q_DECLARE_METATYPE(OpenAPI::OAIObservations)

#endif // OAIObservations_H
