/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIEnvironment.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAIEnvironment::OAIEnvironment(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAIEnvironment::OAIEnvironment() {
    this->initializeModel();
}

OAIEnvironment::~OAIEnvironment() {}

void OAIEnvironment::initializeModel() {

    m_time_of_days_isSet = false;
    m_time_of_days_isValid = false;

    m_visibility_distances_isSet = false;
    m_visibility_distances_isValid = false;

    m_frictions_isSet = false;
    m_frictions_isValid = false;

    m_weathers_isSet = false;
    m_weathers_isValid = false;

    m_traffic_rules_isSet = false;
    m_traffic_rules_isValid = false;
}

void OAIEnvironment::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAIEnvironment::fromJsonObject(QJsonObject json) {

    m_time_of_days_isValid = ::OpenAPI::fromJsonValue(time_of_days, json[QString("timeOfDays")]);
    m_time_of_days_isSet = !json[QString("timeOfDays")].isNull() && m_time_of_days_isValid;

    m_visibility_distances_isValid = ::OpenAPI::fromJsonValue(visibility_distances, json[QString("visibilityDistances")]);
    m_visibility_distances_isSet = !json[QString("visibilityDistances")].isNull() && m_visibility_distances_isValid;

    m_frictions_isValid = ::OpenAPI::fromJsonValue(frictions, json[QString("frictions")]);
    m_frictions_isSet = !json[QString("frictions")].isNull() && m_frictions_isValid;

    m_weathers_isValid = ::OpenAPI::fromJsonValue(weathers, json[QString("weathers")]);
    m_weathers_isSet = !json[QString("weathers")].isNull() && m_weathers_isValid;

    m_traffic_rules_isValid = ::OpenAPI::fromJsonValue(traffic_rules, json[QString("trafficRules")]);
    m_traffic_rules_isSet = !json[QString("trafficRules")].isNull() && m_traffic_rules_isValid;
}

QString OAIEnvironment::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAIEnvironment::asJsonObject() const {
    QJsonObject obj;
    if (time_of_days.size() > 0) {
        obj.insert(QString("timeOfDays"), ::OpenAPI::toJsonValue(time_of_days));
    }
    if (visibility_distances.size() > 0) {
        obj.insert(QString("visibilityDistances"), ::OpenAPI::toJsonValue(visibility_distances));
    }
    if (frictions.size() > 0) {
        obj.insert(QString("frictions"), ::OpenAPI::toJsonValue(frictions));
    }
    if (weathers.size() > 0) {
        obj.insert(QString("weathers"), ::OpenAPI::toJsonValue(weathers));
    }
    if (m_traffic_rules_isSet) {
        obj.insert(QString("trafficRules"), ::OpenAPI::toJsonValue(traffic_rules));
    }
    return obj;
}

QList<OAITimeOfDay> OAIEnvironment::getTimeOfDays() const {
    return time_of_days;
}
void OAIEnvironment::setTimeOfDays(const QList<OAITimeOfDay> &time_of_days) {
    this->time_of_days = time_of_days;
    this->m_time_of_days_isSet = true;
}

bool OAIEnvironment::is_time_of_days_Set() const{
    return m_time_of_days_isSet;
}

bool OAIEnvironment::is_time_of_days_Valid() const{
    return m_time_of_days_isValid;
}

QList<OAIVisibilityDistance> OAIEnvironment::getVisibilityDistances() const {
    return visibility_distances;
}
void OAIEnvironment::setVisibilityDistances(const QList<OAIVisibilityDistance> &visibility_distances) {
    this->visibility_distances = visibility_distances;
    this->m_visibility_distances_isSet = true;
}

bool OAIEnvironment::is_visibility_distances_Set() const{
    return m_visibility_distances_isSet;
}

bool OAIEnvironment::is_visibility_distances_Valid() const{
    return m_visibility_distances_isValid;
}

QList<OAIFriction> OAIEnvironment::getFrictions() const {
    return frictions;
}
void OAIEnvironment::setFrictions(const QList<OAIFriction> &frictions) {
    this->frictions = frictions;
    this->m_frictions_isSet = true;
}

bool OAIEnvironment::is_frictions_Set() const{
    return m_frictions_isSet;
}

bool OAIEnvironment::is_frictions_Valid() const{
    return m_frictions_isValid;
}

QList<OAIWeather> OAIEnvironment::getWeathers() const {
    return weathers;
}
void OAIEnvironment::setWeathers(const QList<OAIWeather> &weathers) {
    this->weathers = weathers;
    this->m_weathers_isSet = true;
}

bool OAIEnvironment::is_weathers_Set() const{
    return m_weathers_isSet;
}

bool OAIEnvironment::is_weathers_Valid() const{
    return m_weathers_isValid;
}

QString OAIEnvironment::getTrafficRules() const {
    return traffic_rules;
}
void OAIEnvironment::setTrafficRules(const QString &traffic_rules) {
    this->traffic_rules = traffic_rules;
    this->m_traffic_rules_isSet = true;
}

bool OAIEnvironment::is_traffic_rules_Set() const{
    return m_traffic_rules_isSet;
}

bool OAIEnvironment::is_traffic_rules_Valid() const{
    return m_traffic_rules_isValid;
}

bool OAIEnvironment::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (time_of_days.size() > 0) {
            isObjectUpdated = true;
            break;
        }

        if (visibility_distances.size() > 0) {
            isObjectUpdated = true;
            break;
        }

        if (frictions.size() > 0) {
            isObjectUpdated = true;
            break;
        }

        if (weathers.size() > 0) {
            isObjectUpdated = true;
            break;
        }

        if (m_traffic_rules_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAIEnvironment::isValid() const {
    // only required properties are required for the object to be considered valid
    return true;
}

} // namespace OpenAPI
