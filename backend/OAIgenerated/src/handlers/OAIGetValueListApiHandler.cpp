/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetValueListApiHandler.h"
#include "OAIGetValueListApiRequest.h"

namespace OpenAPI {

OAIGetValueListApiHandler::OAIGetValueListApiHandler(){

}

OAIGetValueListApiHandler::~OAIGetValueListApiHandler(){

}

void OAIGetValueListApiHandler::apiGetValueListGet(QString name) {
    Q_UNUSED(name);
    auto reqObj = qobject_cast<OAIGetValueListApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAI_api_getValueList_get_200_response res;
        reqObj->apiGetValueListGetResponse(res);
    }
}


}
