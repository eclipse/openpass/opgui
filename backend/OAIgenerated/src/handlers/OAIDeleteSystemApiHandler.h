/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIDeleteSystemApiHandler_H
#define OAI_OAIDeleteSystemApiHandler_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError404.h"
#include "OAIError500.h"
#include <QString>

namespace OpenAPI {

class OAIDeleteSystemApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIDeleteSystemApiHandler();
    virtual ~OAIDeleteSystemApiHandler();


public slots:
    virtual void apiDeleteSystemDelete(QString path);
    

};

}

#endif // OAI_OAIDeleteSystemApiHandler_H
