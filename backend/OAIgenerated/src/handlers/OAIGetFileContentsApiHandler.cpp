/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetFileContentsApiHandler.h"
#include "OAIGetFileContentsApiRequest.h"

namespace OpenAPI {

OAIGetFileContentsApiHandler::OAIGetFileContentsApiHandler(){

}

OAIGetFileContentsApiHandler::~OAIGetFileContentsApiHandler(){

}

void OAIGetFileContentsApiHandler::apiGetFileContentsGet(QString path, bool encoded) {
    Q_UNUSED(path);
    Q_UNUSED(encoded);
    auto reqObj = qobject_cast<OAIGetFileContentsApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIDefault200Response res;
        reqObj->apiGetFileContentsGetResponse(res);
    }
}


}
