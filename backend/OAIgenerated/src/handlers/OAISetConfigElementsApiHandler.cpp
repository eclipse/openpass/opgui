/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAISetConfigElementsApiHandler.h"
#include "OAISetConfigElementsApiRequest.h"

namespace OpenAPI {

OAISetConfigElementsApiHandler::OAISetConfigElementsApiHandler(){

}

OAISetConfigElementsApiHandler::~OAISetConfigElementsApiHandler(){

}

void OAISetConfigElementsApiHandler::apiConfigElementsPost(QList<OAIConfigElement> oai_config_element) {
    Q_UNUSED(oai_config_element);
    auto reqObj = qobject_cast<OAISetConfigElementsApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIDefault200Response res;
        reqObj->apiConfigElementsPostResponse(res);
    }
}


}
