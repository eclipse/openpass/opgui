/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAISaveSystemApiHandler.h"
#include "OAISaveSystemApiRequest.h"

namespace OpenAPI {

OAISaveSystemApiHandler::OAISaveSystemApiHandler(){

}

OAISaveSystemApiHandler::~OAISaveSystemApiHandler(){

}

void OAISaveSystemApiHandler::apiSaveSystemPost(OAI_api_saveSystem_post_request oai_api_save_system_post_request) {
    Q_UNUSED(oai_api_save_system_post_request);
    auto reqObj = qobject_cast<OAISaveSystemApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        OAIDefault200Response res;
        reqObj->apiSaveSystemPostResponse(res);
    }
}


}
