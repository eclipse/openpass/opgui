/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetValueListApiHandler_H
#define OAI_OAIGetValueListApiHandler_H

#include <QObject>

#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_getValueList_get_200_response.h"
#include <QString>

namespace OpenAPI {

class OAIGetValueListApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIGetValueListApiHandler();
    virtual ~OAIGetValueListApiHandler();


public slots:
    virtual void apiGetValueListGet(QString name);
    

};

}

#endif // OAI_OAIGetValueListApiHandler_H
