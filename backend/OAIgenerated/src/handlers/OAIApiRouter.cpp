/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>


#include "OAIApiRouter.h"
#include "OAIComponentsApiRequest.h"
#include "OAIConvertToConfigsApiRequest.h"
#include "OAIDeleteInformationApiRequest.h"
#include "OAIDeleteSystemApiRequest.h"
#include "OAIExportOpsimulationManagerXmlApiRequest.h"
#include "OAIExportToSimulationApiRequest.h"
#include "OAIGetConfigElementsApiRequest.h"
#include "OAIGetFileContentsApiRequest.h"
#include "OAIGetFileTreeApiRequest.h"
#include "OAIGetSystemApiRequest.h"
#include "OAIGetValueListApiRequest.h"
#include "OAIIsSimulationRunningApiRequest.h"
#include "OAIPathToConvertedCasesApiRequest.h"
#include "OAIRunOpSimulationManagerApiRequest.h"
#include "OAISaveSystemApiRequest.h"
#include "OAISendPCMFileApiRequest.h"
#include "OAISetConfigElementsApiRequest.h"
#include "OAIVerifyPathApiRequest.h"


namespace OpenAPI {

OAIApiRouter::OAIApiRouter() {
    createApiHandlers();
}

OAIApiRouter::~OAIApiRouter(){

}

void OAIApiRouter::createApiHandlers() { 
    mOAIComponentsApiHandler = QSharedPointer<OAIComponentsApiHandler>::create();
    mOAIConvertToConfigsApiHandler = QSharedPointer<OAIConvertToConfigsApiHandler>::create();
    mOAIDeleteInformationApiHandler = QSharedPointer<OAIDeleteInformationApiHandler>::create();
    mOAIDeleteSystemApiHandler = QSharedPointer<OAIDeleteSystemApiHandler>::create();
    mOAIExportOpsimulationManagerXmlApiHandler = QSharedPointer<OAIExportOpsimulationManagerXmlApiHandler>::create();
    mOAIExportToSimulationApiHandler = QSharedPointer<OAIExportToSimulationApiHandler>::create();
    mOAIGetConfigElementsApiHandler = QSharedPointer<OAIGetConfigElementsApiHandler>::create();
    mOAIGetFileContentsApiHandler = QSharedPointer<OAIGetFileContentsApiHandler>::create();
    mOAIGetFileTreeApiHandler = QSharedPointer<OAIGetFileTreeApiHandler>::create();
    mOAIGetSystemApiHandler = QSharedPointer<OAIGetSystemApiHandler>::create();
    mOAIGetValueListApiHandler = QSharedPointer<OAIGetValueListApiHandler>::create();
    mOAIIsSimulationRunningApiHandler = QSharedPointer<OAIIsSimulationRunningApiHandler>::create();
    mOAIPathToConvertedCasesApiHandler = QSharedPointer<OAIPathToConvertedCasesApiHandler>::create();
    mOAIRunOpSimulationManagerApiHandler = QSharedPointer<OAIRunOpSimulationManagerApiHandler>::create();
    mOAISaveSystemApiHandler = QSharedPointer<OAISaveSystemApiHandler>::create();
    mOAISendPCMFileApiHandler = QSharedPointer<OAISendPCMFileApiHandler>::create();
    mOAISetConfigElementsApiHandler = QSharedPointer<OAISetConfigElementsApiHandler>::create();
    mOAIVerifyPathApiHandler = QSharedPointer<OAIVerifyPathApiHandler>::create();
}


void OAIApiRouter::setOAIComponentsApiHandler(QSharedPointer<OAIComponentsApiHandler> handler){
    mOAIComponentsApiHandler = handler;
}
void OAIApiRouter::setOAIConvertToConfigsApiHandler(QSharedPointer<OAIConvertToConfigsApiHandler> handler){
    mOAIConvertToConfigsApiHandler = handler;
}
void OAIApiRouter::setOAIDeleteInformationApiHandler(QSharedPointer<OAIDeleteInformationApiHandler> handler){
    mOAIDeleteInformationApiHandler = handler;
}
void OAIApiRouter::setOAIDeleteSystemApiHandler(QSharedPointer<OAIDeleteSystemApiHandler> handler){
    mOAIDeleteSystemApiHandler = handler;
}
void OAIApiRouter::setOAIExportOpsimulationManagerXmlApiHandler(QSharedPointer<OAIExportOpsimulationManagerXmlApiHandler> handler){
    mOAIExportOpsimulationManagerXmlApiHandler = handler;
}
void OAIApiRouter::setOAIExportToSimulationApiHandler(QSharedPointer<OAIExportToSimulationApiHandler> handler){
    mOAIExportToSimulationApiHandler = handler;
}
void OAIApiRouter::setOAIGetConfigElementsApiHandler(QSharedPointer<OAIGetConfigElementsApiHandler> handler){
    mOAIGetConfigElementsApiHandler = handler;
}
void OAIApiRouter::setOAIGetFileContentsApiHandler(QSharedPointer<OAIGetFileContentsApiHandler> handler){
    mOAIGetFileContentsApiHandler = handler;
}
void OAIApiRouter::setOAIGetFileTreeApiHandler(QSharedPointer<OAIGetFileTreeApiHandler> handler){
    mOAIGetFileTreeApiHandler = handler;
}
void OAIApiRouter::setOAIGetSystemApiHandler(QSharedPointer<OAIGetSystemApiHandler> handler){
    mOAIGetSystemApiHandler = handler;
}
void OAIApiRouter::setOAIGetValueListApiHandler(QSharedPointer<OAIGetValueListApiHandler> handler){
    mOAIGetValueListApiHandler = handler;
}
void OAIApiRouter::setOAIIsSimulationRunningApiHandler(QSharedPointer<OAIIsSimulationRunningApiHandler> handler){
    mOAIIsSimulationRunningApiHandler = handler;
}
void OAIApiRouter::setOAIPathToConvertedCasesApiHandler(QSharedPointer<OAIPathToConvertedCasesApiHandler> handler){
    mOAIPathToConvertedCasesApiHandler = handler;
}
void OAIApiRouter::setOAIRunOpSimulationManagerApiHandler(QSharedPointer<OAIRunOpSimulationManagerApiHandler> handler){
    mOAIRunOpSimulationManagerApiHandler = handler;
}
void OAIApiRouter::setOAISaveSystemApiHandler(QSharedPointer<OAISaveSystemApiHandler> handler){
    mOAISaveSystemApiHandler = handler;
}
void OAIApiRouter::setOAISendPCMFileApiHandler(QSharedPointer<OAISendPCMFileApiHandler> handler){
    mOAISendPCMFileApiHandler = handler;
}
void OAIApiRouter::setOAISetConfigElementsApiHandler(QSharedPointer<OAISetConfigElementsApiHandler> handler){
    mOAISetConfigElementsApiHandler = handler;
}
void OAIApiRouter::setOAIVerifyPathApiHandler(QSharedPointer<OAIVerifyPathApiHandler> handler){
    mOAIVerifyPathApiHandler = handler;
}

void OAIApiRouter::setUpRoutes() {
    
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/components").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIComponentsApiRequest(socket, mOAIComponentsApiHandler);
            reqObj->apiComponentsGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/convertToConfigs").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIConvertToConfigsApiRequest(socket, mOAIConvertToConfigsApiHandler);
            reqObj->apiConvertToConfigsPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/delete-information").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIDeleteInformationApiRequest(socket, mOAIDeleteInformationApiHandler);
            reqObj->deleteInformationRequest();
    });
    Routes.insert(QString("%1 %2").arg("DELETE").arg("/api/deleteSystem").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIDeleteSystemApiRequest(socket, mOAIDeleteSystemApiHandler);
            reqObj->apiDeleteSystemDeleteRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/exportOpsimulationManagerXml").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIExportOpsimulationManagerXmlApiRequest(socket, mOAIExportOpsimulationManagerXmlApiHandler);
            reqObj->apiExportOpsimulationManagerXmlPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/exportToSimulations").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIExportToSimulationApiRequest(socket, mOAIExportToSimulationApiHandler);
            reqObj->apiExportToSimulationsPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/configElements").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIGetConfigElementsApiRequest(socket, mOAIGetConfigElementsApiHandler);
            reqObj->apiConfigElementsGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/getFileContents").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIGetFileContentsApiRequest(socket, mOAIGetFileContentsApiHandler);
            reqObj->apiGetFileContentsGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/getFileTree").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIGetFileTreeApiRequest(socket, mOAIGetFileTreeApiHandler);
            reqObj->apiGetFileTreePostRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/getSystem").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIGetSystemApiRequest(socket, mOAIGetSystemApiHandler);
            reqObj->apiGetSystemGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/getValueList").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIGetValueListApiRequest(socket, mOAIGetValueListApiHandler);
            reqObj->apiGetValueListGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/isSimulationRunning").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIIsSimulationRunningApiRequest(socket, mOAIIsSimulationRunningApiHandler);
            reqObj->apiIsSimulationRunningGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/pathToConvertedCases").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIPathToConvertedCasesApiRequest(socket, mOAIPathToConvertedCasesApiHandler);
            reqObj->apiPathToConvertedCasesPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("GET").arg("/api/runOpSimulationManager").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIRunOpSimulationManagerApiRequest(socket, mOAIRunOpSimulationManagerApiHandler);
            reqObj->apiRunOpSimulationManagerGetRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/saveSystem").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAISaveSystemApiRequest(socket, mOAISaveSystemApiHandler);
            reqObj->apiSaveSystemPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/sendPCMFile").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAISendPCMFileApiRequest(socket, mOAISendPCMFileApiHandler);
            reqObj->apiSendPCMFilePostRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/configElements").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAISetConfigElementsApiRequest(socket, mOAISetConfigElementsApiHandler);
            reqObj->apiConfigElementsPostRequest();
    });
    Routes.insert(QString("%1 %2").arg("POST").arg("/api/verify-path").toLower(), [this](QHttpEngine::Socket *socket) {
            auto reqObj = new OAIVerifyPathApiRequest(socket, mOAIVerifyPathApiHandler);
            reqObj->verifyPathRequest();
    });
}

void OAIApiRouter::processRequest(QHttpEngine::Socket *socket){
    if( handleRequest(socket) ){
        return;
    }
    if( handleRequestAndExtractPathParam(socket) ){
        return;
    }
    socket->setStatusCode(QHttpEngine::Socket::NotFound);
    if(socket->isOpen()){
        socket->writeHeaders();
        socket->close();
    }
}

bool OAIApiRouter::handleRequest(QHttpEngine::Socket *socket){
    auto reqPath = QString("%1 %2").arg(fromQHttpEngineMethod(socket->method())).arg(socket->path()).toLower();
    if ( Routes.contains(reqPath) ) {
        Routes.value(reqPath).operator()(socket);
        return true;
    }
    return false;
}

bool OAIApiRouter::handleRequestAndExtractPathParam(QHttpEngine::Socket *socket){
    auto reqPath = QString("%1 %2").arg(fromQHttpEngineMethod(socket->method())).arg(socket->path()).toLower();
    return false;
}

}
