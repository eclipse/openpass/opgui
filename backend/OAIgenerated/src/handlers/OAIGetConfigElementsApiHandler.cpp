/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetConfigElementsApiHandler.h"
#include "OAIGetConfigElementsApiRequest.h"

namespace OpenAPI {

OAIGetConfigElementsApiHandler::OAIGetConfigElementsApiHandler(){

}

OAIGetConfigElementsApiHandler::~OAIGetConfigElementsApiHandler(){

}

void OAIGetConfigElementsApiHandler::apiConfigElementsGet() {
    auto reqObj = qobject_cast<OAIGetConfigElementsApiRequest*>(sender());
    if( reqObj != nullptr )
    {
        QList<OAIConfigElement> res;
        reqObj->apiConfigElementsGetResponse(res);
    }
}


}
