/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetFileContentsApiHandler_H
#define OAI_OAIGetFileContentsApiHandler_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError404.h"
#include "OAIError500.h"
#include <QString>

namespace OpenAPI {

class OAIGetFileContentsApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIGetFileContentsApiHandler();
    virtual ~OAIGetFileContentsApiHandler();


public slots:
    virtual void apiGetFileContentsGet(QString path, bool encoded);
    

};

}

#endif // OAI_OAIGetFileContentsApiHandler_H
