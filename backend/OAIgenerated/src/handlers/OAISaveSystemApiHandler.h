/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISaveSystemApiHandler_H
#define OAI_OAISaveSystemApiHandler_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_saveSystem_post_request.h"
#include <QString>

namespace OpenAPI {

class OAISaveSystemApiHandler : public QObject
{
    Q_OBJECT

public:
    OAISaveSystemApiHandler();
    virtual ~OAISaveSystemApiHandler();


public slots:
    virtual void apiSaveSystemPost(OAI_api_saveSystem_post_request oai_api_save_system_post_request);
    

};

}

#endif // OAI_OAISaveSystemApiHandler_H
