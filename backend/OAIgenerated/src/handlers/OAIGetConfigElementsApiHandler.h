/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetConfigElementsApiHandler_H
#define OAI_OAIGetConfigElementsApiHandler_H

#include <QObject>

#include "OAIConfigElement.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include <QList>
#include <QString>

namespace OpenAPI {

class OAIGetConfigElementsApiHandler : public QObject
{
    Q_OBJECT

public:
    OAIGetConfigElementsApiHandler();
    virtual ~OAIGetConfigElementsApiHandler();


public slots:
    virtual void apiConfigElementsGet();
    

};

}

#endif // OAI_OAIGetConfigElementsApiHandler_H
