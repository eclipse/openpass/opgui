/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIGetConfigElementsApiHandlerOP.h"
#include "OAIGetConfigElementsApiRequest.h"
#include "OPGUICore.h"
#include "OAICustomHelpers.h"

namespace OpenAPI {

OAIGetConfigElementsApiHandlerOP::OAIGetConfigElementsApiHandlerOP() = default;

OAIGetConfigElementsApiHandlerOP::~OAIGetConfigElementsApiHandlerOP() = default;

void OAIGetConfigElementsApiHandlerOP::apiConfigElementsGet() {
    auto reqObj = qobject_cast<OAIGetConfigElementsApiRequest*>(sender());

    QList<OAIConfigElement> res;

    if( reqObj != nullptr )
    {
        QString errorMsg; 

        if (OPGUICore::api_get_configElements(res, errorMsg))
        {
            reqObj->apiConfigElementsGetResponse(res);
        }
        else
        {
            int code=500;
            OAIError500 error;
            error.setMessage("Server error");
            if (!errorMsg.isEmpty()) {
                error.setMessage(error.getMessage() + " - Details: " + errorMsg);
            }
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }
    }
    else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
    }
}


}
