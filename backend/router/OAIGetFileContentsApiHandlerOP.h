/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIFileContentsApiHandler_OP_H
#define OAI_OAIFileContentsApiHandler_OP_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError404.h"
#include "OAIError500.h"
#include <QString>
#include "OAIGetFileContentsApiHandler.h"

namespace OpenAPI {

class OAIGetFileContentsApiHandlerOP : public OAIGetFileContentsApiHandler
{
    Q_OBJECT

public:
    OAIGetFileContentsApiHandlerOP();
    ~OAIGetFileContentsApiHandlerOP() override;


public slots:
    void apiGetFileContentsGet(QString path, bool encoded) override;
    

};

}

#endif // OAI_OAIFileContentsApiHandler_OP_H
