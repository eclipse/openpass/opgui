/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAICustomHelpers.h"
#include "OAISetConfigElementsApiHandlerOP.h"
#include "OAISetConfigElementsApiRequest.h"
#include "OPGUICore.h"

namespace OpenAPI {

OAISetConfigElementsApiHandlerOP::OAISetConfigElementsApiHandlerOP() = default;

OAISetConfigElementsApiHandlerOP::~OAISetConfigElementsApiHandlerOP() = default;

void OAISetConfigElementsApiHandlerOP::apiConfigElementsPost(QList<OAIConfigElement> oai_config_elements) {
    auto reqObj = qobject_cast<OAISetConfigElementsApiRequest*>(sender());
        QString errorMsg;
        OAIDefault200Response res;
        
        if( reqObj != nullptr )
        {
            if (OPGUICore::api_set_configElements(oai_config_elements,res,errorMsg)){
                reqObj->apiConfigElementsPostResponse(res);
            }
            else{
                int code=500;
                OAIError500 error;
                error.setMessage("Server error");
                if (!errorMsg.isEmpty()) {
                    error.setMessage(error.getMessage() + " - Details: " + errorMsg);
                }
                handleSocketResponse(reqObj, error.asJsonObject(), code);
            }
        }
        else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }
    }
}
