/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>
#include <QNetworkReply>

#include "OAIVerifyPathApiHandlerOP.h"
#include "OAIVerifyPathApiRequest.h"
#include "OAIVerifyPath_200_response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAICustomHelpers.h"
#include <OPGUICore.h>

namespace OpenAPI {

    OAIVerifyPathApiHandlerOP::OAIVerifyPathApiHandlerOP() = default;

    OAIVerifyPathApiHandlerOP::~OAIVerifyPathApiHandlerOP() = default;

    void OAIVerifyPathApiHandlerOP::verifyPath(OAIPathRequest oai_path_request) 
    {
        auto reqObj = qobject_cast<OAIVerifyPathApiRequest*>(sender());

        QString errorMsg;
        OAIVerifyPath_200_response res;

        if( reqObj != nullptr )
        {
            if (OPGUICore::api_verify_path(oai_path_request,res,errorMsg)){
                reqObj->verifyPathResponse(res);
            }
            else{
                int code=500;
                OAIError500 error;
                error.setMessage("Server error");
                if (!errorMsg.isEmpty()) {
                    error.setMessage(error.getMessage() + " - Details: " + errorMsg);
                }
                handleSocketResponse(reqObj, error.asJsonObject(), code);
            }
        }
        else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }
    }
}
