/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetValuesListApiHandlerOP_H
#define OAI_OAIGetValuesListApiHandlerOP_H

#include <QObject>
#include <QString>

#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_getValueList_get_200_response.h"
#include "OAIGetValueListApiHandler.h"


namespace OpenAPI {

class OAIGetValueListApiHandlerOP : public OAIGetValueListApiHandler
{
    Q_OBJECT

public:
    OAIGetValueListApiHandlerOP();
    ~OAIGetValueListApiHandlerOP() override;


public slots:
    void apiGetValueListGet(QString name) override;
};

}

#endif // OAI_OAIGetValuesListApiHandlerOP_H
