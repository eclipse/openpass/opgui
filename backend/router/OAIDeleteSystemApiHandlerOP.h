/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIDeleteSystemApiHandler_OP_H
#define OAI_OAIDeleteSystemApiHandler_OP_H

#include <QObject>
#include <QString>

#include "OAIDeleteSystemApiHandler.h"
#include "OAIError400.h"
#include "OAIError404.h"
#include "OAIError500.h"
#include "OAIDefault200Response.h"


namespace OpenAPI {

class OAIDeleteSystemApiHandlerOP : public OAIDeleteSystemApiHandler
{
    Q_OBJECT

public:
    OAIDeleteSystemApiHandlerOP();
    ~OAIDeleteSystemApiHandlerOP() override;


public slots:
    void apiDeleteSystemDelete(QString path) override;
    

};

}

#endif // OAI_OAIDeleteSystemApiHandler_OP_H
