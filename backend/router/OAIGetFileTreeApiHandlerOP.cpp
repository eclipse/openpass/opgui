/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OPGUICore.h"
#include "OAIGetFileTreeApiHandlerOP.h"
#include "OAIGetFileTreeApiRequest.h"
#include "OAICustomHelpers.h"

namespace OpenAPI {

OAIGetFileTreeApiHandlerOP::OAIGetFileTreeApiHandlerOP() = default;

OAIGetFileTreeApiHandlerOP::~OAIGetFileTreeApiHandlerOP() = default;

void OAIGetFileTreeApiHandlerOP::apiGetFileTreePost(OAIFileTreeRequest oai_file_tree_request) {
    auto reqObj = qobject_cast<OAIGetFileTreeApiRequest*>(sender());

    OAIFileTreeExtended res;

    if( reqObj != nullptr )
    {
        QString errorMsg; 

        if (OPGUICore::api_get_fileTree(oai_file_tree_request,res, errorMsg))
        {
            reqObj->apiGetFileTreePostResponse(res);
        }
        else
        {
            int code=500;
            OAIError500 error;
            error.setMessage("Server error");
            if (!errorMsg.isEmpty()) {
                error.setMessage(error.getMessage() + " - Details: " + errorMsg);
            }
            handleSocketResponse(reqObj, error.asJsonObject(), code);
        }
    }
    else{
            int code=400;
            OAIError400 error;
            error.setMessage("Bad client request");
            handleSocketResponse(reqObj, error.asJsonObject(), code);
    }
}


}
