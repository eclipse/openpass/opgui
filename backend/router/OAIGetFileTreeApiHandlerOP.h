/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAIGetFileTreeApiHandler_OP_H
#define OAI_OAIGetFileTreeApiHandler_OP_H

#include <QObject>

#include "OAIError400.h"
#include "OAIError500.h"
#include "OAIFileTreeExtended.h"
#include "OAIFileTreeRequest.h"
#include "OAIGetFileTreeApiHandler.h"

namespace OpenAPI {

class OAIGetFileTreeApiHandlerOP : public OAIGetFileTreeApiHandler
{
    Q_OBJECT

public:
    OAIGetFileTreeApiHandlerOP();
    ~OAIGetFileTreeApiHandlerOP() override;


public slots:
    void apiGetFileTreePost(OAIFileTreeRequest oai_file_tree_request) override;
    

};

}

#endif // OAI_OAIGetFileTreeApiHandler_H
