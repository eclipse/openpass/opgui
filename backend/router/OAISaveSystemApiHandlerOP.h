/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef OAI_OAISaveSystemApiHandler_OP_H
#define OAI_OAISaveSystemApiHandler_OP_H

#include <QObject>

#include "OAIDefault200Response.h"
#include "OAIError400.h"
#include "OAIError500.h"
#include "OAI_api_saveSystem_post_request.h"
#include "OAISaveSystemApiHandler.h"
#include <QString>

namespace OpenAPI {

class OAISaveSystemApiHandlerOP : public OAISaveSystemApiHandler
{
    Q_OBJECT

public:
    OAISaveSystemApiHandlerOP();
    ~OAISaveSystemApiHandlerOP() override;


public slots:
    virtual void apiSaveSystemPost(OAI_api_saveSystem_post_request oai_api_save_system_post_request) override;
    

};

}

#endif // OAI_OAISaveSystemApiHandler_OP_H
