
# Running Tests for the Project

This document provides the instructions necessary to execute tests for the application. Follow these steps to ensure a correct setup and execution of tests.

## Prerequisites

Before running the tests, make sure you have compiled the application with the required test configurations enabled. This can be achieved using the following commands based on your operating system:

- For Windows:
  \`\`\`bash
  yarn win-install
  \`\`\`

- For Debian-based Linux distributions:
  \`\`\`bash
  yarn deb-install
  \`\`\`

These commands compile the application and ensure that CMake is called with the `WITH_TESTS=ON` option, which is crucial for the tests to be included in the build.

## Configuration

After compilation, you must adapt the `backendconfig.json` file with the correct values. Refer to the main project README file for detailed instructions on the values that need to be configured.

## Running the Tests

To execute the tests:

1. Navigate to the `backend/build` directory:
   \`\`\`bash
   cd backend/build
   \`\`\`

2. Run the tests using the following command:
   \`\`\`bash
   make run-tests
   \`\`\`

3. Ensure that the tests pass without any errors. If there are failures, check the output logs for details and adjust configurations or setups as necessary.

## Test Coverage

Once you have built the project and executed the tests as outlined in the Test Execution chapter, you can proceed to obtain and review the test coverage data. Follow these steps to generate the coverage report:

### Generating Coverage Data

1. **Navigate to the Build Folder**:
   Change to the build directory where your project has been compiled.
   \`\`\`bash
   cd backend/build  # Go to the build folder
   \`\`\`

2. **Capture the Coverage Information**:
   Use `lcov` to capture the coverage data from all test executions. This command consolidates all coverage data into a single file.
   \`\`\`bash
   lcov --capture --directory . --output-file coverage.info --ignore-errors inconsistent     # Capture the coverage info
   \`\`\`

3. **Filter Coverage to Target Specific Files**:
   Extract coverage data specifically for the project's own developed `.cpp` files within the `backend/core` directory, ignoring third-party or external code.
   \`\`\`bash
   lcov --extract coverage.info "*/backend/core/**/*.cpp" --output-file coverage_filtered.info --ignore-errors inconsistent  # Focus on the project's own files
   \`\`\`

4. **Generate the HTML Coverage Report**:
   Convert the filtered coverage data into an HTML format, which is easier to read and navigate.
   \`\`\`bash
   genhtml coverage_filtered.info --output-directory coverage_report_specific --ignore-errors inconsistent # Generate human-readable coverage report
   \`\`\`

### Viewing the Coverage Report

After generating the coverage report, you can view it in a web browser to analyze the covered and uncovered code segments visually:

\`\`\`bash
open coverage_report_specific/index.html # Open the coverage report in a browser
\`\`\`

This will launch your default web browser and display the coverage report. Navigate through the report to review detailed coverage statistics and identify areas of the code that may need additional testing.
