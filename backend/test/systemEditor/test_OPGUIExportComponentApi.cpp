/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>
#include <QDebug>
#include <QDateTime>

#include "test_OPGUIExportComponentApi.h"
#include "OPGUICoreGlobalConfig.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include "OAIComponentUI.h"

QString componentExp1Xml = QStringLiteral(
    R"(<component>
      <type>Algorithm</type>
      <library>Algorithm_Test</library>
      <title>Algorithm_Test</title>
      <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
      </schedule>
      <parameters>
        <parameter>
			<title>Driver aggressiveness</title>
			<id>0</id>
			<type>double</type>
			<unit></unit>
			<value>1</value>
		</parameter>
		<parameter>
			<title>Max. engine power</title>
			<id>1</id>
			<type>double</type>
			<unit>W</unit>
			<value>100000</value>
		</parameter>
		<parameter>
			<title>Min. brake torque</title>
			<id>2</id>
			<type>double</type>
			<unit>Nm</unit>
			<value>-10000</value>
		</parameter>
		<parameter>
			<title>Kp of the pedals PID control</title>
			<id>3</id>
			<type>double</type>
			<unit></unit>
			<value>-0.5</value>
		</parameter>
		<parameter>
			<title>Ki of the pedals PID control</title>
			<id>4</id>
			<type>string</type>
			<unit></unit>
			<value>test</value>
		</parameter>
		<parameter>
			<title>Kd of the pedals PID control</title>
			<id>5</id>
			<type>bool</type>
			<unit></unit>
			<value>0</value>
		</parameter>
		<parameter>
			<title>Kp of the steering PID control</title>
			<id>6</id>
			<type>doubleVector</type>
			<unit></unit>
			<value>1.2,6.8,7,-0.15</value>
		</parameter>
		<parameter>
			<title>Ki of the steering PID control</title>
			<id>7</id>
			<type>intVector</type>
			<unit></unit>
			<value>1,3,5,0</value>
		</parameter>
		<parameter>
			<title>Kd of the steering PID control</title>
			<id>8</id>
			<type>boolVector</type>
			<unit></unit>
			<value>true,false,false,true</value>
		</parameter>
        <parameter>
            <title>Param Normal distribution</title>
            <id>9</id>
            <type>normalDistribution</type>
            <unit>-</unit>
            <value>
                <mean>0</mean>
                <sd>1</sd>
                <min>-3</min>
                <max>3</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Log normal distribution</title>
            <id>10</id>
            <type>logNormalDistribution</type>
            <unit>-</unit>
            <value>
                <mean>0.03</mean>
                <sd>0.5</sd>
                <mu>0</mu>
                <sigma>1</sigma>		
                <min>0.01</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Exponential distribution</title>
            <id>11</id>
            <type>exponentialDistribution</type>
            <unit>-</unit>
            <value>
                <mean>2</mean>
                <lambda>0.5</lambda>
                <min>0</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Uniform distribution</title>
            <id>12</id>
            <type>uniformDistribution</type>
            <unit>-</unit>
            <value>
                <min>0</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Gamma distribution</title>
            <id>13</id>
            <type>gammaDistribution</type>
            <unit>-</unit>
            <value>
                <mean>5</mean>
                <sd>2</sd>
                <shape>2</shape>
                <scale>2</scale>
                <min>0</min>
                <max>20</max>
            </value>
        </parameter>
      </parameters>
      <inputs>
        <input>
          <id>0</id>
          <type>TrajectoryEvent</type>
          <title>Desired trajectory</title>
          <unit></unit>
          <cardinality>1</cardinality>
        </input>
      </inputs>
      <outputs>
        <output>
          <id>0</id>
          <type>ControlData</type>
          <title>Control</title>
          <unit></unit>
        </output>
      </outputs>
    </component>)"
);

QString componentExp1JSON = QStringLiteral(
    R"({
        "id": 0,
        "inputs": [
            {
                "cardinality": 1,
                "id": 0,
                "title": "Desired trajectory",
                "type": "TrajectoryEvent",
                "unit": ""
            }
        ],
        "library": "Algorithm_Test",
        "outputs": [
            {
                "id": 0,
                "title": "Control",
                "type": "ControlData",
                "unit": ""
            }
        ],
        "parameters": [
            {
                "id": "0",
                "title": "Driver aggressiveness",
                "type": "double",
                "unit": "",
                "value": "1"
            },
            {
                "id": "1",
                "title": "Max. engine power",
                "type": "double",
                "unit": "W",
                "value": "100000"
            },
            {
                "id": "2",
                "title": "Min. brake torque",
                "type": "double",
                "unit": "Nm",
                "value": "-10000"
            },
            {
                "id": "3",
                "title": "Kp of the pedals PID control",
                "type": "double",
                "unit": "",
                "value": "-0.5"
            },
            {
                "id": "4",
                "title": "Ki of the pedals PID control",
                "type": "string",
                "unit": "",
                "value": "test"
            },
            {
                "id": "5",
                "title": "Kd of the pedals PID control",
                "type": "bool",
                "unit": "",
                "value": "0"
            },
            {
                "id": "6",
                "title": "Kp of the steering PID control",
                "type": "doubleVector",
                "unit": "",
                "value": "[1.2,6.8,7,-0.15]"
            },
            {
                "id": "7",
                "title": "Ki of the steering PID control",
                "type": "intVector",
                "unit": "",
                "value": "[1,3,5,0]"
            },
            {
                "id": "8",
                "title": "Kd of the steering PID control",
                "type": "boolVector",
                "unit": "",
                "value": "[true,false,false,true]"
            },
            {
                "id": "9",
                "type": "normalDistribution",
                "title": "Param Normal distribution",
                "unit": "-",
                "value": "{\"mean\":0,\"sd\":1,\"min\": -3,\"max\":3}"
            },
            {
                "id": "10",
                "type": "logNormalDistribution",
                "title": "Param Log normal distribution",
                "unit": "-",
                "value": "{\"mean\":0.03,\"sd\":0.5,\"mu\":0,\"sigma\":1,\"min\":0.01,\"max\":10}"
            },
            {
                "id": "11",
                "type": "exponentialDistribution",
                "title": "Param Exponential distribution",
                "unit": "-",
                "value": "{\"mean\":2,\"lambda\":0.5,\"min\":0,\"max\": 10}"
            },
            {
                "id": "12",
                "type": "uniformDistribution",
                "title": "Param Uniform distribution",
                "unit": "-",
                "value": "{\"min\":0,\"max\":10}"
            },
            {
                "id": "13",
                "type": "gammaDistribution",
                "title": "Param Gamma distribution",
                "unit": "-",
                "value": "{\"mean\":5,\"sd\":2,\"shape\":2,\"scale\":2,\"min\":0,\"max\":20}"
            }
        ],
        "schedule": {
            "cycle": 10,
            "offset": 0,
            "response": 0
        },
        "title": "Algorithm_Test",
        "type": "Algorithm"
       })"
);

bool EXPORT_COMPONENT_TEST::parseComponentJson(const QString& jsonStr, OpenAPI::OAIComponentUI& oai_component) {
    QJsonDocument doc = QJsonDocument::fromJson(jsonStr.toUtf8());

    if (doc.isNull()) {
        qDebug() << "Failed to create JSON doc.";
        return false;
    }

    if (!doc.isObject()) {
        qDebug() << "JSON doc is not an object.";
        return false;
    }

    QJsonObject jsonObj = doc.object();

    OpenAPI::fromJsonValue(oai_component, jsonObj);

    return true;
}

void EXPORT_COMPONENT_TEST::SetUp() {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    //ceck path to workspace exists
    QDir dirWkspc(OPGUICoreGlobalConfig::getInstance().workspace());
    ASSERT_TRUE(dirWkspc.exists())<<"Core directory not found under:"+OPGUICoreGlobalConfig::getInstance().workspace().toStdString();
    
    this->testDirComponentsPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_components");
    OPGUICoreGlobalConfig::getInstance().setFullPathComponentsFolder(this->testDirComponentsPath);
    OPGUICoreGlobalConfig::getInstance().setFullPathModulesFolder(this->testDirComponentsPath);

    // If test directory exists, remove it.
    QDir dirTest(this->testDirComponentsPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove existing test directory: " <<  this->testDirComponentsPath.toStdString();
    }

    QString libraryFileFullPath=this->testDirComponentsPath+"/Algorithm_Test.dll";
    ASSERT_TRUE(dirTest.mkpath(this->testDirComponentsPath)) << "Failed to create test directory at: " << this->testDirComponentsPath.toStdString();
    ASSERT_TRUE(TestHelpers::createAndCheckFile(libraryFileFullPath,"placeholder"))<<"Could not save mock .dll file as: "+libraryFileFullPath.toStdString();
}

void EXPORT_COMPONENT_TEST::TearDown() {
    QDir dirTest(this->testDirComponentsPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove test directory: " << this->testDirComponentsPath.toStdString();
    }
}

TEST_F(EXPORT_COMPONENT_TEST, Export_component_positive) {
    OpenAPI::OAIComponentUI componentReq;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    ASSERT_TRUE(parseComponentJson(componentExp1JSON, componentReq))<< "Failed preparing the test, unable parse the systems JSON for the test";

    bool result = this->systemEditorExport.exportComponentToXml(xmlWriter, componentReq, false,errorMsg);

    ASSERT_TRUE(result) << "Export component failed";

    QString generatedXML = QString::fromUtf8(byteArray);

    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(componentExp1Xml.simplified()),
    TestHelpers::removeSpacesBetweenTags(generatedXML.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << TestHelpers::removeSpacesBetweenTags(componentExp1Xml.simplified()).toStdString() << "\n"
        << "Actual XML:\n" << TestHelpers::removeSpacesBetweenTags(generatedXML.simplified()).toStdString();
        
}

TEST_F(EXPORT_COMPONENT_TEST, Export_component_no_title_negative) {
    OpenAPI::OAIComponentUI componentReq;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    QString modifiedJSON = TestHelpers::removeJsonElement(componentExp1JSON,"title");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseComponentJson(modifiedJSON, componentReq))<< "Failed preparing the test, unable parse the components JSON for the test";

    bool result = this->systemEditorExport.exportComponentToXml(xmlWriter, componentReq, false,errorMsg);

    ASSERT_FALSE(result) << "Export component succeed but fail was expected";  
}


TEST_F(EXPORT_COMPONENT_TEST, Export_component_wrong_schedule_negative) {
    OpenAPI::OAIComponentUI componentReq;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    QString modifiedJSON = TestHelpers::removeJsonElement(componentExp1JSON,"offset");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseComponentJson(modifiedJSON, componentReq))<< "Failed preparing the test, unable parse the components JSON for the test";

    bool result = this->systemEditorExport.exportComponentToXml(xmlWriter, componentReq, false,errorMsg);

    ASSERT_FALSE(result) << "Export component succeed but fail was expected";  
}

TEST_F(EXPORT_COMPONENT_TEST, Export_component_wrong_parameter_negative) {
    OpenAPI::OAIComponentUI componentReq;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    QString modifiedJSON = TestHelpers::removeJsonElement(componentExp1JSON,"unit");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseComponentJson(modifiedJSON, componentReq))<< "Failed preparing the test, unable parse the components JSON for the test";

    bool result = this->systemEditorExport.exportComponentToXml(xmlWriter, componentReq, false,errorMsg);

    ASSERT_FALSE(result) << "Export component succeed but fail was expected";  
}

TEST_F(EXPORT_COMPONENT_TEST, Export_component_wrong_input_negative) {
    OpenAPI::OAIComponentUI componentReq;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    QString modifiedJSON = TestHelpers::removeJsonElement(componentExp1JSON,"cardinality");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseComponentJson(modifiedJSON, componentReq))<< "Failed preparing the test, unable parse the components JSON for the test";

    bool result = this->systemEditorExport.exportComponentToXml(xmlWriter, componentReq, false,errorMsg);

    ASSERT_FALSE(result) << "Export component succeed but fail was expected";  
}

TEST_F(EXPORT_COMPONENT_TEST, Export_component_wrong_output_negative) {
    OpenAPI::OAIOutput output;
    QString errorMsg;

    QByteArray byteArray;
    QXmlStreamWriter xmlWriter(&byteArray);

    output.setTitle("test");

    bool result = this->systemEditorExport.exportOutputToXml(xmlWriter, output, errorMsg);

    ASSERT_FALSE(result) << "Export component succeed but fail was expected";  
}