/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>

#include "test_OPGUILoadComponentsApi.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include "OAIComponentUI.h"
#include "OAIError500.h"

QString component1Xml = QStringLiteral(
    R"(<component>
      <type>AlgorithmTest</type>
      <library>Algorithm_Test</library>
      <title>Algorithm_Test</title>
      <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
      </schedule>
      <parameters>
        <parameter>
          <id>0</id>
          <type>double</type>
          <title>Driver aggressiveness</title>
          <unit></unit>
          <value>1.0</value>
        </parameter>
        <parameter>
          <id>1</id>
          <type>double</type>
          <title>Max. engine power</title>
          <unit>W</unit>
          <value>100000</value>
        </parameter>
            <parameter>
          <id>2</id>
          <type>int</type>
          <title>Min. brake torque</title>
          <unit>Nm</unit>
          <value>100</value>
        </parameter>
        <parameter>
          <id>3</id>
          <type>bool</type>
          <title>Kp of the pedals PID control</title>
          <unit></unit>
          <value>true</value>
        </parameter>
        <parameter>
          <id>4</id>
          <type>string</type>
          <title>Ki of the pedals PID control</title>
          <unit></unit>
          <value>string param</value>
        </parameter>
        <parameter>
          <id>5</id>
          <type>boolVector</type>
          <title>boolVector test</title>
          <unit></unit>
          <value>true,false,0,true,false</value>
        </parameter>
        <parameter>
          <id>6</id>
          <type>intVector</type>
          <title>intVector test</title>
          <unit></unit>
          <value>2,3,4,-2</value>
        </parameter>
        <parameter>
          <id>7</id>
          <type>doubleVector</type>
          <title>doubleVector test</title>
          <unit></unit>
          <value>1.0,2.0,0.1</value>
        </parameter>
        <parameter>
          <id>8</id>
          <type>stringVector</type>
          <title>stringVector test</title>
          <unit></unit>
          <value>someString,anotherone,another,another</value>
        </parameter>
        <parameter>
            <title>Param Normal distribution</title>
            <id>9</id>
            <type>normalDistribution</type>
            <unit>-</unit>
            <value>
                <mean>0</mean>
                <sd>1</sd>
                <min>-3</min>
                <max>3</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Log normal distribution</title>
            <id>10</id>
            <type>logNormalDistribution</type>
            <unit>-</unit>
            <value>
                <mean>0.03</mean>
                <sd>0.5</sd>
                <mu>0</mu>
                <sigma>1</sigma>		
                <min>0.01</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Exponential distribution</title>
            <id>11</id>
            <type>exponentialDistribution</type>
            <unit>-</unit>
            <value>
                <mean>2</mean>
                <lambda>0.5</lambda>
                <min>0</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Uniform distribution</title>
            <id>12</id>
            <type>uniformDistribution</type>
            <unit>-</unit>
            <value>
                <min>0</min>
                <max>10</max>
            </value>
        </parameter>
        <parameter>
            <title>Param Gamma distribution</title>
            <id>13</id>
            <type>gammaDistribution</type>
            <unit>-</unit>
            <value>
                <mean>5</mean>
                <sd>2</sd>
                <shape>2</shape>
                <scale>2</scale>
                <min>0</min>
                <max>20</max>
            </value>
        </parameter>
      </parameters>
      <inputs>
        <input>
          <id>0</id>
          <type>TrajectoryEvent</type>
          <title>Desired trajectory</title>
          <unit></unit>
          <cardinality>1</cardinality>
        </input>
      </inputs>
      <outputs>
        <output>
          <id>0</id>
          <type>ControlData</type>
          <title>Control</title>
          <unit></unit>
        </output>
      </outputs>
    </component>)"
);

QString component1JSON = QStringLiteral(
    R"([{"id": 0,
        "inputs": [
            {
                "cardinality": 1,
                "id": 0,
                "title": "Desired trajectory",
                "type": "TrajectoryEvent",
                "unit": ""
            }
      ],
      "library": "Algorithm_Test",
        "outputs": [
            {
                "id": 0,
                "title": "Control",
                "type": "ControlData",
                "unit": ""
            }
        ],
        "parameters": [
            {
                "id": "0",
                "title": "Driver aggressiveness",
                "type": "double",
                "unit": "",
                "value": "1.0"
            },
            {
                "id": "1",
                "title": "Max. engine power",
                "type": "double",
                "unit": "W",
                "value": "100000"
            },
            {
                "id": "2",
                "title": "Min. brake torque",
                "type": "int",
                "unit": "Nm",
                "value": "100"
            },
            {
                "id": "3",
                "title": "Kp of the pedals PID control",
                "type": "bool",
                "unit": "",
                "value": "true"
            },
            {
                "id": "4",
                "title": "Ki of the pedals PID control",
                "type": "string",
                "unit": "",
                "value": "string param"
            },
            {
                "id": "5",
                "title": "boolVector test",
                "type": "boolVector",
                "unit": "",
                "value": "[true,false,false,true,false]"
            },
            {
                "id": "6",
                "title": "intVector test",
                "type": "intVector",
                "unit": "",
                "value": "[2,3,4,-2]"
            },
            {
                "id": "7",
                "title": "doubleVector test",
                "type": "doubleVector",
                "unit": "",
                "value": "[1.0,2.0,0.1]"
            },
            {
                "id": "8",
                "title": "stringVector test",
                "type": "stringVector",
                "unit": "",
                "value": "[someString,anotherone,another,another]"
            },
            {
                "id": "9",
                "type": "normalDistribution",
                "title": "Param Normal distribution",
                "unit": "-",
                "value": "{\"max\":3,\"mean\":0,\"min\":-3,\"sd\":1}"
            },
            {
                "id": "10",
                "type": "logNormalDistribution",
                "title": "Param Log normal distribution",
                "unit": "-",
                "value": "{\"max\":10,\"mean\":0.03,\"min\":0.01,\"mu\":0,\"sd\":0.5,\"sigma\":1}"
            },
            {
                "id": "11",
                "type": "exponentialDistribution",
                "title": "Param Exponential distribution",
                "unit": "-",
                "value": "{\"lambda\":0.5,\"max\":10,\"mean\":2,\"min\":0}"
            },
            {
                "id": "12",
                "type": "uniformDistribution",
                "title": "Param Uniform distribution",
                "unit": "-",
                "value": "{\"max\":10,\"min\":0}"
            },
            {
                "id": "13",
                "type": "gammaDistribution",
                "title": "Param Gamma distribution",
                "unit": "-",
                "value": "{\"max\":20,\"mean\":5,\"min\":0,\"scale\":2,\"sd\":2,\"shape\":2}"
            }
        ],
        "position": {
            "x": 0,
            "y": 0
        },
        "schedule": {
            "cycle": 10,
            "offset": 0,
            "response": 0,
            "priority": 0
        },
        "title": "Algorithm_Test",
        "type": "AlgorithmTest"
    }])"
);


void LOAD_COMPONENTS_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    //ceck path to workspace exists
    QDir dirWkspc(OPGUICoreGlobalConfig::getInstance().workspace());
    ASSERT_TRUE(dirWkspc.exists())<<"Core directory not found under:"+OPGUICoreGlobalConfig::getInstance().workspace().toStdString();
    
    this->testDirComponentsPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_components");
    OPGUICoreGlobalConfig::getInstance().setFullPathComponentsFolder(this->testDirComponentsPath);
    OPGUICoreGlobalConfig::getInstance().setFullPathModulesFolder(this->testDirComponentsPath);

    // If test directory exists, remove it.
    QDir dirTest(this->testDirComponentsPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove existing test directory: " <<  this->testDirComponentsPath.toStdString();
    }

    QString libraryFileFullPath=this->testDirComponentsPath+"/Algorithm_Test.dll";
    ASSERT_TRUE(dirTest.mkpath(this->testDirComponentsPath)) << "Failed to create test directory at: " << this->testDirComponentsPath.toStdString();
    ASSERT_TRUE(TestHelpers::createAndCheckFile(libraryFileFullPath,"placeholder"))<<"Could not save mock .dll file as: "+libraryFileFullPath.toStdString();
}

void LOAD_COMPONENTS_TEST::TearDown()  {
    QDir dirTest(this->testDirComponentsPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove test directory: " << this->testDirComponentsPath.toStdString();
    }
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_no_components_dir_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    OPGUICoreGlobalConfig::getInstance().setFullPathComponentsFolder("/tmp/not_existing_dir");

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_EQ(errorMsg,"Directory does not exist.")<<"Error message was expected to be 'Directory does not exist.' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_no_components_inside_dir_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_EQ(errorMsg,"No components files found in directory.")<<"Error message was expected to be 'No components files found in directory.' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_empty_component_file_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath, ""))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Component XML file is empty"))<<"Error message was expected to contain 'Component XML file is empty' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_empty_bad_xml_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath, "CONTENT NOT VALID XML"))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Invalid XML content in XML component file"))<<"Error message was expected to contain 'Invalid XML content in XML component file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_title_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"title");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_type_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"type");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_parameters_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"parameters");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_parameter_title_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"title","parameter");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_parameter_id_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"id","parameter");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_inputs_field_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"inputs");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_no_outputs_field_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"inputs");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_invalid_input_field_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"id","input");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_invalid_output_field_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"id","output");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_invalid_schedule_field_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    QString modifiedXml = TestHelpers::removeXmlElement(component1Xml,"offset","schedule");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_component_has_several_invalid_sub_fields_NEGATIVE) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    //parameter id not int
    QString modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"id","","parameter");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    bool result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //parameter type int but value not convertible
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"type","int","parameter");
    modifiedXml = TestHelpers::replaceXmlElementContent(modifiedXml,"value","bad int","parameter");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //parameter type bool but value not convertible
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"type","bool","parameter");
    modifiedXml = TestHelpers::replaceXmlElementContent(modifiedXml,"value","bad bool","parameter");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //parameter type float but value not convertible
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"type","float","parameter");
    modifiedXml = TestHelpers::replaceXmlElementContent(modifiedXml,"value","bad float","parameter");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //parameter type unnknown
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"type","unknown","parameter");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //input id not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"id","bad int","input");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //input cardinality not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"cardinality","bad int","input");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //output id not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"id","bad int","output");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //schedule offset not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"offset","bad int","schedule");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //schedule cycle not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"cycle","bad int","schedule");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();

    //schedule response not int
    modifiedXml = TestHelpers::replaceXmlElementContent(component1Xml,"response","bad int","schedule");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath,modifiedXml))<<"Could not save xml file as: "+filePath.toStdString();
    result = OPGUICore::api_get_components(components, errorMsg);
    ASSERT_FALSE(result) << "Load components was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML component from file"))<<"Error message was expected to contain 'Failed to parse XML component from file' but was "+errorMsg.toStdString();
}

TEST_F(LOAD_COMPONENTS_TEST, Load_components_1_component_loaded_positive) {
    QList<OpenAPI::OAIComponentUI> components;
    QString errorMsg;

    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component.xml");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath, component1Xml))<<"Could not save xml file as: "+filePath.toStdString();

    bool result = OPGUICore::api_get_components(components, errorMsg);

    ASSERT_TRUE(result) << "Load components API call failed";

    QJsonDocument resDoc(::OpenAPI::toJsonValue(components).toArray());

    QByteArray jsonBytes = resDoc.toJson(QJsonDocument::Indented);
    QString actualJsonString = QString(jsonBytes);

    QJsonDocument expectedDoc = QJsonDocument::fromJson(component1JSON.toUtf8());
    QJsonDocument actualDoc = QJsonDocument::fromJson(actualJsonString.toUtf8());

    QString expectedJsonStr = QString(expectedDoc.toJson(QJsonDocument::Compact));
    QString actualJsonStr = QString(actualDoc.toJson(QJsonDocument::Compact));

    ASSERT_EQ(expectedJsonStr, actualJsonStr) 
        << "JSON objects are not equal.\nExpected: " << expectedJsonStr.toStdString() 
        << "\nActual: " << actualJsonStr.toStdString();
}






