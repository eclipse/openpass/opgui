/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>
#include <QDebug>
#include <QDateTime>

#include "test_OPGUISaveSystemApi.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include "OAIComponentUI.h"
#include "OAIError500.h"
#include "OAISystemUI.h"
#include "OAIConnection.h"
#include "OAIOutput.h"
#include "OAIInput.h"

QString componentSysExp1Xml = QStringLiteral(
    R"(<component>
      <type>AlgorithmTest</type>
      <library>Algorithm_Test</library>
      <title>Algorithm_Test</title>
      <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
      </schedule>
      <parameters>
        <parameter>
          <id>0</id>
          <type>double</type>
          <title>Driver aggressiveness</title>
          <unit></unit>
          <value>1.0</value>
        </parameter>
        <parameter>
          <id>1</id>
          <type>double</type>
          <title>Max. engine power</title>
          <unit>W</unit>
          <value>100000</value>
        </parameter>
        <parameter>
          <id>2</id>
          <type>int</type>
          <title>Min. brake torque</title>
          <unit>Nm</unit>
          <value>100</value>
        </parameter>
        <parameter>
          <id>3</id>
          <type>bool</type>
          <title>Kp of the pedals PID control</title>
          <unit></unit>
          <value>true</value>
        </parameter>
        <parameter>
          <id>4</id>
          <type>string</type>
          <title>Ki of the pedals PID control</title>
          <unit></unit>
          <value>string param</value>
        </parameter>
      </parameters>
      <inputs>
        <input>
          <id>0</id>
          <type>TrajectoryEvent</type>
          <title>Desired trajectory</title>
          <unit></unit>
          <cardinality>1</cardinality>
        </input>
      </inputs>
      <outputs>
        <output>
          <id>0</id>
          <type>ControlData</type>
          <title>Control</title>
          <unit></unit>
        </output>
      </outputs>
    </component>)"
);

QString componentSysExp2Xml = QStringLiteral(
    R"(<component>
    <type>Action</type>
    <library>Algorithm_Test</library>
    <title>Action_Test</title>
    <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
    </schedule>
    <parameters>
        <parameter>
        <id>0</id>
        <type>double</type>
        <title>Spring coefficient</title>
        <unit></unit>
        <value>1200000</value>
        </parameter>
        <parameter>
        <id>1</id>
        <type>double</type>
        <title>Damper coefficient</title>
        <unit></unit>
        <value>12000</value>
        </parameter>
    </parameters>
    <inputs>
        <input>
        <id>0</id>
        <type>VectorDouble</type>
        <title>InertiaForce</title>
        <unit>N</unit>
        <cardinality>1</cardinality>
        </input>
    </inputs>
    <outputs>
        <output>
        <id>0</id>
        <type>VectorDouble</type>
        <title>VerticalForce</title>
        <unit>N</unit>
        </output>
    </outputs>
    </component>)"
);

QString systemExp1Xml = QStringLiteral(
    R"(<?xml version="1.0" encoding="UTF-8"?>
    <systems>
        <system>
        <id>0</id>
        <title>System_test</title>
        <priority>0</priority>
        <components>
        <component>
            <id>0</id>
            <library>Algorithm_Test</library>
            <title>Algorithm_Test</title>
            <schedule>
            <offset>0</offset>
            <cycle>10</cycle>
            <response>0</response>
            <priority>30</priority>
            </schedule>
            <parameters>
            <parameter>
            <!--parameter's title: Driver aggressiveness-->
            <id>0</id>
            <type>double</type>
            <unit/>
            <value>1</value>
            </parameter>
            <parameter>
            <!--parameter's title: Max. engine power-->
            <id>1</id>
            <type>double</type>
            <unit>W</unit>
            <value>100000</value>
            </parameter>
            <parameter>
            <!--parameter's title: Min. brake torque-->
            <id>2</id>
            <type>double</type>
            <unit>Nm</unit>
            <value>-10000</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kp of the pedals PID control-->
            <id>3</id>
            <type>double</type>
            <unit/>
            <value>-0.5</value>
            </parameter>
            <parameter>
            <!--parameter's title: Ki of the pedals PID control-->
            <id>4</id>
            <type>double</type>
            <unit/>
            <value>-0.4</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kd of the pedals PID control-->
            <id>5</id>
            <type>double</type>
            <unit/>
            <value>0.0</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kp of the steering PID control-->
            <id>6</id>
            <type>double</type>
            <unit/>
            <value>-18</value>
            </parameter>
            <parameter>
            <!--parameter's title: Ki of the steering PID control-->
            <id>7</id>
            <type>double</type>
            <unit/>
            <value>-0.6</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kd of the steering PID control-->
            <id>8</id>
            <type>double</type>
            <unit/>
            <value>0.0</value>
            </parameter>
            </parameters>
            <position>
            <x>296</x>
            <y>20</y>
            </position>
        </component>
        <component>
            <id>1</id>
            <library>Action_Test</library>
            <title>Action_Test</title>
            <schedule>
            <offset>0</offset>
            <cycle>10</cycle>
            <response>0</response>
            <priority>1</priority>
            </schedule>
            <parameters>
            <parameter>
            <!--parameter's title: Spring coefficient-->
            <id>0</id>
            <type>double</type>
            <unit/>
            <value>1.2e+06</value>
            </parameter>
            <parameter>
            <!--parameter's title: Damper coefficient-->
            <id>1</id>
            <type>double</type>
            <unit/>
            <value>12000</value>
            </parameter>
            </parameters>
            <position>
            <x>815</x>
            <y>692</y>
            </position>
        </component>
        </components>
        <connections>
            <connection>
                <id>0</id>
                <source>
                <!--Component Title: Action_Test-->
                <component>1</component>
                <!--Output Title: VerticalForce-->
                <output>0</output>
                </source>
                <target>
                <!--Component Title: Algorithm_Test-->
                <component>0</component>
                <!--Input Title: Desired trajectory-->
                <input>0</input>
                </target>
            </connection>
            <connection>
                <id>1</id>
                <source>
                <!--Component Title: Algorithm_Test-->
                <component>0</component>
                <!--Output Title: Control-->
                <output>0</output>
                </source>
                <target>
                <!--Component Title: Action_Test-->
                <component>1</component>
                <!--Input Title: InertiaForce-->
                <input>0</input>
                </target>
            </connection>
        </connections>
        <comments>
            <comment>
                <message>this is just an example comment</message>
                <title>comment 1</title>
                <position>
                <x>1</x>
                <y>20</y>
                </position>
            </comment>
            <comment>
                <message>this is another example comment</message>
                <title>comment 2</title>
                <position>
                <x>815</x>
                <y>692</y>
                </position>
            </comment>
        </comments>
        </system>
    </systems>)"
);

QString systemExp1JSON = QStringLiteral(
    R"({
        "components": [
            {
                "id": 0,
                "inputs": [
                    {
                        "cardinality": 1,
                        "id": 0,
                        "title": "Desired trajectory",
                        "type": "TrajectoryEvent",
                        "unit": ""
                    }
                ],
                "library": "Algorithm_Test",
                "outputs": [
                    {
                        "id": 0,
                        "title": "Control",
                        "type": "ControlData",
                        "unit": ""
                    }
                ],
                "parameters": [
                    {
                        "id": 0,
                        "title": "Driver aggressiveness",
                        "type": "double",
                        "unit": "",
                        "value": "1"
                    },
                    {
                        "id": 1,
                        "title": "Max. engine power",
                        "type": "double",
                        "unit": "W",
                        "value": "100000"
                    },
                    {
                        "id": 2,
                        "title": "Min. brake torque",
                        "type": "double",
                        "unit": "Nm",
                        "value": "-10000"
                    },
                    {
                        "id": 3,
                        "title": "Kp of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.5"
                    },
                    {
                        "id": 4,
                        "title": "Ki of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.4"
                    },
                    {
                        "id": 5,
                        "title": "Kd of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "0.0"
                    },
                    {
                        "id": 6,
                        "title": "Kp of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-18"
                    },
                    {
                        "id": 7,
                        "title": "Ki of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.6"
                    },
                    {
                        "id": 8,
                        "title": "Kd of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "0.0"
                    }
                ],
                "position": {
                    "x": 296,
                    "y": 20
                },
                "schedule": {
                    "cycle": 10,
                    "offset": 0,
                    "priority": 30,
                    "response": 0
                },
                "title": "Algorithm_Test",
                "type": "Algorithm"
            },
            {
                "id": 1,
                "inputs": [
                    {
                        "cardinality": 1,
                        "id": 0,
                        "title": "InertiaForce",
                        "type": "VectorDouble",
                        "unit": "N"
                    }
                ],
                "library": "Action_Test",
                "outputs": [
                    {
                        "id": 0,
                        "title": "VerticalForce",
                        "type": "VectorDouble",
                        "unit": "N"
                    }
                ],
                "parameters": [
                    {
                        "id": 0,
                        "title": "Spring coefficient",
                        "type": "double",
                        "unit": "",
                        "value": "1.2e+06"
                    },
                    {
                        "id": 1,
                        "title": "Damper coefficient",
                        "type": "double",
                        "unit": "",
                        "value": "12000"
                    }
                ],
                "position": {
                    "x": 815,
                    "y": 692
                },
                "schedule": {
                    "cycle": 10,
                    "offset": 0,
                    "priority": 1,
                    "response": 0
                },
                "title": "Action_Test",
                "type": "Action"
            }
        ],
        "connections": [
            {
                "id": 0,
                "source": {
                    "component": 1,
                    "output": 0
                },
                "targets": [
                    {
                    "component": 0,
                    "input": 0
                    }
                ]
            },
            {
                "id": 1,
                "source": {
                    "component": 0,
                    "output": 0
                },
                "targets": [
                    {
                    "component": 1,
                    "input": 0
                    }
                ]
            }
        ],
        "comments": [
            {
                "title": "comment 1",
                "message": "this is just an example comment",
                "position": {
                        "x": 1,
                        "y": 20
                }
            },
            {
                "title": "comment 2",
                "message": "this is another example comment",
                "position": {
                        "x": 815,
                        "y": 692
                }
            }
        ],
        "id": 0,
        "priority": 0,
        "title": "System_test",
        "deleted":false,
        "file":"test_system.xml"
    })"
);

bool SAVE_SYSTEM_TEST::parseSystemJson(const QString& jsonStr, OpenAPI::OAISystemUI& oai_system_ui) {
    QJsonDocument doc = QJsonDocument::fromJson(jsonStr.toUtf8());
    if (doc.isNull()) {
        qDebug() << "Failed to create JSON doc.";
        return false; 
    }

    // Check if the JSON document is an object
    if (!doc.isObject()) {
        qDebug() << "JSON obtained is not an object";
        return false;  
    }
    
    QJsonObject jsonObject = doc.object();

    OpenAPI::fromJsonValue(oai_system_ui, jsonObject);

    return true;
}

void SAVE_SYSTEM_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    //ceck path to workspace exists
    QDir dirWkspc(OPGUICoreGlobalConfig::getInstance().workspace());
    ASSERT_TRUE(dirWkspc.exists())<<"Core directory not found under:"+OPGUICoreGlobalConfig::getInstance().workspace().toStdString();
    
    this->testDirComponentsPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_components");
    OPGUICoreGlobalConfig::getInstance().setFullPathComponentsFolder(this->testDirComponentsPath);

    this->testDirPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_systems");
    this->testSystemFilePath = TestHelpers::joinPaths(this->testDirPath,"test_system.xml");

    // If test directory exists, remove them.
    QDir dirCompTest(this->testDirComponentsPath);
    if(dirCompTest.exists()){
        ASSERT_TRUE(dirCompTest.removeRecursively()) << "Failed to remove existing components test directory: " << this->testDirComponentsPath.toStdString();
    }
    QDir dirSysTest(this->testDirPath);
    if(dirSysTest.exists()){
        ASSERT_TRUE(dirSysTest.removeRecursively()) << "Failed to remove existing systems test directory: " << this->testDirPath.toStdString();
    }

    ASSERT_TRUE(dirCompTest.mkpath(this->testDirComponentsPath)) << "Failed to create components test directory at: " << this->testDirComponentsPath.toStdString();
    ASSERT_TRUE(dirSysTest.mkpath(this->testDirPath)) << "Failed to create systems test directory at: " << this->testDirPath.toStdString();
    
    //add the two used components used by the system in the test folder
    QString filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component_1.xml");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath, componentSysExp1Xml))<<"Could not save xml file as: "+filePath.toStdString();
    filePath = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component_2.xml");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(filePath, componentSysExp2Xml))<<"Could not save xml file as: "+filePath.toStdString();

    //add the library for the module (just a mock file)
    OPGUICoreGlobalConfig::getInstance().setFullPathModulesFolder(this->testDirComponentsPath);

    QString dllFilePath1 = OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder()+"/"+"Algorithm_Test.dll";
    ASSERT_TRUE(TestHelpers::deleteFileIfExists(dllFilePath1))<<"Could not delete mock .dll lib as: "+dllFilePath1.toStdString();
    ASSERT_TRUE(TestHelpers::createAndCheckFile(dllFilePath1,"mock file contents for test"))<<"Could not save mock .dll lib as: "+dllFilePath1.toStdString();
    QString dllFilePath2 = OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder()+"/"+"Action_Test.dll";
    ASSERT_TRUE(TestHelpers::deleteFileIfExists(dllFilePath2))<<"Could not delete mock .dll lib as: "+dllFilePath2.toStdString();
    ASSERT_TRUE(TestHelpers::createAndCheckFile(dllFilePath2,"mock file contents for test"))<<"Could not save mock .dll lib as: "+dllFilePath2.toStdString();
}

void SAVE_SYSTEM_TEST::TearDown()  {
    QDir dirCompTest(this->testDirComponentsPath);
    if(dirCompTest.exists()){
        ASSERT_TRUE(dirCompTest.removeRecursively()) << "Failed to remove components test directory: " << this->testDirComponentsPath.toStdString();
    }
    QDir dirSysTest(this->testDirPath);
    if(dirSysTest.exists()){
        ASSERT_TRUE(dirSysTest.removeRecursively()) << "Failed to remove systems test directory: " << this->testDirPath.toStdString();
    }

    QString dllFilePath1 = OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder()+"/"+"Algorithm_Test.dll";
    ASSERT_TRUE(TestHelpers::deleteFileIfExists(dllFilePath1))<<"Could not delete mock .dll lib as: "+dllFilePath1.toStdString();
    QString dllFilePath2 = OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder()+"/"+"Action_Test.dll";
    ASSERT_TRUE(TestHelpers::deleteFileIfExists(dllFilePath2))<<"Could not delete mock .dll lib as: "+dllFilePath2.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_POSITIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;


    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response,errorMsg);

    ASSERT_TRUE(result) << "Export Systems API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("System correctly saved"))<<"Error message was expected to contain 'System correctly saved' but was "+this->response.getResponse().toStdString();

    QString actualXml = TestHelpers::readFile(this->testSystemFilePath);
    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(systemExp1Xml.simplified()),
    TestHelpers::removeSpacesBetweenTags(actualXml.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << systemExp1Xml.simplified().toStdString() << "\n"
        << "Actual XML:\n" << actualXml.simplified().toStdString();   
}

TEST_F(SAVE_SYSTEM_TEST, Overwrite_system_POSITIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;


    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    //
    // First operation to generate the initial file
    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_TRUE(result) << "Export Systems API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("System correctly saved"))<<"Error message was expected to contain 'System correctly saved' but was "+this->response.getResponse().toStdString();

    QString actualXml = TestHelpers::readFile(this->testSystemFilePath);
    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(systemExp1Xml.simplified()),
    TestHelpers::removeSpacesBetweenTags(actualXml.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << systemExp1Xml.simplified().toStdString() << "\n"
        << "Actual XML:\n" << actualXml.simplified().toStdString(); 
    
    // Temporarily modify the file to ensure it gets overwritten
    QString temporaryModification = "temporary modification to file";
    QFile file(this->testSystemFilePath);
    if (file.open(QIODevice::Append | QIODevice::Text)) {
        QTextStream out(&file);
        out << temporaryModification;
        file.close();
    }

    rec.setIsNew(false);

    result = OPGUICore::api_save_system(rec, this->response,errorMsg);

    ASSERT_TRUE(result) << "Export Systems API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("System correctly saved"))<<"Error message was expected to contain 'System correctly saved' but was "+this->response.getResponse().toStdString();

    //read exported file and compare with the one obtained
    // Read the content of the generated file
    actualXml = TestHelpers::readFile(this->testSystemFilePath);
    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(systemExp1Xml.simplified()),
    TestHelpers::removeSpacesBetweenTags(actualXml.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << systemExp1Xml.simplified().toStdString() << "\n"
        << "Actual XML:\n" << actualXml.simplified().toStdString();  


    // Make sure the temporary modification is gone, confirming the file was overwritten
    ASSERT_FALSE(actualXml.contains(temporaryModification))
        << "The temporary modification is still present, indicating the file was not overwritten."; 
}

TEST_F(SAVE_SYSTEM_TEST, Overwrite_not_existing_system_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(false);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export Systems API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("No system file was found under the specified path"))<<"Error message was expected to contain 'No system file was found under the specified path' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_already_existing_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;


    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_TRUE(result) << "Export Systems API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("System correctly saved"))<<"Error message was expected to contain 'System correctly saved' but was "+this->response.getResponse().toStdString();

    result = OPGUICore::api_save_system(rec, this->response,errorMsg);

    ASSERT_FALSE(result) << "Export Systems API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("There already exists a system file under this path"))<<"Error message was expected to contain 'There already exists a system file under this path' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_no_id_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"id");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Missing one or more required fields id, title, priority in system"))<<"Error message was expected to contain 'Missing one or more required fields id, title, priority in system' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_system_has_no_components_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::emptyJsonArray(systemExp1JSON,"components");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    // First operation to generate the initial file
    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Invalid System object"))<<"Error message was expected to contain 'Invalid System object' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_component_invalid_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"library");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    // First operation to generate the initial file
    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Failed to export component"))<<"Error message was expected to contain 'Failed to export component' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Export_system_component_schedule_invalid_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"offset");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Schedule object is not valid"))<<"Error message was expected to contain 'Schedule object is not valid' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Export_systems_component_parameter_invalid_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"unit");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Failed to export parameter"))<<"Error message was expected to contain 'Failed to export parameter' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Export_systems_system_component_position_invalid_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"x");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Invalid position object"))<<"Error message was expected to contain 'Invalid position object' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_path_in_request_empty_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    rec.setSystem(system);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);

    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";
    ASSERT_TRUE(errorMsg.contains("Path of file is empty"))<<"Error message was expected to contain 'Path of file is empty' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_system_in_request_empty_NEGATIVE) {
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);

    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";
    ASSERT_TRUE(errorMsg.contains("System to save not valid/empty"))<<"Error message was expected to contain 'System to save not valid/empty' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_connections_invalid_schema_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;

    QString modifiedJSON = TestHelpers::removeJsonElement(systemExp1JSON,"output");
    ASSERT_FALSE(modifiedJSON.isEmpty())<<"JSON element removal delivered empty JSON string";

    ASSERT_TRUE(parseSystemJson(modifiedJSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    // First operation to generate the initial file
    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Connection object schema is not valid"))<<"Error message was expected to contain 'Connection object schema is not valid' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_connections_invalid_source_component_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;
    int wrongId = 30;

    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    QList<OpenAPI::OAIConnection> connections = system.getConnections();

    if (!connections.isEmpty()) {
        OpenAPI::OAISource source = connections.first().getSource();
        source.setComponent(wrongId); // setting an id for a not existing component
        connections.first().setSource(source); 
    }

    system.setConnections(connections); 

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    // First operation to generate the initial file
    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Source component with ID"))<<"Error message was expected to contain 'Source component with ID...' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_connections_invalid_target_component_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;
    int wrongId = 30; // Example invalid component ID

    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system)) << "Failed preparing the test, unable to parse the systems JSON for the test";

    QList<OpenAPI::OAIConnection> connections = system.getConnections();

    auto targets = connections.first().getTargets();
    if (!targets.isEmpty()) {
        OpenAPI::OAITarget target = targets.first();
        target.setComponent(wrongId); 
        targets[0] = target; 
        connections.first().setTargets(targets); 
    }

    system.setConnections(connections);

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was successful but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Target component with ID")) << "Error message was expected to contain 'Target component with ID...' but was " + errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_connections_invalid_source_output_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;
    int wrongId = 30;

    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    QList<OpenAPI::OAIConnection> connections = system.getConnections();

    if (!connections.isEmpty()) {
        OpenAPI::OAISource source = connections.first().getSource();
        source.setOutput(wrongId); // setting an id for a not existing component
        connections.first().setSource(source); 
    }

    system.setConnections(connections); 

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Source output with ID"))<<"Error message was expected to contain 'Source output with ID...' but was "+errorMsg.toStdString();
}

TEST_F(SAVE_SYSTEM_TEST, Save_system_connections_invalid_target_input_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    OpenAPI::OAI_api_saveSystem_post_request rec;
    int wrongId = 30;

    ASSERT_TRUE(parseSystemJson(systemExp1JSON, system))<< "Failed preparing the test, unable parse the systems JSON for the test";

    QList<OpenAPI::OAIConnection> connections = system.getConnections();

    if (!connections.isEmpty()) {
        auto targets = connections.first().getTargets(); 
        if (!targets.isEmpty()) {
            OpenAPI::OAITarget target = targets.first();
            target.setInput(wrongId);
            targets[0] = target;
            connections.first().setTargets(targets);
        }
    }

    system.setConnections(connections);

    rec.setSystem(system);
    rec.setPath(this->testSystemFilePath);
    rec.setIsNew(true);

    bool result = OPGUICore::api_save_system(rec, this->response, errorMsg);
    ASSERT_FALSE(result) << "Export of systems was succesfull but it was expected to fail";

    ASSERT_TRUE(errorMsg.contains("Target input with ID"))<<"Error message was expected to contain 'Target input with ID...' but was "+errorMsg.toStdString();
}