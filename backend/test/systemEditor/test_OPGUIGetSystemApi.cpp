/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>
#include <QDebug>

#include "test_OPGUIGetSystemApi.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include "OAIComponentUI.h"
#include "OAIError500.h"
#include "OAISystemUI.h"

QString mockComponentXml1 = QStringLiteral(
    R"(<component>
      <type>AlgorithmTest</type>
      <library>Algorithm_Test</library>
      <title>Algorithm_Test</title>
      <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
      </schedule>
      <parameters>
        <parameter>
          <id>0</id>
          <type>double</type>
          <title>Driver aggressiveness</title>
          <unit></unit>
          <value>1.0</value>
        </parameter>
        <parameter>
          <id>1</id>
          <type>double</type>
          <title>Max. engine power</title>
          <unit>W</unit>
          <value>100000</value>
        </parameter>
        <parameter>
          <id>2</id>
          <type>int</type>
          <title>Min. brake torque</title>
          <unit>Nm</unit>
          <value>100</value>
        </parameter>
        <parameter>
          <id>3</id>
          <type>bool</type>
          <title>Kp of the pedals PID control</title>
          <unit></unit>
          <value>true</value>
        </parameter>
        <parameter>
          <id>4</id>
          <type>string</type>
          <title>Ki of the pedals PID control</title>
          <unit></unit>
          <value>string param</value>
        </parameter>
      </parameters>
      <inputs>
        <input>
          <id>0</id>
          <type>TrajectoryEvent</type>
          <title>Desired trajectory</title>
          <unit></unit>
          <cardinality>1</cardinality>
        </input>
      </inputs>
      <outputs>
        <output>
          <id>0</id>
          <type>ControlData</type>
          <title>Control</title>
          <unit></unit>
        </output>
      </outputs>
    </component>)"
);

QString mockComponentXml2 = QStringLiteral(
    R"(<component>
    <type>Action</type>
    <library>Algorithm_Test</library>
    <title>Action_Test</title>
    <schedule>
        <offset>0</offset>
        <cycle>10</cycle>
        <response>0</response>
    </schedule>
    <parameters>
        <parameter>
        <id>0</id>
        <type>double</type>
        <title>Spring coefficient</title>
        <unit></unit>
        <value>1200000</value>
        </parameter>
        <parameter>
        <id>1</id>
        <type>double</type>
        <title>Damper coefficient</title>
        <unit></unit>
        <value>12000</value>
        </parameter>
    </parameters>
    <inputs>
        <input>
        <id>0</id>
        <type>VectorDouble</type>
        <title>InertiaForce</title>
        <unit>N</unit>
        <cardinality>1</cardinality>
        </input>
    </inputs>
    <outputs>
        <output>
        <id>0</id>
        <type>VectorDouble</type>
        <title>VerticalForce</title>
        <unit>N</unit>
        </output>
    </outputs>
    </component>)"
);

QString mockSystem1Xml = QStringLiteral(
    R"(<?xml version='1.0' encoding='UTF-8'?>
    <systems>
        <system>
        <id>0</id>
        <title>System_test</title>
        <priority>0</priority>
        <components>
        <component>
            <id>0</id>
            <library>Algorithm_Test</library>
            <title>Algorithm_Test</title>
            <schedule>
            <priority>30</priority>
            <offset>0</offset>
            <cycle>10</cycle>
            <response>0</response>
            </schedule>
            <parameters>
            <parameter>
            <!--parameter's title: Driver aggressiveness-->
            <id>0</id>
            <type>double</type>
            <unit/>
            <value>1</value>
            </parameter>
            <parameter>
            <!--parameter's title: Max. engine power-->
            <id>1</id>
            <type>double</type>
            <unit>W</unit>
            <value>100000</value>
            </parameter>
            <parameter>
            <!--parameter's title: Min. brake torque-->
            <id>2</id>
            <type>double</type>
            <unit>Nm</unit>
            <value>-10000</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kp of the pedals PID control-->
            <id>3</id>
            <type>double</type>
            <unit/>
            <value>-0.5</value>
            </parameter>
            <parameter>
            <!--parameter's title: Ki of the pedals PID control-->
            <id>4</id>
            <type>double</type>
            <unit/>
            <value>-0.4</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kd of the pedals PID control-->
            <id>5</id>
            <type>double</type>
            <unit/>
            <value>0.0</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kp of the steering PID control-->
            <id>6</id>
            <type>double</type>
            <unit/>
            <value>-18</value>
            </parameter>
            <parameter>
            <!--parameter's title: Ki of the steering PID control-->
            <id>7</id>
            <type>double</type>
            <unit/>
            <value>-0.6</value>
            </parameter>
            <parameter>
            <!--parameter's title: Kd of the steering PID control-->
            <id>8</id>
            <type>double</type>
            <unit/>
            <value>0.0</value>
            </parameter>
            </parameters>
            <position>
            <x>296</x>
            <y>20</y>
            </position>
        </component>
        <component>
            <id>1</id>
            <library>Action_Test</library>
            <title>Action_Test</title>
            <schedule>
            <priority>1</priority>
            <offset>0</offset>
            <cycle>10</cycle>
            <response>0</response>
            </schedule>
            <parameters>
            <parameter>
            <!--parameter's title: Spring coefficient-->
            <id>0</id>
            <type>double</type>
            <unit/>
            <value>1.2e+06</value>
            </parameter>
            <parameter>
            <!--parameter's title: Damper coefficient-->
            <id>1</id>
            <type>double</type>
            <unit/>
            <value>12000</value>
            </parameter>
            </parameters>
            <position>
            <x>815</x>
            <y>692</y>
            </position>
        </component>
        </components>
        <connections>
        <connection>
            <id>0</id>
            <source>
            <!--component title: Action_Test-->
            <component>1</component>
            <!--output title: VerticalForce-->
            <output>0</output>
            </source>
            <target>
            <!--component title: Algorithm_Test-->
            <component>0</component>
            <!--input title: Desired trajectory-->
            <input>0</input>
            </target>
        </connection>
        <connection>
            <id>1</id>
            <source>
            <!--component title: Algorithm_Test-->
            <component>0</component>
            <!--output title: Control-->
            <output>0</output>
            </source>
            <target>
            <!--component title: Action_Test-->
            <component>1</component>
            <!--input title: InertiaForce-->
            <input>0</input>
            </target>
        </connection>
        </connections>
        <comments>
            <comment>
                <message>this is just an example comment</message>
                <title>comment 1</title>
                <position>
                <x>1</x>
                <y>20</y>
                </position>
            </comment>
            <comment>
                <message>this is another example comment</message>
                <title>comment 2</title>
                <position>
                <x>815</x>
                <y>692</y>
                </position>
            </comment>
        </comments>
        </system>
    </systems>)"
);

QString system1JSON = QStringLiteral(
    R"({
        "components": [
            {
                "id": 0,
                "inputs": [
                    {
                        "cardinality": 1,
                        "id": 0,
                        "title": "Desired trajectory",
                        "type": "TrajectoryEvent",
                        "unit": ""
                    }
                ],
                "library": "Algorithm_Test",
                "outputs": [
                    {
                        "id": 0,
                        "title": "Control",
                        "type": "ControlData",
                        "unit": ""
                    }
                ],
                "parameters": [
                    {
                        "id": "0",
                        "title": "Driver aggressiveness",
                        "type": "double",
                        "unit": "",
                        "value": "1"
                    },
                    {
                        "id": "1",
                        "title": "Max. engine power",
                        "type": "double",
                        "unit": "W",
                        "value": "100000"
                    },
                    {
                        "id": "2",
                        "title": "Min. brake torque",
                        "type": "double",
                        "unit": "Nm",
                        "value": "-10000"
                    },
                    {
                        "id": "3",
                        "title": "Kp of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.5"
                    },
                    {
                        "id": "4",
                        "title": "Ki of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.4"
                    },
                    {
                        "id": "5",
                        "title": "Kd of the pedals PID control",
                        "type": "double",
                        "unit": "",
                        "value": "0.0"
                    },
                    {
                        "id": "6",
                        "title": "Kp of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-18"
                    },
                    {
                        "id": "7",
                        "title": "Ki of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "-0.6"
                    },
                    {
                        "id": "8",
                        "title": "Kd of the steering PID control",
                        "type": "double",
                        "unit": "",
                        "value": "0.0"
                    }
                ],
                "position": {
                    "x": 296,
                    "y": 20
                },
                "schedule": {
                    "cycle": 10,
                    "offset": 0,
                    "priority": 30,
                    "response": 0
                },
                "title": "Algorithm_Test",
                "type": "AlgorithmTest"
            },
            {
                "id": 1,
                "inputs": [
                    {
                        "cardinality": 1,
                        "id": 0,
                        "title": "InertiaForce",
                        "type": "VectorDouble",
                        "unit": "N"
                    }
                ],
                "library": "Action_Test",
                "outputs": [
                    {
                        "id": 0,
                        "title": "VerticalForce",
                        "type": "VectorDouble",
                        "unit": "N"
                    }
                ],
                "parameters": [
                    {
                        "id": "0",
                        "title": "Spring coefficient",
                        "type": "double",
                        "unit": "",
                        "value": "1.2e+06"
                    },
                    {
                        "id": "1",
                        "title": "Damper coefficient",
                        "type": "double",
                        "unit": "",
                        "value": "12000"
                    }
                ],
                "position": {
                    "x": 815,
                    "y": 692
                },
                "schedule": {
                    "cycle": 10,
                    "offset": 0,
                    "priority": 1,
                    "response": 0
                },
                "title": "Action_Test",
                "type": "Action"
            }
        ],
        "connections": [
            {
                "id": 0,
                "source": {
                    "component": 1,
                    "output": 0
                },
                "targets": [{
                    "component": 0,
                    "input": 0
                }]
            },
            {
                "id": 1,
                "source": {
                    "component": 0,
                    "output": 0
                },
                "targets": [{
                    "component": 1,
                    "input": 0
                }]
            }
        ],
        "comments": [
            {
                "title": "comment 1",
                "message": "this is just an example comment",
                "position": {
                        "x": 1,
                        "y": 20
                }
            },
            {
                "title": "comment 2",
                "message": "this is another example comment",
                "position": {
                        "x": 815,
                        "y": 692
                }
            }
        ],
        "id": 0,
        "priority": 0,
        "title": "System_test",
        "file":"$$PATH_SYSTEM_FILE$$"
    })"
);


void GET_SYSTEM_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    //ceck path to workspace exists
    QDir dirWkspc(OPGUICoreGlobalConfig::getInstance().workspace());
    ASSERT_TRUE(dirWkspc.exists())<<"Workspace directory not found under:"+OPGUICoreGlobalConfig::getInstance().workspace().toStdString();
    
    this->testDirComponentsPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_components");
    OPGUICoreGlobalConfig::getInstance().setFullPathComponentsFolder(this->testDirComponentsPath);

    this->testDirSystemsPath = TestHelpers::joinPaths(OPGUICoreGlobalConfig::getInstance().workspace(),"test_systems");
    this->mockSystemFilePath = TestHelpers::joinPaths(this->testDirSystemsPath,"test_system.xml");

    // If test directories exists, remove them.
    QDir dirCompTest(this->testDirComponentsPath);
    if(dirCompTest.exists()){
        ASSERT_TRUE(dirCompTest.removeRecursively()) << "Failed to remove existing components test directory: " << this->testDirComponentsPath.toStdString();
    }
    QDir dirSysTest(this->testDirSystemsPath);
    if(dirSysTest.exists()){
        ASSERT_TRUE(dirSysTest.removeRecursively()) << "Failed to remove existing systems test directory: " << this->testDirSystemsPath.toStdString();
    }

    ASSERT_TRUE(dirCompTest.mkpath(this->testDirComponentsPath)) << "Failed to create components test directory at: " << this->testDirComponentsPath.toStdString();
    ASSERT_TRUE(dirSysTest.mkpath(this->testDirSystemsPath)) << "Failed to create systems test directory at: " << this->testDirSystemsPath.toStdString();
    
    //add the two used components used by the system in the test folder
    this->mockComponentPath1 = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component_1.xml");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockComponentPath1, mockComponentXml1))<<"Could not save xml file as: "+this->mockComponentPath1.toStdString();
    this->mockComponentPath2 = TestHelpers::joinPaths(this->testDirComponentsPath, "test_component_2.xml");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockComponentPath2, mockComponentXml2))<<"Could not save xml file as: "+this->mockComponentPath2.toStdString();

    QString mockComponentLibPath1=this->testDirComponentsPath+"/Algorithm_Test.dll";
    ASSERT_TRUE(TestHelpers::createAndCheckFile(mockComponentLibPath1,"placeholder"))<<"Could not save mock .dll file as: "+mockComponentLibPath1.toStdString();
    QString mockComponentLibPath2=this->testDirComponentsPath+"/Action_Test.dll";
    ASSERT_TRUE(TestHelpers::createAndCheckFile(mockComponentLibPath2,"placeholder"))<<"Could not save mock .dll file as: "+mockComponentLibPath2.toStdString();
}

void GET_SYSTEM_TEST::TearDown()  {
    QDir dirCompTest(this->testDirComponentsPath);
    if(dirCompTest.exists()){
        ASSERT_TRUE(dirCompTest.removeRecursively()) << "Failed to remove components test directory: " << this->testDirComponentsPath.toStdString();
    }
    QDir dirSysTest(this->testDirSystemsPath);
    if(dirSysTest.exists()){
        ASSERT_TRUE(dirSysTest.removeRecursively()) << "Failed to remove systems test directory: " << this->testDirSystemsPath.toStdString();
    }
}

TEST_F(GET_SYSTEM_TEST, Get_system_empty_path_in_request_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    QString pathNotInited="";

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath, ""))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(pathNotInited,system,errorMsg);

    ASSERT_FALSE(result) << "Get system was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("Path of file is empty."))<<"Error message was expected to contain 'Path of file is empty.' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_empty_system_file_NEGATIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath, ""))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Get system was succesfull but should be failed";

    ASSERT_TRUE(errorMsg.contains("System XML file is empty"))<<"Error message was expected to contain 'System XML file is empty' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_file_is_unparseable_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath, "some wrong xml>fewfe>wefwef"))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Invalid XML content in system XML file"))<<"Error message was expected to contain 'Invalid XML content in system XML file' but was "+errorMsg.toStdString();
}


TEST_F(GET_SYSTEM_TEST, Get_system_1_system_loaded_POSITIVE) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath, mockSystem1Xml))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_TRUE(result) << "Load Systems API call failed";

    QJsonDocument resDoc(::OpenAPI::toJsonValue(system).toObject());

    QByteArray jsonBytes = resDoc.toJson(QJsonDocument::Indented);
    QString actualJsonString = QString(jsonBytes);

    system1JSON.replace("$$PATH_SYSTEM_FILE$$", this->mockSystemFilePath + QStringLiteral(""));

    QJsonDocument expectedDoc = QJsonDocument::fromJson(system1JSON.toUtf8());
    QJsonDocument actualDoc = QJsonDocument::fromJson(actualJsonString.toUtf8());

    QString expectedJsonStr = QString(expectedDoc.toJson(QJsonDocument::Compact));
    QString actualJsonStr = QString(actualDoc.toJson(QJsonDocument::Compact));

    ASSERT_EQ(expectedJsonStr, actualJsonStr) 
        << "JSON objects are not equal. Expected: " << expectedJsonStr.toStdString() 
        << ", Actual: " << actualJsonStr.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_has_no_title_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    
    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"title");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("System element has missing/empty one or more required elements: id, title, priority, components, connections"))<<"Error message was expected to contain 'System element has missing/empty one or more required elements: id, title, priority, components, connections' but was "+errorMsg.toStdString();
}


TEST_F(GET_SYSTEM_TEST, Get_system_fail_original_components_load_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    
    QDir dirCompTest(this->testDirComponentsPath);
    ASSERT_TRUE(dirCompTest.removeRecursively()) << "Failed to remove components directory: " << this->testDirComponentsPath.toStdString();

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,mockSystem1Xml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse XML system from file"))<<"Error message was expected to contain 'Failed to parse XML system from file' but was "+errorMsg.toStdString();
}


TEST_F(GET_SYSTEM_TEST, Get_system_system_has_no_components_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    
    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"component","system");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Failed to parse connection"))<<"Error message was expected to contain 'Failed to parse connection' but was "+errorMsg.toStdString();
}


TEST_F(GET_SYSTEM_TEST, Get_system_system_component_missing_library_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;
    
    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"library","components");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Title, parameters, library or schedule for component are not present or empty"))<<"Error message was expected to contain 'Title, parameters, library or schedule for component are not present or empty' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_component_missing_parameter_component_in_server_negative) {
    QFile::remove(TestHelpers::joinPaths(this->testDirComponentsPath,"test_component_1.xml"));

    OpenAPI::OAISystemUI system;
    QString errorMsg;
    
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,mockSystem1Xml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Could not find the .xml file of the component with title"))<<"Error message was expected to contain 'Could not find the .xml file of the component with title' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_component_missing_priority_in_schedule_component_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"priority","schedule");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Priority field missing in schedule"))<<"Error message was expected to contain 'Priority field missing in schedule' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_component_missing_position_x_in_component_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"x","position");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Missing one or more required fields x or y in position"))<<"Error message was expected to contain 'Missing one or more required fields x or y in position' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_component_wrong_position_x_in_component_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    QString modifiedXml = TestHelpers::replaceXmlElementContent(mockSystem1Xml,"x","not int","position");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("x coordinate is not a valid integer"))<<"Error message was expected to contain 'x coordinate is not a valid integer' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_component_wrong_position_y_in_component_negative) {
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    QString modifiedXml = TestHelpers::replaceXmlElementContent(mockSystem1Xml,"y","not int","position");

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();

    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);

    ASSERT_FALSE(result) << "Load Systems API call successed but should have failed";

    ASSERT_TRUE(errorMsg.contains("y coordinate is not a valid integer"))<<"Error message was expected to contain 'y coordinate is not a valid integer' but was "+errorMsg.toStdString();
}

TEST_F(GET_SYSTEM_TEST, Get_system_system_various_problems_with_connections_negative){
    OpenAPI::OAISystemUI system;
    QString errorMsg;

    QString modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"id","connection");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    bool result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse connection"))<<"Error message was expected to contain 'Failed to parse connection...' but was "+errorMsg.toStdString();

    //connection id not int
    modifiedXml = TestHelpers::replaceXmlElementContent(mockSystem1Xml,"id","bad int","connection");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse connection"))<<"Error message was expected to contain 'Failed to parse connection...' but was "+errorMsg.toStdString();

    //connection source not present
    modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"source","connection");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse connection"))<<"Error message was expected to contain 'Failed to parse connection...' but was "+errorMsg.toStdString();

    //connection target not present
    qDebug() << "normalXml";
    qDebug() << mockSystem1Xml.simplified();
    modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"target","connection");
    qDebug() << "modifiedXml";
    qDebug() << modifiedXml.simplified();
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("No target elements found in connection"))<<"Error message was expected to contain 'No target elements found in connection' but was "+errorMsg.toStdString();

    //source component not valid
    modifiedXml = TestHelpers::replaceXmlElementContent(mockSystem1Xml,"component","bad int","source");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML system from file"))<<"Error message was expected to contain 'Failed to parse XML system from file' but was "+errorMsg.toStdString();

    //target component not present
    modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"component","target");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML system from file"))<<"Error message was expected to contain 'Failed to parse XML system from file' but was "+errorMsg.toStdString();

    //target input not valid
    modifiedXml = TestHelpers::replaceXmlElementContent(mockSystem1Xml,"input","bad int","target");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("not a valid integer"))<<"Error message was expected to contain 'not a valid integer' but was "+errorMsg.toStdString();

    //source output not present
    modifiedXml = TestHelpers::removeXmlElement(mockSystem1Xml,"output","source");
    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath,modifiedXml ))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
    result = OPGUICore::api_get_system(this->mockSystemFilePath,system, errorMsg);
    ASSERT_FALSE(result) << "Load systems was succesfull but should be failed";
    ASSERT_TRUE(errorMsg.contains("Failed to parse XML system from file"))<<"Error message was expected to contain 'Failed to parse XML system from file' but was "+errorMsg.toStdString();
}
