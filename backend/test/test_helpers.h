/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QRegularExpression>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QDomDocument>
#include <QDomElement>
#include <QDir>
#include <QJsonValue>


namespace TestHelpers {

/**
 * Reads the content of a file and returns it as a QString.
 *
 * @param filePath The path to the file to read.
 * @return The content of the file as a QString, or an empty QString if the file cannot be opened.
 */
QString readFile(const QString &filePath);

/**
 * Removes spaces between XML tags.
 *
 * @param input The input QString containing XML data.
 * @return The input QString with spaces between tags removed.
 */
QString removeSpacesBetweenTags(const QString& input);

/**
 * Creates a file with the specified content and checks if the file is created successfully.
 *
 * @param filePath The path to the file to create.
 * @param content The content to write to the file.
 * @return true if the file is created successfully, false otherwise.
 */
bool createAndCheckFile(const QString& filePath, const QString& content);

/**
 * Parses an XML string and returns the root element.
 *
 * @param xmlString The input XML string.
 * @return The root QDomElement of the parsed XML.
 */
QDomElement parseXMLString(const QString& xmlString);

/**
 * Joins two file paths and returns the combined path.
 *
 * @param path1 The first part of the path.
 * @param path2 The second part of the path.
 * @return The combined file path as a QString.
 */
QString joinPaths(const QString& path1, const QString& path2);

/**
 * Removes an XML element by tag name from the input XML string.
 *
 * @param xmlStr The input XML string.
 * @param tagNameToRemove The tag name of the element to remove.
 * @param parentTagName The tag name of the parent element (optional). If empty, it removes all elements with the specified tag name.
 * @return The modified XML string with the specified element removed.
 */
QString removeXmlElement(const QString& xmlStr, const QString& tagNameToRemove, const QString& parentTagName = "");

/**
 * Replaces the content of an XML element by tag name in the input XML string.
 *
 * @param xmlStr The input XML string.
 * @param tagNameToReplace The tag name of the element to replace.
 * @param newContent The new content to set for the specified element.
 * @param parentTagName The tag name of the parent element (optional). If empty, it replaces all elements with the specified tag name.
 * @return The modified XML string with the specified element's content replaced.
 */
QString replaceXmlElementContent(const QString& xmlStr, const QString& tagNameToReplace, const QString& newContent, const QString& parentTagName = "");

/**
 * Removes XML comments from the input XML string.
 *
 * @param xmlString The input XML string.
 * @return The XML string with comments removed.
 */
QString removeXmlComments(const QString &xmlString);

/**
 * Recursively removes a key from a JSON value.
 *
 * @param value The input JSON value.
 * @param keyToRemove The key to remove from the JSON value.
 * @return The modified JSON value with the specified key removed.
 */
QJsonValue recursiveRemove(const QJsonValue &value, const QString &keyToRemove);

/**
 * Removes a specified key from a JSON string.
 *
 * @param jsonStr The input JSON string.
 * @param keyToRemove The key to remove from the JSON string.
 * @return The modified JSON string with the specified key removed.
 */
QString removeJsonElement(const QString& jsonStr, const QString& keyToRemove);

/**
 * Recursively empties arrays in a JSON value for a specified key.
 *
 * @param value The input JSON value.
 * @param keyToEmptyArray The key for which arrays should be emptied.
 * @return The modified JSON value with arrays emptied for the specified key.
 */
QJsonValue recursiveEmptyArray(const QJsonValue &value, const QString &keyToEmptyArray);

/**
 * Empties arrays for a specified key in a JSON string.
 *
 * @param jsonStr The input JSON string.
 * @param keyToEmptyArray The key for which arrays should be emptied.
 * @return The modified JSON string with arrays emptied for the specified key.
 */
QString emptyJsonArray(const QString& jsonStr, const QString& keyToEmptyArray);

/**
 * Deletes a file if it exists.
 *
 * @param filePath The path to the file to delete.
 * @return true if the file is deleted successfully or does not exist, false otherwise.
 */
bool deleteFileIfExists(const QString& filePath);

/**
 * Modifies the value of a specified XML element.
 *
 * @param originalXml The input XML string.
 * @param parentTag The tag name of the parent element. If empty, the function will look for the target element at the root level.
 * @param targetTag The tag name of the target element to modify.
 * @param targetIndex The index of the target element within the parent element.
 * @param valueToModify The name of the child element whose value needs to be modified.
 * @param newValue The new value to set for the specified element.
 * @return The modified XML string with the specified element's value modified.
 * 
 * This function searches for the parent element specified by `parentTag`. If `parentTag` is empty, the function will look for `targetTag` elements at the root level.
 * It then finds the `targetTag` element at the specified `targetIndex` within the parent element.
 * The function modifies the value of the child element specified by `valueToModify` within the `targetTag` element and sets it to `newValue`.
 */
QString modifyXml(const QString& originalXml, const QString& parentTag, const QString& targetTag, int targetIndex, const QString& valueToModify, const QString& newValue);

} // namespace TestHelpers