/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIObservationsExport.h"
#include "OPGUIObservationsExport.h"
#include "OAIObservations.h"
#include "OAILoggingGroup.h"
#include "OAILog.h"
#include "OAIEntityRepository.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include <QDebug>

QString observationsXmlToCompare = QStringLiteral(
    R"(<Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            <Observation>
            <Library>Observation_EntityRepository</Library>
            <Parameters>
                <String Key="FilenamePrefix" Value="Repository"/>
                <String Key="WritePersistentEntities" Value="Consolidated"/>
            </Parameters>
            </Observation>
        </Observations>)"
);

QString observationsJSONToExp = QStringLiteral(
    R"({
        "log": {
            "loggingCyclesToCsv": false,
            "fileName": "simulationOutput.xml",
            "loggingGroups": [
            {
                "enabled": true,
                "group": "Trace",
                "values": "XPosition,YPosition,YawAngle"
            },
            {
                "enabled": true,
                "group": "RoadPosition",
                "values": "AgentInFront,Lane,PositionRoute,Road,TCoordinate"
            },
            {
                "enabled": false,
                "group": "RoadPositionExtended",
                "values": "SecondaryLanes"
            },
            {
                "enabled": true,
                "group": "Sensor",
                "values": "Sensor*_DetectedAgents,Sensor*_VisibleAgents"
            },
            {
                "enabled": false,
                "group": "Vehicle",
                "values": "AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"
            },
            {
                "enabled": true,
                "group": "Visualization",
                "values": "AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"
            }
            ]
        },
        "entityRepository": {
            "fileNamePrefix": "Repository",
            "writePersistentEntities": "Consolidated"
        }
        })");

void OBSERVATIONS_EXPORT_TEST::SetUp()  {
}

void OBSERVATIONS_EXPORT_TEST::TearDown()  {
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIObservations observations(observationsJSONToExp);

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export observations was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = observationsXmlToCompare.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_EntityRepository_not_existing_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    // Remove the second observation object
    QString noEntityRepoObs = QStringLiteral(
         R"(
            <Observations>
            <Observation>
            <Library>Observation_Log</Library>
            <Parameters>
                <String Key="OutputFilename" Value="simulationOutput.xml"/>
                <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
            </Parameters>
            </Observation>
            </Observations>
         )");

    OpenAPI::OAIObservations observations(observationsJSONToExp);
    observations.setEntityRepository(OpenAPI::OAIEntityRepository()); // Set an empty entity repository

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export observations was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = noEntityRepoObs.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_EntityRepository_no_FilenamePrefix_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    // Remove the second observation object
    QString noFileNamePrefix = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
                <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="WritePersistentEntities" Value="Consolidated"/>
                </Parameters>
                </Observation>
            </Observations>
         )");


    OpenAPI::OAIObservations observations(observationsJSONToExp);
    OpenAPI::OAIEntityRepository entityRepNoFileNamePrefix = observations.getEntityRepository();
    entityRepNoFileNamePrefix.setFileNamePrefix("");
    observations.setEntityRepository(entityRepNoFileNamePrefix); // Set an empty fileNamePrefix

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export observations was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = noFileNamePrefix.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_EntityRepository_no_WritePersistentEntities_exported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    // Remove the second observation object
    QString noFileNamePrefix = QStringLiteral(
         R"(
            <Observations>
                <Observation>
                <Library>Observation_Log</Library>
                <Parameters>
                    <String Key="OutputFilename" Value="simulationOutput.xml"/>
                    <Bool Key="LoggingCyclicsToCsv" Value="false"/>
                    <StringVector Key="LoggingGroup_Trace" Value="XPosition,YPosition,YawAngle"/>
                    <StringVector Key="LoggingGroup_RoadPosition" Value="AgentInFront,Lane,PositionRoute,Road,TCoordinate"/>
                    <StringVector Key="LoggingGroup_RoadPositionExtended" Value="SecondaryLanes"/>
                    <StringVector Key="LoggingGroup_Sensor" Value="Sensor*_DetectedAgents,Sensor*_VisibleAgents"/>
                    <StringVector Key="LoggingGroup_Vehicle" Value="AccelerationPedalPosition,BrakePedalPosition,EngineMoment,Gear,SteeringAngle,TotalDistanceTraveled,YawRate"/>
                    <StringVector Key="LoggingGroup_Visualization" Value="AccelerationEgo,BrakeLight,IndicatorState,LightStatus,VelocityEgo"/> 
                    <StringVector Key="LoggingGroups" Value="RoadPosition,Sensor,Trace,Visualization"/>
                </Parameters>
                </Observation>
                <Observation>
                <Library>Observation_EntityRepository</Library>
                <Parameters>
                    <String Key="FilenamePrefix" Value="Repository"/>
                </Parameters>
                </Observation>
            </Observations>
         )");


    OpenAPI::OAIObservations observations(observationsJSONToExp);
    OpenAPI::OAIEntityRepository entityRepNoWritePersistentEntities = observations.getEntityRepository();
    entityRepNoWritePersistentEntities.setWritePersistentEntities("");
    observations.setEntityRepository(entityRepNoWritePersistentEntities); // Set an empty fileNamePrefix

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_TRUE(result) << "Test: Result of export observations was expected to succeed.";

    QString generatedXmlString = QString::fromUtf8(xmlData).simplified().replace(" >", ">");
    QString normalizedExpectedXml = noFileNamePrefix.simplified().replace(" >", ">");

    bool xmlMatch = generatedXmlString == normalizedExpectedXml;
    EXPECT_TRUE(xmlMatch) << "The generated XML does not match the expected XML.\nGenerated XML:\n" << generatedXmlString.toStdString() << "\nExpected XML:\n" << normalizedExpectedXml.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_Empty_Logging_Groups_exported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIObservations observations(observationsJSONToExp);
    observations.setLog(OpenAPI::OAILog()); 

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_FALSE(result)<<"Test: Result of observations export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("FileName cannot be empty."))<<"Error message was expected to contain 'FileName cannot be empty.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_wrong_logging_groups_exported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIObservations observations(observationsJSONToExp);
    OpenAPI::OAILog log = observations.getLog();
    QList<OpenAPI::OAILoggingGroup> loggingGroupList = observations.getLog().getLoggingGroups();
    loggingGroupList[0].setGroup("");
    log.setLoggingGroups(loggingGroupList);
    observations.setLog(log);

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_FALSE(result)<<"Test: Result of observations export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Logging group has no name."))<<"Error message was expected to contain 'Logging group has no name.' but was "+errorMsg.toStdString();

    loggingGroupList[0].setGroup("Trace");
    loggingGroupList[0].setValues("");
    log.setLoggingGroups(loggingGroupList);
    observations.setLog(log);

    result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_FALSE(result)<<"Test: Result of observations export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("is enabled but has no values."))<<"Error message was expected to contain 'is enabled but has no values.' but was "+errorMsg.toStdString();
}

TEST_F(OBSERVATIONS_EXPORT_TEST, observations_Wrong_WritePersistentEntities_exported_NEGATIVE) {
    OPGUIConfigEditor::OPGUIObservationsExport observationsExporter;
    QString errorMsg;

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);

    OpenAPI::OAIObservations observations(observationsJSONToExp);
    OpenAPI::OAIEntityRepository entityrepo =  observations.getEntityRepository();
    entityrepo.setWritePersistentEntities("wrong value");
    observations.setEntityRepository(entityrepo);

    bool result = observationsExporter.exportObservations(xmlWriter, observations, errorMsg);

    buffer.close();

    EXPECT_FALSE(result)<<"Test: Result of observations export was expected to be false";
    ASSERT_TRUE(errorMsg.contains("Invalid value for WritePersistentEntities"))<<"Error message was expected to contain 'Invalid value for WritePersistentEntities' but was "+errorMsg.toStdString();
}











