/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QDebug>
#include <QJsonObject>
#include <QXmlStreamWriter>
#include <QBuffer>

#include "test_OPGUIExperimentImport.h"
#include "OPGUIExperimentImport.h"
#include "OAIExperiment.h"
#include "OAIExperiment_libraries.h"
#include "OAIHelpers.h"
#include "test_helpers.h"

QString experimentXmlToImp = QStringLiteral(
    R"(<Experiment>
        <ExperimentID>123</ExperimentID>
        <NumberOfInvocations>1</NumberOfInvocations>
        <RandomSeed>532725206</RandomSeed>
        <Libraries>
            <WorldLibrary>World_OSI</WorldLibrary>
            <DataBufferLibrary>DataBuffer</DataBufferLibrary>
            <StochasticsLibrary>Stochastics</StochasticsLibrary>
        </Libraries>
    </Experiment>)"
);

QString experimentJSONToCompare = QStringLiteral(
    R"({
        "experimentID": 123,
        "numberOfInvocations": 1,
        "randomSeed": 532725206,
        "libraries": {
            "worldLibrary": "World_OSI",
            "dataBufferLibrary": "DataBuffer",
            "stochasticsLibrary": "Stochastics"
        }
    })");

void EXPERIMENT_IMPORT_TEST::SetUp()  {
}

void EXPERIMENT_IMPORT_TEST::TearDown()  {
    
}

TEST_F(EXPERIMENT_IMPORT_TEST, experiment_imported_correctly_POSITIVE) {
    OPGUIConfigEditor::OPGUIExperimentImport experimentImporter;
    QString errorMsg;

    // Parse the XML string into a QDomDocument
    QDomDocument doc;
    ASSERT_TRUE(doc.setContent(experimentXmlToImp)) << "Failed to parse XML data.";

    QDomElement root = doc.documentElement(); // Get the root element of the document

    OpenAPI::OAIExperiment experiment; // Assuming this object will be populated

    bool result = experimentImporter.importExperiment(root, experiment, errorMsg);

    ASSERT_TRUE(result) << "Import should succeed. Error: " << errorMsg.toStdString();

    // Convert the experiment object to a QJsonDocument for comparison
    QJsonDocument actualDoc(::OpenAPI::toJsonValue(experiment).toObject());

    // Convert the actual JSON document to a compact string for comparison
    QString actualJsonString = QString(actualDoc.toJson(QJsonDocument::Compact));

    // Load the expected JSON for comparison
    QJsonDocument expectedDoc = QJsonDocument::fromJson(experimentJSONToCompare.toUtf8());
    QString expectedJsonString = QString(expectedDoc.toJson(QJsonDocument::Compact));

    // Visually compare the expected and actual JSON strings in case of an error
    if (expectedJsonString != actualJsonString) {
        FAIL() << "The imported experiment does not match the expected JSON.\n"
               << "Expected JSON:\n" << expectedJsonString.toStdString() << "\n"
               << "Actual JSON:\n" << actualJsonString.toStdString();
    }
} 

// Negative tests for experimentID
TEST_F(EXPERIMENT_IMPORT_TEST, experiment_invalid_experimentID_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIExperimentImport experimentImporter;
    OpenAPI::OAIExperiment experiment;
    QDomDocument doc;
    QDomElement root;

    // No ExperimentID element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "ExperimentID", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing ExperimentID.";
    ASSERT_TRUE(errorMsg.contains("ExperimentID element is missing.")) << "Error message was expected to contain 'ExperimentID element is missing.' but was " << errorMsg.toStdString();

    // Negative ExperimentID
    QString wrongExperimentId = "-9999";
    incorrectXml =TestHelpers::replaceXmlElementContent(experimentXmlToImp, "ExperimentID", wrongExperimentId, "Experiment");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to negative ExperimentID.";
    ASSERT_TRUE(errorMsg.contains("ExperimentID must be a positive integer.")) << "Error message was expected to contain 'ExperimentID must be a positive integer.' but was " << errorMsg.toStdString();
    
}

// Negative tests for randomSeed
TEST_F(EXPERIMENT_IMPORT_TEST, experiment_invalid_randomSeed_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIExperimentImport experimentImporter;
    OpenAPI::OAIExperiment experiment;
    QDomDocument doc;
    QDomElement root;

    // No RandomSeed element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "RandomSeed", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing RandomSeed.";
    ASSERT_TRUE(errorMsg.contains("RandomSeed element is missing.")) << "Error message was expected to contain 'RandomSeed element is missing.' but was " << errorMsg.toStdString();
    
    // Negative RandomSeed
    QString wrongRandomSeed = "-532725206";
    incorrectXml =TestHelpers::replaceXmlElementContent(experimentXmlToImp, "RandomSeed", wrongRandomSeed, "Experiment");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to negative RandomSeed.";
    ASSERT_TRUE(errorMsg.contains("Random seed must be a non-negative integer.")) << "Error message was expected to contain 'Random seed must be a non-negative integer.' but was " << errorMsg.toStdString();
}

// Negative tests for numberOfInvocations
TEST_F(EXPERIMENT_IMPORT_TEST, experiment_invalid_numberOfInvocations_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIExperimentImport experimentImporter;
    OpenAPI::OAIExperiment experiment;
    QDomDocument doc;
    QDomElement root;

    // No RandomSeed element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "NumberOfInvocations", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing NumberOfInvocations.";
    ASSERT_TRUE(errorMsg.contains("NumberOfInvocations element is missing.")) << "Error message was expected to contain 'NumberOfInvocations element is missing.' but was " << errorMsg.toStdString();
    
    // Negative RandomSeed
    QString wrongNumberOfInvocations = "-532725206";
    incorrectXml =TestHelpers::replaceXmlElementContent(experimentXmlToImp, "NumberOfInvocations", wrongNumberOfInvocations, "Experiment");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to negative NumberOfInvocations.";
    ASSERT_TRUE(errorMsg.contains("NumberOfInvocations must be a positive integer.")) << "Error message was expected to contain 'NumberOfInvocations must be a positive integer.' but was " << errorMsg.toStdString();
}


// Negative tests for libraries
TEST_F(EXPERIMENT_IMPORT_TEST, experiment_invalid_libraries_negative) {
    QString errorMsg;
    bool result = false;
    QString incorrectXml;
    OPGUIConfigEditor::OPGUIExperimentImport experimentImporter;
    OpenAPI::OAIExperiment experiment;
    QDomDocument doc;
    QDomElement root;

    // No Libraries element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "Libraries", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing Libraries.";
    ASSERT_TRUE(errorMsg.contains("Libraries element is missing.")) << "Error message was expected to contain 'Libraries element is missing.' but was " << errorMsg.toStdString();

    // No WorldLibrary element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "WorldLibrary", "Libraries");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing WorldLibrary.";
    ASSERT_TRUE(errorMsg.contains("WorldLibrary element is missing.")) << "Error message was expected to contain 'WorldLibrary element is missing.' but was " << errorMsg.toStdString();

    // Empty WorldLibrary
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "WorldLibrary", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    qDebug() << "XML after modifying WorldLibrary:\n" << incorrectXml;
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty WorldLibrary.";
    ASSERT_TRUE(errorMsg.contains("WorldLibrary element is empty.")) << "Error message was expected to contain 'WorldLibrary element is empty.' but was " << errorMsg.toStdString();

    // Non existing WorldLibrary in value list
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "WorldLibrary", "NOT EXISTING IN LIST");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty WorldLibrary.";
    ASSERT_TRUE(errorMsg.contains("Invalid WorldLibrary value")) << "Error message was expected to contain 'Invalid WorldLibrary value' but was " << errorMsg.toStdString();

    // No DataBufferLibrary element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "DataBufferLibrary", "Libraries");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing DataBufferLibrary.";
    ASSERT_TRUE(errorMsg.contains("DataBufferLibrary element is missing.")) << "Error message was expected to contain 'DataBufferLibrary element is missing.' but was " << errorMsg.toStdString();

    // Empty DataBufferLibrary
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "DataBufferLibrary", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty DataBufferLibrary.";
    ASSERT_TRUE(errorMsg.contains("DataBufferLibrary element is empty.")) << "Error message was expected to contain 'DataBufferLibrary element is empty.' but was " << errorMsg.toStdString();

    // Non existing DataBufferLibrary in value list
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "DataBufferLibrary", "NOT EXISTING IN LIST");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty DataBufferLibrary.";
    ASSERT_TRUE(errorMsg.contains("Invalid dataBuffer Library value")) << "Error message was expected to contain 'Invalid dataBuffer Library value' but was " << errorMsg.toStdString();

    // No StochasticsLibrary element
    incorrectXml = TestHelpers::removeXmlElement(experimentXmlToImp, "StochasticsLibrary", "Libraries");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to missing StochasticsLibrary.";
    ASSERT_TRUE(errorMsg.contains("StochasticsLibrary element is missing.")) << "Error message was expected to contain 'StochasticsLibrary element is missing.' but was " << errorMsg.toStdString();

    // Empty StochasticsLibrary
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "StochasticsLibrary", "");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty StochasticsLibrary.";
    ASSERT_TRUE(errorMsg.contains("StochasticsLibrary element is empty.")) << "Error message was expected to contain 'StochasticsLibrary element is empty.' but was " << errorMsg.toStdString();

    // Non existing StochasticsLibrary in value list
    incorrectXml = TestHelpers::modifyXml(experimentXmlToImp, "Experiment", "Libraries", 0, "StochasticsLibrary", "NOT EXISTING IN LIST");
    ASSERT_TRUE(doc.setContent(incorrectXml)) << "Failed to parse modified XML data.";
    root = doc.documentElement();
    result = experimentImporter.importExperiment(root, experiment, errorMsg);
    EXPECT_FALSE(result) << "Import should fail due to empty StochasticsLibrary.";
    ASSERT_TRUE(errorMsg.contains("Invalid stochastics Library value")) << "Error message was expected to contain 'Invalid stochastics Library value' but was " << errorMsg.toStdString();

}


