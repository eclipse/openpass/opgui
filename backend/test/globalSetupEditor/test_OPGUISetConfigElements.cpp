/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonParseError>

#include "test_OPGUISetConfigElements.h"
#include "test_helpers.h"
#include "OAIHelpers.h"
#include "OPGUICore.h"
#include "OPGUICoreGlobalConfig.h"

QString configFileMockSet = QStringLiteral(
    R"({
    "BACKEND_IP": "127.0.0.1",
    "BACKEND_PORT": 8848,
    "PATH_OPENPASS_CORE": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "WORKSPACE": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "MODULES_FOLDER": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "OPSIMULATION_EXE": "$$TEST_FOLDER_PATH_FILE$$",
    "OPSIMULATION_MANAGER_EXE": "$$TEST_FOLDER_PATH_FILE$$",
    "OPSIMULATION_MANAGER_XML": "$$TEST_FOLDER_PATH_FILE$$",
    "PATH_CONVERTED_CASES": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "PATH_LOG_FILE": "$$TEST_FOLDER_PATH_FILE$$"
})"
);

QString requestMockJSON = QStringLiteral(
    R"([
        {
            "id": "PATH_OPENPASS_CORE",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "WORKSPACE",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "MODULES_FOLDER",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "PATH_CONVERTED_CASES",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "OPSIMULATION_EXE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "OPSIMULATION_MANAGER_EXE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "OPSIMULATION_MANAGER_XML",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "PATH_LOG_FILE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        }
    ])"
);

QString requestMockJSONFileChanged = QStringLiteral(
    R"([
        {
            "id": "PATH_LOG_FILE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE2$$"
        }
    ])"
);

QString requestMockJSONFolderChanged = QStringLiteral(
    R"([
        {
            "id": "MODULES_FOLDER",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER2$$"
        }
    ])"
);

QString requestMockJSONFileChangedWrongValue = QStringLiteral(
    R"([
        {
            "id": "PATH_LOG_FILE",
            "type": "file",
            "value": "/some/not/exising/folder/file.txt"
        }
    ])"
);

QString requestMockJSONFolderChangedWrongValue = QStringLiteral(
    R"([
        {
            "id": "MODULES_FOLDER",
            "type": "folder",
            "value": "/some/not/exising/folder"
        }
    ])"
);

QString requestMockJSONNotExistingId = QStringLiteral(
    R"([
        {
            "id": "NOT_EXISTING_ID",
            "type": "folder",
            "value": "/some/not/exising/folder"
        }
    ])"
);

bool TEST_SET_CONFIG_ELEMENTS_API::parseConfigElementsJson(const QString& jsonStr, QList<OpenAPI::OAIConfigElement>& oai_config_element_list) {
    QJsonDocument doc = QJsonDocument::fromJson(jsonStr.toUtf8());

    if (doc.isNull()) {
        qDebug() << "Failed to create JSON doc.";
        return false;
    }

    if (!doc.isArray()) {
        qDebug() << "JSON doc is not an array.";
        return false;
    }

    QJsonArray jsonArray = doc.array();

    foreach (QJsonValue obj, jsonArray) {
        OpenAPI::OAIConfigElement o;
        OpenAPI::fromJsonValue(o, obj);
        oai_config_element_list.append(o);
    }

    return true;
}
    
void TEST_SET_CONFIG_ELEMENTS_API::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";
    // Check if workspace exists.
    this->workSpacePath = OPGUICoreGlobalConfig::getInstance().workspace();

    this->testDirFullPath = TestHelpers::joinPaths(this->workSpacePath, "test");
    QDir dir(this->workSpacePath);
    ASSERT_TRUE(dir.exists()) << "Workspace directory doesn't exist: " << this->workSpacePath.toStdString();

    // If test directory exists, remove it.
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove existing test directory: " << this->testDirFullPath.toStdString();
    }
    ASSERT_TRUE(dir.mkpath(this->testDirFullPath)) << "Failed to create test directory at: " << this->testDirFullPath.toStdString();

    this->mockConfigFileFullPath = TestHelpers::joinPaths(this->testDirFullPath, "backendConfig.json");

    //create mock file and dir
    this->testDirFullPathFile = TestHelpers::joinPaths(this->testDirFullPath, "mockFile.txt");
    QFile mockFile(this->testDirFullPathFile);
    if (mockFile.open(QIODevice::WriteOnly)) {
        // Optionally write some data to the file
        mockFile.write("Test data");
        mockFile.close();
    }
    ASSERT_TRUE(QFile::exists(this->testDirFullPathFile)) << "Failed to create or find dummy test file at: " << this->testDirFullPathFile.toStdString();

    this->testDirFullPathSubFolder = TestHelpers::joinPaths(this->testDirFullPath, "mockFolder");
    QDir dirFullPathSubFolder;
    ASSERT_TRUE(dirFullPathSubFolder.mkpath(this->testDirFullPathSubFolder)) << "Failed to create dummy test directory at: " << this->testDirFullPathSubFolder.toStdString();

    this->testDirFullPathFile2 = TestHelpers::joinPaths(this->testDirFullPath, "mockFile2.txt");
    QFile mockFile2(this->testDirFullPathFile2);
    if (mockFile2.open(QIODevice::WriteOnly)) {
        mockFile2.write("Test data");
        mockFile2.close();
    }
    ASSERT_TRUE(QFile::exists(this->testDirFullPathFile2)) << "Failed to create or find dummy test file at: " << this->testDirFullPathFile2.toStdString();

    this->testDirFullPathSubFolder2 = TestHelpers::joinPaths(this->testDirFullPath, "mockFolder2");
    QDir dirFullPathSubFolder2;
    ASSERT_TRUE(dirFullPathSubFolder2.mkpath(this->testDirFullPathSubFolder2)) << "Failed to create dummy test directory at: " << this->testDirFullPathSubFolder2.toStdString();
    

    //set config folder path to the test one
    OPGUICoreGlobalConfig::getInstance().getInstance().setPathConfigFile(this->mockConfigFileFullPath);
}

void TEST_SET_CONFIG_ELEMENTS_API::TearDown()  {
    QDir dir(this->testDirFullPath);
    if(dir.exists()){
        ASSERT_TRUE(dir.removeRecursively()) << "Failed to remove test directory during tear down: " << this->testDirFullPath.toStdString();
    }
    OPGUICoreGlobalConfig::getInstance().setPathConfigFile(OPGUICoreGlobalConfig::getInstance().defaultPathConfigFile());
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_no_value_changed_POSITIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSON.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    requestMockJSON.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSON, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";
    
    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);

    ASSERT_TRUE(result) << "Set config API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("No values were modified."))<<"Error message was expected to contain 'No values were modified.' but was "+this->response.getResponse().toStdString();

    //read config file and compare with the one obtained
    // Read the content of the generated file
    QString actualFile = TestHelpers::readFile(this->mockConfigFileFullPath);

    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(configFileMockSet.simplified()),
    TestHelpers::removeSpacesBetweenTags(actualFile.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << configFileMockSet.simplified().toStdString() << "\n"
        << "Actual XML:\n" << actualFile.simplified().toStdString(); 
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_one_file_name_changed_POSITIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONFileChanged.replace("$$TEST_FOLDER_PATH_FILE2$$", this->testDirFullPathFile2 + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONFileChanged, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";
    
    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);

    ASSERT_TRUE(result) << "Set config API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("Updated element"))<<"Error message was expected to contain 'Updated element' but was "+this->response.getResponse().toStdString();

    //read config file and compare with the one obtained
    // Read the content of the generated file
    QString actualFile = TestHelpers::readFile(this->mockConfigFileFullPath);
    QString modifiedPart = "\"PATH_LOG_FILE\": \""+this->testDirFullPathFile2+"\"";
    bool containsExpected = actualFile.contains(modifiedPart);

    ASSERT_TRUE(containsExpected) 
    << "Test: The contents of the generated systems XML file do not contain the expected text.\n"
    << "Expected XML (Processed):\n" << modifiedPart.toStdString() << "\n"
    << "Actual XML (Processed):\n" << actualFile.toStdString();
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_one_folder_name_changed_POSITIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONFolderChanged.replace("$$TEST_FOLDER_PATH_SUBFOLDER2$$", this->testDirFullPathSubFolder2 + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONFolderChanged, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";
    
    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);

    ASSERT_TRUE(result) << "Set config API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("Updated element"))<<"Error message was expected to contain 'Updated element' but was "+this->response.getResponse().toStdString();

    //read config file and compare with the one obtained
    // Read the content of the generated file
    QString actualFile = TestHelpers::readFile(this->mockConfigFileFullPath);
    QString modifiedPart = "\"MODULES_FOLDER\": \""+this->testDirFullPathSubFolder2+"\"";
    bool containsExpected = actualFile.contains(modifiedPart);

    ASSERT_TRUE(containsExpected) 
    << "Test: The contents of the generated systems XML file do not contain the expected text.\n"
    << "Expected XML (Processed):\n" << modifiedPart.toStdString() << "\n"
    << "Actual XML (Processed):\n" << actualFile.toStdString();
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_one_folder_name_changed_folder_not_exist_Negative) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONFolderChangedWrongValue.replace("$$TEST_FOLDER_PATH_SUBFOLDER2$$", "aaaaaa");

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONFolderChangedWrongValue, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";
    
    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);

    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("does not exist"))<<"Error message was expected to contain 'does not exist' but was "+errorMsg.toStdString();
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_one_file_name_changed_folder_not_exist_NEGATIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral("wrong"));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONFileChangedWrongValue.replace("$$TEST_FOLDER_PATH_SUBFOLDER2$$", this->testDirFullPathSubFolder2 + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONFileChangedWrongValue, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";
    
    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);

    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("does not exist"))<<"Error message was expected to contain 'does not exist' but was "+errorMsg.toStdString();
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Set_configs_try_not_existing_id_POSITIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral("wrong"));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONNotExistingId.replace("$$TEST_FOLDER_PATH_SUBFOLDER2$$", this->testDirFullPathSubFolder2 + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONNotExistingId, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";

    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);
    
    ASSERT_TRUE(result) << "Set config API call failed";

    ASSERT_TRUE(this->response.getResponse().contains("No values were modified."))<<"Error message was expected to contain 'No values were modified.' but was "+this->response.getResponse().toStdString();

    //read config file and compare with the one obtained
    // Read the content of the generated file
    QString actualFile = TestHelpers::readFile(this->mockConfigFileFullPath);

    ASSERT_EQ(
    TestHelpers::removeSpacesBetweenTags(configFileMockSet.simplified()),
    TestHelpers::removeSpacesBetweenTags(actualFile.simplified())
    ) << "Test: contents of expected and generated systems xml file differ.\n"
        << "Expected XML:\n" << configFileMockSet.simplified().toStdString() << "\n"
        << "Actual XML:\n" << actualFile.simplified().toStdString(); 
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Empty_configs_list_in_request_NEGATIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);
    
    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Received JSON list of config elements is empty"))<<"Error message was expected to contain 'Received JSON list of config elements is empty' but was "+errorMsg.toStdString();
}

TEST_F(TEST_SET_CONFIG_ELEMENTS_API, Config_file_not_existing_NEGATIVE) {
    QList<OpenAPI::OAIConfigElement> configsReq;
    QString errorMsg;

    configFileMockSet.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockSet.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral("wrong"));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockSet);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    requestMockJSONNotExistingId.replace("$$TEST_FOLDER_PATH_SUBFOLDER2$$", this->testDirFullPathSubFolder2 + QStringLiteral(""));

    ASSERT_TRUE(parseConfigElementsJson(requestMockJSONNotExistingId, configsReq))<< "Failed preparing the test, unable parse the systems JSON for the test";

    //set config folder path to wrong one
    OPGUICoreGlobalConfig::getInstance().getInstance().setPathConfigFile(this->mockConfigFileFullPath+"wrong_path");

    bool result = OPGUICore::api_set_configElements(configsReq, this->response,errorMsg);
    
    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Config file does not exist"))<<"Error message was expected to contain 'Config file does not exist' but was "+errorMsg.toStdString();
}







