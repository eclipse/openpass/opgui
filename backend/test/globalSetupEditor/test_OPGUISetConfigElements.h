/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <gtest/gtest.h>
#include <QString>

#include "OPGUICoreGlobalConfig.h"
#include "OAIConfigElement.h"
#include "OAIDefault200Response.h"
#include "OAIConfigsRequest.h"

class OPGUICoreGlobalConfig;

class TEST_SET_CONFIG_ELEMENTS_API : public ::testing::Test {
  public:
    QString testDirFullPath;
    QString testDirFullPathFile;
    QString testDirFullPathSubFolder;
    QString testDirFullPathFile2;
    QString testDirFullPathSubFolder2;
    QString workSpacePath;
    QString mockConfigFileFullPath;
    OpenAPI::OAIDefault200Response response;
    OpenAPI::OAIDefault200Response response200Expected;
    

    OPGUICoreGlobalConfig* config;

    void SetUp() override;
    
    void TearDown() override;

    bool parseConfigElementsJson(const QString& jsonStr, QList<OpenAPI::OAIConfigElement>& oai_config_element_list);
};





