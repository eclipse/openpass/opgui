/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonParseError>

#include "test_OPGUIGetConfigElements.h"
#include "test_helpers.h"
#include "OAIHelpers.h"
#include "OPGUICore.h"
#include "OPGUICoreGlobalConfig.h"

QString configFileMock = QStringLiteral(
    R"({
    "BACKEND_IP": "127.0.0.1",
    "BACKEND_PORT": 8848,
    "PATH_OPENPASS_CORE": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "WORKSPACE": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "MODULES_FOLDER": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "OPSIMULATION_EXE": "$$TEST_FOLDER_PATH_FILE$$",
    "OPSIMULATION_MANAGER_EXE": "$$TEST_FOLDER_PATH_FILE$$",
    "OPSIMULATION_MANAGER_XML": "$$TEST_FOLDER_PATH_FILE$$",
    "PATH_CONVERTED_CASES": "$$TEST_FOLDER_PATH_SUBFOLDER$$",
    "PATH_LOG_FILE": "$$TEST_FOLDER_PATH_FILE$$"
})"
);

QString configFileMockMalformed = QStringLiteral(
    R"({
    "BACKEND_IP": "127.0.0.1",
    "BACKEND_PORT": 8848,
    "PATH_OPENPASS_C
    "WORKSPACE": "$$TEST_FOLDER_PATH_S
})"
);

QString responseMockJSON = QStringLiteral(
    R"([
        {
            "id": "PATH_OPENPASS_CORE",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "WORKSPACE",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "MODULES_FOLDER",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "PATH_CONVERTED_CASES",
            "type": "folder",
            "value": "$$TEST_FOLDER_PATH_SUBFOLDER$$"
        },
        {
            "id": "OPSIMULATION_EXE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "OPSIMULATION_MANAGER_EXE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "OPSIMULATION_MANAGER_XML",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        },
        {
            "id": "PATH_LOG_FILE",
            "type": "file",
            "value": "$$TEST_FOLDER_PATH_FILE$$"
        }
    ])"
);

void GET_CONFIG_ELEMENTS_API_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";
    // Check if workspace exists.
    this->workSpacePath = OPGUICoreGlobalConfig::getInstance().workspace();

    this->testDirFullPath = TestHelpers::joinPaths(this->workSpacePath, "test");
    QDir dir(this->workSpacePath);
    ASSERT_TRUE(dir.exists()) << "Workspace directory doesn't exist: " << this->workSpacePath.toStdString();

    // If test directory exists, remove it.
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove existing test directory: " << this->testDirFullPath.toStdString();
    }
    ASSERT_TRUE(dir.mkpath(this->testDirFullPath)) << "Failed to create test directory at: " << this->testDirFullPath.toStdString();

    this->mockConfigFileFullPath = TestHelpers::joinPaths(this->testDirFullPath, "backendConfig.json");

    //create mock file and dir
    this->testDirFullPathFile = TestHelpers::joinPaths(this->testDirFullPath, "mockFile.txt");
    QFile mockFile(this->testDirFullPathFile);
    if (mockFile.open(QIODevice::WriteOnly)) {
        // Optionally write some data to the file
        mockFile.write("Test data");
        mockFile.close();
    }
    ASSERT_TRUE(QFile::exists(this->testDirFullPathFile)) << "Failed to create or find dummy test file at: " << this->testDirFullPathFile.toStdString();

    this->testDirFullPathSubFolder = TestHelpers::joinPaths(this->testDirFullPath, "mockFolder");
    QDir dirFullPathSubFolder;
    ASSERT_TRUE(dirFullPathSubFolder.mkpath(this->testDirFullPathSubFolder)) << "Failed to create dummy test directory at: " << this->testDirFullPathSubFolder.toStdString();

    //set config folder path to the test one
    OPGUICoreGlobalConfig::getInstance().getInstance().setPathConfigFile(this->mockConfigFileFullPath);
}

void GET_CONFIG_ELEMENTS_API_TEST::TearDown()  {
    QDir dir(this->testDirFullPath);
    if(dir.exists()){
        ASSERT_TRUE(dir.removeRecursively()) << "Failed to remove test directory during tear down: " << this->testDirFullPath.toStdString();
    }
    OPGUICoreGlobalConfig::getInstance().setPathConfigFile(OPGUICoreGlobalConfig::getInstance().defaultPathConfigFile());
}

TEST_F(GET_CONFIG_ELEMENTS_API_TEST, get_file_contents_positive) {
    QList<OpenAPI::OAIConfigElement> response;
    QString errorMsg;
    configFileMock.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMock.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMock);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    bool result = OPGUICore::api_get_configElements(response,errorMsg);

    EXPECT_TRUE(result) << "Get config API call failed";

    responseMockJSON.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    responseMockJSON.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    QJsonDocument resDoc(::OpenAPI::toJsonValue(response).toArray());

    QJsonDocument expectedDoc = QJsonDocument::fromJson(responseMockJSON.toUtf8());

    // Convert QJsonDocuments to compact string representations for comparison
    QString expectedJsonStr = QString(expectedDoc.toJson(QJsonDocument::Compact));
    QString actualJsonStr = QString(resDoc.toJson(QJsonDocument::Compact));

    // Perform the assertion
    ASSERT_EQ(expectedJsonStr, actualJsonStr) 
        << "JSON objects are not equal.\nExpected: " << expectedJsonStr.toStdString() 
        << "\nActual: " << actualJsonStr.toStdString();
}

TEST_F(GET_CONFIG_ELEMENTS_API_TEST, Config_file_not_existing_NEGATIVE) {
    QList<OpenAPI::OAIConfigElement> response;
    QString errorMsg;
    configFileMock.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMock.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMock);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    OPGUICoreGlobalConfig::getInstance().getInstance().setPathConfigFile(this->mockConfigFileFullPath+"wrong_path");

    bool result = OPGUICore::api_get_configElements(response,errorMsg);
    
    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Config file does not exist"))<<"Error message was expected to contain 'Config file does not exist' but was "+errorMsg.toStdString();
}

TEST_F(GET_CONFIG_ELEMENTS_API_TEST, Config_file_malformed_NEGATIVE) {
    QList<OpenAPI::OAIConfigElement> response;
    QString errorMsg;
    configFileMockMalformed.replace("$$TEST_FOLDER_PATH_FILE$$", this->testDirFullPathFile + QStringLiteral(""));
    configFileMockMalformed.replace("$$TEST_FOLDER_PATH_SUBFOLDER$$", this->testDirFullPathSubFolder + QStringLiteral(""));

    bool fileCreated = TestHelpers::createAndCheckFile(this->mockConfigFileFullPath,configFileMockMalformed);
    ASSERT_TRUE(fileCreated) << "Test: " << this->mockConfigFileFullPath.toStdString() << " mock config file could not be created";

    OPGUICoreGlobalConfig::getInstance().getInstance().setPathConfigFile(this->mockConfigFileFullPath);

    bool result = OPGUICore::api_get_configElements(response,errorMsg);
    
    ASSERT_FALSE(result) << "Set config API call succeed but should have failed";

    ASSERT_TRUE(errorMsg.contains("Invalid or malformed JSON content"))<<"Error message was expected to contain 'Invalid or malformed JSON content' but was "+errorMsg.toStdString();
}




