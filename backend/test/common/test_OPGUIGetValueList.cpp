/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <QStandardPaths>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>

#include "test_OPGUIGetValueList.h"
#include "OPGUICore.h"

    
void GET_VALUE_LIST_TEST::SetUp()  {}

void GET_VALUE_LIST_TEST::TearDown()  {}

TEST_F(GET_VALUE_LIST_TEST, EXISTING_LIST_POSITIVE) {
  QString listName = "trafficRules";

  QString errorMsg;

  QList<QString> expectedResponse = {"DE"};

  bool result = OPGUICore::api_get_value_list(listName, response, errorMsg);

  ASSERT_TRUE(result);
  ASSERT_EQ(response.getListValues(), expectedResponse);
}

TEST_F(GET_VALUE_LIST_TEST, EXISTING_LIST_POSITIVE_SEVERAL_VALUES) {
  QString listName = "worldLibraries";

  QString errorMsg;

  QList<QString> expectedResponse = {"World","World_OSI"};

  bool result = OPGUICore::api_get_value_list(listName, response, errorMsg);

  ASSERT_TRUE(result);
  ASSERT_EQ(response.getListValues(), expectedResponse);
}

TEST_F(GET_VALUE_LIST_TEST, NON_EXISTING_LIST_NEGATIVE) {
  QString listName = "wrongList";

  QString errorMsg;

  bool result = OPGUICore::api_get_value_list(listName, response, errorMsg);

  ASSERT_FALSE(result) << "Get value list was succesfull but should be failed";

  ASSERT_TRUE(errorMsg.contains(QString("List '%1' not found.").arg(listName)))<<QString("Error message was expected to contain 'List '%1' not found.' but was ").arg(listName).toStdString()+errorMsg.toStdString();
}

TEST_F(GET_VALUE_LIST_TEST, EXISTING_LIST_BUT_EMPTY_NEGATIVE) {
  QString listName = "emptyList";

  QString errorMsg;

  bool result = OPGUICore::api_get_value_list(listName, response, errorMsg);

  ASSERT_FALSE(result) << "Get value list was succesfull but should be failed";

  ASSERT_TRUE(errorMsg.contains(QString("List '%1' is empty.").arg(listName)))<<QString("Error message was expected to contain 'List '%1' is empty.' but was ").arg(listName).toStdString()+errorMsg.toStdString();
}

TEST_F(GET_VALUE_LIST_TEST, EMPTY_LIST_NAME_NEGATIVE) {
  QString listName = "";

  QString errorMsg;

  bool result = OPGUICore::api_get_value_list(listName, response, errorMsg);

  ASSERT_FALSE(result) << "Get value list was succesfull but should be failed";

  ASSERT_TRUE(errorMsg.contains("List name is empty."))<<"Error message was expected to contain 'List name is empty.' is empty.' but was "+errorMsg.toStdString();
}



