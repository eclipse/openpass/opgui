/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <QStandardPaths>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>

#include "test_OPGUICoreGlobalConfig.h"

    
void GLOBAL_CONFIG::SetUp()  {
    config = &OPGUICoreGlobalConfig::getInstance();
    config->setDefaultValues();
    this->originalFilePath = config->pathConfigFile();
    this->backupFilePath = this->originalFilePath + ".bk";

    QString jsonContent = R"({
        "BACKEND_IP": "127.0.0.1",
        "BACKEND_PORT": 8848,
        "OPSIMULATION_EXE": "/Users/test/opSimulation/bin/core/opSimulation",
        "OPSIMULATION_MANAGER_EXE": "/Users/test/opSimulation/bin/core/opSimulationManager",
        "OPSIMULATION_MANAGER_XML": "/Users/test/opSimulation/bin/core/opSimulationManager.xml",
        "PATH_CONVERTED_CASES": "/Users/test/Documents/workspaces/opGUI-workspace1/test_new_5",
        "PATH_LOG_FILE": "/Users/test/Documents/workspaces/opGUI-workspace1/opGuiCore.log",
        "PATH_OPENPASS_CORE": "/Users/test/opSimulation/bin/core",
        "WORKSPACE": "/Users/test/Documents/workspaces/opGUI-workspace1",
        "PATH_DATABASE_PCM": "/Users/test/Documents/workspaces/opGUI-workspace1/",
        "MODULES_FOLDER": "/Users/test/opSimulation/bin/core/modules",
        "COMPONENTS_FOLDER": "/Users/test/opSimulation/bin/core/components",
        "SYSTEMS_FOLDER": "/Users/test/opSimulation/bin/core/systems"
    })";

    if (QFile::exists(this->backupFilePath)) {
        QFile::remove(this->backupFilePath);
    }

    ASSERT_TRUE(QFile::copy(this->originalFilePath,this->backupFilePath))
        << "Failed to create backup. Source: " << this->originalFilePath.toStdString()
        << ", Backup: " << this->backupFilePath.toStdString();

    // Overwrite original file with JSON content
    QFile file(this->originalFilePath);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream stream(&file);
        stream << jsonContent;
        file.close();
    }
}

void GLOBAL_CONFIG::TearDown()  {
    config = &OPGUICoreGlobalConfig::getInstance();

    if (QFile::exists(this->originalFilePath)) {
        QFile::remove(this->originalFilePath);
    }

    ASSERT_TRUE(QFile::rename(this->backupFilePath, this->originalFilePath))
        << "Failed to rename the backup file. Backup: " << this->backupFilePath.toStdString()
        << ", Original: " << this->originalFilePath.toStdString();

    config->loadFromConfigFile();
}

TEST_F(GLOBAL_CONFIG, DEFAULT_PATH_CFG_FILE_IS_CORRECTLY_SET_POSITIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    #ifdef _WIN32
        ASSERT_EQ(config->pathConfigFile(), QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first() + "\\opgui\\backend\\backendConfig.json")
            << "Default Path for Config File is not correctly set on Windows.";
    #elif __APPLE__ || __linux__
        ASSERT_EQ(config->pathConfigFile(), QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first() + "/opgui/backend/backendConfig.json")
            << "Default Path for Config File is not correctly set on macOS/Linux.";
    #endif
}

TEST_F(GLOBAL_CONFIG, DEFAULT_PATH_HOME_IS_CORRECTLY_SET_POSITIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    ASSERT_EQ(config->pathHome(), QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first())
        << "Default Home Path is not correctly set.";
}

TEST_F(GLOBAL_CONFIG, ALL_SETTERS_SET_BY_DEFAULT_POSITIVE) {
    QString homePath = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();
    #ifdef _WIN32
        QString configFilePath = homePath + "\\opgui\\backend\\backendConfig.json";
    #elif __APPLE__ || __linux__
        QString configFilePath = homePath + "/opgui/backend/backendConfig.json";
    #endif

    config = &OPGUICoreGlobalConfig::getInstance();

    ASSERT_EQ(config->pathConfigFile(), configFilePath) 
        << "The config file path is incorrect. Expected: '" << configFilePath.toStdString() << "', Actual: '" << config->pathConfigFile().toStdString() << "'.";

    ASSERT_EQ(config->pathHome(), homePath) 
        << "The home path is incorrect. Expected: '" << homePath.toStdString() << "', Actual: '" << config->pathHome().toStdString() << "'.";

    ASSERT_EQ(config->backendPort(), 8848) 
        << "The backend port is incorrect. Expected: 8848, Actual: " << config->backendPort() << ".";

    ASSERT_EQ(config->backendIP(), "127.0.0.1") 
        << "The backend IP address is incorrect. Expected: '127.0.0.1', Actual: '" << config->backendIP().toStdString() << "'.";

    ASSERT_EQ(config->pathOpenpassCore(), "") 
        << "The OpenPASS core path should be empty but is not. Actual: '" << config->pathOpenpassCore().toStdString() << "'.";

    ASSERT_EQ(config->workspace(), "") 
        << "The workspace path should be empty but is not. Actual: '" << config->workspace().toStdString() << "'.";

    ASSERT_EQ(config->pathConvertedCases(), "") 
        << "The converted cases path should be empty but is not. Actual: '" << config->pathConvertedCases().toStdString() << "'.";

    ASSERT_EQ(config->pathLogFile(), config->workspace() + "/opGuiCore.log") 
        << "The log file path is incorrect. Expected: '" << (config->workspace() + "/opGuiCore.log").toStdString() << "', Actual: '" << config->pathLogFile().toStdString() << "'.";

}

TEST_F(GLOBAL_CONFIG, PATHS_CAN_BE_CORRECTLY_UPDATED_POSITIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();

    config->setFullPathComponentsFolder("mod_value");
    config->setFullPathModulesFolder("mod_value");
    config->setFullPathOpSimulationManagerXml("mod_value");
    config->setFullPathOpSimulationManagerExe("mod_value");

    config->setPathConfigFile("mod_value");
    config->setPathLogFile("mod_value");
    config->setPathHome("mod_value");
    config->setBackendPort(9999);
    config->setBackendIP("mod_value");
    config->setPathOpenpassCore("mod_value");
    config->setWorkspace("mod_value");
    config->setPathConvertedCases("mod_value");

    ASSERT_EQ(config->fullPathComponentsFolder(), "mod_value") 
        << "Error in componentsFolder: Expected 'mod_value', got '" << config->fullPathComponentsFolder().toStdString() << "'";

    ASSERT_EQ(config->fullPathModulesFolder(), "mod_value") 
        << "Error in modulesFolder: Expected 'mod_value', got '" << config->fullPathModulesFolder().toStdString() << "'";

    ASSERT_EQ(config->fullPathopSimulationManagerXml(), "mod_value") 
        << "Error in opSimulationManagerXml: Expected 'mod_value', got '" << config->fullPathopSimulationManagerXml().toStdString() << "'";

    ASSERT_EQ(config->fullPathopSimulationManagerExe(), "mod_value") 
        << "Error in opSimulationManagerExe: Expected 'mod_value', got '" << config->fullPathopSimulationManagerExe().toStdString() << "'";

    ASSERT_EQ(config->pathConfigFile(), "mod_value") 
        << "Error in pathConfigFile: Expected 'mod_value', got '" << config->pathConfigFile().toStdString() << "'";

    ASSERT_EQ(config->pathHome(), "mod_value") 
        << "Error in pathHome: Expected 'mod_value', got '" << config->pathHome().toStdString() << "'";

    ASSERT_EQ(config->backendPort(), 9999) 
        << "Error in backendPort: Expected 9999, got '" << config->backendPort() << "'";

    ASSERT_EQ(config->backendIP(), "mod_value") 
        << "Error in backendIP: Expected 'mod_value', got '" << config->backendIP().toStdString() << "'";

    ASSERT_EQ(config->pathOpenpassCore(), "mod_value") 
        << "Error in pathOpenpassCore: Expected 'mod_value', got '" << config->pathOpenpassCore().toStdString() << "'";

    ASSERT_EQ(config->opSimulationExe(), "mod_value") 
        << "Error in opSimulationExe: Expected 'mod_value', got '" << config->opSimulationExe().toStdString() << "'";

    ASSERT_EQ(config->workspace(), "mod_value") 
        << "Error in workspace: Expected 'mod_value', got '" << config->workspace().toStdString() << "'";

    ASSERT_EQ(config->pathConvertedCases(), "mod_value") 
        << "Error in pathConvertedCases: Expected 'mod_value', got '" << config->pathConvertedCases().toStdString() << "'";

    ASSERT_EQ(config->pathLogFile(), "mod_value") 
        << "Error in pathLogFile: Expected '" <<  "mod_value" << "', got '" << config->pathLogFile().toStdString() << "'";
} 

TEST_F(GLOBAL_CONFIG, CONFIGS_CAN_BE_LOADED_FROM_FILE_POSITIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    ASSERT_TRUE(config->loadFromConfigFile());
}

TEST_F(GLOBAL_CONFIG, CONFIGS_CAN_NOT_BE_LOADED_FROM_FILE_NOT_EXIST_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    ASSERT_TRUE(QFile::remove(this->originalFilePath));
    ASSERT_FALSE(config->loadFromConfigFile())
        << "config file load returned true but it should have failed";; 
}

TEST_F(GLOBAL_CONFIG, CONFIGS_CAN_NOT_BE_LOADED_FILE_WRONG_FORMATED_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();

    QFile configFile(this->originalFilePath);
    ASSERT_TRUE(configFile.open(QIODevice::WriteOnly)) << "Unable to open config file for writing.";

    QTextStream stream(&configFile);
    stream << "This is not valid JSON content.";

    configFile.close();

    ASSERT_FALSE(config->loadFromConfigFile())
        << "config file load returned true but it should have failed as it is wrong formatted";
}

TEST_F(GLOBAL_CONFIG, CONFIGS_CAN_NOT_BE_LOADED_FILE_WRONG_PERMISSIONS_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();

    QString configFilePath = config->pathConfigFile();
    ASSERT_EQ(chmod(configFilePath.toStdString().c_str(), 0000), 0) 
        << "Failed to change file permissions to restrict read access.";

    ASSERT_FALSE(config->loadFromConfigFile())
        << "config file load returned true but it should have failed as it is wrong formatted";; 
}

TEST_F(GLOBAL_CONFIG, CONFIG_FILE_CAN_BE_UPDATED_POSITIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    config->modifyOrAddValueToConfigFile("FAKE_VALUE","XXX");

    QString configFilePath = config->pathConfigFile();
    QFile configFile(configFilePath);
    ASSERT_TRUE(configFile.open(QIODevice::ReadOnly)) << "Failed to open config file for reading.";

    QByteArray fileContent = configFile.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(fileContent);
    ASSERT_FALSE(doc.isNull()) << "Failed to parse JSON content.";
    ASSERT_TRUE(doc.isObject()) << "JSON content is not an object.";

    // Get the JSON object and check the value
    QJsonObject jsonObj = doc.object();
    ASSERT_TRUE(jsonObj.contains("FAKE_VALUE")) << "Config file does not contain the key 'FAKE_VALUE'.";
    ASSERT_EQ(jsonObj.value("FAKE_VALUE").toString(), "XXX") << "The value of 'FAKE_VALUE' is not correctly updated in the config file.";
}

TEST_F(GLOBAL_CONFIG, CONFIG_FILE_CAN_NOT_BE_UPDATED_NOREAD_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    QString configFilePath = config->pathConfigFile();

    ASSERT_EQ(chmod(configFilePath.toStdString().c_str(), 0000), 0) 
        << "Failed to change file permissions to restrict read access.";

    ASSERT_FALSE(config->modifyOrAddValueToConfigFile("FAKE_VALUE", "XXX")); 
}

TEST_F(GLOBAL_CONFIG, CONFIG_FILE_CAN_NOT_BE_UPDATED_NOWRITE_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();
    QString configFilePath = config->pathConfigFile();

    ASSERT_EQ(chmod(configFilePath.toStdString().c_str(), 0000), 0)
        << "Failed to change file permissions to restrict write access.";

    ASSERT_FALSE(config->modifyOrAddValueToConfigFile("FAKE_VALUE", "XXX")); 
}

TEST_F(GLOBAL_CONFIG, CONFIG_FILE_CAN_NOT_BE_UPDATED_WRONG_JSON_NEGATIVE) {
    config = &OPGUICoreGlobalConfig::getInstance();

    QFile configFile(this->originalFilePath);
    ASSERT_TRUE(configFile.open(QIODevice::WriteOnly)) << "Unable to open config file for writing.";

    // Write invalid JSON content
    QTextStream stream(&configFile);
    stream << "This is not valid JSON content.";

    configFile.close();

    ASSERT_FALSE(config->modifyOrAddValueToConfigFile("FAKE_VALUE", "XXX"))
        << "config file load returned true but it should have failed as it is wrong formatted";
}


