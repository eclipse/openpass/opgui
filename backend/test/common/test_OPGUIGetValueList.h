/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <gtest/gtest.h>
#include <QString>
#include <QList>
#include "OAI_api_getValueList_get_200_response.h"


class GET_VALUE_LIST_TEST : public ::testing::Test {
  public:
    QString workSpacePath;
    QString testDir;
    QString testDirFullPath;
    QString request;
    OpenAPI::OAI_api_getValueList_get_200_response response;

    void SetUp() override;
    void TearDown() override;
};