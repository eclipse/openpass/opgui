/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "gtest/gtest.h"
#include <QString>
#include <QDebug>
#include <QDateTime>

#include "test_OPGUIDeleteSystemApi.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUICore.h"
#include "OAIHelpers.h"
#include "test_helpers.h"
#include "OAIComponentUI.h"
#include "OAIError500.h"
#include "OAISystemUI.h"


void DELETE_SYSTEM_TEST::SetUp()  {
    ASSERT_TRUE(OPGUICoreGlobalConfig::getInstance().isInitializationSuccessful())
            << "Failed to initialize the global configuration.";

    this->workspacePath = OPGUICoreGlobalConfig::getInstance().workspace();
    this->testDirFullPath = TestHelpers::joinPaths(this->workspacePath, "test");

    this->mockSystemFilePath = TestHelpers::joinPaths(this->testDirFullPath, "mockSystem.txt");

    QDir dir(this->workspacePath);
    ASSERT_TRUE(dir.exists()) 
        << "The workspace directory does not exist: " << this->workspacePath.toStdString();

    // If test dir exists, remove it
    QDir dirTest(this->testDirFullPath);
    if (dirTest.exists()) {
        ASSERT_TRUE(dirTest.removeRecursively()) 
            << "Failed to remove the test directory: " << this->testDirFullPath.toStdString();
    }

    ASSERT_TRUE(dir.mkpath(this->testDirFullPath)) 
        << "Failed to create directory path: " << this->testDirFullPath.toStdString();

    ASSERT_TRUE(TestHelpers::createAndCheckFile(this->mockSystemFilePath, ""))<<"Could not save xml file as: "+this->mockSystemFilePath.toStdString();
}

void DELETE_SYSTEM_TEST::TearDown()  {
    QDir dirTest(this->testDirFullPath);
    if(dirTest.exists()){
        ASSERT_TRUE(dirTest.removeRecursively()) << "Failed to remove systems test directory: " << this->testDirFullPath.toStdString();
    }
}

TEST_F(DELETE_SYSTEM_TEST, Delete_system_POSITIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_delete_system(this->mockSystemFilePath, this->response,errorMsg);

    ASSERT_TRUE(result) << "Delete System was successfull but should have failed.";

    ASSERT_TRUE(this->response.getResponse().contains("System file successfully deleted"))<<"Error message was expected to contain 'System file successfully deleted' but was "+this->response.getResponse().toStdString(); 
}

TEST_F(DELETE_SYSTEM_TEST, Delete_system_no_existing_system_NEGATIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_delete_system("/some/fake/path/to/file.txt", this->response,errorMsg);

    ASSERT_FALSE(result) << "Delete System was successfull but should have failed.";

    ASSERT_TRUE(errorMsg.contains("File does not exist at path"))<<"Error message was expected to contain 'File does not exist at path' but was "+errorMsg.toStdString(); 
}

TEST_F(DELETE_SYSTEM_TEST, Delete_system_empty_path_NEGATIVE) {
    QString errorMsg;

    bool result = OPGUICore::api_delete_system("", this->response,errorMsg);

    ASSERT_FALSE(result) << "Delete System was successfull but should have failed.";

    ASSERT_TRUE(errorMsg.contains("Path of file is empty."))<<"Error message was expected to contain 'Path of file is empty.' but was "+errorMsg.toStdString(); 
}
