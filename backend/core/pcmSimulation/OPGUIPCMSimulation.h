/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QStringList>

#include "GUI_Definitions.h"
#include "ConfigGeneratorPcm.h"

namespace OPGUIPCMSimulation
{

/**
 * @brief Handles PCM (Product Change Management) simulation operations.
 *
 * This class manages operations related to PCM simulations, including initialization,
 * configuration generation, and execution control.
 */
class PCMSimulation
{
public:
    bool isInited = false;
    QString pathGeneratedOPSimulationManagerConfig;

    PCMSimulation() = default;
    ~PCMSimulation();

    /**
     * Initializes the PCM simulation environment.
     *
     * @param pcmDBPath Path to the PCM database.
     * @param pathModulesFolder Path to the modules folder.
     * @param pathPCMResultFolder Path to store PCM results.
     * @param pathOpSimulationExe Path to the Operational Simulation executable.
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if initialization is successful, false otherwise.
     */
    bool Init(const QString &pcmDBPath, const QString &pathModulesFolder, const QString &pathPCMResultFolder, const QString &pathOpSimulationExe, QString &errorMsg);

    /**
     * Sets the path to the folder containing converted PCM cases.
     *
     * @param path String path to the converted cases folder.
     * @return True if the path is set successfully, false otherwise.
     */
    bool SetPathToConvertedCases(const QString &path);

    /**
     * Generates configuration files for selected PCM cases.
     *
     * @param pcmSelectedCasesForSimulation List of selected PCM cases.
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if configurations are generated successfully, false otherwise.
     */
    bool UIGenerateConfigOfPCMCases(const QStringList &pcmSelectedCasesForSimulation, QString &errorMsg);

    /**
     * Loads PCM cases from a database file.
     *
     * @param path Path to the PCM file.
     * @param pcm_cases List to store the loaded PCM cases.
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if cases are loaded successfully, false otherwise.
     */
    bool LoadCasesFromPcmFile(QString path, QStringList &pcm_cases, QString &errorMsg) const;

    /**
     * Generates a simulation XML file.
     *
     * @param simConfigs List of simulation configurations.
     * @param logLevel Logging level for the simulation.
     * @param simulationManagerLogFile Path to the simulation manager log file.
     * @param libraries Path to simulation libraries.
     * @param simulationManagerXml Path to the simulation manager XML.
     * @param pathCore Path to the core simulation folder.
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if the XML is generated successfully, false otherwise.
     */
    bool GenerateSimulationXml(QList<OpenAPI::OAISimulationConfig> simConfigs, int logLevel, QString simulationManagerLogFile, QString libraries,
                               QString simulationManagerXml, QString pathCore, QString &errorMsg) const;

    /**
     * Generates a simulation XML using a shared configuration.
     *
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if the XML is generated successfully, false otherwise.
     */
    bool GenerateSimulationXmlUsingSharedConfig(QString &errorMsg);

    /**
     * Retrieves the path to the generated Operational Simulation Manager configuration.
     *
     * @return Path to the configuration.
     */
    QString getPathGeneratedOPSimulationManagerConfig() const;

    // Setter methods for various paths and parameters
    void setAgentsCar1(const QStringList &agentsCar1);
    void setAgentsCar2(const QStringList &agentsCar2);
    void setAgentsOther(const QStringList &agentsOther);
    void setPathModulesFolder(const QString &pathModules);
    void setPathOpSimulationExe(const QString &pathOpSimulationExe);

private:
    int variationCount = VARIATION_COUNT_DEFAULT;
    int initRandomSeed = INIT_RANDOM_SEED;

    /**
     * Creates configurations for PCM cases.
     *
     * This method orchestrates the configuration generation for PCM cases using provided case indices.
     * It handles configurations for different system lists such as cars and other vehicles.
     *
     * @param configGenerator Reference to the configuration generator utility.
     * @param pcmCaseIndexList List of PCM case indices to generate configurations for.
     * @param otherSystemList List of other systems involved in the simulation.
     * @param car1SystemList List of car1 systems involved in the simulation.
     * @param car2SystemList List of car2 systems involved in the simulation.
     * @param errorMsg Reference to a string for reporting errors.
     * @return True if configurations are successfully created, false otherwise.
     */
    bool CreateConfigs(ConfigGenerator &configGenerator, QStringList &pcmCaseIndexList, QStringList &otherSystemList, QStringList &car1SystemList, QStringList &car2SystemList, QString &errorMsg);

    /**
     * Reads a PCM simulation set from the database.
     *
     * This method retrieves simulation set data from the PCM database based on a given case identifier.
     * The simulation set includes all necessary details to perform a PCM simulation.
     *
     * @param pcmCase The PCM case identifier.
     * @param errorMsg Reference to a string for reporting errors.
     * @return Pointer to the loaded PCM_SimulationSet or nullptr if an error occurred.
     */
    PCM_SimulationSet* ReadSimulationSetFromDb(QString pcmCase, QString &errorMsg);

    /**
     * Adjusts the center of gravity for the simulation set.
     *
     * This method modifies the position of the center of gravity (COG) for each participant
     * in the simulation set, which affects the physics calculations during the simulation.
     *
     * @param simulationSet Pointer to the PCM_SimulationSet containing the participants.
     */
    void RelocateCog(PCM_SimulationSet *simulationSet) const;

    QString pcmDBPath;
    QStringList agentsCar1; 
    QStringList agentsCar2;
    QStringList agentsOther;
    QString pathModulesFolder;
    QString pathOpSimulationExe;
    QString pathPCMResultFolder;
    QString logLevel;
    QStringList pcmCases;
    QStringList pcmSelectedCasesForSimulation;
    ConfigGenerator* configGeneratorShared = nullptr;
};

}
