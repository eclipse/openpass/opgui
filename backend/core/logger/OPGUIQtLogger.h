/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QFile>
#include <QMutex>
#include <QString>

/**
 * @brief The OPGUIQtLogger class handles application-wide logging.
 *
 * This class provides a singleton logger that can be used to log messages at various levels (info, error, warn, debug).
 * It ensures thread-safety and can be easily accessed through static methods.
 */
class OPGUIQtLogger {
public:
    /**
     * @brief Sets the filename for the log file.
     * @param filename The name of the file to log to.
     */
    static void setLogFilename(const QString& filename);

    /**
     * @brief Provides access to the singleton instance of the logger.
     * @return Returns a reference to the singleton logger.
     */
    static OPGUIQtLogger& instance();

    /**
     * @brief Writes a message to the log with the specified level.
     * @param message The message to log.
     * @param level The severity level of the log message.
     */
    void write(const QString &message, const QString &level = "INFO");

    /**
     * @brief Logs an informational message.
     * @param message The message to log.
     */
    void info(const QString &message);

    /**
     * @brief Logs an error message.
     * @param message The message to log.
     */
    void error(const QString &message);

    /**
     * @brief Logs a warning message.
     * @param message The message to log.
     */
    void warn(const QString &message);

    /**
     * @brief Logs a debug message.
     * @param message The message to log.
     */
    void debug(const QString &message);

    /**
     * @brief Resets the logger, typically by closing and reopening the log file.
     */
    void resetLogger();

private:
    QFile logFile; ///< File object for the log file.
    QMutex mutex; ///< Mutex for thread safety in logging operations.
    static QString logFilename; ///< Static storage for the log filename.

    // Delete copy and assignment constructors to prevent multiple instances.
    OPGUIQtLogger(const OPGUIQtLogger&) = delete;
    OPGUIQtLogger& operator=(const OPGUIQtLogger&) = delete;

protected:
    /**
     * @brief Protected constructor for the singleton pattern.
     * @param filename The filename to log to.
     */
    explicit OPGUIQtLogger(const QString &filename);
};

// Macros for easier access:
#define LOGGER OPGUIQtLogger::instance()
#define LOG_INFO(message) LOGGER.info(message)
#define LOG_ERROR(message) LOGGER.error(message)
#define LOG_WARN(message) LOGGER.warn(message)
#define LOG_DEBUG(message) LOGGER.debug(message)
