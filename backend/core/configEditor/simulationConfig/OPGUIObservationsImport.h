/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include <QDomElement>
#include "OAIObservations.h"

/**
 * Namespace OPGUIConfigEditor encapsulates the functionality related to configuration editing
 * within the OPGUI application.
 */
namespace OPGUIConfigEditor {

/**
 * Class OPGUIObservationsImport is responsible for importing observation settings from an XML format.
 * It parses the XML to recreate the observations data structures as defined by the OpenAPI specifications.
 */
class OPGUIObservationsImport {
public:
    /**
     * @brief Imports observations from the given XML element and populates the OAIObservations object.
     * 
     * @param observationsEl The QDomElement containing the observations XML data.
     * @param observations The OAIObservations object to populate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the import is successful, false otherwise.
     */
    bool importObservations(const QDomElement &observationsEl, OpenAPI::OAIObservations &observations, QString &errorMsg) const;

private:
    /**
     * @brief Imports entity repository information from the given XML element and populates the OAIEntityRepository object.
     * 
     * @param paramsEl The QDomElement containing the entity repository parameters.
     * @param entityRepository The OAIEntityRepository object to populate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the import is successful, false otherwise.
     */
    bool importEntityRepository(const QDomElement &paramsEl, OpenAPI::OAIEntityRepository &entityRepository, QString &errorMsg) const;

    /**
     * @brief Imports log information from the given XML element and populates the OAILog object.
     * 
     * @param logEl The QDomElement containing the log parameters.
     * @param log The OAILog object to populate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the import is successful, false otherwise.
     */
    bool importLog(const QDomElement &logEl, OpenAPI::OAILog &log, QString &errorMsg) const;

    /**
     * @brief Imports logging groups information from the given XML element and populates the list of OAILoggingGroup objects.
     * 
     * @param paramsEl The QDomElement containing the logging groups parameters.
     * @param loggingGroups The list of OAILoggingGroup objects to populate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the import is successful, false otherwise.
     */
    bool importLoggingGroups(const QDomElement &paramsEl, QList<OpenAPI::OAILoggingGroup> &loggingGroups, QString &errorMsg) const;

    /**
     * @brief Validates the observations XML element.
     * 
     * @param observationsEl The QDomElement to validate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the XML element is valid, false otherwise.
     */
    bool isValidObservationsXml(const QDomElement &observationsEl, QString &errorMsg) const;

    /**
     * @brief Validates the entity repository XML element.
     * 
     * @param paramsEl The QDomElement to validate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the XML element is valid, false otherwise.
     */
    bool isValidEntityRepositoryXml(const QDomElement &paramsEl, QString &errorMsg) const;

    /**
     * @brief Validates the log XML element.
     * 
     * @param logEl The QDomElement to validate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the XML element is valid, false otherwise.
     */
    bool isValidLogXml(const QDomElement &logEl, QString &errorMsg) const;

    /**
     * @brief Validates the logging groups XML element.
     * 
     * @param paramsEl The QDomElement to validate.
     * @param errorMsg A reference to a QString to hold any error messages.
     * @return true if the XML element is valid, false otherwise.
     */
    bool isValidLoggingGroupsXml(const QDomElement &paramsEl, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor
