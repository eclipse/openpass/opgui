/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QDomElement>
#include <QString>
#include "OAIExperiment.h"
#include "OAIExperiment_libraries.h"

namespace OPGUIConfigEditor {

class OPGUIExperimentImport {
public:
    OPGUIExperimentImport() = default;

    /**
     * @brief Imports an experiment from the provided XML element.
     *
     * @param experimentEl The XML element representing the experiment.
     * @param experiment The object to populate with the imported experiment data.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the import was successful, false otherwise.
     */
    bool importExperiment(const QDomElement &experimentEl, OpenAPI::OAIExperiment &experiment, QString &errorMsg) const;

private:
    /**
     * @brief Imports the libraries from the provided XML element.
     *
     * @param librariesEl The XML element representing the libraries.
     * @param libraries The object to populate with the imported libraries data.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the import was successful, false otherwise.
     */
    bool importLibraries(const QDomElement &librariesEl, OpenAPI::OAIExperiment_libraries &libraries, QString &errorMsg) const;

    /**
     * @brief Validates the provided experiment XML element.
     *
     * @param experimentEl The XML element representing the experiment.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the XML is valid, false otherwise.
     */
    bool isValidExperimentXml(const QDomElement &experimentEl, QString &errorMsg) const;

    /**
     * @brief Validates the provided libraries XML element.
     *
     * @param librariesEl The XML element representing the libraries.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the XML is valid, false otherwise.
     */
    bool isValidLibrariesXml(const QDomElement &librariesEl, QString &errorMsg) const;

    /**
     * @brief Validates the ExperimentID in the provided experiment XML element.
     *
     * @param experimentEl The XML element representing the experiment.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the ExperimentID is valid, false otherwise.
     */
    bool isValidExperimentID(const QDomElement &experimentEl, QString &errorMsg) const;

    /**
     * @brief Validates the NumberOfInvocations in the provided experiment XML element.
     *
     * @param experimentEl The XML element representing the experiment.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the NumberOfInvocations is valid, false otherwise.
     */
    bool isValidNumberOfInvocations(const QDomElement &experimentEl, QString &errorMsg) const;

    /**
     * @brief Validates the RandomSeed in the provided experiment XML element.
     *
     * @param experimentEl The XML element representing the experiment.
     * @param errorMsg A reference to a string to store any error message in case of failure.
     * @return True if the RandomSeed is valid, false otherwise.
     */
    bool isValidRandomSeed(const QDomElement &experimentEl, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor
