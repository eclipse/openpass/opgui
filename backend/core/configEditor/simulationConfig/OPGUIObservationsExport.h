/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include <QXmlStreamWriter>
#include "OAIObservations.h"
#include "OAILog.h"
#include "OAIEntityRepository.h"

/**
 * Namespace OPGUIConfigEditor encapsulates the functionality related to configuration editing
 * within the OPGUI application.
 */
namespace OPGUIConfigEditor {

/**
 * Class OPGUIObservationsExport handles the export of observation settings into an XML format.
 * It is designed to serialize the observations data into a structured XML based on the provided
 * JSON object structures from the OpenAPI specifications.
 */
class OPGUIObservationsExport {
public:
    /**
     * Exports the observations settings into an XML document.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param observations The observations data to be exported, as defined by the OpenAPI spec.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportObservations(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIObservations &observations, QString &errorMsg) const;

private:
    /**
     * Validates the overall structure of the observations JSON object.
     *
     * @param observations The observations data as a JSON object.
     * @param errorMsg Reference to a string where an error message will be stored if validation fails.
     * @return Returns true if the JSON structure is valid, false otherwise.
     */
    bool isValidObservationsJSON(const OpenAPI::OAIObservations &observations, QString &errorMsg) const;

    /**
     * Validates the log subobject within the observations.
     *
     * @param log The log subobject.
     * @param errorMsg Reference to a string where an error message will be stored if validation fails.
     * @return Returns true if the log subobject structure is valid, false otherwise.
     */
    bool isValidLogJSON(const OpenAPI::OAILog &log, QString &errorMsg) const;

    /**
     * Validates the entity repository subobject within the observations.
     * Checks if values are within the valid options if not empty.
     *
     * @param entityRepository The entity repository subobject.
     * @param errorMsg Reference to a string where an error message will be stored if validation fails.
     * @return Returns true if the entity repository subobject structure is valid, false otherwise.
     */
    bool isValidEntityRepositoryJSON(const OpenAPI::OAIEntityRepository &entityRepository, QString &errorMsg) const;

    /**
     * Helper function to export the log subobject to XML.
     * Processes each logging group and exports details only for enabled groups.
     *
     * @param xmlWriter Reference to the XML writer.
     * @param log The log subobject to export.
     * @param errorMsg Reference to a string where an error message will be stored if an error occurs during export.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportLog(QXmlStreamWriter &xmlWriter, const OpenAPI::OAILog &log, QString &errorMsg) const;

    /**
     * Helper function to export the entity repository subobject to XML.
     * Exports attributes only if they are not empty, and does not create an XML element for the subobject if both attributes are empty.
     *
     * @param xmlWriter Reference to the XML writer.
     * @param entityRepository The entity repository subobject to export.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportEntityRepository(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIEntityRepository &entityRepository) const;
};

} // namespace OPGUIConfigEditor
