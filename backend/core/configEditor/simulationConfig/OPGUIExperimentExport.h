/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>
#include <QXmlStreamWriter>
#include "OAIExperiment.h"
#include "OAIExperiment_libraries.h"

namespace OPGUIConfigEditor {

/**
 * Class OPGUIExperimentExport handles the export of experiment settings into an XML format.
 * It provides functionalities to serialize experiment components such as IDs, invocation counts, seeds, and libraries.
 */
class OPGUIExperimentExport {
public:
    /**
     * Default constructor for the OPGUIExperimentExport class.
     */
    OPGUIExperimentExport() = default;

    /**
     * Exports the given experiment settings into an XML document.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param experiment The experiment settings to be exported.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the export is successful, false otherwise.
     */
    bool exportExperiment(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIExperiment& experiment, QString& errorMsg) const;

private:
    /**
     * Validates the given experiment JSON structure before exporting.
     *
     * @param experiment The experiment data to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the JSON is valid, false otherwise.
     */
    bool isValidExperimentJSON(const OpenAPI::OAIExperiment& experiment, QString& errorMsg) const;

    /**
     * Exports the libraries details nested within the experiment object.
     *
     * @param xmlWriter Reference to an XML stream writer to write the data.
     * @param libraries The libraries object nested within the experiment.
     */
    void exportLibraries(QXmlStreamWriter& xmlWriter, const OpenAPI::OAIExperiment_libraries& libraries) const;

    /**
     * Validates the libraries JSON structure before exporting.
     *
     * @param libraries The libraries data to validate.
     * @param errorMsg Output parameter for any error messages.
     * @return Returns true if the JSON is valid, false otherwise.
     */
    bool isValidLibrariesJSON(const OpenAPI::OAIExperiment_libraries& libraries, QString& errorMsg) const;
};

} // namespace OPGUIConfigEditor
