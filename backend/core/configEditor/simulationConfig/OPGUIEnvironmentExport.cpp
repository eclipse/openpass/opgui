/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QList>
#include <QString>
#include <QXmlStreamWriter>

#include "OPGUIEnvironmentExport.h"
#include "OPGUIValueList.h"
#include "OPGUIConstants.h"

namespace OPGUIConfigEditor {

OPGUIEnvironmentExport::OPGUIEnvironmentExport() = default;

//export functions
bool OPGUIEnvironmentExport::exportEnvironment(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIEnvironment &environment, QString &errorMsg) const{
    if (!isValidEnvironmentJSON(environment, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Environment");

    if (!exportTimeOfDays(xmlWriter, environment.getTimeOfDays(), errorMsg)) {
        return false;
    }
    if (!exportVisibilityDistances(xmlWriter, environment.getVisibilityDistances(), errorMsg)) {
        return false;
    }
    if (!exportFrictions(xmlWriter, environment.getFrictions(), errorMsg)) {
        return false;
    }
    if (!exportWeathers(xmlWriter, environment.getWeathers(), errorMsg)) {
        return false;
    }
    if (!exportTrafficRules(xmlWriter, environment.getTrafficRules(), errorMsg)) {
        return false;
    }
    xmlWriter.writeEndElement(); // Environment

    return true;
};

bool OPGUIEnvironmentExport::exportTimeOfDays(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const{
    if (!isValidTimeOfDaysJSON(timeOfDays, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("TimeOfDays");
    for (const auto &timeOfDay : timeOfDays) {
        xmlWriter.writeStartElement("TimeOfDay");
        xmlWriter.writeTextElement("Probability", QString::number(timeOfDay.getProbability()));
        xmlWriter.writeTextElement("Value", QString::number(timeOfDay.getValue()));
        xmlWriter.writeEndElement(); // timeOfDay
    }
    xmlWriter.writeEndElement(); // timeOfDays

    return true;
};

bool OPGUIEnvironmentExport::exportVisibilityDistances(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const{
    if (!isValidVisibilityDistancesJSON(visibilityDistances, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("VisibilityDistances");
    for (const auto &visibilityDistance : visibilityDistances) {
        xmlWriter.writeStartElement("VisibilityDistance");
        xmlWriter.writeTextElement("Probability", QString::number(visibilityDistance.getProbability()));
        xmlWriter.writeTextElement("Value", QString::number(visibilityDistance.getValue()));
        xmlWriter.writeEndElement(); // visibilityDistance
    }
    xmlWriter.writeEndElement(); // visibilityDistances

    return true;
};

bool OPGUIEnvironmentExport::exportFrictions(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const{
    if (!isValidFrictionsJSON(frictions, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Frictions");
    for (const auto &friction : frictions) {
        xmlWriter.writeStartElement("Friction");
        xmlWriter.writeTextElement("Probability", QString::number(friction.getProbability()));
        xmlWriter.writeTextElement("Value", QString::number(friction.getValue()));
        xmlWriter.writeEndElement(); // friction
    }
    xmlWriter.writeEndElement(); // frictions

    return true;
};

bool OPGUIEnvironmentExport::exportWeathers(QXmlStreamWriter &xmlWriter, const QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const{
    if (!isValidWeathersJSON(weathers, errorMsg)) {
        return false;
    }

    xmlWriter.writeStartElement("Weathers");
    for (const auto &weather : weathers) {
        xmlWriter.writeStartElement("Weather");
        xmlWriter.writeTextElement("Probability", QString::number(weather.getProbability()));
        xmlWriter.writeTextElement("Value",weather.getValue());
        xmlWriter.writeEndElement(); // weather
    }
    xmlWriter.writeEndElement(); // weathers

    return true;
};

bool OPGUIEnvironmentExport::exportTrafficRules(QXmlStreamWriter &xmlWriter, const QString &trafficRules, QString &errorMsg) const{
    if (!isValidTrafficRulesJSON(trafficRules, errorMsg)) {
        return false;
    }
    xmlWriter.writeTextElement("TrafficRules", trafficRules);
    return true;
};

//validation functions
bool OPGUIEnvironmentExport::isValidEnvironmentJSON(const OpenAPI::OAIEnvironment &environment, QString &errorMsg) const{
    if(!environment.isValid()){
        errorMsg = "Environment object is not valid.";
        return false;
    }
    return true;
};

bool OPGUIEnvironmentExport::isValidTimeOfDaysJSON(const QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const{
    double totalProbability = 0.0;
    if(timeOfDays.size()<=0){
        errorMsg = QString("Times of day array is empty.");
        return false;
    }
    for (int i = 0; i < timeOfDays.size(); ++i) {
        const auto &timeOfDay = timeOfDays.at(i);
        if (timeOfDay.getProbability() < 0 || timeOfDay.getProbability() > 1) {
            errorMsg = QString("Probability out of range for time of day at position %1.").arg(i);
            return false;
        }
        if (timeOfDay.getValue() < 1 || timeOfDay.getValue() > 24) {
            errorMsg = QString("Value out of range for time of day at position %1.").arg(i);
            return false;
        }
        totalProbability += timeOfDay.getProbability();
    }
    if (abs(totalProbability - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of all times of day does not equal 1 (total = %1).").arg(totalProbability);
        return false;
    }
    return true;
};

bool OPGUIEnvironmentExport::isValidVisibilityDistancesJSON(const QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const{
    double totalProbability = 0.0;
    if(visibilityDistances.size()<=0){
        errorMsg = QString("Visibility distances array is empty.");
        return false;
    }
    for (int i = 0; i < visibilityDistances.size(); ++i) {
        const auto &visibilityDistance = visibilityDistances.at(i);
        if (visibilityDistance.getProbability() < 0 || visibilityDistance.getProbability() > 1) {
            errorMsg = QString("Probability out of range for visibility distance at position %1.").arg(i);
            return false;
        }
        if (visibilityDistance.getValue() < 0) {
            errorMsg = QString("Value out of range for visibility distance at position %1.").arg(i);
            return false;
        }
        totalProbability += visibilityDistance.getProbability();
    }
    if (abs(totalProbability - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of all visibility distances does not equal 1 (total = %1).").arg(totalProbability);
        return false;
    }
    return true;
};

bool OPGUIEnvironmentExport::isValidFrictionsJSON(const QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const{
    double totalProbability = 0.0;
    if(frictions.size()<=0){
        errorMsg = QString("Frictions array is empty.");
        return false;
    }
    for (int i = 0; i < frictions.size(); ++i) {
        const auto &friction = frictions.at(i);
        if (friction.getProbability() < 0 || friction.getProbability() > 1) {
            errorMsg = QString("Probability out of range for friction at position %1.").arg(i);
            return false;
        }
        if (friction.getValue() < 0 || friction.getValue() > 1) {
            errorMsg = QString("Value out of range for friction at position %1.").arg(i);
            return false;
        }
        totalProbability += friction.getProbability();
    }
    if (abs(totalProbability - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of all frictions does not equal 1 (total = %1).").arg(totalProbability);
        return false;
    }
    return true;
};

bool OPGUIEnvironmentExport::isValidWeathersJSON(const QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const{
    double totalProbability = 0.0;
    if(weathers.size()<=0){
        errorMsg = QString("Weathers array is empty.");
        return false;
    }
    for (int i = 0; i < weathers.size(); ++i) {
        const auto &weather = weathers.at(i);
        if (weather.getProbability() < 0 || weather.getProbability() > 1) {
            errorMsg = QString("Probability out of range for weather at position %1.").arg(i);
            return false;
        }
        if (weather.getValue().isEmpty()) {
            errorMsg = QString("Empty weather value at position %1.").arg(i);
            return false;
        }
        if (!OPGUIValueList::getInstance().hasValue("weathers",weather.getValue())) {
            errorMsg = QString("Invalid weather value '%1' at position %2. Available options are: %3.").arg(weather.getValue(), 
                QString::number(i), OPGUIValueList::getInstance().listToString("weathers"));
            return false;
        }
        totalProbability += weather.getProbability();
    }
    if (abs(totalProbability - 1.0) > OPGUIConstants::EPSILON) {
        errorMsg = QString("The sum of probabilities of all weathers does not equal 1 (total = %1).").arg(totalProbability);
        return false;
    }
    return true;
};

bool OPGUIEnvironmentExport::isValidTrafficRulesJSON(const QString &trafficRules, QString &errorMsg) const{
    if (trafficRules.isEmpty()) {
        errorMsg = QString("Traffic rules value is empty");
        return false;
    }
    if (!OPGUIValueList::getInstance().hasValue("trafficRules",trafficRules)) {
        errorMsg = QString("Invalid Traffic Rules value %1. Available options are: %2.").arg(trafficRules, 
            OPGUIValueList::getInstance().listToString("trafficRules"));
        return false;
    }
    return true;
};

}