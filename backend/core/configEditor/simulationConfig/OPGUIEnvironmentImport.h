/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QList>
#include <QString>
#include <QDomElement>

#include "OAITimeOfDay.h"
#include "OAIEnvironment.h"
#include "OAIVisibilityDistance.h"
#include "OAIFriction.h"
#include "OAIWeather.h"

/**
 * Namespace OPGUIConfigEditor encapsulates functionalities related to the configuration editing
 * within the OPGUI application.
 */
namespace OPGUIConfigEditor {

/**
 * Class OPGUIEnvironmentImport is responsible for importing environment settings from an XML format
 * into the application. It handles parsing and validating the XML for environmental components
 * such as weather, time of day, visibility, friction values, and traffic rules.
 */
class OPGUIEnvironmentImport {
public:
    /**
     * Constructor for the OPGUIEnvironmentImport class.
     */
    OPGUIEnvironmentImport();

    /**
     * Imports environment settings from an XML element into the OpenAPI::OAIEnvironment structure.
     *
     * @param environmentEl The XML element containing the environment settings.
     * @param environment The structure where the parsed environment data will be stored.
     * @param errorMsg Output parameter that will hold the error message if an error occurs.
     * @return Returns true if the import is successful, false otherwise.
     */
    bool importEnvironment(const QDomElement &environmentEl, OpenAPI::OAIEnvironment &environment, QString &errorMsg) const;

private:
    // Import functions for different environmental components
    bool importTimeOfDays(const QDomElement &timeOfDaysEl, QList<OpenAPI::OAITimeOfDay> &timeOfDays, QString &errorMsg) const;
    bool importVisibilityDistances(const QDomElement &visibilityDistancesEl, QList<OpenAPI::OAIVisibilityDistance> &visibilityDistances, QString &errorMsg) const;
    bool importFrictions(const QDomElement &frictionsEl, QList<OpenAPI::OAIFriction> &frictions, QString &errorMsg) const;
    bool importWeathers(const QDomElement &weathersEl, QList<OpenAPI::OAIWeather> &weathers, QString &errorMsg) const;
    bool importTrafficRules(const QDomElement &trafficRulesEl, QString &trafficRules, QString &errorMsg) const;

    // Validation functions for the XML structure of environmental components
    bool isValidEnvironmentXml(const QDomElement &environmentEl, QString &errorMsg) const;
    bool isValidTimeOfDaysXml(const QDomElement &timeOfDaysEl, QString &errorMsg) const;
    bool isValidVisibilityDistancesXml(const QDomElement &visibilityDistancesEl, QString &errorMsg) const;
    bool isValidFrictionsXml(const QDomElement &frictionsEl, QString &errorMsg) const;
    bool isValidWeathersXml(const QDomElement &weathersEl, QString &errorMsg) const;
    bool isValidTrafficRulesXml(const QDomElement &trafficRulesEl, QString &errorMsg) const;
};

} // namespace OPGUIConfigEditor
