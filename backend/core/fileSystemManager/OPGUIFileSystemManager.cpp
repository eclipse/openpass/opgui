/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QFileInfoList>
#include <QFileInfo>
#include <QStack>

#include "OPGUIQtLogger.h"
#include "OPGUICoreGlobalConfig.h"
#include "OPGUIFileSystemManager.h"

namespace OPGUIFileSystemManager {

FileSystemManager::FileSystemManager() = default;

bool FileSystemManager::getFileTree(const OpenAPI::OAIFileTreeRequest &fileTreeRequest, OpenAPI::OAIFileTreeExtended &fileTreeExtended, QString &errorMsg) {
    QString basePathGenericStr = fileTreeRequest.getBasePathGeneric(); // Assuming getBasePathGeneric() returns a QString

    if (basePathGenericStr.isEmpty()) {
        errorMsg = "The base path requested is empty.";
        LOG_ERROR(errorMsg);
        return false;
    }

    QString dirPath = getRealBasePath(basePathGenericStr, errorMsg); 
    if (dirPath.isEmpty()) {
        // errorMsg is already set by getRealBasePath
        return false;
    }

    LOG_INFO("Loading file tree with generic base path " + dirPath);
    OpenAPI::OAITreeNode node;
    node = buildTree(dirPath, fileTreeRequest.isIsFolder(), fileTreeRequest.getExtension());

    fileTreeExtended.setFileSystemTree(node);
    fileTreeExtended.setBasePath(dirPath);

    return true;
}

QString FileSystemManager::getRealBasePath(const QString &basePathGenericStr, QString &errorMsg) const{
    QString basePath;

    if (basePathGenericStr == "workspace") {
        basePath = OPGUICoreGlobalConfig::getInstance().workspace();
    } else if (basePathGenericStr == "core") {
        basePath = OPGUICoreGlobalConfig::getInstance().pathOpenpassCore();
    } else if (basePathGenericStr == "home") {
        basePath = OPGUICoreGlobalConfig::getInstance().pathHome();
    } else {
        errorMsg = "The base path is not a valid value: " + basePathGenericStr;
        LOG_ERROR(errorMsg);
        return QString(); // Return an empty QString to indicate an error
    }

    return basePath;
}

bool FileSystemManager::getFileContents(const QString &filePath, const bool isB64Encoded, QString &contents, QString &errorMsg) const{
    QFile file(filePath);

    if (!file.exists()) {
        errorMsg = "File does not exist at path "+filePath;
        LOG_ERROR(errorMsg);
        return false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        errorMsg = "Failed to open file.";
        LOG_ERROR(errorMsg); 
        return false;
    } else {
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close(); 

        if (isB64Encoded) {
            QByteArray base64ByteArray = content.toUtf8().toBase64();
            contents = QString(base64ByteArray);
        } else {
            contents = content;
        }
    }

    return true;
}

OpenAPI::OAITreeNode FileSystemManager::buildTree(const QDir &directory, const bool isFolder, const QString &extension) {
    OpenAPI::OAITreeNode node;
    node.setName(directory.dirName());
    QList<OpenAPI::OAITreeNode> childrenList;

    QDir::Filters filters = QDir::NoDotAndDotDot | QDir::Dirs; // Always add directories
    if (!isFolder) {
        filters |= QDir::Files; // Add files if we're not just looking for folders
    }

    QFileInfoList entries = directory.entryInfoList(filters, QDir::DirsFirst | QDir::IgnoreCase);

    for (const QFileInfo &entry : entries) {
        OpenAPI::OAITreeNode childNode;
        childNode.setName(entry.fileName());

        if (entry.isDir()) {
            QDir subDir(entry.absoluteFilePath());
            // Build tree for subdirectory
            childNode = buildTree(subDir, isFolder, extension);
            // Add placeholder if the directory is empty
            if (childNode.getChildren().isEmpty()) {
                OpenAPI::OAITreeNode emptyChild;
                emptyChild.setName("");
                QList<OpenAPI::OAITreeNode> placeholderList;
                placeholderList.append(emptyChild);
                childNode.setChildren(placeholderList);
            }
        } else {
            // If it's a file and we're not just looking for folders, check the extension if provided
            QString fileExtension = entry.suffix();
            if (!isFolder && (extension.isEmpty() || fileExtension.compare(extension, Qt::CaseInsensitive) == 0)) {
                // This file should be included in the file tree
            } else {
                // Skip this file if the extension does not match
                continue;
            }
        }

        childrenList.append(childNode);
    }

    // If the directory is empty and isFolder is true, add a placeholder
    if (isFolder && childrenList.isEmpty()) {
        OpenAPI::OAITreeNode emptyChild;
        emptyChild.setName("");
        childrenList.append(emptyChild);
    }

    node.setChildren(childrenList);
    return node;
}

bool FileSystemManager::deleteFile(const QString &path ,QString &errorMsg) const{

    QFile file(path);

    if(!file.exists()){
        errorMsg = "File does not exist at path:" + path; 
        return false;
    }

    if (!file.remove()) {
        errorMsg = "Failed to delete file at path: " + path; 
        return false;
    }
    return true;
}

bool FileSystemManager::deleteInformation(const QString &path ,QString &errorMsg) const{
    if(!QDir(path).exists())
    {
        errorMsg="Invalid Path, Path does not exist";
        return false;
    }
            
    if(!QDir(path).removeRecursively() && QDir(path).mkpath(".") && QDir(path).entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty())
    {
        errorMsg="Error in deleting content. Check Permissions.";
        return false;
    }
    
    return true;
}

bool FileSystemManager::verifyPath(const QString& path, bool isFullPath, QString& realPath, bool& isEmpty, QString& errorMsg) const{
    isEmpty = true;

    if (isFullPath) {
        return verifyFullPath(path, realPath, isEmpty, errorMsg);
    } else {
        return verifyRelativePath(path, realPath, isEmpty, errorMsg);
    }
}

bool FileSystemManager::verifyFullPath(const QString& path, QString& realPath, bool& isEmpty, QString& errorMsg) const{
    QFileInfo fileInfo(path);
    realPath = fileInfo.absoluteFilePath();
    if (!fileInfo.exists()) {
        errorMsg = "The path was not found.";
        return false;
    }
    if (fileInfo.isDir()) {
        QDir dir(path);
        isEmpty = dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty();
    }
    return true;
}

bool FileSystemManager::processDirectory(const QDir& currentDir, const QString& path, QString& realPath, bool& isEmpty, int& count, QStack<QDir>& dirsToCheck, QString& errorMsg) const {
    QFileInfoList currentEntries = currentDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

    for (const QFileInfo& entry : currentEntries) {
        if (entry.fileName() == path) {
            if (++count > 1) {
                errorMsg = "The folder/file was found more than once in the workspace directory or subdirectories.";
                return false;
            }

            realPath = entry.absoluteFilePath();
            isEmpty = entry.isDir() && QDir(entry.absoluteFilePath()).entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty();
        }

        if (entry.isDir()) {
            dirsToCheck.push(QDir(entry.absoluteFilePath()));
        }
    }

    return true;
}

bool FileSystemManager::verifyRelativePath(const QString& path, QString& realPath, bool& isEmpty, QString& errorMsg) const {
    QDir dir(OPGUICoreGlobalConfig::getInstance().workspace());
    QStack<QDir> dirsToCheck;
    dirsToCheck.push(dir);
    int count = 0;

    while (!dirsToCheck.isEmpty()) {
        QDir currentDir = dirsToCheck.pop();
        if (!processDirectory(currentDir, path, realPath, isEmpty, count, dirsToCheck, errorMsg)) {
            return false;
        }
    }

    if (count == 0) {
        errorMsg = "The path was not found in the workspace.";
        return false;
    }
    return true;
}

}  // namespace OPGUIFileSystemManager




