/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDomDocument>
#include <QBuffer>

#include "OPGUISystemEditorExport.h"
#include "SystemEditorHelpers.h"
#include "OAITarget.h"
#include "OAISource.h"
#include "OPGUIQtLogger.h"
#include "OPGUICoreGlobalConfig.h"

namespace OPGUISystemEditor {

OPGUISystemEditorExport::OPGUISystemEditorExport() = default;

bool OPGUISystemEditorExport::saveSystemToXmlFile(const OpenAPI::OAISystemUI &system, const QString &path, const bool isNew, QString &errorMsg) const{

    QByteArray xmlData;
    QBuffer buffer(&xmlData);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWriter(&buffer);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    if (!exportSystemToXml(xmlWriter, system,errorMsg)) {
        errorMsg = QString("Failed exporting system to the file %1. %2 ").arg(path).arg(errorMsg);
        buffer.close();
        return false;
    }   

    xmlWriter.writeEndDocument();
    buffer.close();


    QFile file(path);
    bool existsFile = file.exists(); 

    if(isNew){ //save as
        if(existsFile){
            errorMsg = QString("There already exists a system file under this path: %1").arg(path);
            return false;
        }
    }
    else{   //save
        if(!existsFile){
            errorMsg = QString("No system file was found under the specified path: %1").arg(path);
            return false;
        }
    }
    
    if (!file.open(QIODevice::WriteOnly)) {
        errorMsg = QString("Failed to open/create file %1 for writing the System: %1").arg(path);
        return false; 
    }
    file.write(xmlData);
    file.close();

    return true;
}

bool OPGUISystemEditorExport::exportScheduleToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISchedule &schedule, bool isInsideSystem, QString &errorMsg) const{
    if(!isValidSchedule(schedule,isInsideSystem,errorMsg)){
        return false;
    }
    xmlWriter.writeStartElement("schedule");
    xmlWriter.writeTextElement("offset", QString::number(schedule.getOffset()));
    xmlWriter.writeTextElement("cycle", QString::number(schedule.getCycle()));
    xmlWriter.writeTextElement("response", QString::number(schedule.getResponse()));
    if(isInsideSystem){
        xmlWriter.writeTextElement("priority", QString::number(schedule.getPriority()));
    }
    xmlWriter.writeEndElement(); 
    return true;
}

bool OPGUISystemEditorExport::exportParameterToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIParameter &param, bool isInsideSystem, QString &errorMsg) const{
    if(!isValidParameter(param)){
        errorMsg = "Invalid Parameter object";
        return false;
    }
    
    xmlWriter.writeStartElement("parameter");

    if (isInsideSystem) {
        // Write title as a comment
        xmlWriter.writeComment(QString("parameter's title: %1").arg(param.getTitle()));
    } else {
        // Write title as a text element
        xmlWriter.writeTextElement("title", param.getTitle());
    }

    xmlWriter.writeTextElement("id", param.getId());
    xmlWriter.writeTextElement("type", param.getType());
    if(param.getUnit().isEmpty() && isInsideSystem)
        xmlWriter.writeEmptyElement("unit");
    else
        xmlWriter.writeTextElement("unit", param.getUnit());

    QString value = param.getValue();

    if (param.getType() == "normalDistribution" ||
        param.getType() == "logNormalDistribution" ||
        param.getType() == "exponentialDistribution" ||
        param.getType() == "uniformDistribution" ||
        param.getType() == "gammaDistribution") {
        // Parse the JSON string from the value field
        QJsonDocument valueDoc = QJsonDocument::fromJson(param.getValue().toUtf8());
        QJsonObject valueObj = valueDoc.object();

        // Construct XML for distribution parameters
        xmlWriter.writeStartElement("value");
        if (param.getType() == "normalDistribution") {
            xmlWriter.writeTextElement("mean", QString::number(valueObj["mean"].toDouble()));
            xmlWriter.writeTextElement("sd", QString::number(valueObj["sd"].toDouble()));
        } else if (param.getType() == "logNormalDistribution") {
            xmlWriter.writeTextElement("mean", QString::number(valueObj["mean"].toDouble()));
            xmlWriter.writeTextElement("sd", QString::number(valueObj["sd"].toDouble()));
            xmlWriter.writeTextElement("mu", QString::number(valueObj["mu"].toDouble()));
            xmlWriter.writeTextElement("sigma", QString::number(valueObj["sigma"].toDouble()));
        } else if (param.getType() == "exponentialDistribution") {
            xmlWriter.writeTextElement("mean", QString::number(valueObj["mean"].toDouble()));
            xmlWriter.writeTextElement("lambda", QString::number(valueObj["lambda"].toDouble()));
        } else if (param.getType() == "gammaDistribution") {
            xmlWriter.writeTextElement("mean", QString::number(valueObj["mean"].toDouble()));
            xmlWriter.writeTextElement("sd", QString::number(valueObj["sd"].toDouble()));
            xmlWriter.writeTextElement("shape", QString::number(valueObj["shape"].toDouble()));
            xmlWriter.writeTextElement("scale", QString::number(valueObj["scale"].toDouble()));
        }
        xmlWriter.writeTextElement("min", QString::number(valueObj["min"].toDouble()));
        xmlWriter.writeTextElement("max", QString::number(valueObj["max"].toDouble()));
        xmlWriter.writeEndElement(); //value
    } else {
        if (param.getType() == "intVector" || param.getType() == "boolVector" || param.getType() == "doubleVector" || param.getType() == "stringVector") {
            value.remove('[');
            value.remove(']');
        }
        xmlWriter.writeTextElement("value", value);
    }

    xmlWriter.writeEndElement(); // End of "parameter"

    return true;
}

bool OPGUISystemEditorExport::exportConnectionToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIConnection &connection, const QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const{
    if(!isValidConnection(connection,components,errorMsg)){
        return false;
    }
    xmlWriter.writeStartElement("connection");

    // Exporting OAIConnection fields
    xmlWriter.writeTextElement("id", QString::number(connection.getId()));

    const OpenAPI::OAIComponentUI* sourceComp = findComponentById(connection.getSource().getComponent(),components);

    const OpenAPI::OAIOutput* output = findOutputById(connection.getSource().getOutput(),sourceComp->getOutputs());

    // Export Source
    xmlWriter.writeStartElement("source");
    xmlWriter.writeComment(QString("Component Title: %1").arg(sourceComp->getTitle()));
    xmlWriter.writeTextElement("component", QString::number(connection.getSource().getComponent()));
    xmlWriter.writeComment(QString("Output Title: %1").arg(output->getTitle()));
    xmlWriter.writeTextElement("output", QString::number(connection.getSource().getOutput()));
    xmlWriter.writeEndElement(); // End of Source

    for (const auto& target : connection.getTargets()) {
        const OpenAPI::OAIComponentUI* targetComp = findComponentById(target.getComponent(), components);
        const OpenAPI::OAIInput* input = findInputById(target.getInput(), targetComp->getInputs());

        xmlWriter.writeStartElement("target");
        xmlWriter.writeComment(QString("Component Title: %1").arg(targetComp->getTitle()));
        xmlWriter.writeTextElement("component", QString::number(target.getComponent()));
        xmlWriter.writeComment(QString("Input Title: %1").arg(input->getTitle()));
        xmlWriter.writeTextElement("input", QString::number(target.getInput()));
        xmlWriter.writeEndElement(); // End of Target
    }

    xmlWriter.writeEndElement(); // End of Connection
    return true;
}

bool OPGUISystemEditorExport::exportInputToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIInput &input, QString &errorMsg) const{
    if(!isValidInput(input)){
        errorMsg="Invalid input object.";
        return false;
    }
    xmlWriter.writeStartElement("input");
    xmlWriter.writeTextElement("id", QString::number(input.getId()));
    xmlWriter.writeTextElement("type", input.getType());
    xmlWriter.writeTextElement("title", input.getTitle());
    xmlWriter.writeTextElement("unit", input.getUnit());
    xmlWriter.writeTextElement("cardinality", QString::number(input.getCardinality()));
    xmlWriter.writeEndElement(); 
    return true;
}

bool OPGUISystemEditorExport::exportOutputToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIOutput &output, QString &errorMsg) const{
    if(!isValidOutput(output)){
        errorMsg="Invalid outpùt object.";
        return false;
    }
    xmlWriter.writeStartElement("output");
    xmlWriter.writeTextElement("id", QString::number(output.getId()));
    xmlWriter.writeTextElement("type", output.getType());
    xmlWriter.writeTextElement("title", output.getTitle());
    xmlWriter.writeTextElement("unit", output.getUnit());
    xmlWriter.writeEndElement(); 
    return true;
}

bool OPGUISystemEditorExport::exportPositionToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIPosition &position, QString &errorMsg) const{
    if(!isValidPosition(position)){
        errorMsg="Invalid position object.";
        return false;
    }
    xmlWriter.writeStartElement("position");
    xmlWriter.writeTextElement("x", QString::number(position.getX()));
    xmlWriter.writeTextElement("y", QString::number(position.getY()));
    xmlWriter.writeEndElement(); 
    return true;
}

bool OPGUISystemEditorExport::exportComponentToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIComponentUI &component, bool isInsideSystem, QString &errorMsg) const{
    if (!isValidComponent(component,isInsideSystem,errorMsg)){
        return false;
    }

    xmlWriter.writeStartElement("component");

    // Exporting OAIComponentUI fields
    if (isInsideSystem) {
        xmlWriter.writeTextElement("id", QString::number(component.getId()));
    }
    else{
        xmlWriter.writeTextElement("type", component.getType());
    }

    xmlWriter.writeTextElement("library", component.getLibrary());
    xmlWriter.writeTextElement("title", component.getTitle());

    LOG_DEBUG(QString("Exporting component with id %1 and title %2").arg(component.getId()).arg(component.getTitle())); 
    if (!exportScheduleToXml(xmlWriter, component.getSchedule(),isInsideSystem,errorMsg)){
        return false;
    }
    
    xmlWriter.writeStartElement("parameters");
    QList<OpenAPI::OAIParameter> parameters = component.getParameters();
    for (int i = 0; i < parameters.size(); ++i) {
        OpenAPI::OAIParameter param = parameters.at(i);
        if (!exportParameterToXml(xmlWriter, param, isInsideSystem,errorMsg)) {
            errorMsg = QString("Failed to export parameter at position %1th. %2").arg(i).arg(errorMsg);
            return false;
        }
    }
    xmlWriter.writeEndElement(); 

    if (!isInsideSystem) {
        xmlWriter.writeStartElement("inputs");
        QList<OpenAPI::OAIInput> inputs = component.getInputs(); 
        for (int i = 0; i < inputs.size(); ++i) {
            OpenAPI::OAIInput input = inputs.at(i); 
            if (!exportInputToXml(xmlWriter, input,errorMsg)) {
                errorMsg = QString("Failed to export input at position %1th. %2").arg(i).arg(errorMsg);
                return false;
            }
        }
        xmlWriter.writeEndElement(); 

        xmlWriter.writeStartElement("outputs");
        QList<OpenAPI::OAIOutput> outputs = component.getOutputs(); 
        for (int i = 0; i < outputs.size(); ++i) {
            OpenAPI::OAIOutput output = outputs.at(i);  
            if (!exportOutputToXml(xmlWriter, output, errorMsg)) {
                errorMsg = QString("Failed to export output at position %1th. %2").arg(i).arg(errorMsg);
                return false;
            }
        }
        xmlWriter.writeEndElement(); 
    }
    else{
        OpenAPI::OAIPosition position = component.getPosition(); 
        if (!exportPositionToXml(xmlWriter,position,errorMsg)) {
            return false;
        }
    }

    xmlWriter.writeEndElement(); 
    return true;
}

bool OPGUISystemEditorExport::exportCommentToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAIComment &comment, QString &errorMsg) const{
    if (!isValidComment(comment,errorMsg)) {
        return false;
    }
    xmlWriter.writeStartElement("comment");

    xmlWriter.writeTextElement("message", comment.getMessage());
    xmlWriter.writeTextElement("title", comment.getTitle());

    xmlWriter.writeStartElement("position");
    xmlWriter.writeTextElement("x", QString::number(comment.getPosition().getX()));
    xmlWriter.writeTextElement("y", QString::number(comment.getPosition().getY()));
    xmlWriter.writeEndElement(); // End of position

    xmlWriter.writeEndElement(); // End of comment
    return true;
}

bool OPGUISystemEditorExport::exportSystemToXml(QXmlStreamWriter &xmlWriter, const OpenAPI::OAISystemUI &system, QString &errorMsg) const{
    if (!isValidSystem(system,errorMsg)){
        return false;
    }
    xmlWriter.writeStartElement("systems");
    xmlWriter.writeStartElement("system");

    // Exporting OAISystem fields
    xmlWriter.writeTextElement("id", QString::number(system.getId()));
    xmlWriter.writeTextElement("title", system.getTitle());
    xmlWriter.writeTextElement("priority", QString::number(system.getPriority()));

    LOG_DEBUG(QString("Exporting system with id %1 and title %2").arg(system.getId()).arg(system.getTitle()));

    // Export Components
    xmlWriter.writeStartElement("components");
    QList<OpenAPI::OAIComponentUI> components = system.getComponents();
    if(components.empty()){
        errorMsg = "Invalid System object, 0 components found";
        return false;
    }
    for (int i = 0; i < components.size(); ++i) {
        OpenAPI::OAIComponentUI component = components.at(i);
        if (!exportComponentToXml(xmlWriter, component, true, errorMsg)) {
            errorMsg = QString("Failed to export component at position %1th of the system. %2").arg(i).arg(errorMsg);
            return false;
        }
    }
    xmlWriter.writeEndElement(); // End of Components

    // Export Connections
    xmlWriter.writeStartElement("connections");
    QList<OpenAPI::OAIConnection> connections = system.getConnections();
    for (int i = 0; i < connections.size(); ++i) {
        OpenAPI::OAIConnection connection = connections.at(i);
        if (!exportConnectionToXml(xmlWriter, connection, components, errorMsg)) {
            errorMsg = QString("Failed to export connection at position %1th. %2").arg(i).arg(errorMsg);
            return false;
        }
    }
    xmlWriter.writeEndElement(); // End of Connections

    // Export Comments
    xmlWriter.writeStartElement("comments");
    QList<OpenAPI::OAIComment> comments = system.getComments();
    for (int i = 0; i < comments.size(); ++i) {
        OpenAPI::OAIComment comment = comments.at(i);
        if (!exportCommentToXml(xmlWriter, comment,errorMsg)) {
            errorMsg = QString("Failed to export comment at position %1: ").arg(i + 1) + errorMsg;
            return false;
        }
    }
    xmlWriter.writeEndElement(); // End of Comments

    xmlWriter.writeEndElement(); // End of System
    xmlWriter.writeEndElement(); // End of Systems

    return true;
}

bool OPGUISystemEditorExport::isValidSystem(const OpenAPI::OAISystemUI &system, QString &errorMsg) const{
        if( !system.is_id_Valid() || !system.is_title_Valid() || !system.is_priority_Valid()){
            errorMsg = "Missing one or more required fields id, title, priority in system";
            return false;
        }
        return true;
};

bool OPGUISystemEditorExport::isValidComponent(const OpenAPI::OAIComponentUI &component, bool isInsideSystem, QString &errorMsg) const{
    if(!component.is_library_Valid() || !component.is_title_Valid() ){
        errorMsg = "Missing one or more required fields library or title in component";
        return false;
    }

    if(isInsideSystem){
        if( !component.is_id_Valid()){
            errorMsg = "Missing field id in component";
            return false;
        }
    }
    else{
        if( !component.is_type_Valid()){
            errorMsg = "Missing type field in component";
            return false;
        }  
    }

    if (!existComponentLibraryDll(OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder(),component.getLibrary(),errorMsg)){
        return false;
    }

    return true;
};

bool OPGUISystemEditorExport::isValidSchedule(const OpenAPI::OAISchedule &schedule, bool isInsideSystem, QString &errorMsg) const{
    if(isInsideSystem ){
        if(!schedule.isValid()){
            errorMsg = "Schedule object is not valid.";
            return false;
        }
    }
    else{
        if(!schedule.is_offset_Valid() || !schedule.is_cycle_Valid() || !schedule.is_response_Valid()){
            errorMsg = "Missing one or more required offset, cycle or response in schedule.";
            return false;
        }
    }
    return true;
};

bool OPGUISystemEditorExport::isValidParameter(const OpenAPI::OAIParameter &param) const{
    return param.isValid();
};

bool OPGUISystemEditorExport::isValidInput(const OpenAPI::OAIInput &input) const{
    return input.isValid();
};

bool OPGUISystemEditorExport::isValidOutput(const OpenAPI::OAIOutput &output) const{
    return output.isValid();
};

bool OPGUISystemEditorExport::isValidPosition(const OpenAPI::OAIPosition &position) const{
    return position.isValid();
};

bool OPGUISystemEditorExport::isValidComment(const OpenAPI::OAIComment &comment, QString &errorMsg) const{
    if(!comment.isValid())
        errorMsg = "Connection object schema is not valid.";
    return comment.isValid();
};

bool OPGUISystemEditorExport::isValidConnection(const OpenAPI::OAIConnection &connection, const QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const{
    if (!connection.isValid()) {
        errorMsg = "Connection object schema is not valid.";
        return false;
    }

    const OpenAPI::OAIComponentUI* sourceComp = findComponentById(connection.getSource().getComponent(), components);
    if (!sourceComp) {
        errorMsg = QString("Source component with ID %1 not found.").arg(connection.getSource().getComponent());
        return false;
    }

    if (const OpenAPI::OAIOutput* sourceOutput = findOutputById(connection.getSource().getOutput(), sourceComp->getOutputs()); !sourceOutput) {
        errorMsg = QString("Source output with ID %1 not found in component ID %2.").arg(connection.getSource().getOutput()).arg(connection.getSource().getComponent());
        return false;
    } else if (sourceOutput->getTitle().isEmpty()) {
        errorMsg = QString("Source output with ID %1 in component ID %2 has an empty title.").arg(connection.getSource().getOutput()).arg(connection.getSource().getComponent());
        return false;
    }

    for (const auto& target : connection.getTargets()) {
        const OpenAPI::OAIComponentUI* eachTargetComp = findComponentById(target.getComponent(), components);
        if (!eachTargetComp) {
            errorMsg = QString("Target component with ID %1 not found.").arg(target.getComponent());
            return false;
        }

        const OpenAPI::OAIInput* eachTargetInput = findInputById(target.getInput(), eachTargetComp->getInputs());
        if (!eachTargetInput) {
            errorMsg = QString("Target input with ID %1 not found in component ID %2.").arg(target.getInput()).arg(target.getComponent());
            return false;
        } else if (eachTargetInput->getTitle().isEmpty()) {
            errorMsg = QString("Target input with ID %1 in component ID %2 has an empty title.").arg(target.getInput()).arg(target.getComponent());
            return false;
        }
    }

    return true;
}
}  // namespace OPGUISystemEditor




