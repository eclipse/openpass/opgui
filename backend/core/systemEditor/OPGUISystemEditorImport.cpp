/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDomDocument>

#include "OPGUISystemEditorImport.h"
#include "SystemEditorHelpers.h"
#include "OAITarget.h"
#include "OAISource.h"
#include "OPGUIQtLogger.h"
#include "OPGUICoreGlobalConfig.h"


namespace OPGUISystemEditor {

OPGUISystemEditorImport::OPGUISystemEditorImport() = default;

bool OPGUISystemEditorImport::getSystemFromXml(const QString &fileName, OpenAPI::OAISystemUI &system, QString &errorMsg) const{
    LOG_DEBUG("Parsing " + fileName + " system XML file");
    if (QString xmlContent = readXmlFile(fileName); xmlContent.isEmpty()) {
        errorMsg = QString("System XML file is empty: %1").arg(fileName);
        return false;
    } else {
        QDomDocument doc;
        if (!doc.setContent(xmlContent)) {
            errorMsg = QString("Invalid XML content in system XML file: %1").arg(fileName);
            return false;
        }

        QDomElement systemsElement = doc.documentElement();
        
        if(systemsElement.tagName() != "systems") {
            errorMsg = QString("The root element is not <systems> as expected in file: %1").arg(fileName);
            return false;
        }

        QDomElement systemElement = systemsElement.firstChildElement("system");
        
        if(systemElement.isNull()) {
            errorMsg = QString("No <system> element found within <systems> in file: %1").arg(fileName);
            return false;
        }

        if (!parseXmlSystem(systemElement, system, errorMsg)) { 
            errorMsg = QString("Failed to parse XML system from file: %1. Error: %2").arg(fileName).arg(errorMsg);
            return false;
        }

        system.setFile(fileName);
    }
    return true;
}

bool OPGUISystemEditorImport::loadComponentsFromDirectory(const QString &dirPath, QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const{
    LOG_INFO("Loading components from directory "+dirPath);
    QDir dir(dirPath);
    if (!dir.exists()) {
        errorMsg = QString("Directory does not exist.");
        return false;
    }

    QStringList xmlFiles = dir.entryList(QStringList() << "*.xml", QDir::Files);
    LOG_DEBUG(QString("Found %1 component files in components directory").arg(xmlFiles.size()));
    if(xmlFiles.empty()){
        errorMsg = QString("No components files found in directory.");
        return false;
    }
    foreach (const QString &fileName, xmlFiles) {
        LOG_DEBUG("Parsing "+fileName+" component XML file");
        QString xmlContent = readXmlFile(dir.absoluteFilePath(fileName));
        if (xmlContent.isEmpty()) {
            errorMsg = QString("Component XML file is empty: %1").arg(fileName);
            return false;
        } else {
            QDomDocument doc;
            if (!doc.setContent(xmlContent)) {
                errorMsg = QString("Invalid XML content in XML component file: %1").arg(fileName);
                return false;
            }

            OpenAPI::OAIComponentUI component;
            QDomElement rootElement = doc.documentElement();
            if (auto emptyList = QList<OpenAPI::OAIComponentUI>(); !parseXmlComponent(rootElement, component, false,emptyList,errorMsg)) {
                errorMsg = QString("Failed to parse XML component from file: %1. Error: %2").arg(fileName).arg(errorMsg);
                return false;
            }
            components.append(component);
        }
    }
    return true;
}

QString OPGUISystemEditorImport::readXmlFile(const QString &filePath) const{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return QString();  
    }
    QTextStream in(&file);
    QString xmlContent = in.readAll();
    file.close();
    return xmlContent;
}

bool OPGUISystemEditorImport::parseXmlSystem(const QDomElement &systemEl, OpenAPI::OAISystemUI &system, QString &errorMsg) const{
    QList<OpenAPI::OAIComponentUI> originalComponents;

    if (!loadComponentsFromDirectory(OPGUICoreGlobalConfig::getInstance().fullPathComponentsFolder(), originalComponents, errorMsg)) {
        return false;
    }

    if (!isValidSystemXml(systemEl,errorMsg)) {
        return false;
    }

    QString systemId = systemEl.firstChildElement("id").text();
    QString systemTitle = systemEl.firstChildElement("title").text();
    QString systemPriority = systemEl.firstChildElement("priority").text(); 
    

    LOG_DEBUG(QString("Parsing system with id %1 and title: %2").arg(systemId).arg(systemTitle)); 

    system.setId(systemId.toInt());  
    system.setTitle(systemTitle);
    system.setPriority(systemPriority.toInt());

    QDomElement componentsNode = systemEl.firstChildElement("components");

    QList<OpenAPI::OAIComponentUI> componentList; 

    QDomNodeList componentsList = componentsNode.elementsByTagName("component");
    if(componentsList.count()==0){
        errorMsg="System has no components";
        return false;
    }
    for (int i = 0; i < componentsList.count(); ++i) {
        QDomElement componentElem = componentsList.at(i).toElement();
        OpenAPI::OAIComponentUI component;
        if (!parseXmlComponent(componentElem, component, true, originalComponents,errorMsg)) { 
            errorMsg = QString("Failed to parse component at %1th position of the components array. %2").arg(i).arg(errorMsg);
            return false;
        }

        componentList.append(component);
    }

    QList<OpenAPI::OAIConnection> connectionList;
    QDomElement connectionsNode = systemEl.firstChildElement("connections");
    QDomNodeList connectionElems = connectionsNode.elementsByTagName("connection");
    for (int i = 0; i < connectionElems.count(); ++i) {
        QDomElement connectionElem = connectionElems.at(i).toElement();
        OpenAPI::OAIConnection connection;
        if (!parseXmlConnection(connectionElem, connection,errorMsg)) { 
            errorMsg = QString("Failed to parse connection at %1th position of the connections array. %2").arg(i).arg(errorMsg);
            return false;
        }

        connectionList.append(connection);
    }

    QList<OpenAPI::OAIComment> commentList; 

    QDomElement commentsNode = systemEl.firstChildElement("comments");
    QDomNodeList commentElems = commentsNode.elementsByTagName("comment");
    for (int i = 0; i < commentElems.count(); ++i) {
        QDomElement commentElem = commentElems.at(i).toElement();
        OpenAPI::OAIComment comment; 

        if (!parseXmlComment(commentElem, comment, errorMsg)) { 
            errorMsg = QString("Failed to parse comment at %1th position: %2. ").arg(i).arg(errorMsg);
            return false;
        }

        commentList.append(comment);
    }

    system.setComponents(componentList);
    system.setConnections(connectionList);
    system.setComments(commentList);


    return true;
}

bool OPGUISystemEditorImport::parseXmlComponent(const QDomElement &componentEl, OpenAPI::OAIComponentUI &component, bool isInsideSystem, const QList<OpenAPI::OAIComponentUI> &originalComponents, QString &errorMsg) const{
    if (!isValidComponentXml(componentEl, isInsideSystem,errorMsg)) { 
        return false; 
    }

    if (isInsideSystem) {
        QDomElement idElement = componentEl.firstChildElement("id");
        component.setId(idElement.text().toInt());
    } else {
        component.setId(0); 
    }

    QString title = componentEl.firstChildElement("title").text();
    component.setTitle(title);

    LOG_DEBUG(QString("Parsing component with id %1 and title %2").arg(component.getId()).arg(component.getTitle())); 
    
    QString library = componentEl.firstChildElement("library").text();
    component.setLibrary(library);

    QList<OpenAPI::OAIParameter> parameters;
    if(!parseXmlParameters(componentEl.firstChildElement("parameters"), parameters,isInsideSystem,errorMsg)) {
        return false;
    }
    component.setParameters(parameters);

    QList<OpenAPI::OAIInput> inputs;
    QList<OpenAPI::OAIOutput> outputs;
    QString type;
    if (isInsideSystem){
        const OpenAPI::OAIComponentUI* originalComponentPtr = findComponentByTitle(component.getTitle(), originalComponents);
        
        if (originalComponentPtr == nullptr) {
            errorMsg=QString("Could not find the .xml file of the component with title %1 included in the system").arg(component.getTitle());
            return false; 
        }

        inputs = originalComponentPtr->getInputs();
        outputs = originalComponentPtr->getOutputs();
        type = originalComponentPtr->getType();
    }
    else{
        if(!parseXmlInputs(componentEl.firstChildElement("inputs"), inputs,errorMsg)) {
            return false;
        }
        if(!parseXmlOutputs(componentEl.firstChildElement("outputs"), outputs,errorMsg)) {
            return false;
        }
        type = componentEl.firstChildElement("type").text();
    }

    component.setType(type);
    component.setInputs(inputs);
    component.setOutputs(outputs);

    OpenAPI::OAISchedule schedule;
    if(!parseXmlSchedule(componentEl.firstChildElement("schedule"), schedule, isInsideSystem,errorMsg)) {
        return false;
    }
    component.setSchedule(schedule);
    
    OpenAPI::OAIPosition position;
    if (isInsideSystem){
            if(!parseXmlPosition(componentEl.firstChildElement("position"), position,errorMsg)){
                return false;
            }
    }
    else{
        position.setX(0);
        position.setY(0);
    }
    component.setPosition(position);

    return true;  
}

bool OPGUISystemEditorImport::parseXmlParameters(const QDomElement &parametersEl, QList<OpenAPI::OAIParameter> &parameters,bool isInsideSystem,QString &errorMsg) const{
    if (parametersEl.isNull()) {
        errorMsg="Parameters element not found in XML";
        return false;
    }

    QDomNodeList parametersList = parametersEl.elementsByTagName("parameter");
    for (int i = 0; i < parametersList.count(); i++) {
        QDomElement paramElement = parametersList.at(i).toElement();

        if (!isValidParameterXml(paramElement, isInsideSystem,errorMsg)) {
            errorMsg=QString("Invalid parameter Xml element in %1th position of parameters array: %2").arg(i).arg(errorMsg);
            return false; 
        }

        OpenAPI::OAIParameter param;

        param.setId(paramElement.firstChildElement("id").text());
        QString type = paramElement.firstChildElement("type").text();
        param.setType(type);
        param.setUnit(paramElement.firstChildElement("unit").text());
        
        if (type == "bool" || type == "boolVector" || type == "intVector" || type == "doubleVector" || type == "stringVector") {
            normalizeParameterValues(paramElement, param);
        } else if (type == "normalDistribution" || type == "logNormalDistribution" ||
                   type == "exponentialDistribution" || type == "uniformDistribution" ||
                   type == "gammaDistribution") {
            handleDistributionTypeLogic(paramElement, param);
        } else {
            param.setValue(paramElement.firstChildElement("value").text());
        }

        if (isInsideSystem) {
            extractTitleFromComments(paramElement, param);
        } else {
            param.setTitle(paramElement.firstChildElement("title").text());
        }

        parameters.append(param);
    }

    return true;
}

bool OPGUISystemEditorImport::parseXmlInputs(const QDomElement &inputsEl, QList<OpenAPI::OAIInput> &inputs,QString &errorMsg) const{
    QDomNodeList inputList = inputsEl.elementsByTagName("input");
    for (int i = 0; i < inputList.count(); i++) {
        QDomElement inputElem = inputList.at(i).toElement();

        if (!isValidInputXml(inputElem, errorMsg)) {
            errorMsg = QString("Invalid input Xml element at %1th position of the inputs array: %2").arg(i).arg(errorMsg);
            return false;
        }

        OpenAPI::OAIInput input;
        input.setId(inputElem.firstChildElement("id").text().toInt());
        input.setType(inputElem.firstChildElement("type").text());
        input.setTitle(inputElem.firstChildElement("title").text());
        input.setUnit(inputElem.firstChildElement("unit").text());
        input.setCardinality(inputElem.firstChildElement("cardinality").text().toInt());

        inputs.append(input);
    }
    return true;  // Return true if successful
}

bool OPGUISystemEditorImport::parseXmlOutputs(const QDomElement &outputsEl, QList<OpenAPI::OAIOutput> &outputs,QString &errorMsg) const{
    QDomNodeList outputList = outputsEl.elementsByTagName("output");
    for (int i = 0; i < outputList.count(); i++) {
        QDomElement outputElem = outputList.at(i).toElement();

        if (!isValidOutputXml(outputElem,errorMsg)) {
            errorMsg=QString("Invalid output Xml element at %1th position of the outputs array: %2").arg(i).arg(errorMsg);
            return false;
        }

        OpenAPI::OAIOutput output;
        output.setId(outputElem.firstChildElement("id").text().toInt());
        output.setType(outputElem.firstChildElement("type").text());
        output.setTitle(outputElem.firstChildElement("title").text());
        output.setUnit(outputElem.firstChildElement("unit").text());

        outputs.append(output);
    }
    return true;
}

bool OPGUISystemEditorImport::parseXmlSchedule(const QDomElement &scheduleEl, OpenAPI::OAISchedule &schedule, bool isInsideSystem,QString &errorMsg) const{
    if (!isValidScheduleXml(scheduleEl, isInsideSystem,errorMsg)) {
        return false;
    }

    if (isInsideSystem) {
        schedule.setPriority(scheduleEl.firstChildElement("priority").text().toInt());
    }
    else{
        schedule.setPriority(0);
    }
    
    schedule.setOffset(scheduleEl.firstChildElement("offset").text().toInt());
    schedule.setCycle(scheduleEl.firstChildElement("cycle").text().toInt());
    schedule.setResponse(scheduleEl.firstChildElement("response").text().toInt());

    return true;
}

bool OPGUISystemEditorImport::parseXmlPosition(const QDomElement &positionEl, OpenAPI::OAIPosition &position,QString &errorMsg) const{
    if (!isValidPositionXml(positionEl,errorMsg)) {
        return false;
    }

    position.setX(positionEl.firstChildElement("x").text().toInt());
    position.setY(positionEl.firstChildElement("y").text().toInt());
    return true;  
}

bool OPGUISystemEditorImport::parseXmlConnection(const QDomElement &connectionEl, OpenAPI::OAIConnection &connection,QString &errorMsg) const{
    if (!isValidConnectionXml(connectionEl,errorMsg)) {
        return false;
    }

    connection.setId(connectionEl.firstChildElement("id").text().toInt());

    QDomElement sourceEl = connectionEl.firstChildElement("source");
    OpenAPI::OAISource source;
    source.setComponent(sourceEl.firstChildElement("component").text().toInt());
    source.setOutput(sourceEl.firstChildElement("output").text().toInt());
    connection.setSource(source);

    QList<OpenAPI::OAITarget> targets; 
    QDomElement targetEl = connectionEl.firstChildElement("target");
    while (!targetEl.isNull()) {
        OpenAPI::OAITarget target;
        target.setComponent(targetEl.firstChildElement("component").text().toInt());
        target.setInput(targetEl.firstChildElement("input").text().toInt());

        targets.append(target);

        targetEl = targetEl.nextSiblingElement("target");
    }

    connection.setTargets(targets);

    return true;
}

bool OPGUISystemEditorImport::parseXmlComment(const QDomElement &commentEl, OpenAPI::OAIComment &comment, QString &errorMsg) const{
    if (!isValidCommentXml(commentEl,errorMsg)) {
        return false;
    }

    QString message = commentEl.firstChildElement("message").text();
    QString title = commentEl.firstChildElement("title").text();
    QDomElement positionEl = commentEl.firstChildElement("position");

    OpenAPI::OAIPosition position;
    if (!parseXmlPosition(positionEl, position, errorMsg)) {
        return false;
    }

    comment.setMessage(message);
    comment.setTitle(title);

    comment.setPosition(position);

    return true;
}

void OPGUISystemEditorImport::normalizeParameterValues(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const {
    QString type = paramElement.firstChildElement("type").text();
    QString value = paramElement.firstChildElement("value").text();

    if (type == "bool") {
        value = (value.toLower() == "true" || value == "1") ? "true" : "false";
    } else if (type == "boolVector") {
        QStringList boolList = value.split(',', Qt::SkipEmptyParts);
        QStringList normalizedBoolList;
        for (const QString &boolStr : boolList) {
            normalizedBoolList << ((boolStr.trimmed().toLower() == "true" || boolStr == "1") ? "true" : "false");
        }
        value = normalizedBoolList.join(",");
        value = "[" + value + "]";
    } else if (type == "intVector" || type == "doubleVector" || type == "stringVector") {
        value = "[" + value + "]";
    }

    param.setType(type);
    param.setValue(value);
}

void OPGUISystemEditorImport::handleDistributionTypeLogic(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const {
    QString type = paramElement.firstChildElement("type").text();
    QJsonObject valueObj;

    QDomElement valueEl = paramElement.firstChildElement("value");

    QDomElement meanEl = valueEl.firstChildElement("mean");
    QDomElement sdEl = valueEl.firstChildElement("sd");

    bool hasDistributionParams = false;
    if (!meanEl.isNull()) {
        valueObj["mean"] = meanEl.text().toDouble();
        hasDistributionParams = true;
    }

    if (!sdEl.isNull()) {
        valueObj["sd"] = sdEl.text().toDouble();
        hasDistributionParams = true;
    }

    if (type == "logNormalDistribution") {
        QDomElement muEl = valueEl.firstChildElement("mu");
        QDomElement sigmaEl = valueEl.firstChildElement("sigma");
        if (!muEl.isNull()) {
            valueObj["mu"] = muEl.text().toDouble();
            hasDistributionParams = true;
        }
        if (!sigmaEl.isNull()) {
            valueObj["sigma"] = sigmaEl.text().toDouble();
            hasDistributionParams = true;
        }
    } else if (type == "exponentialDistribution") {
        QDomElement lambdaEl = valueEl.firstChildElement("lambda");
        if (!lambdaEl.isNull()) {
            valueObj["lambda"] = lambdaEl.text().toDouble();
            hasDistributionParams = true;
        }
    } else if (type == "gammaDistribution") {
        QDomElement shapeEl = valueEl.firstChildElement("shape");
        QDomElement scaleEl = valueEl.firstChildElement("scale");
        if (!shapeEl.isNull()) {
            valueObj["shape"] = shapeEl.text().toDouble();
            hasDistributionParams = true;
        }
        if (!scaleEl.isNull()) {
            valueObj["scale"] = scaleEl.text().toDouble();
            hasDistributionParams = true;
        }
    }

    QDomElement minEl = valueEl.firstChildElement("min");
    QDomElement maxEl = valueEl.firstChildElement("max");
    if (!minEl.isNull()) {
        valueObj["min"] = minEl.text().toDouble();
        hasDistributionParams = true;
    }

    if (!maxEl.isNull()) {
        valueObj["max"] = maxEl.text().toDouble();
        hasDistributionParams = true;
    }

    if (hasDistributionParams) {
        QString value = QJsonDocument(valueObj).toJson(QJsonDocument::Compact);
        param.setType(type);
        param.setValue(value);
    } else {
        param.setType(type);
        param.setValue("");  // Ensure the value is not empty but indicates no params were found
    }
}

bool OPGUISystemEditorImport::isValidSystemXml(const QDomElement &system,QString &errorMsg) const{
    // Check for all required elements for a system
    if (system.firstChildElement("id").isNull() || system.firstChildElement("id").text().isEmpty() || 
        system.firstChildElement("title").isNull() || system.firstChildElement("title").text().isEmpty() || 
        system.firstChildElement("priority").isNull() || system.firstChildElement("priority").text().isEmpty() || 
        system.firstChildElement("components").isNull() ||
        system.firstChildElement("connections").isNull()) {
        errorMsg="System element has missing/empty one or more required elements: id, title, priority, components, connections";
        return false;
    }
    return true;
}

bool OPGUISystemEditorImport::isValidComponentXml(const QDomElement &component, bool isInsideSystem,QString &errorMsg) const{
    // Check common required elements
    if (component.firstChildElement("title").isNull() || component.firstChildElement("title").text().isEmpty() ||
        component.firstChildElement("library").isNull() || component.firstChildElement("library").text().isEmpty() ||
        component.firstChildElement("parameters").isNull() ||
        component.firstChildElement("schedule").isNull() ) {
        errorMsg="Title, parameters, library or schedule for component are not present or empty";
        return false;
    }

    if (isInsideSystem) {
        if (component.firstChildElement("id").isNull() || component.firstChildElement("id").text().isEmpty() ||
            component.firstChildElement("position").isNull()) {
            errorMsg="Id and position should be present for a component inside a system";
            return false;
        }
    } else {
        if (component.firstChildElement("type").isNull() || component.firstChildElement("type").text().isEmpty() ||
            component.firstChildElement("inputs").isNull() || 
            component.firstChildElement("outputs").isNull()) {
            errorMsg="Inputs, outputs and type should be present for a standalone component";
            return false;
        }
    }

    if (!existComponentLibraryDll(OPGUICoreGlobalConfig::getInstance().fullPathModulesFolder(),component.firstChildElement("library").text(),errorMsg)){
        return false;
    }

    return true;
}

bool OPGUISystemEditorImport::isValidParameterXml(const QDomElement &paramElement, bool isInsideSystem,QString &errorMsg) const{
    if (paramElement.isNull()) {
        errorMsg="Parameter element is null";
        return false;
    }

    if (isInsideSystem) {
        bool foundComment = false;
        QDomNodeList childNodes = paramElement.childNodes();
        for (int i = 0; i < childNodes.size(); ++i) {
            QDomNode childNode = childNodes.at(i);
            if (childNode.isComment() && childNode.nodeValue().contains("parameter's title:")) {
                foundComment = true;
                break;
            }
        }
        
        if (!foundComment) {
            errorMsg="Missing required commented field 'parameter's title' in parameter";
            return false;
        }
    } else {
        if (paramElement.firstChildElement("title").isNull()) {
            errorMsg="Missing required field title in parameter";
            return false;
        }
    }

    if (paramElement.firstChildElement("id").isNull() ||
        paramElement.firstChildElement("type").isNull() ||
        paramElement.firstChildElement("unit").isNull()) {
        errorMsg="Missing required fields id, type or unit in parameter";
        return false;
    }

    if (QString idText = paramElement.firstChildElement("id").text().trimmed(); idText.isEmpty()) {
        errorMsg="ID is empty";
        return false;
    }

    // Validate the parameter's value based on its type
    if (!validateParameterValue(paramElement.firstChildElement("type").text(), paramElement.firstChildElement("value").text(), paramElement.firstChildElement("value"), errorMsg)) {
        return false;
    }
    
    return true;
}

bool OPGUISystemEditorImport::isValidInputXml(const QDomElement &inputEl, QString &errorMsg) const{
    if (inputEl.isNull()) {
        errorMsg="Input element is null";
        return false;
    }

    if (inputEl.firstChildElement("id").isNull() ||
        inputEl.firstChildElement("type").isNull() ||
        inputEl.firstChildElement("title").isNull() ||
        inputEl.firstChildElement("unit").isNull() ||
        inputEl.firstChildElement("cardinality").isNull()) {
        errorMsg="Missing one or more required fields id, type, title, unit or cardinality in input";
        return false;
    }

    bool conversionOk;
    inputEl.firstChildElement("id").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("ID is not a valid integer: %1").arg(inputEl.firstChildElement("id").text());
        return false;
    }

    inputEl.firstChildElement("cardinality").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("Cardinality is not a valid integer: %1").arg(inputEl.firstChildElement("cardinality").text());
        return false;
    }

    return true;
}

bool OPGUISystemEditorImport::isValidOutputXml(const QDomElement &outputEl, QString &errorMsg) const{
    if (outputEl.isNull()) {
        errorMsg="Output element is null.";
        return false;
    }

    if (outputEl.firstChildElement("id").isNull() ||
        outputEl.firstChildElement("type").isNull() ||
        outputEl.firstChildElement("title").isNull() ||
        outputEl.firstChildElement("unit").isNull()) {
        errorMsg="Missing one or more required fields id, type, title, unit or cardinality in output.";
        return false;
    }

    bool conversionOk;
    outputEl.firstChildElement("id").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("ID is not a valid integer: %1.").arg(outputEl.firstChildElement("id").text());
        return false;
    }

    return true;
}

bool OPGUISystemEditorImport::isValidScheduleXml(const QDomElement &scheduleEl, bool isInsideSystem,QString &errorMsg) const{
    if (scheduleEl.isNull()) {
        errorMsg="Schedule element is null";
        return false;
    }

    if (scheduleEl.firstChildElement("offset").isNull() ||
        scheduleEl.firstChildElement("cycle").isNull() ||
        scheduleEl.firstChildElement("response").isNull()) {
        errorMsg="Missing one or more required fields offset, cycle or response in schedule";
        return false;
    }

    if (isInsideSystem && scheduleEl.firstChildElement("priority").isNull()) {
        errorMsg="Priority field missing in schedule";
        return false;
    }

    bool conversionOk;
    scheduleEl.firstChildElement("offset").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("offset is not a valid integer: %1").arg(scheduleEl.firstChildElement("offset").text());
        return false;
    }

    scheduleEl.firstChildElement("cycle").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("cycle is not a valid integer: %1").arg(scheduleEl.firstChildElement("cycle").text());
        return false;
    }

    scheduleEl.firstChildElement("response").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("response is not a valid integer: %1").arg(scheduleEl.firstChildElement("response").text());
        return false;
    }

    return true;
}

bool OPGUISystemEditorImport::isValidConnectionXml(const QDomElement &connectionEl,QString &errorMsg) const{
    bool conversionOk;

    //id
    QDomElement idEl = connectionEl.firstChildElement("id");
    if (idEl.isNull()) {
        errorMsg = "Connection ID element is missing";
        return false;
    }
    idEl.text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg = QString("Connection ID is not a valid integer: %1").arg(idEl.text());
        return false;
    }

    //source part
    QDomElement sourceEl = connectionEl.firstChildElement("source");
    if (sourceEl.isNull()) {
        errorMsg = "Connection source element is missing";
        return false;
    }
    QDomElement sourceComponentEl = sourceEl.firstChildElement("component");
    QDomElement sourceOutputEl = sourceEl.firstChildElement("output");
    if (sourceComponentEl.isNull() || sourceOutputEl.isNull()) {
        errorMsg = "Connection source component or output element is missing";
        return false;
    }
    sourceComponentEl.text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg = QString("Connection Source component is not a valid integer: %1").arg(sourceComponentEl.text());
        return false;
    }
    sourceOutputEl.text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg = QString("Connection Source output is not a valid integer: %1").arg(sourceOutputEl.text());
        return false;
    }

    //target part
    QDomElement targetEl = connectionEl.firstChildElement("target");
    if (targetEl.isNull()) {
        errorMsg = "No target elements found in connection";
        return false;
    }

    int targetIndex = 0;
    while (!targetEl.isNull()) {
        targetIndex++;
        QDomElement targetComponentEl = targetEl.firstChildElement("component");
        QDomElement targetInputEl = targetEl.firstChildElement("input");
        if (targetComponentEl.isNull() || targetInputEl.isNull()) {
            errorMsg = QString("Target component or input element is missing for target #%1").arg(targetIndex);
            return false;
        }
        targetComponentEl.text().toInt(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Target component for target #%1 is not a valid integer: %2").arg(targetIndex).arg(targetComponentEl.text());
            return false;
        }
        targetInputEl.text().toInt(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Target input for target #%1 is not a valid integer: %2").arg(targetIndex).arg(targetInputEl.text());
            return false;
        }

        targetEl = targetEl.nextSiblingElement("target");
    }

    return true; 
}

bool OPGUISystemEditorImport::isValidPositionXml(const QDomElement &positionEl,QString &errorMsg) const{
    if (positionEl.firstChildElement("x").isNull() ||
        positionEl.firstChildElement("y").isNull()) {
        errorMsg="Missing one or more required fields x or y in position";
        return false;
    }

    bool conversionOk;
    positionEl.firstChildElement("x").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("x coordinate is not a valid integer: %1").arg(positionEl.firstChildElement("x").text());
        return false;
    }

    positionEl.firstChildElement("y").text().toInt(&conversionOk);
    if (!conversionOk) {
        errorMsg=QString("y coordinate is not a valid integer: %1").arg(positionEl.firstChildElement("y").text());
        return false;
    }

    return true;
}

bool OPGUISystemEditorImport::isValidCommentXml(const QDomElement &commentEl, QString &errorMsg) const{
    if (commentEl.isNull()) {
        errorMsg = "Comment element is null.";
        return false;
    }

    if (commentEl.firstChildElement("message").isNull() ||
        commentEl.firstChildElement("title").isNull() ||
        commentEl.firstChildElement("position").isNull()) {
        errorMsg = "Missing one or more required fields: message, title, or position in comment.";
        return false;
    }

    if (QDomElement positionEl = commentEl.firstChildElement("position"); !isValidPositionXml(positionEl, errorMsg)) {
        return false;
    }

    return true;
}

void OPGUISystemEditorImport::extractTitleFromComments(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const {
    QDomNodeList childNodes = paramElement.childNodes();
    for (int i = 0; i < childNodes.size(); ++i) {
        QDomNode childNode = childNodes.at(i);
        if (childNode.isComment() && childNode.nodeValue().contains("parameter's title:")) {
            QString title = childNode.nodeValue().section("parameter's title:", 1).trimmed();
            param.setTitle(title);
            break;
        }
    }
}

bool OPGUISystemEditorImport::validateParameterValue(const QString &typeStr, const QString &valueStr, const QDomElement &paramElement, QString &errorMsg) const {
    // Check if the value is empty; if so, it's considered valid
    if (valueStr.isEmpty()) {
        return true;
    }

    if (typeStr == "normalDistribution" || typeStr == "logNormalDistribution" || typeStr == "exponentialDistribution" || typeStr == "uniformDistribution" || typeStr == "gammaDistribution") {
        return validateDistributionParameters(typeStr, paramElement, errorMsg);
    } else {
        return validateSingleType(typeStr, valueStr, errorMsg);
    }
}

bool OPGUISystemEditorImport::validateBoolVector(const QStringList &listValues, QString &errorMsg) const {
    for (const QString& boolStr : listValues) {
        QString lowerStr = boolStr.toLower().trimmed();
        if (!(lowerStr == "true" || lowerStr == "1" || lowerStr == "false" || lowerStr == "0")) {
            errorMsg = QString("Value in boolVector is not a valid boolean: %1").arg(boolStr);
            return false;
        }
    }
    return true;
}

bool OPGUISystemEditorImport::validateIntVector(const QStringList &listValues, QString &errorMsg) const {
    bool conversionOk;
    for (const QString& intStr : listValues) {
        intStr.toInt(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Value in intVector is not a valid integer: %1").arg(intStr);
            return false;
        }
    }
    return true;
}

bool OPGUISystemEditorImport::validateDoubleVector(const QStringList &listValues, QString &errorMsg) const {
    bool conversionOk;
    for (const QString& doubleStr : listValues) {
        doubleStr.toDouble(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Value in doubleVector is not a valid double: %1").arg(doubleStr);
            return false;
        }
    }
    return true;
}

bool OPGUISystemEditorImport::validateSingleType(const QString &typeStr, const QString &valueStr, QString &errorMsg) const {
    bool conversionOk;
    if (typeStr == "int") {
        valueStr.toInt(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Value is not a valid integer: %1").arg(valueStr);
            return false;
        }
    } else if (typeStr == "bool") {
        if (!(valueStr.toLower() == "true" || valueStr == "1" || valueStr.toLower() == "false" || valueStr == "0")) {
            errorMsg = QString("Value is not a valid boolean: %1").arg(valueStr);
            return false;
        }
    } else if (typeStr == "float" || typeStr == "double") {
        valueStr.toDouble(&conversionOk);
        if (!conversionOk) {
            errorMsg = QString("Value is not a valid double: %1").arg(valueStr);
            return false;
        }
    } else if (typeStr == "string") {
        // No additional validation needed for strings
        return true;
    } else if (typeStr.endsWith("Vector")) {
        QStringList listValues = splitString(valueStr, ',');
        if (typeStr == "boolVector") {
            return validateBoolVector(listValues, errorMsg);
        } else if (typeStr == "intVector") {
            return validateIntVector(listValues, errorMsg);
        } else if (typeStr == "doubleVector") {
            return validateDoubleVector(listValues, errorMsg);
        } else if (typeStr == "stringVector") {
            // No additional validation needed for string vectors
            return true;
        }
    } else {
        errorMsg = QString("Unknown type: %1").arg(typeStr);
        return false;
    }
    return true;
}

bool OPGUISystemEditorImport::validateDistributionParameters(const QString& typeStr, const QDomElement& paramElement, QString& errorMsg) const {
    QDomElement minElement = paramElement.firstChildElement("min");
    if (QDomElement maxElement = paramElement.firstChildElement("max"); minElement.isNull() || maxElement.isNull()) {
        errorMsg = QString("Missing minimum or maximum value for distribution type %1").arg(typeStr);
        return false;
    }

    // Validate mean and standard deviation for normal and log-normal distributions
    if (typeStr == "normalDistribution" || typeStr == "logNormalDistribution") {
        QDomElement meanElement = paramElement.firstChildElement("mean");
        if (QDomElement sdElement = paramElement.firstChildElement("sd"); meanElement.isNull() || sdElement.isNull()) {
            errorMsg = QString("Missing mean or standard deviation for %1").arg(typeStr);
            return false;
        }

        if (typeStr == "logNormalDistribution") {
            QDomElement muElement = paramElement.firstChildElement("mu");
            QDomElement sigmaElement = paramElement.firstChildElement("sigma");
            if (muElement.isNull() || sigmaElement.isNull()) {
                errorMsg = QString("Missing mu or sigma for logNormalDistribution");
                return false;
            }
        }
    } else if (typeStr == "exponentialDistribution") {
        QDomElement lambdaElement = paramElement.firstChildElement("lambda");
        if (lambdaElement.isNull()) {
            errorMsg = QString("Missing lambda for exponentialDistribution");
            return false;
        }
    } else if (typeStr == "gammaDistribution") {
        QDomElement shapeElement = paramElement.firstChildElement("shape");
        QDomElement scaleElement = paramElement.firstChildElement("scale");
        if (shapeElement.isNull() || scaleElement.isNull()) {
            errorMsg = QString("Missing shape or scale for gammaDistribution");
            return false;
        }
    }

    return true;
}

}  // namespace OPGUISystemEditor




