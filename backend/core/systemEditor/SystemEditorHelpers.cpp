// SystemEditorHelpers.cpp
#include "SystemEditorHelpers.h"

#include <QFile>

namespace OPGUISystemEditor {

bool existComponentLibraryDll(const QString &pathLibraries, const QString &library, QString &errorMsg) {
    if (QString libraryFullPath = pathLibraries + "/" + library + ".dll"; !QFile::exists(libraryFullPath)) {
        errorMsg = QString("Libary used by component was not found: %1").arg(libraryFullPath);
        return false;
    }
    return true;
}

const OpenAPI::OAIComponentUI* findComponentById(const int id, const QList< OpenAPI::OAIComponentUI>& components) {
    for (const OpenAPI::OAIComponentUI& component : components) {
        if (component.getId() == id) {
            return &component;
        }
    }
    return nullptr;
}

const OpenAPI::OAIComponentUI* findComponentByTitle(const QString &title, const QList<OpenAPI::OAIComponentUI>& components) {
    for (const OpenAPI::OAIComponentUI& component : components) {
        if (component.getTitle() == title) {
            return &component;
        }
    }
    return nullptr;
}

const OpenAPI::OAIInput* findInputById(const int id,const QList<OpenAPI::OAIInput>& inputs) {
    for (const OpenAPI::OAIInput& input : inputs) {
        if (input.getId() == id) {
            return &input;
        }
    }
    return nullptr; 
}
 
const OpenAPI::OAIOutput* findOutputById(const int id,const QList<OpenAPI::OAIOutput>& outputs) {
    for (const OpenAPI::OAIOutput& output : outputs) {
        if (output.getId() == id) {
            return &output;
        }
    }
    return nullptr;
}

QStringList splitString(const QString& str, QChar delimiter) {
    QStringList list = str.split(delimiter, Qt::SkipEmptyParts);
    for (auto& s : list) {
        s = s.trimmed();
    }
    return list;
}

} // namespace OPGUISystemEditor