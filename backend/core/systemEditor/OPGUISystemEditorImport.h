/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>
#include <QDomElement>
#include <QList>

#include "OAISystemUI.h"
#include "OAIComponentUI.h"
#include "OAISchedule.h"
#include "OAIPosition.h"
#include "OAIParameter.h"
#include "OAIInput.h"
#include "OAIOutput.h"
#include "OAIConnection.h"
#include "OAIComment.h"

namespace OPGUISystemEditor {

/**
 * @brief Manages the OpenAPI System User Interface components
 *
 * This class provides methods for reading, importing, and validating
 * JSON representations of OpenAPI System UI components and their associated elements.
 */

class OPGUISystemEditorImport {
public:
    /**
     * Constructs an OPGUISystemEditorImport object.
     */
    OPGUISystemEditorImport();

    /**
     * Reads an XML file and returns its contents as a QString.
     *
     * @param filePath The path to the XML file.
     * @return The content of the XML file as a QString.
     */
    QString readXmlFile(const QString &filePath) const;

    /**
     * Loads components from a specified directory into a list.
     *
     * @param dirPath Path to the directory containing component XML files.
     * @param components Reference to a list where loaded components will be stored.
     * @param errorMsg Reference to a string for error messages.
     * @return True if components are successfully loaded, false otherwise.
     */
    bool loadComponentsFromDirectory(const QString &dirPath, QList<OpenAPI::OAIComponentUI> &components, QString &errorMsg) const;

    /**
     * Loads systems from a specified directory into a list.
     *
     * @param dirPath Path to the directory containing system XML files.
     * @param systems Reference to a list where loaded systems will be stored.
     * @param errorMsg Reference to a string for error messages.
     * @return True if systems are successfully loaded, false otherwise.
     */
    bool loadSystemsFromDirectory(const QString &dirPath, QList<OpenAPI::OAISystemUI> &systems, QString &errorMsg);

    /**
     * Retrieves a system configuration from an XML file.
     *
     * @param path Path to the system XML file.
     * @param system Reference to an OAISystemUI object to populate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the system is successfully retrieved, false otherwise.
     */
    bool getSystemFromXml(const QString &path, OpenAPI::OAISystemUI &system, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAISystemUI object.
     *
     * @param systemEl The QDomElement representing the system.
     * @param system Reference to an OAISystemUI object to populate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlSystem(const QDomElement &systemEl, OpenAPI::OAISystemUI &system, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAIComponentUI object.
     *
     * @param componentEl The QDomElement representing the component.
     * @param component Reference to an OAIComponentUI object to populate.
     * @param isInsideSystem Flag indicating if the component is part of a system.
     * @param originalComponents List of already parsed components for reference.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlComponent(const QDomElement &componentEl, OpenAPI::OAIComponentUI &component, bool isInsideSystem, const QList<OpenAPI::OAIComponentUI> &originalComponents, QString &errorMsg) const;

    /**
     * Parses an XML element into a list of OAIParameter objects.
     *
     * @param parametersEl The QDomElement representing the parameters.
     * @param parameters Reference to a list to populate with parsed parameters.
     * @param isInsideSystem Flag indicating if the parameters are part of a system.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlParameters(const QDomElement &parametersEl, QList<OpenAPI::OAIParameter> &parameters, bool isInsideSystem, QString &errorMsg) const;

    /**
     * @brief Handles type-specific logic for distribution parameters such as normal, log-normal, and others.
     *
     * Constructs JSON objects for distribution types and sets them to the parameter object.
     *
     * @param paramElement The QDomElement containing the parameter information.
     * @param param Reference to the parameter object where the JSON value will be set.
     */
    void handleDistributionTypeLogic(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const;

    /**
     * @brief Handles basic type-specific logic for parameters such as boolean and vector types.
     *
     * Normalizes boolean and vector values based on the type specified in the parameter element.
     *
     * @param paramElement The QDomElement containing the parameter information.
     * @param param Reference to the parameter object where the normalized value will be set.
     */
    void normalizeParameterValues(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const;

    /**
     * Parses an XML element into a list of OAIInput objects.
     *
     * @param inputsEl The QDomElement representing the inputs.
     * @param inputs Reference to a list to populate with parsed inputs.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlInputs(const QDomElement &inputsEl, QList<OpenAPI::OAIInput> &inputs, QString &errorMsg) const;

    /**
     * Parses an XML element into a list of OAIOutput objects.
     *
     * @param outputsEl The QDomElement representing the outputs.
     * @param outputs Reference to a list to populate with parsed outputs.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlOutputs(const QDomElement &outputsEl, QList<OpenAPI::OAIOutput> &outputs, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAISchedule object.
     *
     * @param scheduleEl The QDomElement representing the schedule.
     * @param schedule Reference to an OAISchedule object to populate.
     * @param isInsideSystem Flag indicating if the schedule is part of a system.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlSchedule(const QDomElement &scheduleEl, OpenAPI::OAISchedule &schedule, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAIPosition object.
     *
     * @param positionEl The QDomElement representing the position.
     * @param position Reference to an OAIPosition object to populate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlPosition(const QDomElement &positionEl, OpenAPI::OAIPosition &position, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAIConnection object.
     *
     * @param connectionEl The QDomElement representing the connection.
     * @param connection Reference to an OAIConnection object to populate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlConnection(const QDomElement &connectionEl, OpenAPI::OAIConnection &connection, QString &errorMsg) const;

    /**
     * Parses an XML element into an OAIComment object.
     *
     * @param commentEl The QDomElement representing the comment.
     * @param comment Reference to an OAIComment object to populate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if parsing is successful, false otherwise.
     */
    bool parseXmlComment(const QDomElement &commentEl, OpenAPI::OAIComment &comment, QString &errorMsg) const;

    /* Validation Functions */

    /**
     * Validates the structure of a System XML element.
     *
     * @param systemEl The QDomElement representing the system to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the system XML structure is valid, false otherwise.
     */
    bool isValidSystemXml(const QDomElement &systemEl, QString &errorMsg) const;

    /**
     * Validates the structure of a Component XML element.
     *
     * @param componentEl The QDomElement representing the component to validate.
     * @param isInsideSystem Boolean indicating if the component is part of a system.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the component XML structure is valid, false otherwise.
     */
    bool isValidComponentXml(const QDomElement &componentEl, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Validates the structure of a Parameter XML element.
     *
     * @param paramElement The QDomElement representing the parameter to validate.
     * @param isInsideSystem Boolean indicating if the parameter is part of a system.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the parameter XML structure is valid, false otherwise.
     */
    bool isValidParameterXml(const QDomElement &paramElement, bool isInsideSystem, QString &errorMsg) const;

    /**
     * @brief Validates the value of a parameter based on its specified type.
     *
     * This function checks the correctness of the parameter's value according to the
     * type defined for it (e.g., integer, boolean, vector types, and distribution types).
     * It handles type-specific validations such as checking integer conversions, boolean
     * string correctness, and the structure of statistical distributions. It uses utility
     * functions for handling complex types like vectors and distributions to reduce complexity
     * and improve modularity.
     *
     * @param typeStr The type of the parameter as a string (e.g., "int", "boolVector").
     * @param valueStr The string representation of the parameter's value to validate.
     * @param paramElement The QDomElement containing the parameter for additional context,
     *                     used primarily for distribution types that require multiple sub-elements.
     * @param errorMsg Reference to a string that will store the error message if the validation fails.
     *
     * @return True if the parameter's value is valid according to its type, false otherwise.
     *         If false, errorMsg will contain a description of the issue.
     */
    bool validateParameterValue(const QString &typeStr, const QString &valueStr, const QDomElement &paramElement, QString &errorMsg) const;

    /**
     * Validates the structure of an Input XML element.
     *
     * @param inputEl The QDomElement representing the input to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the input XML structure is valid, false otherwise.
     */
    bool isValidInputXml(const QDomElement &inputEl, QString &errorMsg) const;

    /**
     * Validates the structure of an Output XML element.
     *
     * @param outputEl The QDomElement representing the output to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the output XML structure is valid, false otherwise.
     */
    bool isValidOutputXml(const QDomElement &outputEl, QString &errorMsg) const;

    /**
     * Validates the structure of a Schedule XML element.
     *
     * @param scheduleEl The QDomElement representing the schedule to validate.
     * @param isInsideSystem Boolean indicating if the schedule is part of a system.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the schedule XML structure is valid, false otherwise.
     */
    bool isValidScheduleXml(const QDomElement &scheduleEl, bool isInsideSystem, QString &errorMsg) const;

    /**
     * Validates the structure of a Connection XML element.
     *
     * @param connectionEl The QDomElement representing the connection to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the connection XML structure is valid, false otherwise.
     */
    bool isValidConnectionXml(const QDomElement &connectionEl, QString &errorMsg) const;

    /**
     * Validates the structure of a Position XML element.
     *
     * @param positionEl The QDomElement representing the position to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the position XML structure is valid, false otherwise.
     */
    bool isValidPositionXml(const QDomElement &positionEl, QString &errorMsg) const;

    /**
     * Validates the structure of a Comment XML element.
     *
     * @param commentEl The QDomElement representing the comment to validate.
     * @param errorMsg Reference to a string for error messages.
     * @return True if the comment XML structure is valid, false otherwise.
     */
    bool isValidCommentXml(const QDomElement &commentEl, QString &errorMsg) const;

    /**
     * Extracts the title from XML comment nodes within a parameter element.
     * This method searches through child nodes of a given parameter element to find a comment
     * that contains a title for the parameter. It assumes that the comment will contain a specific
     * tag (e.g., "parameter's title:") from which the title can be parsed.
     *
     * @param paramElement The QDomElement representing the parameter from which the title is to be extracted.
     * @param param Reference to an OAIParameter object where the extracted title will be set.
     */
    void extractTitleFromComments(const QDomElement &paramElement, OpenAPI::OAIParameter &param) const;

    /**
     * @brief Validates a list of strings to ensure each is a valid boolean value.
     *
     * This function checks if each string in the list represents a boolean value ("true", "1", "false", "0").
     * It trims and converts strings to lowercase to ensure case-insensitive matching.
     *
     * @param listValues The list of strings to validate as boolean values.
     * @param errorMsg Reference to a string where the error message will be stored if validation fails.
     * @return True if all entries in the list are valid booleans, false otherwise with an error message.
     */
    bool validateBoolVector(const QStringList &listValues, QString &errorMsg) const;

    /**
     * @brief Validates a list of strings to ensure each is a valid integer.
     *
     * This function attempts to convert each string in the list to an integer. It checks for successful
     * conversion to determine the validity of each string as an integer.
     *
     * @param listValues The list of strings to validate as integers.
     * @param errorMsg Reference to a string where the error message will be stored if validation fails.
     * @return True if all entries in the list are valid integers, false otherwise with an error message.
     */
    bool validateIntVector(const QStringList &listValues, QString &errorMsg) const;

    /**
     * @brief Validates a list of strings to ensure each is a valid double.
     *
     * This function attempts to convert each string in the list to a double. It checks for successful
     * conversion to determine the validity of each string as a double.
     *
     * @param listValues The list of strings to validate as doubles.
     * @param errorMsg Reference to a string where the error message will be stored if validation fails.
     * @return True if all entries in the list are valid doubles, false otherwise with an error message.
     */
    bool validateDoubleVector(const QStringList &listValues, QString &errorMsg) const;

    /**
     * @brief Validates a parameter value based on its type.
     *
     * This function handles the validation of various parameter types, including
     * basic types (int, bool, float, double, string) and vector types (boolVector, intVector, doubleVector, stringVector).
     * It checks if the given value string is valid for the specified type and sets an appropriate error message if it is not.
     *
     * @param typeStr The type of the parameter as a string.
     * @param valueStr The value of the parameter as a string.
     * @param errorMsg Reference to a string that will contain an error message if the validation fails.
     * @return true if the value is valid for the given type, false otherwise.
     */
    bool validateSingleType(const QString &typeStr, const QString &valueStr, QString &errorMsg) const;

    /**
     * @brief Validates distribution parameters from a QDomElement based on the distribution type.
     *
     * This function checks for the presence of necessary XML elements ('min', 'max', 'mean', 'sd', etc.)
     * required by different types of distributions (normal, log-normal, exponential, uniform, gamma).
     * It ensures all required parameters are present for the specified distribution type.
     *
     * @param typeStr The type of distribution to validate.
     * @param paramElement The QDomElement containing the distribution parameters.
     * @param errorMsg Reference to a string where the error message will be stored if validation fails.
     * @return True if all necessary parameters for the specified distribution are present, false otherwise.
     */
    bool validateDistributionParameters(const QString& typeStr, const QDomElement& paramElement, QString& errorMsg) const;
};

}  // namespace OPGUISystemEditor

