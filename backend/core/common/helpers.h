/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include <QString>

/**
 * @namespace Helpers
 * Contains utility functions that perform common tasks and operations used across the application.
 */
namespace Helpers {

    /**
     * Normalizes a file path from Windows-style to Unix-style.
     * This function converts backslashes to slashes and removes the "C:" prefix if present,
     * making paths more consistent for processing in environments that use Unix-style paths.
     *
     * @param path The Windows-style file path to be normalized.
     * @return A QString representing the normalized Unix-style path.
     */
    QString normalizePathToUnix(const QString& path);

}
