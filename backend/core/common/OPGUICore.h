/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#include "OAIPathRequest.h"
#include "OAIExportOpsimulationManagerXmlApiRequest.h"
#include "OAIOpSimulationManagerXmlRequest.h"
#include "OAISelectedExperimentsRequest.h"
#include "OAIVerifyPath_200_response.h"
#include "OAIDefault200Response.h"
#include "OAIBoolean200Response.h"
#include "OPGUIPCMSimulation.h"
#include "OAIComponentUI.h"
#include "OAISystemUI.h"
#include "OAIConfigsRequest.h"
#include "OAIExportedSimConfigSettings.h"
#include "OAIFileTreeExtended.h"
#include "OAIFileTreeRequest.h"
#include "OAIConfigElement.h"
#include "OAI_api_saveSystem_post_request.h"
#include "OAI_api_getValueList_get_200_response.h"

namespace OPGUICore
{

    extern QAtomicInt isRunning;  ///< Tracks whether a simulation is currently running.
    extern OPGUIPCMSimulation::PCMSimulation *sim;  ///< Pointer to the current simulation object.

    /**
     * Checks if a simulation is currently running.
     * @return True if a simulation is in progress, false otherwise.
     */
    bool isSimulationRunning();

    /**
     * Sets the simulation running state.
     * @param value True to indicate simulation is running, false otherwise.
     */
    void setSimulationRunning(bool value);

    /**
     * Retrieves the current simulation object.
     * @return Pointer to the current PCMSimulation object.
     */
    OPGUIPCMSimulation::PCMSimulation* getSimulation();

    /**
     * Sets the current simulation object.
     * @param newSim Pointer to the new PCMSimulation object to be used.
     */
    void setSimulation(OPGUIPCMSimulation::PCMSimulation *newSim);

    /**
     * Verifies if a specified path exists and has data.
     * @param req The path request containing the path to verify.
     * @param resp Response structure to hold the verification result.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the path verification is successful, false otherwise.
     */
    bool api_verify_path(const OpenAPI::OAIPathRequest &req, OpenAPI::OAIVerifyPath_200_response &resp, QString &errorMsg);

    /**
     * Deletes information in a specified directory.
     * @param req The path request containing the directory path.
     * @param resp Response structure to return the operation success status.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the deletion is successful, false otherwise.
     */
    bool api_delete_information(const OpenAPI::OAIPathRequest &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Initiates the opSimulationManager to start a simulation.
     * @param resp Response structure to return the operation result.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the simulation starts successfully, false otherwise.
     */
    bool api_run_opSimulationManager(OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Exports simulation configurations to an XML file for the opSimulationManager.
     * @param req Request data containing simulation and logging details.
     * @param resp Response structure to confirm the export success.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the export is successful, false otherwise.
     */
    bool api_export_opSimulationManager_xml(OpenAPI::OAIOpSimulationManagerXmlRequest req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Sends a PCM formatted file to the backend for processing.
     * @param req The path request containing the path to the PCM file.
     * @param resp Response structure to return the operation result.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the file is sent successfully, false otherwise.
     */
    bool api_send_PCM_file(const OpenAPI::OAIPathRequest &req, OpenAPI::OAISelectedExperimentsRequest &resp, QString &errorMsg);

    /**
     * Saves the path to converted PCM cases.
     * @param req The path request containing the directory path where converted cases should be saved.
     * @param resp Response structure to return the save operation success status.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the path is saved successfully, false otherwise.
     */
    bool api_path_to_converted_cases(const OpenAPI::OAIPathRequest &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Converts selected PCM cases into configuration folders.
     * @param req Request data containing the list of selected experiments.
     * @param resp Response structure to confirm the conversion success.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the conversion is successful, false otherwise.
     */
    bool api_convert_to_configs(const OpenAPI::OAIConfigsRequest &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Executes PCM simulation manager and stores the results.
     * @param req Request data containing the list of experiments to be executed.
     * @param resp Response structure to return the simulation execution result.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the simulation is executed successfully, false otherwise.
     */
    bool api_export_to_simulations(const OpenAPI::OAISelectedExperimentsRequest &req, OpenAPI::OAIExportedSimConfigSettings &resp, QString &errorMsg);

    /**
     * Retrieves the list of components currently available.
     * @param resp List to hold the components retrieved.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if components are retrieved successfully, false otherwise.
     */
    bool api_get_components(QList<OpenAPI::OAIComponentUI> &resp, QString &errorMsg);

    /**
     * Retrieves the file tree starting from a specified directory.
     * @param req Request data containing the starting path for the file tree.
     * @param resp File tree structure to hold the directories and files.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the file tree is retrieved successfully, false otherwise.
     */
    bool api_get_fileTree(const OpenAPI::OAIFileTreeRequest &req, OpenAPI::OAIFileTreeExtended &resp, QString &errorMsg);

    /**
     * Retrieves configuration elements for a specified path.
     * @param resp List to hold the configuration elements retrieved.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the configuration elements are retrieved successfully, false otherwise.
     */
    bool api_get_configElements(QList<OpenAPI::OAIConfigElement> &resp, QString &errorMsg);

    /**
     * Sets or updates configuration elements.
     * @param req List of configuration elements to set or update.
     * @param resp Response structure to return the operation success status.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the configuration is updated successfully, false otherwise.
     */
    bool api_set_configElements(QList<OpenAPI::OAIConfigElement> req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Retrieves system details for a specified system identifier.
     * @param req System identifier for which details are requested.
     * @param resp System details returned.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if system details are retrieved successfully, false otherwise.
     */
    bool api_get_system(const QString &req, OpenAPI::OAISystemUI &resp, QString &errorMsg);

    /**
     * Saves system configurations.
     * @param req System configuration details to be saved.
     * @param resp Response structure to return the operation success status.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the system is saved successfully, false otherwise.
     */
    bool api_save_system(const OpenAPI::OAI_api_saveSystem_post_request &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Deletes a system based on a given system identifier.
     * @param req System identifier for the system to be deleted.
     * @param resp Response structure to return the operation success status.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if the system is deleted successfully, false otherwise.
     */
    bool api_delete_system(const QString &req, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Retrieves the contents of a file at a specified path.
     * @param reqPath Path of the file whose contents are to be retrieved.
     * @param reqEncoded Boolean indicating whether the file contents should be returned encoded.
     * @param resp Response structure to hold the file contents.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if file contents are retrieved successfully, false otherwise.
     */
    bool api_get_file_contents(const QString &reqPath, const bool reqEncoded, OpenAPI::OAIDefault200Response &resp, QString &errorMsg);

    /**
     * Retrieves a list of values based on a specified path.
     * @param reqPath Path for which values are requested.
     * @param resp Response structure to hold the list of values.
     * @param errorMsg Output parameter to hold any error message if the operation fails.
     * @return True if values are retrieved successfully, false otherwise.
     */
    bool api_get_value_list(const QString &reqPath, OpenAPI::OAI_api_getValueList_get_200_response &resp, QString &errorMsg);
}
