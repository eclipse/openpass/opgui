/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QStandardPaths>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDebug>
#include <QDir>
#include <QCoreApplication>

#include "OPGUICoreGlobalConfig.h"

OPGUICoreGlobalConfig::OPGUICoreGlobalConfig() {
    setDefaultValues();
    this->initializationSuccessful = loadFromConfigFile();
}

QString OPGUICoreGlobalConfig::fullPathComponentsFolder() const { return m_fullPathComponentsFolder; }
QString OPGUICoreGlobalConfig::fullPathModulesFolder() const { return m_fullPathModulesFolder; }
QString OPGUICoreGlobalConfig::fullPathOpSimulationManagerXml() const { return m_fullPathOpSimulationManagerXml;}
QString OPGUICoreGlobalConfig::fullPathOpSimulationExe() const { return m_fullPathOpSimulationExe; }
QString OPGUICoreGlobalConfig::fullPathOpSimulationManagerExe() const { return m_fullPathOpSimulationManagerExe; }

QString OPGUICoreGlobalConfig::pathApplication() const { return m_pathApplication; }
QString OPGUICoreGlobalConfig::pathConfigFile() const { return m_pathConfigFile; }
QString OPGUICoreGlobalConfig::pathHome() const { return this->cleanPathNoTrailing(m_pathHome); }
int OPGUICoreGlobalConfig::backendPort() const { return m_backendPort; }
QString OPGUICoreGlobalConfig::backendIP() const { return m_backendIP; }
QString OPGUICoreGlobalConfig::pathOpenpassCore() const { return this->cleanPathNoTrailing(m_pathOpenpassCore); }
QString OPGUICoreGlobalConfig::workspace() const { return this->cleanPathNoTrailing(m_workspace); }
QString OPGUICoreGlobalConfig::pathConvertedCases() const { 
    if(m_pathConvertedCases.isEmpty()){
        return this->workspace();
    }
    else{
        return this->cleanPathNoTrailing(m_pathConvertedCases); 
    }
}
QString OPGUICoreGlobalConfig::pathLogFile() const { 
    if(m_pathLogFile.isEmpty()){
        return this->workspace()+"/"+"opGuiCore.log";
    }
    else{
        return m_pathLogFile; 
    }
}

// Setters
void OPGUICoreGlobalConfig::setFullPathComponentsFolder(const QString &path) { m_fullPathComponentsFolder = path; }
void OPGUICoreGlobalConfig::setFullPathModulesFolder(const QString &path) { m_fullPathModulesFolder = path; }
void OPGUICoreGlobalConfig::setFullPathOpSimulationManagerXml(const QString &path) { m_fullPathOpSimulationManagerXml = path; }
void OPGUICoreGlobalConfig::setFullPathOpSimulationExe(const QString &path) { m_fullPathOpSimulationExe = path; }
void OPGUICoreGlobalConfig::setFullPathOpSimulationManagerExe(const QString &path) { m_fullPathOpSimulationManagerExe = path; }

void OPGUICoreGlobalConfig::setPathConfigFile(const QString &path) { m_pathConfigFile = path; }
void OPGUICoreGlobalConfig::setPathLogFile(const QString &path) { m_pathLogFile = path; }
void OPGUICoreGlobalConfig::setPathHome(const QString &path) { m_pathHome = path; }
void OPGUICoreGlobalConfig::setBackendPort(int port) { m_backendPort = port; }
void OPGUICoreGlobalConfig::setBackendIP(const QString &ip) { m_backendIP = ip; }
void OPGUICoreGlobalConfig::setPathOpenpassCore(const QString &path) { m_pathOpenpassCore = path; }
void OPGUICoreGlobalConfig::setWorkspace(const QString &workspace) { m_workspace = workspace; }
void OPGUICoreGlobalConfig::setPathConvertedCases(const QString &path) { m_pathConvertedCases = path; }
void OPGUICoreGlobalConfig::setPathApplication(const QString &path) { m_pathApplication = path; }

bool OPGUICoreGlobalConfig::isInitializationSuccessful() const{
    return this->initializationSuccessful;
}

void OPGUICoreGlobalConfig::setDefaultValues() {
    m_fullPathComponentsFolder = QStringLiteral("");
    m_fullPathModulesFolder = QStringLiteral("");
    m_fullPathOpSimulationManagerXml = QStringLiteral("");
    m_fullPathOpSimulationExe = QStringLiteral("");
    m_fullPathOpSimulationManagerExe = QStringLiteral("");
    m_pathConfigFile = defaultPathConfigFile();
    m_pathApplication = defaultPathApplication();
    m_pathHome = defaultPathHome();
    m_pathOpenpassCore = QStringLiteral("");
    m_workspace = QStringLiteral("");
    m_pathConvertedCases = QStringLiteral("");
    m_pathLogFile = QStringLiteral("");

    m_backendPort = 8848;
    m_backendIP = QStringLiteral("127.0.0.1");
}

QString OPGUICoreGlobalConfig::defaultPathConfigFile() const {
#ifdef _WIN32
    QString configFilePath = defaultPathApplication() + "\\backendConfig.json";
#elif __APPLE__ || __linux__
    qDebug() << "Default app path is:" << defaultPathApplication();
    QString configFilePath = defaultPathApplication() + "/backendConfig.json";
#else
    QString configFilePath = QStringLiteral("");
#endif
    return configFilePath;
}

QString OPGUICoreGlobalConfig::defaultPathApplication() const {
    QString path = QCoreApplication::applicationDirPath();
    //remove tests so tests can be executed into a subdir but functionality remains
    if (path.endsWith("/test", Qt::CaseInsensitive) || path.endsWith("\\test", Qt::CaseInsensitive)) {
        int lastIndex = path.lastIndexOf(QRegExp(R"([/\\])"));
        if (lastIndex != -1) {
            path = path.left(lastIndex);
        }
    }
    qDebug() << "Application executable path is "+path;

    return path;
}

QString OPGUICoreGlobalConfig::defaultPathHome() const {
    return QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();
}

bool OPGUICoreGlobalConfig::loadFromConfigFile() {
    QString configFilePath = defaultPathConfigFile();
    QFile configFile(configFilePath);

    if (!configFile.exists()) {
        qDebug() << "Config file does not exist at path:" << configFilePath;
        return false; 
    }

    if (!configFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open config file for reading at path:" << configFilePath;
        return false; 
    }

    QByteArray jsonData = configFile.readAll();
    configFile.close();

    QJsonDocument doc = QJsonDocument::fromJson(jsonData);
    if (doc.isNull() || !doc.isObject()) {
            qDebug() << "Invalid or malformed JSON content in config file";
            qDebug() << "File path:" << configFilePath;
            qDebug() << "JSON Data:" << jsonData;
            return false;
    }

    QJsonObject jsonObj = doc.object();

    // Check and load values from JSON, only if they exist
    if (jsonObj.contains("MODULES_FOLDER")) {
        m_fullPathModulesFolder = jsonObj["MODULES_FOLDER"].toString();
    }

    if (jsonObj.contains("COMPONENTS_FOLDER")) {
        m_fullPathComponentsFolder = jsonObj["COMPONENTS_FOLDER"].toString();
    }

    if (jsonObj.contains("OPSIMULATION_MANAGER_XML")) {
        m_fullPathOpSimulationManagerXml = jsonObj["OPSIMULATION_MANAGER_XML"].toString();
    }

    if (jsonObj.contains("OPSIMULATION_EXE")) {
        m_fullPathOpSimulationExe = jsonObj["OPSIMULATION_EXE"].toString();
    }

    if (jsonObj.contains("OPSIMULATION_MANAGER_EXE")) {
        m_fullPathOpSimulationManagerExe = jsonObj["OPSIMULATION_MANAGER_EXE"].toString();
    }

    if (jsonObj.contains("BACKEND_PORT")) {
        m_backendPort = jsonObj["BACKEND_PORT"].toInt();
    }

    if (jsonObj.contains("BACKEND_IP")) {
        m_backendIP = jsonObj["BACKEND_IP"].toString();
    }

    if (jsonObj.contains("PATH_OPENPASS_CORE")) {
        m_pathOpenpassCore = jsonObj["PATH_OPENPASS_CORE"].toString();
    }

    if (jsonObj.contains("WORKSPACE")) {
        m_workspace = jsonObj["WORKSPACE"].toString();
    }

    if (jsonObj.contains("PATH_CONVERTED_CASES")) {
        m_pathConvertedCases = jsonObj["PATH_CONVERTED_CASES"].toString();
    }

    if (jsonObj.contains("PATH_LOG_FILE")) {
        m_pathLogFile = jsonObj["PATH_LOG_FILE"].toString();
    }

    configFile.close();
    return true;
}

OPGUICoreGlobalConfig& OPGUICoreGlobalConfig::getInstance() {
    static OPGUICoreGlobalConfig instance;
    return instance;
}

QString OPGUICoreGlobalConfig::cleanPathNoTrailing(const QString& originalPath) const {
    QString cleanedPath = QDir::cleanPath(originalPath);
    if (cleanedPath.endsWith('/') || cleanedPath.endsWith('\\')) {
        cleanedPath.chop(1);
    }
    return cleanedPath;
}

bool OPGUICoreGlobalConfig::modifyOrAddValueToConfigFile(const QString& key, const QJsonValue& value) const{
    QFile configFile(this->m_pathConfigFile);
    if (!configFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Error: Unable to open config file for reading:" << this->m_pathConfigFile;
        return false;
    }
    QByteArray jsonData = configFile.readAll();
    configFile.close();

    QJsonDocument doc = QJsonDocument::fromJson(jsonData);
    if (doc.isNull() || !doc.isObject()) {
            qDebug() << "Error: JSON document parsed from config file is null.";
            qDebug() << "File path:" << this->m_pathConfigFile;
            qDebug() << "JSON Data:" << jsonData;
            return false;
    }

    QJsonObject obj = doc.object();
    obj[key] = value;
    doc.setObject(obj);
    if (!configFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Error: Unable to open config file for writing:" << this->m_pathConfigFile;
        return false;
    }
    configFile.write(doc.toJson());
    configFile.close();

    return true;
}



