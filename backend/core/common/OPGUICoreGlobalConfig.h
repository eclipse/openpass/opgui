/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QString>

/**
 * Singleton class to manage global configuration settings for OPGUI application.
 */
class OPGUICoreGlobalConfig {
public:
    /**
     * Returns the singleton instance of the global configuration.
     */
    static OPGUICoreGlobalConfig& getInstance();

    /**
     * Checks if the initialization of the configuration was successful.
     * @return True if initialization was successful, false otherwise.
     */
    bool isInitializationSuccessful() const;

    // Getters for configuration paths
    QString fullPathComponentsFolder() const;
    QString fullPathModulesFolder() const;
    QString fullPathOpSimulationManagerXml() const;
    QString fullPathOpSimulationExe() const;
    QString fullPathOpSimulationManagerExe() const;

    // More Getters for application directories
    QString pathApplication() const;
    QString pathConfigFile() const;
    QString pathLogFile() const;
    QString pathHome() const;
    int backendPort() const;
    QString backendIP() const;
    QString pathOpenpassCore() const;
    QString workspace() const;
    QString pathConvertedCases() const;
    
    // Setters for updating configuration paths
    void setFullPathComponentsFolder(const QString &value);
    void setFullPathModulesFolder(const QString &value);
    void setFullPathOpSimulationManagerXml(const QString &value);
    void setFullPathOpSimulationExe(const QString &value);
    void setFullPathOpSimulationManagerExe(const QString &value);

    // Setters for application directories
    void setPathConfigFile(const QString &value);
    void setPathLogFile(const QString &value);
    void setPathHome(const QString &value);
    void setBackendPort(int value);
    void setBackendIP(const QString &value);
    void setPathOpenpassCore(const QString &value);
    void setWorkspace(const QString &value);
    void setPathConvertedCases(const QString &value);
    void setPathApplication(const QString &value);

    /**
     * Modifies an existing configuration key or adds a new key-value pair to the configuration file.
     * @param key Configuration key to be modified or added.
     * @param value Value associated with the key.
     * @return True if the operation was successful, false otherwise.
     */
    bool modifyOrAddValueToConfigFile(const QString& key, const QJsonValue& value) const;

    /**
     * Sets default values for configuration parameters.
     */
    void setDefaultValues();

    /**
     * Loads configuration values from the configuration file.
     * @return True if loading is successful, false otherwise.
     */
    bool loadFromConfigFile();

    /**
     * Returns the default path to the configuration file.
     */
    QString defaultPathConfigFile() const; 

private:
    // Constructor and Destructor
    OPGUICoreGlobalConfig();
    ~OPGUICoreGlobalConfig() = default;

    // Helper methods for default paths
    QString defaultPathApplication() const;
    QString defaultPathHome() const;
    QString defaultOpSimulationManagerExe() const;
    QString defaultOpSimulationExe() const;
    QString cleanPathNoTrailing(const QString& originalPath) const; // Removes trailing slashes from paths

    // Initialization check
    bool initializationSuccessful = true;

    // Member variables for stored paths
    QString m_fullPathComponentsFolder;
    QString m_fullPathModulesFolder;
    QString m_fullPathOpSimulationManagerXml;
    QString m_fullPathOpSimulationExe;
    QString m_fullPathOpSimulationManagerExe;

    QString m_pathConfigFile;
    QString m_pathLogFile;
    QString m_pathHome;
    QString m_pathApplication;
    int m_backendPort;
    QString m_backendIP;
    QString m_pathOpenpassCore;
    QString m_workspace;
    QString m_pathConvertedCases;
};
