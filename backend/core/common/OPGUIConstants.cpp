/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUIConstants.h"

const QList<QString> OPGUIConstants::trafficRulesListConst = {"DE"};
const QList<QString> OPGUIConstants::weathersListConst = {"clear", "rainy", "snowy"};
const QList<QString> OPGUIConstants::worldLibrariesListConst = {"World", "World_OSI"};
const QList<QString> OPGUIConstants::dataBufferLibrariesListConst = {"DataBuffer"};
const QList<QString> OPGUIConstants::stochasticsLibrariesListConst = {"Stochastics"};
const QList<QString> OPGUIConstants::defaultLoggingGroupsConst = {"Trace",
                                                                "RoadPosition",
                                                                "RoadPositionExtended",
                                                                "Sensor",
                                                                "Vehicle",
                                                                "Visualization"};
const QList<QString> OPGUIConstants::writePersistentEntitiesModesConst = {"Consolidated","Separate","Skip"};
