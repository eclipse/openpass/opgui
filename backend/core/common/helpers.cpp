/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "helpers.h"

namespace Helpers {

QString normalizePathToUnix(const QString& path) {
    QString normalizedPath = path;

    // Replace backslashes with forward slashes
    normalizedPath.replace("\\", "/");

    // Remove leading 'C:' if present, but keep the initial '/'
    if (normalizedPath.startsWith("C:", Qt::CaseInsensitive)) {
        normalizedPath.remove(0, 2);
    }

    return normalizedPath;
}

}