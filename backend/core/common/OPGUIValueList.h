/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

#pragma once

#include <QList>
#include <QString>
#include <QMap>

/**
 * Class OPGUIValueList manages lists of string values categorized by unique list names.
 * It provides access to various lists that are used throughout the application to maintain 
 * consistent sets of data like configurations, settings, or any categorized collection of strings.
 */
class OPGUIValueList {
public:
    // Deletes the copy constructor to prevent copying of the singleton instance.
    OPGUIValueList(const OPGUIValueList&) = delete;

    // Deletes the copy assignment operator to prevent assignment copying of the singleton instance.
    OPGUIValueList& operator=(const OPGUIValueList&) = delete;

    /**
     * Provides access to the singleton instance of OPGUIValueList.
     * 
     * @return Returns a reference to the singleton instance of OPGUIValueList.
     */
    static OPGUIValueList& getInstance();

    /**
     * Retrieves the list of values associated with a specific list name.
     *
     * @param listName The name of the list from which values are retrieved.
     * @param resp The list where the values will be stored.
     * @param errorMsg Output parameter to store any error messages encountered during the operation.
     * @return Returns true if the values are successfully retrieved, false if the list name does not exist.
     */
    bool getValues(const QString &listName, QList<QString> &resp, QString &errorMsg);

    /**
     * Checks if a specific value exists within a given list.
     *
     * @param listName The name of the list to check.
     * @param value The value to check for in the list.
     * @return Returns true if the value is found in the specified list, false otherwise.
     */
    bool hasValue(const QString &listName, const QString &value);

    /**
     * Converts the contents of a specific list into a single string representation, 
     * where each element is separated by a newline character.
     *
     * @param listName The name of the list to convert to string.
     * @return Returns a QString containing all the items in the list separated by newlines.
     */
    QString listToString(const QString &listName);

private:
    OPGUIValueList(); // Private constructor to prevent instantiation outside of getInstance.
    void initializeListMap(); // Initializes the map that holds all the lists.

    QMap<QString, QList<QString>> listMap; // Maps list names to their corresponding lists of strings.
};

