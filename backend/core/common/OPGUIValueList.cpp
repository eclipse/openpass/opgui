/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#include <QString>
#include <QDebug> 

#include "OPGUIValueList.h"
#include "OPGUIConstants.h" 


OPGUIValueList::OPGUIValueList() {
    initializeListMap();
}

OPGUIValueList& OPGUIValueList::getInstance() {
    static OPGUIValueList instance;
    return instance;
}

void OPGUIValueList::initializeListMap() {
    listMap["trafficRules"] = OPGUIConstants::trafficRulesListConst;
    listMap["weathers"] = OPGUIConstants::weathersListConst;
    listMap["worldLibraries"] = OPGUIConstants::worldLibrariesListConst;
    listMap["dataBufferLibraries"] = OPGUIConstants::dataBufferLibrariesListConst;
    listMap["stochasticsLibraries"] = OPGUIConstants::stochasticsLibrariesListConst;
    listMap["defaultLoggingGroups"] = OPGUIConstants::defaultLoggingGroupsConst;
    listMap["WritePersistentEntitiesModes"] = OPGUIConstants::writePersistentEntitiesModesConst;
    listMap["emptyList"] = QList<QString>(); //placeholder
}

bool OPGUIValueList::getValues(const QString &listName, QList<QString> &resp, QString &errorMsg) {
    if (listMap.contains(listName)) {
        const QList<QString>& list = listMap[listName];
        if (list.isEmpty()) {
            errorMsg = QString("List '%1' is empty.").arg(listName);
            return false;
        }
        resp = list;
        return true;
    } else {
        errorMsg = QString("List '%1' not found.").arg(listName);
        return false;
    }
}

bool OPGUIValueList::hasValue(const QString &listName, const QString &value) {
    QString errMsg;
    QList<QString> values;
    if(!getValues(listName, values, errMsg)){
        return false;
    }
    return values.contains(value);
}

QString OPGUIValueList::listToString(const QString &listName){
    QString errMsg;
    QList<QString> values;
    
    if(!getValues(listName, values, errMsg)){
        return "";
    }

    return "'" + QStringList(values.begin(), values.end()).join("', '") + "'";
}