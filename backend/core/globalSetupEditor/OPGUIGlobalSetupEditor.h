/*
 * Copyright (c) 2024 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
#pragma once

#include <QJsonArray>
#include "OAIFileTreeRequest.h"
#include "OAIFileTreeExtended.h"
#include "OAIConfigElement.h"

namespace OPGUIGlobalSetupEditor {

/**
 * @brief The GlobalSetupEditor class provides functionalities for managing global configuration settings.
 *
 * This class allows for manipulation and retrieval of user-configurable settings within the application.
 */
class GlobalSetupEditor {
private:
    QList<QPair<QString, QString>> userSetableIds;  ///< List of user-configurable setting IDs.

public:
    /**
     * @brief Constructs a GlobalSetupEditor object.
     */
    GlobalSetupEditor();

    /**
     * @brief Retrieves the configuration elements that can be set by the user.
     *
     * @param errorMsg Output parameter for error messages if the operation fails.
     * @return Returns a list of user-configurable configuration elements.
     */
    QList<OpenAPI::OAIConfigElement> getConfigFileUserElements(QString &errorMsg) const;

    /**
     * @brief Fetches user configuration elements from the system.
     *
     * @param configElements Output parameter that will store the configuration elements if successfully retrieved.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the configuration elements are successfully retrieved, false otherwise.
     */
    bool getUserConfigElements(QList<OpenAPI::OAIConfigElement> &configElements, QString &errorMsg) const;

    /**
     * @brief Sets user configuration elements.
     *
     * This function updates the configuration elements based on the provided new configuration elements.
     * It checks if the elements need updating, updates them if necessary, and handles error messages and responses.
     *
     * @param newConfigElements The new configuration elements to set.
     * @param errorMsg Reference to a string that will contain an error message if an error occurs.
     * @param response Reference to a string that will contain the response message indicating the updates made.
     * @return true if the operation is successful, false otherwise.
     */
    bool setUserConfigElements(const QList<OpenAPI::OAIConfigElement> &newConfigElements, QString &errorMsg, QString &response) const;

private:
    /**
     * @brief Checks and updates elements if necessary.
     *
     * This helper function iterates over the current configuration elements and checks if they need to be updated
     * based on the provided new configuration element. If an update is necessary, it appends the element to the list
     * of elements to be updated.
     *
     * @param newElement The new configuration element to compare against.
     * @param currentConfigElementsList Reference to the list of current configuration elements.
     * @param elementsToUpdate Reference to a list that will contain elements to be updated.
     * @param errorMsg Reference to a string that will contain an error message if an error occurs.
     * @return true if the element was checked and updated successfully, false otherwise.
     */
    bool checkAndUpdateElements(const OpenAPI::OAIConfigElement &newElement, QList<OpenAPI::OAIConfigElement> &currentConfigElementsList, QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg) const;

    /**
     * @brief Performs the existence check and updates the element if necessary.
     *
     * This helper function checks if the value of a current configuration element is different from a new configuration element.
     * If the values are different, it checks if the file or folder specified in the new element exists. If the file or folder exists,
     * the current element is updated, and it is appended to the list of elements to be updated.
     *
     * @param currentElement The current configuration element to check and update.
     * @param newElement The new configuration element to compare against.
     * @param elementsToUpdate Reference to a list that will contain elements to be updated.
     * @param errorMsg Reference to a string that will contain an error message if an error occurs.
     * @return true if the check and update were performed successfully, false otherwise.
     */
    bool checkAndPerformUpdate(OpenAPI::OAIConfigElement &currentElement, const OpenAPI::OAIConfigElement &newElement, QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg) const;

    /**
     * @brief Updates the elements in the configuration.
     *
     * This helper function updates the elements in the configuration file and generates a response message.
     *
     * @param elementsToUpdate The list of elements to update.
     * @param errorMsg Reference to a string that will contain an error message if an error occurs.
     * @param response Reference to a string that will contain the response message indicating the updates made.
     * @return true if the elements were updated successfully, false otherwise.
     */
    bool updateElements(const QList<OpenAPI::OAIConfigElement> &elementsToUpdate, QString &errorMsg, QString &response) const;

    /**
     * @brief Checks if a configuration element's file or folder exists.
     *
     * This function checks if the file or folder specified in the configuration element exists.
     *
     * @param element The configuration element to check.
     * @return true if the file or folder exists, false otherwise.
     */
    bool checkExists(const OpenAPI::OAIConfigElement &element) const;

    /**
     * @brief Modifies a configuration element within the system.
     *
     * @param element The configuration element to modify.
     * @param errorMsg Output parameter for error messages.
     * @return Returns true if the element is successfully modified, false otherwise.
     */
    bool modifyConfigElement(const OpenAPI::OAIConfigElement &element, QString &errorMsg) const;
};

}  // namespace OPGUIGlobalSetupEditor
