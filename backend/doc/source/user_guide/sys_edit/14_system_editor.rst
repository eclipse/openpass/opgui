..
  *******************************************************************************
    Copyright (c) 2023 Hexad GmbH


    This program and the accompanying materials are made available under the
    terms of the Eclipse Public License 2.0 which is available at
    http://www.eclipse.org/legal/epl-2.0.

    SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _system_editor:

System Editor
=============

Navigation
----------

To access the system editor, navigate to this section of the app through the sidebar menu.

.. image:: ../../_static/use_cases/UC4/navigation.png

Screen sections
===============

In the image below, you can observe the default screen that you will encounter after navigating to "System Editor".

.. image:: ../../_static/use_cases/UC4/screen_parts.png


1. Select System File
----------------------
At the top, the user finds a Button to import a system XML file.

.. image:: ../../_static/use_cases/UC4/systems_load.png


2. System editor panel
-----------------------
Here the user can edit an already loaded system or create a new one from scratch.

.. image:: ../../_static/use_cases/UC4/editor_panel.png


3. Save bar
--------------
At the bottom, the user finds a bar with three buttons, "clear all", "save" and "save as". 

.. image:: ../../_static/use_cases/UC4/export_bar.png


Select System File 
==================

To edit an already existing system, the user can click on the "Select system file" button, and navigate to the system's file path and then click the "Select" button.

.. image:: ../../_static/use_cases/UC4/system_select.png


System editor panel
===================

Users can also create systems from scratch. To do this, simply click on the "+" button in the bar above the system editor panel.

.. image:: ../../_static/use_cases/UC4/editor_panel.png

Each system will feature a dropdown with an individual menu containing options that exclusively pertain to the system within that tab.

.. image:: ../../_static/use_cases/UC4/system_dropdown.png

By default, the name "System 1" will appear, but it can be changed by clicking on the dropdwon next to the name and selecting "rename."

.. image:: ../../_static/use_cases/UC4/system_rename.png

Additionally, the user can duplicate a system with the same name, which will create a new tab with a copy of that system.

On the right side of the system configuration dashboard, users will find a side menu containing all the available components.

To configure a component inside a system, simply drag and drop it onto the dashboard. From there, you can set the required parameters and establish connections with other components as needed.

.. image:: ../../_static/use_cases/UC4/component_config.png

To make a connection, an output should always be connected to an input of another component, to do this, click on the output icon of the desired component and all the possible inputs will be highlighted with the same color for user convenience.

Please note that if the data type of the input and the output does not match, the user will receive a warning but will still be able to create the connection.

.. image:: ../../_static/use_cases/UC4/connection_warning.png

Once the connection is established, a line will appear to visually indicate the link between the two components.

.. image:: ../../_static/use_cases/UC4/connection_2.png

The color of a component can be changed at any time if the user desires to do so. Please note that colouring of components is only used during the edit step and it is not persisted.

.. image:: ../../_static/use_cases/UC4/color_selection.png

Importantly, all associated connections will also change in color simultaneously to maintain consistency.


Save bar
==========
.. image:: ../../_static/use_cases/UC4/export_bar.png

Clear All: closes all the systems being edited and discards any unsaved changes.

.. image:: ../../_static/use_cases/UC4/clear_systems.png

Save: saves the current system in the corresponding XML file.

.. image:: ../../_static/use_cases/UC4/export_bar_saves.png

Save As: creates a copy of the current system allowing the user to select a different path and name.

.. image:: ../../_static/use_cases/UC4/export_bar_save_as.png

Please note that if the system is new, both "Save" and "Save As" buttons will produce the same result.
