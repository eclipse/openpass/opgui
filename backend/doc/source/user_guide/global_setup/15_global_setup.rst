..
  *******************************************************************************
    Copyright (c) 2023 Hexad GmbH


    This program and the accompanying materials are made available under the
    terms of the Eclipse Public License 2.0 which is available at
    http://www.eclipse.org/legal/epl-2.0.

    SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _global_setup:

Summary
============

In here you can configure all the variables related to your application.
This is the same as editing the **backendConfig.json** file.

.. image:: ../../_static/global_setup/global_setup_page.png

Sections
========
The page is divided into 4 sections

General
-------
Configurations related to many parts of the application.

Workplace Folder
^^^^^^^^^^^^^^^^
The path to the opGUI workspace folder where systems are saved.

Path to opGUI Log File
^^^^^^^^^^^^^^^^^^^^^^
This is the path to the opGUI file where logs will be stored.

System
------
Configurations related to the system editor page.

Path to Components Folder (.xml files)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the path to the folder where the system editor components will be stored. All components here must me .xml files in order to be read.

PRE
---
Configurations related to the PRE page.

Default path to PCM converted cases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the default path where the PCM cases will be exported once created.

RUN
---
Configurations related to the RUN page.

Path to Open Path Core
^^^^^^^^^^^^^^^^^^^^^^
This is the path to the core folder in case it might be needed.

Path of Modules Folder (.dll files)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the path to the folder containing the libraries the application uses to run the simulations (Each library is it's own .dll file).

Path to opSimulation Executable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the path to the opSimulation. This is needed to run the simulation. It is an executable file (.exe in windows).

Path to opSimulation Manager Executable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the path to the opSimulation manager. It uses the opSimulation Executable to run the simulations. It is an executable file (.exe in windows).

Path to opSimulation Manager XML
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is the path to the XML file used by the opSimulation Manager executable. It contains the information about the simulations that will take place once the opSimulation Manager is ran.
