################################################################################
# Copyright (c) 2021 in-tech GmbH
# Copyright (c) 2023 Hexad GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

# Configuration file for the Sphinx documentation builder. See also:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
import datetime

# -- Path setup --------------------------------------------------------------
sys.path.append(os.path.abspath("_ext"))

# -- Project information -----------------------------------------------------
project = 'opGUI'
copyright = f'{datetime.datetime.now().year} OpenPASS Working Group'
author = 'Hexad GmbH'

# -- Version is generated via cmake
version_file = 'version.txt'
if os.path.exists(version_file):
    with open(version_file) as vf:
        version = vf.read().strip()
        release = version

# -- General configuration ---------------------------------------------------
def setup(app):
   app.add_css_file('css/custom.css')

extensions = [
    "sphinx_rtd_theme",
    'sphinx.ext.todo',
    'sphinx_tabs.tabs',
    'sphinx.ext.imgmath',
    'api_doc'
]

# Try to add rst2pdf extension if available
try:
    import rst2pdf.pdfbuilder
    extensions.append('rst2pdf.pdfbuilder')
    pdf_documents = [
        ('index', 'opGUI', 'opGUI Documentation', 'Hexad GmbH'),
    ]
    pdf_stylesheets = ['sphinx', 'a4']
    pdf_style_path = ['.', '_styles']
    pdf_language = "en_US"
    pdf_fit_mode = "scale"  # Ensures images are scaled to fit the page
    pdf_default_dpi = 150  # Adjust default DPI for better image quality
    pdf_invariant = True  # Maintain aspect ratio of images
    pdf_fit_mode = "shrink"  # Ensures images fit within the available space without distortion
except ImportError:
    pass

templates_path = ['_templates']
exclude_patterns = []
todo_include_todos = True

# -- Options for HTML output -------------------------------------------------
html_static_path = ['_static']
html_theme = 'sphinx_rtd_theme'
html_title = 'opGUI Documentation'
html_short_title = 'opGUI|Doc'
html_favicon = '_static/openPASS.ico'
html_logo = '_static/openPASS.png'

# -- Options for API DOC -----------------------------------------------------
api_doc_title = "Source Code Documentation"

# -- Define global replacements ----------------------------------------------
rst_epilog = """
.. |op| replace:: **openPASS**
.. |Op| replace:: **OpenPASS**
.. |opwg| replace:: **openPASS** Working Group
.. |op_oss| replace:: **openPASS** (Open Source)
.. |Op_oss| replace:: **OpenPASS** (Open Source)
.. |mingw_shell| replace:: ``MinGW 64-bit`` shell
"""
